module Data.User exposing
    ( Role(..)
    , UserName(..)
    , unUserName
    , Password(..)
    , unPassword
    , User
    )

import Data.Common exposing (PersonId(..), UserId(..))

type Role
    = PlayerRole
    | AdminRole


type UserName
    = UserName String


unUserName : UserName -> String
unUserName (UserName x) =
    x

type Password
    = Password String

unPassword : Password -> String
unPassword (Password x) =
    x

type alias User =
    { id : UserId
    , name : UserName
    , password : Maybe Password
    , avatar : Maybe PersonId
    }

