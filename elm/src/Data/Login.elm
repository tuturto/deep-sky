module Data.Login exposing (LoginResponse)

import Data.People exposing (Person)
import Data.User exposing (User)


type alias LoginResponse =
    { user : User
    , avatar : Maybe Person
    }
