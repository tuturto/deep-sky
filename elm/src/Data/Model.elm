module Data.Model exposing
    ( ApiMsg(..)
    , AvatarSelectionModel
    , FullModel
    , LoginModel
    , Model
    , Msg(..)
    , SubModel(..)
    , avatarAvailable
    , fromAvatarSelectionToFullModel
    , fromLoginToAvatarSelection
    , fromLoginToFullModel
    , init
    , initLoginModel
    , withAvatarModel
    , withFullModel
    )

import Bootstrap.Navbar as Navbar
import Browser exposing (UrlRequest)
import Browser.Navigation as Nav exposing (Key)
import Data.Common exposing (ErrorMessage, Resources, StarDate)
import Data.Login exposing (LoginResponse)
import Data.People exposing (Person)
import Data.User exposing (User)
import MessageToast exposing (MessageToast)
import RemoteData exposing (RemoteData(..), WebData)
import SaveData exposing (SaveData(..))
import Url exposing (Url)
import ViewModels.Admin.Main exposing (AdminRMsg, AdminViewModel)
import ViewModels.Admin.People.Add exposing (AdminAddPersonRMsg)
import ViewModels.Admin.People.Edit exposing (AdminEditPersonRMsg)
import ViewModels.Admin.People.List exposing (AdminListPeopleRMsg)
import ViewModels.Admin.Status exposing (AdminStatusRMsg)
import ViewModels.Avatar exposing (AvatarRMsg, AvatarViewModel)
import ViewModels.Designer exposing (DesignerRMsg, DesignerViewModel)
import ViewModels.Login exposing (LoginRMsg, LoginViewModel)
import ViewModels.Messages exposing (MessagesRMsg, MessagesViewModel)
import ViewModels.Person exposing (PersonRMsg, PersonViewModel)
import ViewModels.Planet exposing (PlanetRMsg(..), PlanetViewModel)
import ViewModels.Research exposing (ResearchRMsg(..), ResearchViewModel)
import ViewModels.StarSystem exposing (StarSystemRMsg, StarSystemViewModel)
import ViewModels.StarSystems exposing (StarSystemsRMsg, StarSystemsViewModel)
import ViewModels.Unit exposing (UnitRMsg, UnitViewModel)


type alias Model =
    { key : Key
    , url : Url
    , currentTime : WebData StarDate
    , errors : List ErrorMessage
    , navbarState : Navbar.State
    , messageToast : MessageToast Msg
    , logoutResponse : WebData String
    , subModel : SubModel
    }


type SubModel
    = NotLoggedIn LoginModel
    | UserWithoutAvatar AvatarSelectionModel
    | UserWithAvatar FullModel


type alias LoginModel =
    { loginR : LoginViewModel
    , loginResponse : SaveData LoginResponse
    , newUserResponse : SaveData User
    }


init : Url.Url -> Nav.Key -> Navbar.State -> Model
init url key state =
    { key = key
    , url = url
    , currentTime = NotAsked
    , errors = []
    , navbarState = state
    , messageToast = MessageToast.init UpdatedSimpleMessageToast
    , logoutResponse = NotAsked
    , subModel = NotLoggedIn <| initLoginModel
    }


initLoginModel : LoginModel
initLoginModel =
    { loginR = ViewModels.Login.init
    , loginResponse = RData NotAsked
    , newUserResponse = RData NotAsked
    }


type alias AvatarSelectionModel =
    { user : User
    , avatarR : AvatarViewModel
    }


fromLoginToAvatarSelection : LoginModel -> User -> AvatarSelectionModel
fromLoginToAvatarSelection model user =
    { user = user
    , avatarR = ViewModels.Avatar.init
    }


type alias FullModel =
    { user : User
    , avatar : SaveData Person
    , resources : WebData Resources
    , starSystemR : StarSystemViewModel
    , starSystemsR : StarSystemsViewModel
    , planetR : PlanetViewModel
    , messagesR : MessagesViewModel
    , researchR : ResearchViewModel
    , designerR : DesignerViewModel
    , personR : PersonViewModel
    , adminR : AdminViewModel
    , unitR : UnitViewModel
    }


fromLoginToFullModel : LoginModel -> User -> Person -> FullModel
fromLoginToFullModel _ user avatar =
    { user = user
    , avatar = RData (Success avatar)
    , resources = Loading
    , starSystemR = ViewModels.StarSystem.init
    , starSystemsR = ViewModels.StarSystems.init
    , planetR = ViewModels.Planet.init
    , messagesR = ViewModels.Messages.init
    , researchR = ViewModels.Research.init
    , designerR = ViewModels.Designer.init
    , personR = ViewModels.Person.init
    , adminR = ViewModels.Admin.Main.init
    , unitR = ViewModels.Unit.init
    }


fromAvatarSelectionToFullModel : AvatarSelectionModel -> Person -> FullModel
fromAvatarSelectionToFullModel model avatar =
    { user = model.user
    , avatar = RData (Success avatar)
    , resources = Loading
    , starSystemR = ViewModels.StarSystem.init
    , starSystemsR = ViewModels.StarSystems.init
    , planetR = ViewModels.Planet.init
    , messagesR = ViewModels.Messages.init
    , researchR = ViewModels.Research.init
    , designerR = ViewModels.Designer.init
    , personR = ViewModels.Person.init
    , adminR = ViewModels.Admin.Main.init
    , unitR = ViewModels.Unit.init
    }


type Msg
    = LinkClicked UrlRequest
    | UrlChanged Url
    | ClearErrors
    | NavbarMsg Navbar.State
    | UpdatedSimpleMessageToast (MessageToast Msg)
    | ApiMsgCompleted ApiMsg
    | LogoutRequested
    | LogoutCompleted (WebData String)
    | StarSystemMessage StarSystemRMsg
    | StarSystemsMessage StarSystemsRMsg
    | PlanetMessage PlanetRMsg
    | NewsMessage MessagesRMsg
    | ResearchMessage ResearchRMsg
    | DesignerMessage DesignerRMsg
    | PersonMessage PersonRMsg
    | UnitMessage UnitRMsg
    | LoginMessage LoginRMsg
    | AvatarMessage AvatarRMsg
    | AdminMessage AdminRMsg
    | AdminListPeopleMessage AdminListPeopleRMsg
    | AdminEditPersonMessage AdminEditPersonRMsg
    | AdminAddPersonMessage AdminAddPersonRMsg
    | AdminStatusMessage AdminStatusRMsg


type ApiMsg
    = StarDateReceived (WebData StarDate)
    | ResourcesReceived (WebData Resources)
    | AvatarReceived (SaveData LoginResponse)
    | NewUserCreated (SaveData User)


avatarAvailable : Model -> Bool
avatarAvailable model =
    case model.subModel of
        NotLoggedIn _ ->
            False

        UserWithoutAvatar _ ->
            False

        UserWithAvatar fullModel ->
            case fullModel.avatar of
                RData NotAsked ->
                    False

                RData Loading ->
                    False

                Saving _ ->
                    False

                RData (Failure _) ->
                    False

                RData (Success _) ->
                    True


{-| Run a function on a full model. If current submodel is something
else than UserWithAvatar, default is returned
-}
withFullModel : Model -> a -> (FullModel -> a) -> a
withFullModel model default fn =
    case model.subModel of
        NotLoggedIn _ ->
            default

        UserWithoutAvatar _ ->
            default

        UserWithAvatar m ->
            fn m


withAvatarModel : Model -> a -> (AvatarSelectionModel -> a) -> a
withAvatarModel model default fn =
    case model.subModel of
        NotLoggedIn _ ->
            default

        UserWithoutAvatar m ->
            fn m

        UserWithAvatar _ ->
            default
