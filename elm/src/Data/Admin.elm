module Data.Admin exposing
    ( AgeOptions(..)
    , Count(..)
    , FactionCountType(..)
    , Person
    , PersonOptions
    , PlanetCountType(..)
    , Simulation
    , SimulationSummary
    , StarSystemCountType(..)
    , SystemStatus(..)
    , UserCountType(..)
    , unCount
    )

import Data.Common
    exposing
        ( DynastyId(..)
        , FactionId(..)
        , PersonId(..)
        , PlanetId(..)
        , StarDate(..)
        , StarSystemId(..)
        )
import Data.People
    exposing
        ( Age(..)
        , Gender(..)
        , Sex(..)
        , StatValue(..)
        )
import Data.PersonNames exposing (PersonName(..))


{-| General summary of the simulation
-}
type alias SimulationSummary =
    { users : Count UserCountType
    , factions : Count FactionCountType
    , planets : Count PlanetCountType
    , starSystems : Count StarSystemCountType
    }


{-| Number of some specific kind of thing
-}
type Count a
    = Count Int


{-| Unwrap Count
-}
unCount : Count a -> Int
unCount (Count n) =
    n


type UserCountType
    = UserCount


type FactionCountType
    = FactionCount


type PlanetCountType
    = PlanetCount


type StarSystemCountType
    = StarSystemCount


type alias Simulation =
    { time : StarDate
    , status : SystemStatus
    }


type SystemStatus
    = Offline
    | Maintenance
    | Online
    | ProcessingTurn


type alias Person =
    { id : PersonId
    , name : PersonName
    , sex : Sex
    , gender : Gender
    , dateOfBirth : StarDate
    , diplomacy : StatValue
    , learning : StatValue
    , martial : StatValue
    , intrigue : StatValue
    , stewardship : StatValue
    , factionId : Maybe FactionId
    , planetTitle : Maybe PlanetId
    , starSystemTitle : Maybe StarSystemId
    , dynastyId : Maybe DynastyId
    }


{-| Available options for person creation
-}
type alias PersonOptions =
    { age : Maybe AgeOptions
    }


type AgeOptions
    = AgeBracket Age Age
    | ExactAge Age
