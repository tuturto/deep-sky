module Main exposing (handleApiMsg, init, main, subscriptions, update)

{-| Main module of the application
-}

import Accessors exposing (over, set)
import Api.Common exposing (getResources, getStarDate)
import Api.Endpoints exposing (NPCSearch(..))
import Api.User exposing (logout)
import Bootstrap.Navbar as Navbar
import Browser
import Browser.Navigation as Nav
import Data.Accessors
    exposing
        ( adminAddPersonRA
        , adminEditPersonRA
        , adminListPeopleRA
        , adminRA
        , adminStatusRA
        , avatarA
        , currentTimeA
        , designerRA
        , errorsA
        , loginRA
        , logoutResponseA
        , messageToastA
        , messagesRA
        , modeA
        , newUserResponseA
        , password2A
        , passwordA
        , personRA
        , planetRA
        , researchRA
        , resourcesA
        , setFullModel
        , setLoginModel
        , starSystemRA
        , subModelA
        , unitRA
        , userNameA
        )
import Data.Common
    exposing
        ( ErrorMessage(..)
        , InfoPanelStatus(..)
        , Route(..)
        , error
        )
import Data.Model
    exposing
        ( ApiMsg(..)
        , Model
        , Msg(..)
        , SubModel(..)
        , fromAvatarSelectionToFullModel
        , fromLoginToAvatarSelection
        , fromLoginToFullModel
        , initLoginModel
        )
import Data.User exposing (Role(..))
import Http exposing (Error(..))
import MessageToast
import Navigation exposing (parseLocation)
import RemoteData exposing (RemoteData(..))
import SaveData exposing (SaveData(..))
import Url exposing (Url)
import ViewModels.Admin.Main
import ViewModels.Admin.People.Add
import ViewModels.Admin.People.Edit
import ViewModels.Admin.People.List
import ViewModels.Admin.Status
import ViewModels.Designer exposing (DesignerRMsg(..))
import ViewModels.Login exposing (LoginMode(..))
import ViewModels.Messages exposing (MessagesRMsg(..))
import ViewModels.Person
import ViewModels.Planet exposing (PlanetRMsg(..))
import ViewModels.Research exposing (ResearchRMsg(..))
import ViewModels.StarSystem exposing (StarSystemRMsg(..))
import ViewModels.Unit
import Views.Admin.Main
import Views.Admin.People.Add
import Views.Admin.People.Edit
import Views.Admin.People.List
import Views.Admin.Status
import Views.Avatar
import Views.Bases
import Views.Construction
import Views.Designer
import Views.Fleet
import Views.Home
import Views.Layout exposing (view)
import Views.Login
import Views.Messages
import Views.Person
import Views.Planet
import Views.Profile
import Views.Research
import Views.StarSystem
import Views.StarSystems
import Views.Unit



-- MAIN


{-| Main function
-}
main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }


{-| Initialize the application
-}
init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    let
        ( navbarState, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
    ( Data.Model.init url key navbarState
    , Cmd.batch
        [ navbarCmd
        , getStarDate (ApiMsgCompleted << StarDateReceived)
        ]
    )


initViewModel : Url -> Model -> Model
initViewModel url m =
    case m.subModel of
        NotLoggedIn _ ->
            setLoginModel loginRA ViewModels.Login.init m

        UserWithoutAvatar _ ->
            m

        UserWithAvatar _ ->
            case parseLocation url of
                AdminR ->
                    setFullModel adminRA ViewModels.Admin.Main.init m

                HomeR ->
                    m

                ProfileR ->
                    m

                StarSystemsR ->
                    m

                StarSystemR _ ->
                    setFullModel starSystemRA ViewModels.StarSystem.init m

                PlanetR _ ->
                    setFullModel planetRA ViewModels.Planet.init m

                BasesR ->
                    m

                FleetR ->
                    m

                DesignerR ->
                    setFullModel designerRA ViewModels.Designer.init m

                ConstructionR ->
                    m

                MessagesR ->
                    setFullModel messagesRA ViewModels.Messages.init m

                PersonR _ ->
                    setFullModel personRA ViewModels.Person.init m

                UnitR _ ->
                    setFullModel unitRA ViewModels.Unit.init m

                AdminListPeopleR ->
                    setFullModel (adminRA << adminListPeopleRA) ViewModels.Admin.People.List.init m

                AdminPersonR _ ->
                    setFullModel (adminRA << adminEditPersonRA) ViewModels.Admin.People.Edit.init m

                AdminNewPersonR ->
                    setFullModel (adminRA << adminAddPersonRA) ViewModels.Admin.People.Add.init m

                AdminStatusR ->
                    setFullModel (adminRA << adminStatusRA) ViewModels.Admin.Status.init m

                ResearchR ->
                    setFullModel researchRA ViewModels.Research.init m


{-| Handle update messages
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg m =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( m
                    , Nav.pushUrl m.key (Url.toString url)
                    )

                Browser.External href ->
                    ( m
                    , Nav.pushUrl m.key href
                    )

        UrlChanged url ->
            let
                nm =
                    { m | url = url }
            in
            ( initViewModel url nm
            , currentInit url nm
            )

        UpdatedSimpleMessageToast updatedMessageToast ->
            ( { m | messageToast = updatedMessageToast }
            , Cmd.none
            )

        ClearErrors ->
            ( set errorsA [] m
            , Cmd.none
            )

        NavbarMsg state ->
            ( { m | navbarState = state }
            , Cmd.none
            )

        LogoutRequested ->
            ( set logoutResponseA Loading m
            , logout LogoutCompleted
            )

        LogoutCompleted NotAsked ->
            ( m
            , Cmd.none
            )

        LogoutCompleted Loading ->
            ( m
            , Cmd.none
            )

        LogoutCompleted (Success res) ->
            ( set logoutResponseA (Success res) m
                |> set subModelA (NotLoggedIn initLoginModel)
            , Cmd.none
            )

        LogoutCompleted (Failure err) ->
            ( set logoutResponseA (Failure err) m
                |> over errorsA (\errors -> error err "Failed to logout" :: errors)
            , Cmd.none
            )

        ApiMsgCompleted message ->
            handleApiMsg message m

        StarSystemMessage message ->
            Views.StarSystem.update message m

        StarSystemsMessage message ->
            Views.StarSystems.update message m

        PlanetMessage message ->
            Views.Planet.update message m

        NewsMessage message ->
            Views.Messages.update message m

        ResearchMessage message ->
            Views.Research.update message m

        DesignerMessage message ->
            Views.Designer.update message m

        PersonMessage message ->
            Views.Person.update message m

        UnitMessage message ->
            Views.Unit.update message m

        LoginMessage message ->
            case m.subModel of
                NotLoggedIn model ->
                    let
                        ( sm, sc ) =
                            Views.Login.update message model

                        -- TODO: LoginModel -> Model
                    in
                    ( set subModelA (NotLoggedIn sm) m
                    , sc
                    )

                _ ->
                    ( m
                    , Cmd.none
                    )

        AvatarMessage message ->
            Views.Avatar.update message m

        AdminMessage message ->
            Views.Admin.Main.update message m

        AdminListPeopleMessage message ->
            Views.Admin.People.List.update message m

        AdminEditPersonMessage message ->
            Views.Admin.People.Edit.update message m

        AdminAddPersonMessage message ->
            Views.Admin.People.Add.update message m

        AdminStatusMessage message ->
            Views.Admin.Status.update message m


{-| Handle messages related to API calls
-}
handleApiMsg : ApiMsg -> Model -> ( Model, Cmd Msg )
handleApiMsg msg model =
    case msg of
        StarDateReceived NotAsked ->
            ( model
            , Cmd.none
            )

        StarDateReceived Loading ->
            ( model
            , Cmd.none
            )

        StarDateReceived (Success starDate) ->
            ( set currentTimeA (Success starDate) model
            , Cmd.none
            )

        StarDateReceived (Failure err) ->
            ( set currentTimeA (Failure err) model
                |> over errorsA (\errors -> error err "Failed to load star date" :: errors)
            , Cmd.none
            )

        ResourcesReceived NotAsked ->
            ( model
            , Cmd.none
            )

        ResourcesReceived Loading ->
            ( model
            , Cmd.none
            )

        ResourcesReceived (Success resources) ->
            ( setFullModel resourcesA (Success resources) model
            , Cmd.none
            )

        ResourcesReceived (Failure err) ->
            ( setFullModel resourcesA (Failure err) model
                |> over errorsA (\errors -> error err "Failed to load resources" :: errors)
            , Cmd.none
            )

        AvatarReceived (RData NotAsked) ->
            ( model
            , Cmd.none
            )

        AvatarReceived (RData Loading) ->
            ( model
            , Cmd.none
            )

        AvatarReceived (Saving _) ->
            ( model
            , Cmd.none
            )

        AvatarReceived (RData (Success res)) ->
            -- User and maybe their avatar has been received
            case model.subModel of
                NotLoggedIn m ->
                    -- login has been performed
                    case res.avatar of
                        Just avatar ->
                            -- user has avatar, move to full mode
                            ( set subModelA (UserWithAvatar <| fromLoginToFullModel m res.user avatar) model
                            , Cmd.batch
                                [ getResources (ApiMsgCompleted << ResourcesReceived)
                                , getStarDate (ApiMsgCompleted << StarDateReceived)
                                , currentInit model.url model
                                ]
                            )

                        Nothing ->
                            -- user has no avatar, move to avatar selection
                            ( set subModelA (UserWithoutAvatar <| fromLoginToAvatarSelection m res.user) model
                            , Views.Avatar.init model
                            )

                UserWithoutAvatar m ->
                    -- avatar has been selected
                    case res.avatar of
                        Just avatar ->
                            ( set subModelA (UserWithAvatar <| fromAvatarSelectionToFullModel m avatar) model
                            , getResources (ApiMsgCompleted << ResourcesReceived)
                            )

                        Nothing ->
                            ( model
                            , Cmd.none
                            )

                UserWithAvatar _ ->
                    -- this shouldn't normally happen
                    ( model
                    , Cmd.none
                    )

        AvatarReceived (RData (Failure err)) ->
            case model.subModel of
                NotLoggedIn _ ->
                    ( over errorsA (\errors -> error err "Failed to load avatar" :: errors) model
                    , Cmd.none
                    )

                UserWithoutAvatar _ ->
                    ( over errorsA (\errors -> error err "Failed to load avatar" :: errors) model
                    , Cmd.none
                    )

                UserWithAvatar _ ->
                    ( over errorsA (\errors -> error err "Failed to load avatar" :: errors) model
                        |> setFullModel avatarA (RData (Failure err))
                    , Cmd.none
                    )

        NewUserCreated (RData NotAsked) ->
            ( model
            , Cmd.none
            )

        NewUserCreated (RData Loading) ->
            ( model
            , Cmd.none
            )

        NewUserCreated (Saving _) ->
            ( model
            , Cmd.none
            )

        NewUserCreated (RData (Success res)) ->
            ( setLoginModel newUserResponseA (RData (Success res)) model
                |> setLoginModel (loginRA << modeA) LogIn
                |> setLoginModel (loginRA << userNameA) ""
                |> setLoginModel (loginRA << passwordA) ""
                |> setLoginModel (loginRA << password2A) ""
                |> set messageToastA
                    (model.messageToast
                        |> MessageToast.success
                        |> MessageToast.withMessage "New account has been created"
                    )
            , Cmd.none
            )

        NewUserCreated (RData (Failure err)) ->
            ( over errorsA (\errors -> error err "Failed to create user" :: errors) model
                |> setLoginModel newUserResponseA (RData (Failure err))
            , Cmd.none
            )



-- SUBSCRIPTIONS


{-| Create subscriptions at the start of program
-}
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Navbar.subscriptions model.navbarState NavbarMsg
        , MessageToast.subscriptions model.messageToast
        ]



-- VIEW


{-| Give current page's init function
-}
currentInit : Url -> (Model -> Cmd Msg)
currentInit url =
    case parseLocation url of
        AdminR ->
            Views.Admin.Main.init

        AdminListPeopleR ->
            Views.Admin.People.List.init

        AdminPersonR pId ->
            Views.Admin.People.Edit.init pId

        AdminNewPersonR ->
            Views.Admin.People.Add.init

        AdminStatusR ->
            Views.Admin.Status.init

        BasesR ->
            Views.Bases.init

        ConstructionR ->
            Views.Construction.init

        DesignerR ->
            Views.Designer.init

        FleetR ->
            Views.Fleet.init

        HomeR ->
            Views.Home.init

        MessagesR ->
            Views.Messages.init

        ProfileR ->
            Views.Profile.init

        ResearchR ->
            Views.Research.init

        StarSystemR systemId ->
            Views.StarSystem.init systemId

        StarSystemsR ->
            Views.StarSystems.init

        PlanetR planetId ->
            Views.Planet.init planetId

        PersonR personId ->
            Views.Person.init personId

        UnitR unitId ->
            Views.Unit.init unitId
