module Views.Admin.Status exposing (init, isLoading, page, update)

import Accessors exposing (over)
import Api.Admin
    exposing
        ( getSimulationStatus
        , reseedDatabase
        , simulationSummary
        , wipeDatabase
        )
import Data.Accessors
    exposing
        ( actionA
        , adminRA
        , adminStatusRA
        , errorsA
        , setFullModel
        , summaryA
        )
import Data.Admin
    exposing
        ( SimulationSummary
        , unCount
        )
import Data.Common
    exposing
        ( Route(..)
        , error
        )
import Data.Model
    exposing
        ( FullModel
        , Model
        , Msg(..)
        , SubModel(..)
        , withFullModel
        )
import Dialog
import Html
    exposing
        ( Html
        , button
        , div
        , h3
        , text
        )
import Html.Attributes exposing (class, id)
import Html.Events exposing (onClick)
import Maybe.Extra exposing (isJust)
import RemoteData exposing (RemoteData(..))
import SaveData exposing (SaveData(..))
import ViewModels.Admin.Main exposing (AdminRMsg(..))
import ViewModels.Admin.Status
    exposing
        ( AdminStatusRMsg(..)
        , AdminStatusViewModel
        , StatusAction(..)
        )
import Views.Admin.Main
import Views.Admin.Menu exposing (adminLayout, statusMenu)


{-| Render status view
-}
page : Model -> Html Msg
page m =
    withFullModel m
        (div [] [])
        (\model ->
            adminLayout statusMenu
                [ div [ class "row" ]
                    [ div [ class "col-lg-12" ]
                        (case model.adminR.adminStatusR.summary of
                            RData NotAsked ->
                                [ text "..." ]

                            RData Loading ->
                                [ text "Loading..." ]

                            RData (Failure _) ->
                                [ text "Failed to load data" ]

                            RData (Success summary) ->
                                summaryText summary

                            Saving summary ->
                                summaryText summary
                        )
                    ]
                , div [ class "row" ]
                    [ div [ class "col-lg-2" ]
                        [ div
                            ([ id "wipe-database", class "btn btn-block btn-default command-button" ]
                                ++ (case model.adminR.adminStatusR.action of
                                        Nothing ->
                                            [ onClick (AdminStatusMessage WipeRequested) ]

                                        Just _ ->
                                            []
                                   )
                            )
                            [ text "Wipe" ]
                        ]
                    , div [ class "col-lg-10 space-top-sm" ]
                        [ text "Wiping destroys all game information (planets, people, ships and such). Users are left in the system. "
                        , text "This allows administrator to create a new simulation state from scratch. It is very time consuming process, "
                        , text "but gives the greatest freedom in terms of what kind of universe the new simulation is set."
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "col-lg-2" ]
                        [ div
                            ([ id "reseed-database", class "btn btn-block btn-default command-button" ]
                                ++ (case model.adminR.adminStatusR.action of
                                        Nothing ->
                                            [ onClick (AdminStatusMessage ReseedRequested) ]

                                        Just _ ->
                                            []
                                   )
                            )
                            [ text "Reseed" ]
                        ]
                    , div [ class "col-lg-10 space-top-sm" ]
                        [ text "Reseeding first performs wipe and then creates default starting state. Users are "
                        , text "left in the system. While not offering as great freedom as greating the whole universe from scratch, "
                        , text "this is the fastest way of starting a new simulation."
                        ]
                    ]
                , Dialog.view
                    (if
                        model.adminR.adminStatusR.action
                            == Just ConfirmingWipe
                            || model.adminR.adminStatusR.action
                            == Just ConfirmingReseed
                     then
                        Just (dialogConfig model.adminR.adminStatusR)

                     else
                        Nothing
                    )
                ]
                model
        )


summaryText : SimulationSummary -> List (Html Msg)
summaryText summary =
    let
        users =
            String.fromInt <| unCount summary.users

        factions =
            String.fromInt <| unCount summary.factions

        planets =
            String.fromInt <| unCount summary.planets

        starSystems =
            String.fromInt <| unCount summary.starSystems
    in
    [ text
        ("There are currently "
            ++ users
            ++ " users who are in "
            ++ factions
            ++ " factions. There are total of "
            ++ planets
            ++ " planets and "
            ++ starSystems
            ++ " star systems."
        )
    ]


dialogConfig : AdminStatusViewModel -> Dialog.Config Msg
dialogConfig model =
    let
        confirmMessage =
            case model.action of
                Just ConfirmingWipe ->
                    AdminStatusMessage WipeConfirmed

                Just ConfirmingReseed ->
                    AdminStatusMessage ReseedConfirmed

                _ ->
                    AdminStatusMessage BogusDialog

        cancelMessage =
            case model.action of
                Just ConfirmingWipe ->
                    AdminStatusMessage WipeCancelled

                Just ConfirmingReseed ->
                    AdminStatusMessage ReseedCancelled

                _ ->
                    AdminStatusMessage BogusDialog

        confirmText =
            case model.action of
                Just ConfirmingWipe ->
                    Just <| text "Are you sure you want to wipe database? Simulation state will be gone forever."

                Just ConfirmingReseed ->
                    Just <| text "Are you sure you want to reseed database? Simulation state will return to initial state."

                _ ->
                    Just <| text "This dialog should not be open."
    in
    { closeMessage = Nothing
    , containerClass = Nothing
    , header = Just (h3 [] [ text "Warning!" ])
    , body = confirmText
    , footer =
        [ button
            [ class "btn btn-default"
            , onClick confirmMessage
            ]
            [ text "OK" ]
        , button
            [ class "btn btn-default"
            , onClick cancelMessage
            ]
            [ text "Cancel" ]
        ]
    }


{-| Initialize data retrieval from server
-}
init : Model -> Cmd Msg
init _ =
    Cmd.batch
        [ getSimulationStatus (AdminMessage << SimulationStatusReceived)
        , simulationSummary (AdminStatusMessage << SummaryReceived)
        ]


isLoading : FullModel -> Bool
isLoading model =
    isJust model.adminR.adminStatusR.action
        || Views.Admin.Main.isLoading model


{-| Handle incoming messages
-}
update : AdminStatusRMsg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        WipeRequested ->
            ( setFullModel (adminRA << adminStatusRA << actionA) (Just ConfirmingWipe) model
            , Cmd.none
            )

        WipeConfirmed ->
            ( setFullModel (adminRA << adminStatusRA << actionA) (Just WipeInProgress) model
            , wipeDatabase (AdminStatusMessage << DatabaseActionCompleted)
            )

        WipeCancelled ->
            ( setFullModel (adminRA << adminStatusRA << actionA) Nothing model
            , Cmd.none
            )

        ReseedRequested ->
            ( setFullModel (adminRA << adminStatusRA << actionA) (Just ConfirmingReseed) model
            , Cmd.none
            )

        ReseedConfirmed ->
            ( setFullModel (adminRA << adminStatusRA << actionA) (Just ReseedInProgress) model
            , reseedDatabase (AdminStatusMessage << DatabaseActionCompleted)
            )

        ReseedCancelled ->
            ( setFullModel (adminRA << adminStatusRA << actionA) Nothing model
            , Cmd.none
            )

        BogusDialog ->
            ( setFullModel (adminRA << adminStatusRA << actionA) Nothing model
            , Cmd.none
            )

        DatabaseActionCompleted (RData (Failure err)) ->
            ( over errorsA (\errors -> error err "Failed to perform database operation" :: errors) model
                |> setFullModel (adminRA << adminStatusRA << summaryA) (RData <| Failure err)
                |> setFullModel (adminRA << adminStatusRA << actionA) Nothing
            , Cmd.none
            )

        DatabaseActionCompleted (RData (Success res)) ->
            ( setFullModel (adminRA << adminStatusRA << summaryA) (RData <| Success res) model
                |> setFullModel (adminRA << adminStatusRA << actionA) Nothing
            , Cmd.none
            )

        DatabaseActionCompleted (Saving _) ->
            ( model
            , Cmd.none
            )

        DatabaseActionCompleted (RData NotAsked) ->
            ( model
            , Cmd.none
            )

        DatabaseActionCompleted (RData Loading) ->
            ( model
            , Cmd.none
            )

        SummaryReceived (RData (Failure err)) ->
            ( over errorsA (\errors -> error err "Failed to load simulation summary" :: errors) model
                |> setFullModel (adminRA << adminStatusRA << summaryA) (RData <| Failure err)
            , Cmd.none
            )

        SummaryReceived (RData (Success res)) ->
            ( setFullModel (adminRA << adminStatusRA << summaryA) (RData <| Success res) model
            , Cmd.none
            )

        SummaryReceived (Saving _) ->
            ( model
            , Cmd.none
            )

        SummaryReceived (RData NotAsked) ->
            ( model
            , Cmd.none
            )

        SummaryReceived (RData Loading) ->
            ( model
            , Cmd.none
            )
