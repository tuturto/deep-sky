module Views.Admin.Main exposing
    ( init
    , isLoading
    , page
    , update
    )

import Accessors exposing (over, set)
import Api.Admin exposing (getSimulationStatus, putSimulationStatus)
import Data.Accessors
    exposing
        ( adminRA
        , currentTimeA
        , errorsA
        , setFullModel
        , simulationA
        , statusA
        , timeA
        )
import Data.Admin exposing (SystemStatus(..))
import Data.Common exposing (Route(..), error, nextStarDate)
import Data.Model
    exposing
        ( FullModel
        , Model
        , Msg(..)
        , SubModel(..)
        , withFullModel
        )
import Data.User exposing (Role(..))
import Html exposing (Html, div)
import RemoteData exposing (RemoteData(..))
import ViewModels.Admin.Main exposing (AdminRMsg(..))
import Views.Admin.Menu exposing (adminLayout, mainMenu)


{-| Render admin view
-}
page : Model -> Html Msg
page model =
    withFullModel model
        (div [] [])
        (\m ->
            adminLayout mainMenu
                []
                m
        )


{-| Initialize data retrieval from server
-}
init : Model -> Cmd Msg
init _ =
    getSimulationStatus (AdminMessage << SimulationStatusReceived)


isLoading : FullModel -> Bool
isLoading model =
    let
        vm =
            model.adminR
    in
    RemoteData.isLoading vm.simulation


{-| Handle incoming messages
-}
update : AdminRMsg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        SimulationStatusReceived (Failure err) ->
            ( setFullModel (adminRA << simulationA) (Failure err) model
                |> over errorsA (\errors -> error err "Failed to load simulation status" :: errors)
            , Cmd.none
            )

        SimulationStatusReceived (Success simulation) ->
            ( setFullModel (adminRA << simulationA) (Success simulation) model
                |> set currentTimeA (Success simulation.time)
            , Cmd.none
            )

        SimulationStatusReceived Loading ->
            ( model
            , Cmd.none
            )

        SimulationStatusReceived NotAsked ->
            ( model
            , Cmd.none
            )

        ChangeStatusRequested ProcessingTurn ->
            ( setFullModel (adminRA << simulationA) Loading model
            , withFullModel model
                Cmd.none
                (\m ->
                    case m.adminR.simulation of
                        Success simulation ->
                            putSimulationStatus (AdminMessage << SimulationStatusReceived)
                                (set timeA (nextStarDate simulation.time) simulation)

                        _ ->
                            Cmd.none
                )
            )

        ChangeStatusRequested status ->
            ( setFullModel (adminRA << simulationA) Loading model
            , withFullModel model
                Cmd.none
                (\m ->
                    case m.adminR.simulation of
                        Success simulation ->
                            putSimulationStatus (AdminMessage << SimulationStatusReceived)
                                (set statusA status simulation)

                        _ ->
                            Cmd.none
                )
            )
