module Views.Avatar exposing
    ( init
    , isLoading
    , page
    , update
    )

import Accessors exposing (over, set)
import Api.Endpoints exposing (NPCSearch(..))
import Api.People exposing (queryPeople)
import Api.User exposing (putUser)
import Bootstrap.Button as Button
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Modal as Modal
import Bootstrap.Table as Table
import Data.Accessors
    exposing
        ( availableAvatarsA
        , avatarA
        , avatarListStatusA
        , avatarRA
        , avatarsPageA
        , dialogStatusA
        , errorsA
        , messageToastA
        , setAvatarModel
        , subModelA
        )
import Data.Common
    exposing
        ( InfoPanelStatus(..)
        , Route(..)
        , error
        , unDynastyName
        )
import Data.Model
    exposing
        ( ApiMsg(..)
        , AvatarSelectionModel
        , Model
        , Msg(..)
        , SubModel(..)
        , fromAvatarSelectionToFullModel
        , withAvatarModel
        )
import Data.People exposing (Person, unAge)
import Data.PersonNames exposing (nameWithTitle, personName)
import Data.User exposing (Role(..))
import Dict exposing (Dict)
import Html
    exposing
        ( Html
        , div
        , p
        , text
        )
import Html.Attributes exposing (class, id)
import Html.Events exposing (onClick)
import MessageToast
import RemoteData exposing (RemoteData(..), WebData)
import SaveData exposing (SaveData(..))
import ViewModels.Avatar exposing (AvatarRMsg(..))
import Views.Helpers exposing (infoPanel)
import Api.Common exposing (getResources)


page : Model -> Html Msg
page m =
    withAvatarModel m
        (div [] [])
        (\model ->
            div []
                (searchParameters model
                    ++ infoPanel
                        { title = "Available avatars"
                        , currentStatus = model.avatarR.avatarListStatus
                        , openingMessage = AvatarMessage <| AvatarListStatusChanged InfoPanelOpen
                        , closingMessage = AvatarMessage <| AvatarListStatusChanged InfoPanelClosed
                        , refreshMessage = Just (AvatarMessage AvatarListUpdatedRequested)
                        }
                        (Just
                            { pageSize = model.avatarR.avatarsPageSize
                            , currentPage = model.avatarR.avatarsPage
                            , maxPage = lastPageWithResults model.avatarR.availableAvatars
                            , pageChangedMessage = AvatarMessage << AvatarListPageChanged
                            }
                        )
                        avatarListContent
                        model
                    ++ [ avatarSelectionHelp
                       , avatarDialog model
                       ]
                )
        )


lastPageWithResults : Dict Int (WebData (List Person)) -> Int
lastPageWithResults =
    Dict.foldl
        (\k v a ->
            case v of
                NotAsked ->
                    a

                Loading ->
                    a

                Failure _ ->
                    a

                Success [] ->
                    a

                Success _ ->
                    k
        )
        0


searchParameters : AvatarSelectionModel -> List (Html Msg)
searchParameters _ =
    []


avatarSelectionHelp : Html Msg
avatarSelectionHelp =
    Grid.row []
        [ Grid.col [ Col.lg12 ]
            [ text <|
                "Here you must choose your avatar. Avatar is the in-world person "
                    ++ "you play as. You see the world from the viewpoint of the avatar "
                    ++ "and direct their actions. When your avatar eventually dies, you "
                    ++ "are automatically given a new one, usually the heir of the just "
                    ++ "deceased avatar. So pay attention to not only well being of your "
                    ++ "avatar, but also to their family and heir."
            ]
        ]


{-| Render info box content showing a list of avatars
-}
avatarListContent : AvatarSelectionModel -> List (Html Msg)
avatarListContent model =
    let
        avatars =
            Dict.get model.avatarR.avatarsPage model.avatarR.availableAvatars
                |> Maybe.withDefault NotAsked
                |> RemoteData.withDefault []
    in
    [ Grid.row []
        [ Grid.col [ Col.lg12 ]
            [ Table.table
                { options = [ Table.striped, Table.hover, Table.attr <| id "person-table" ]
                , thead =
                    Table.simpleThead
                        [ Table.th [] [ text "Name" ]
                        , Table.th [] [ text "Dynasty" ]
                        , Table.th [] [ text "Age" ]
                        ]
                , tbody =
                    Table.tbody []
                        (List.indexedMap avatarEntry avatars)
                }
            ]
        ]
    ]


{-| Render a single table row displaying a person. Int parameter is used to
construct id attributes of row and columns
-}
avatarEntry : Int -> Person -> Table.Row Msg
avatarEntry index person =
    let
        idLabel =
            "person-" ++ String.fromInt index

        dName =
            Maybe.map (.name >> unDynastyName) person.dynasty
                |> Maybe.withDefault " "
                |> text
    in
    Table.tr
        [ Table.rowAttr <| id idLabel
        , Table.rowAttr <| onClick (AvatarMessage <| AvatarSelected person)
        , Table.rowAttr <| class "clickable"
        ]
        [ Table.td [ Table.cellAttr <| id <| idLabel ++ "-name" ] [ text <| nameWithTitle person.name person.shortTitle ]
        , Table.td [ Table.cellAttr <| id <| idLabel ++ "-dynasty" ] [ dName ]
        , Table.td [ Table.cellAttr <| id <| idLabel ++ "-age" ] [ text <| (unAge >> String.fromInt) person.age ]
        ]


{-| Handle messages specific to this page
-}
update : AvatarRMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AvailableAvatarsReceived NotAsked ->
            ( model
            , Cmd.none
            )

        AvailableAvatarsReceived Loading ->
            ( model
            , Cmd.none
            )

        AvailableAvatarsReceived (Success res) ->
            withAvatarModel model
                ( model
                , Cmd.none
                )
                (\m ->
                    ( setAvatarModel (avatarRA << availableAvatarsA)
                        (Dict.insert res.page (Success res.results) m.avatarR.availableAvatars)
                        model
                    , Cmd.none
                    )
                )

        AvailableAvatarsReceived (Failure err) ->
            ( over errorsA (\errors -> error err "Failed to load avatars" :: errors) model
            , Cmd.none
            )

        AvatarListStatusChanged status ->
            ( setAvatarModel (avatarRA << avatarListStatusA) status model
            , Cmd.none
            )

        AvatarListUpdatedRequested ->
            withAvatarModel model
                ( model, Cmd.none )
                (\m ->
                    ( setAvatarModel (avatarRA << availableAvatarsA)
                        (Dict.fromList
                            [ ( 0, Loading )
                            , ( 1, Loading )
                            ]
                        )
                        model
                        |> setAvatarModel (avatarRA << avatarsPageA) 0
                    , Cmd.batch
                        [ queryPeople (AvatarMessage << AvailableAvatarsReceived)
                            (Just 0)
                            (Just m.avatarR.avatarsPageSize)
                            (Just OnlyNPCs)
                        , queryPeople (AvatarMessage << AvailableAvatarsReceived)
                            (Just m.avatarR.avatarsPageSize)
                            (Just m.avatarR.avatarsPageSize)
                            (Just OnlyNPCs)
                        ]
                    )
                )

        AvatarListPageChanged n ->
            withAvatarModel model
                ( model
                , Cmd.none
                )
                (\m ->
                    let
                        lastPage =
                            lastPageWithResults m.avatarR.availableAvatars

                        clamped =
                            if n < 0 then
                                0

                            else if n > lastPage then
                                lastPage

                            else
                                n
                    in
                    ( setAvatarModel (avatarRA << avatarsPageA) clamped model
                    , case Dict.get (clamped + 1) m.avatarR.availableAvatars of
                        Nothing ->
                            queryPeople (AvatarMessage << AvailableAvatarsReceived)
                                (Just <| (clamped + 1) * ViewModels.Avatar.defaultQuery.avatarsPageSize)
                                (Just ViewModels.Avatar.defaultQuery.avatarsPageSize)
                                (Just ViewModels.Avatar.defaultQuery.personType)

                        Just Loading ->
                            Cmd.none

                        Just NotAsked ->
                            queryPeople (AvatarMessage << AvailableAvatarsReceived)
                                (Just <| (clamped + 1) * ViewModels.Avatar.defaultQuery.avatarsPageSize)
                                (Just ViewModels.Avatar.defaultQuery.avatarsPageSize)
                                (Just ViewModels.Avatar.defaultQuery.personType)

                        Just (Failure _) ->
                            queryPeople (AvatarMessage << AvailableAvatarsReceived)
                                (Just <| (clamped + 1) * ViewModels.Avatar.defaultQuery.avatarsPageSize)
                                (Just ViewModels.Avatar.defaultQuery.avatarsPageSize)
                                (Just ViewModels.Avatar.defaultQuery.personType)

                        Just (Success _) ->
                            Cmd.none
                    )
                )

        AvatarSelected person ->
            ( setAvatarModel (avatarRA << avatarA) (RData <| Success person) model
                |> setAvatarModel (avatarRA << dialogStatusA) Modal.shown
            , Cmd.none
            )

        AvatarConfirmed person ->
            withAvatarModel model
                ( model, Cmd.none )
                (\m ->
                    ( setAvatarModel (avatarRA << avatarA) (Saving person) model
                        |> setAvatarModel (avatarRA << dialogStatusA) Modal.hidden
                    , putUser (set avatarA (Just person.id) m.user)
                        (AvatarMessage << UserReceived)
                    )
                )

        AvatarCancelled ->
            ( setAvatarModel (avatarRA << avatarA) (RData NotAsked) model
                |> setAvatarModel (avatarRA << dialogStatusA) Modal.hidden
            , Cmd.none
            )

        UserReceived (RData NotAsked) ->
            ( model
            , Cmd.none
            )

        UserReceived (RData Loading) ->
            ( model
            , Cmd.none
            )

        UserReceived (Saving _) ->
            ( model
            , Cmd.none
            )

        UserReceived (RData (Success _)) ->
            withAvatarModel model
                ( model
                , Cmd.none
                )
                (\m ->
                    case m.avatarR.avatar of
                        Saving person ->
                            ( set subModelA (UserWithAvatar <| fromAvatarSelectionToFullModel m person) model
                                |> set messageToastA
                                    (model.messageToast
                                        |> MessageToast.success
                                        |> MessageToast.withMessage "Avatar selected successfully"
                                    )
                            , getResources (ApiMsgCompleted << ResourcesReceived)
                              -- TODO: call appropriate viewmodel's init (currentInit)
                            )

                        _ ->
                            ( model
                            , Cmd.none
                            )
                )

        UserReceived (RData (Failure err)) ->
            ( over errorsA (\errors -> error err "Failed to select avatar" :: errors) model
            , Cmd.none
            )


avatarDialog : AvatarSelectionModel -> Html Msg
avatarDialog model =
    let
        confirmMessage =
            SaveData.map (\person -> AvatarMessage <| AvatarConfirmed person) model.avatarR.avatar
                |> SaveData.toMaybe
                |> Maybe.withDefault (AvatarMessage AvatarCancelled)

        cancelMessage =
            AvatarMessage AvatarCancelled

        confirmText =
            SaveData.map (\person -> text ("Do you want to select " ++ personName person.name person.shortTitle ++ " as your avatar?")) model.avatarR.avatar
                |> SaveData.toMaybe
                |> Maybe.withDefault (text "There is no avatar selected, please close the dialog and try again.")

        headerText =
            text "Confirm avatar selection"
    in
    Modal.config cancelMessage
        |> Modal.small
        |> Modal.hideOnBackdropClick True
        |> Modal.h3 [] [ headerText ]
        |> Modal.body [] [ p [] [ confirmText ] ]
        |> Modal.footer []
            [ Button.button
                [ Button.outlinePrimary
                , Button.attrs [ id "ok-button", onClick confirmMessage ]
                ]
                [ text "OK" ]
            , Button.button
                [ Button.outlineSecondary
                , Button.attrs [ id "cancel-button", onClick cancelMessage ]
                ]
                [ text "Cancel" ]
            ]
        |> Modal.view model.avatarR.dialogStatus


init : Model -> Cmd Msg
init _ =
    Cmd.batch
        [ queryPeople (AvatarMessage << AvailableAvatarsReceived)
            (Just 0)
            (Just ViewModels.Avatar.defaultQuery.avatarsPageSize)
            (Just ViewModels.Avatar.defaultQuery.personType)
        , queryPeople (AvatarMessage << AvailableAvatarsReceived)
            (Just ViewModels.Avatar.defaultQuery.avatarsPageSize)
            (Just ViewModels.Avatar.defaultQuery.avatarsPageSize)
            (Just ViewModels.Avatar.defaultQuery.personType)
        ]


isLoading : AvatarSelectionModel -> Bool
isLoading model =
    let
        vm =
            model.avatarR

        avatarsLoading =
            Dict.values vm.availableAvatars
                |> List.any RemoteData.isLoading
    in
    avatarsLoading
        || SaveData.isLoading vm.avatar
