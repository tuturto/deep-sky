module Views.Person exposing
    ( init
    , isLoading
    , page
    , update
    )

{-| Page displaying person details. The shown information depends on human
intelligence level. The higher the level, the more details are shown.
-}

import Accessors exposing (get, over)
import Accessors.Library exposing (try)
import Api.People exposing (changeLifeFocus, getDemesne, getPersonDetails)
import Bootstrap.Button as Button
import Bootstrap.ButtonGroup as ButtonGroup exposing (RadioButtonItem)
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Modal as Modal
import Bootstrap.Table as Table
import Bootstrap.Utilities.Spacing as Spacing
import Data.Accessors
    exposing
        ( ageA
        , avatarA
        , avatarOpinionA
        , demesneA
        , demesneCurrentPageA
        , demesneStatusA
        , dialogStatusA
        , diplomacyA
        , dynastyA
        , errorsA
        , explanationA
        , focusA
        , genderA
        , idA
        , intelTypesA
        , intrigueA
        , learningA
        , lifeFocusModalA
        , locationA
        , martialA
        , nameA
        , opinionOfAvatarA
        , overFullModel
        , personA
        , personDetailsStatusA
        , personRA
        , relationsA
        , relationsCurrentPageA
        , relationsStatusA
        , setFullModel
        , shortTitleA
        , statsA
        , statsStatusA
        , stewardshipA
        , traitsA
        , traitsCurrentPageA
        , traitsStatusA
        )
import Data.Common
    exposing
        ( InfoPanelStatus(..)
        , PersonId
        , Route(..)
        , error
        , listOrdering
        , maxPage
        , unDemesneName
        , unDynastyName
        , unPlanetName
        , unStarSystemName
        )
import Data.Model
    exposing
        ( FullModel
        , Model
        , Msg(..)
        , SubModel(..)
        , withFullModel
        )
import Data.People
    exposing
        ( DemesneShortInfo(..)
        , Gender(..)
        , LifeFocus(..)
        , OnUnitData
        , OpinionFeeling(..)
        , OpinionReport(..)
        , OpinionScore(..)
        , Person
        , PersonLocation(..)
        , RelationLink
        , RelationType(..)
        , Trait
        , formalName
        , lifeFocusToExplanation
        , lifeFocusToString
        , personIntelToString
        , relationTypeOrdering
        , relationTypeToString
        , traitOrdering
        , unAge
        , unOpinionScore
        , unStatValue
        , unTraitDescription
        , unTraitName
        )
import Data.PersonNames
    exposing
        ( PersonName(..)
        , personName
        , personNameOrdering
        )
import Data.Vehicles
    exposing
        ( CrewPosition(..)
        , crewPositionToString
        , unUnitName
        )
import Html exposing (Attribute, Html, a, div, p, text)
import Html.Attributes exposing (id, title)
import Html.Events exposing (onMouseEnter)
import Maybe
import Ordering exposing (Ordering)
import RemoteData exposing (RemoteData(..))
import SaveData exposing (SaveData(..))
import Set
import ViewModels.Person exposing (PersonRMsg(..))
import Views.Helpers
    exposing
        ( PanelSizing(..)
        , href
        , infoPanel
        , twinPanels
        )


{-| Initiate retrieval of data needed by this page
-}
init : PersonId -> Model -> Cmd Msg
init pId _ =
    Cmd.batch
        [ getPersonDetails (PersonMessage << PersonDetailsReceived) pId
        , getDemesne (PersonMessage << DemesneReceived) pId
        ]


{-| Render page of displaying the person
-}
page : Model -> Html Msg
page model =
    withFullModel model
        (div [] [])
        (\m ->
            div []
                (lifeFocusDialog m :: twinPanels EqualPanels leftPanel rightPanel m)
        )


lifeFocusDialog : FullModel -> Html Msg
lifeFocusDialog model =
    Modal.config (PersonMessage (LifeFocusModalVisibilityChanged Modal.hidden))
        |> Modal.large
        |> Modal.hideOnBackdropClick True
        |> Modal.scrollableBody True
        |> Modal.h3 [] [ text "Select you life focus" ]
        |> Modal.body []
            [ Grid.containerFluid []
                [ Grid.row []
                    [ Grid.col
                        [ Col.lg4 ]
                        [ Grid.row []
                            [ Grid.col [ Col.lg12 ]
                                [ ButtonGroup.radioButtonGroup [ ButtonGroup.vertical ]
                                    [ lifeFocusButton model RulershipFocus
                                    , lifeFocusButton model BusinessFocus
                                    , lifeFocusButton model SeductionFocus
                                    , lifeFocusButton model IntrigueFocus
                                    , lifeFocusButton model HuntingFocus
                                    , lifeFocusButton model WarFocus
                                    , lifeFocusButton model CarousingFocus
                                    , lifeFocusButton model FamilyFocus
                                    , lifeFocusButton model ScholarshipFocus
                                    , lifeFocusButton model TheologyFocus
                                    ]
                                ]
                            ]
                        ]
                    , Grid.col
                        [ Col.lg8 ]
                        [ Grid.row []
                            [ Grid.col [ Col.lg12 ]
                                [ p [] [ text model.personR.lifeFocusModal.explanation ] ]
                            ]
                        ]
                    ]
                ]
            ]
        |> Modal.footer []
            [ Button.button
                (case focusChangeDetails model of
                    Nothing ->
                        [ Button.primary
                        , Button.disabled True
                        , Button.attrs [ id "confirm-lifefocus" ]
                        ]

                    Just ( person, focus ) ->
                        [ Button.primary
                        , Button.onClick (PersonMessage (LifeFocusConfirmed person focus))
                        , Button.attrs [ id "confirm-lifefocus" ]
                        ]
                )
                [ text "Ok" ]
            , Button.button
                [ Button.secondary
                , Button.onClick (PersonMessage (LifeFocusModalVisibilityChanged Modal.hidden))
                , Button.attrs [ id "cancel-lifefocus" ]
                ]
                [ text "Cancel" ]
            ]
        |> Modal.view model.personR.lifeFocusModal.dialogStatus


{-| Determine what details if any, should be included in LifeFocusConfirmed message.
Message should be raised only if the avatar has been succesfully loaded and user
has selected a life focus that is different from the current one.
-}
focusChangeDetails : FullModel -> Maybe ( Person, LifeFocus )
focusChangeDetails model =
    case model.personR.person of
        RData (Success person) ->
            if person.lifeFocus /= model.personR.lifeFocusModal.focus then
                Maybe.map (\focus -> ( person, focus )) model.personR.lifeFocusModal.focus

            else
                Nothing

        _ ->
            Nothing


lifeFocusButton : FullModel -> LifeFocus -> RadioButtonItem Msg
lifeFocusButton model focus =
    ButtonGroup.radioButton
        (model.personR.lifeFocusModal.focus == Just focus)
        [ Button.outlineSecondary
        , Button.attrs
            [ focusToId focus
            , Spacing.mt1
            , onMouseEnter (PersonMessage <| LifeFocusEnter focus)
            ]
        , Button.onClick (PersonMessage <| LifeFocusChanged focus)
        ]
        [ lifeFocusToString focus
            |> (\s ->
                    String.concat
                        [ String.left 1 s |> String.toUpper
                        , String.dropLeft 1 s
                        ]
               )
            |> text
        ]


focusToId : LifeFocus -> Attribute Msg
focusToId focus =
    id <|
        case focus of
            RulershipFocus ->
                "rulership-button"

            BusinessFocus ->
                "business-button"

            SeductionFocus ->
                "seduction-button"

            IntrigueFocus ->
                "intrigue-button"

            HuntingFocus ->
                "hunting-button"

            WarFocus ->
                "war-button"

            CarousingFocus ->
                "carousing-button"

            FamilyFocus ->
                "family-button"

            ScholarshipFocus ->
                "scholarship-button"

            TheologyFocus ->
                "theology-button"


{-| Render left side of the screen
-}
leftPanel : FullModel -> List (Html Msg)
leftPanel model =
    personDetailsPanel model
        ++ demesnePanel model
        ++ relationsPanel model


{-| Render right side of the screen
-}
rightPanel : FullModel -> List (Html Msg)
rightPanel model =
    statsPanel model
        ++ traitsPanel model


{-| Panel showing basic details of the person
-}
personDetailsPanel : FullModel -> List (Html Msg)
personDetailsPanel model =
    infoPanel
        { title = "Details"
        , currentStatus = model.personR.personDetailsStatus
        , openingMessage = PersonMessage <| PersonDetailsStatusChanged InfoPanelOpen
        , closingMessage = PersonMessage <| PersonDetailsStatusChanged InfoPanelClosed
        , refreshMessage = Just <| PersonMessage <| PersonDetailsRefreshRequested
        }
        Nothing
        personDetailsContent
        model


{-| Render panel showing details of the person
-}
personDetailsContent : FullModel -> List (Html Msg)
personDetailsContent model =
    let
        aName =
            get (personRA << personA << SaveData.try << nameA) model

        dynastyText =
            get (personRA << personA << SaveData.try << dynastyA << try << nameA) model
                |> SaveData.map
                    (\x ->
                        case x of
                            Nothing ->
                                "lowborn"

                            Just s ->
                                unDynastyName s
                    )
                |> SaveData.withDefault "-"

        aTitle =
            get (personRA << personA << SaveData.try << shortTitleA) model

        fullName =
            SaveData.map2 personName aName aTitle
                |> SaveData.withDefault "-"

        isPlayerAvatar =
            get (personRA << personA << SaveData.try << avatarA) model
                |> SaveData.withDefault False

        age =
            get (personRA << personA << SaveData.try << ageA) model
                |> SaveData.map (String.fromInt << unAge)
                |> SaveData.withDefault "-"
                |> text

        gender =
            get (personRA << personA << SaveData.try << genderA) model
                |> SaveData.map displayGender
                |> SaveData.withDefault (text "-")

        intel =
            get (personRA << personA << SaveData.try << intelTypesA) model
                |> SaveData.withDefault []
                |> List.map personIntelToString
                |> Set.fromList
                |> Set.toList
                |> String.join ", "
                |> text

        avatarOpinion =
            get (personRA << personA << SaveData.try << avatarOpinionA) model
                |> SaveData.map displayOpinion
                |> SaveData.withDefault (text "-")

        opinionOfAvatar =
            get (personRA << personA << SaveData.try << opinionOfAvatarA) model
                |> SaveData.map displayOpinion
                |> SaveData.withDefault (text "-")

        location =
            get (personRA << personA << SaveData.try << locationA) model
                |> SaveData.map
                    (\x ->
                        case x of
                            OnPlanet pDetails ->
                                a [ href (PlanetR pDetails.planetId) ]
                                    [ text <| unPlanetName pDetails.planetName ]

                            OnUnit uDetails ->
                                displayOnUnitLocation uDetails

                            UnknownLocation ->
                                text "Unknown"
                    )
                |> SaveData.withDefault (text "Unknown")
    in
    [ Table.table
        { options =
            [ Table.small
            , Table.hover
            , Table.attr <| id "person-details-table"
            ]
        , thead = Table.thead [] []
        , tbody =
            Table.tbody []
                [ Table.tr []
                    [ Table.th [] [ text "Name" ]
                    , Table.td []
                        [ if isPlayerAvatar then
                            text <| fullName ++ " (you)"

                          else
                            text fullName
                        ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Dynasty" ]
                    , Table.td [] [ text dynastyText ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Age" ]
                    , Table.td [] [ age ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Gender" ]
                    , Table.td [] [ gender ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Location" ]
                    , Table.td [] [ location ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Opinion" ]
                    , Table.td []
                        [ avatarOpinion
                        , text " / "
                        , opinionOfAvatar
                        ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Focus" ]
                    , Table.td []
                        [ if isPlayerAvatar then
                            Button.button
                                [ Button.roleLink
                                , Button.small
                                , Button.onClick (PersonMessage (LifeFocusModalVisibilityChanged Modal.shown))
                                , Button.disabled (SaveData.isLoading model.personR.person)
                                , Button.attrs [ id "lifefocus-button" ]
                                ]
                                [ SaveData.map
                                    (\person ->
                                        Maybe.map lifeFocusToString person.lifeFocus
                                            |> Maybe.withDefault "Select life focus"
                                    )
                                    model.personR.person
                                    |> SaveData.withDefault ""
                                    |> text
                                ]

                          else
                            SaveData.map
                                (\person ->
                                    Maybe.map lifeFocusToString person.lifeFocus
                                        |> Maybe.withDefault "-"
                                )
                                model.personR.person
                                |> SaveData.withDefault ""
                                |> text
                        ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Intel" ]
                    , Table.td [] [ intel ]
                    ]
                ]
        }
    ]


{-| Render location on unit into html link
-}
displayOnUnitLocation : OnUnitData -> Html Msg
displayOnUnitLocation pos =
    case pos.position of
        Just position ->
            let
                name =
                    crewPositionToString position
                        ++ " on "
                        ++ unUnitName pos.unitName
                        |> text
            in
            a [ href (UnitR pos.unitId) ] [ name ]

        Nothing ->
            a [ href (UnitR pos.unitId) ] [ text <| unUnitName pos.unitName ]


displayOpinion : OpinionReport -> Html Msg
displayOpinion report =
    case report of
        BaseOpinionReport feeling ->
            displayOpinionFeeling feeling

        OpinionReasonReport feeling _ ->
            displayOpinionFeeling feeling

        DetailedOpinionReport score _ ->
            displayOpinionScore score


displayOpinionFeeling : OpinionFeeling -> Html Msg
displayOpinionFeeling feeling =
    case feeling of
        PositiveFeeling ->
            text "positive"

        NeutralFeeling ->
            text "neutral"

        NegativeFeeling ->
            text "negative"


displayOpinionScore : OpinionScore -> Html Msg
displayOpinionScore score =
    unOpinionScore score
        |> String.fromInt
        |> text


{-| Map Gender into displayable tex
-}
displayGender : Gender -> Html msg
displayGender g =
    case g of
        Man ->
            text "Man"

        Woman ->
            text "Woman"

        Agender ->
            text "Agender"

        Nonbinary ->
            text "Nonbinary"


{-| Panel showing stats of the person
-}
statsPanel : FullModel -> List (Html Msg)
statsPanel model =
    infoPanel
        { title = "Stats"
        , currentStatus = model.personR.statsStatus
        , openingMessage = PersonMessage <| StatsStatusChanged InfoPanelOpen
        , closingMessage = PersonMessage <| StatsStatusChanged InfoPanelClosed
        , refreshMessage = Just <| PersonMessage <| PersonDetailsRefreshRequested
        }
        Nothing
        statsContent
        model


{-| Contents of stats panel
-}
statsContent : FullModel -> List (Html Msg)
statsContent model =
    let
        diplomacy =
            get (personRA << personA << SaveData.try << statsA << try << diplomacyA) model
                |> SaveData.withDefault Nothing
                |> Maybe.map (text << String.fromInt << unStatValue)
                |> Maybe.withDefault (text "-")

        intrigue =
            get (personRA << personA << SaveData.try << statsA << try << intrigueA) model
                |> SaveData.withDefault Nothing
                |> Maybe.map (text << String.fromInt << unStatValue)
                |> Maybe.withDefault (text "-")

        stewardship =
            get (personRA << personA << SaveData.try << statsA << try << stewardshipA) model
                |> SaveData.withDefault Nothing
                |> Maybe.map (text << String.fromInt << unStatValue)
                |> Maybe.withDefault (text "-")

        learning =
            get (personRA << personA << SaveData.try << statsA << try << learningA) model
                |> SaveData.withDefault Nothing
                |> Maybe.map (text << String.fromInt << unStatValue)
                |> Maybe.withDefault (text "-")

        martial =
            get (personRA << personA << SaveData.try << statsA << try << martialA) model
                |> SaveData.withDefault Nothing
                |> Maybe.map (text << String.fromInt << unStatValue)
                |> Maybe.withDefault (text "-")
    in
    [ Table.table
        { options =
            [ Table.small
            , Table.hover
            , Table.attr <| id "person-stats-table"
            ]
        , thead = Table.thead [] []
        , tbody =
            Table.tbody []
                [ Table.tr []
                    [ Table.th [] [ text "Diplomacy" ]
                    , Table.td [] [ diplomacy ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Stewardship" ]
                    , Table.td [] [ stewardship ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Martial" ]
                    , Table.td [] [ martial ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Intrigue" ]
                    , Table.td [] [ intrigue ]
                    ]
                , Table.tr []
                    [ Table.th [] [ text "Learning" ]
                    , Table.td [] [ learning ]
                    ]
                ]
        }
    ]


{-| Panel showing relations
-}
relationsPanel : FullModel -> List (Html Msg)
relationsPanel model =
    infoPanel
        { title = "Relations"
        , currentStatus = model.personR.relationsStatus
        , openingMessage = PersonMessage <| RelationsStatusChanged InfoPanelOpen
        , closingMessage = PersonMessage <| RelationsStatusChanged InfoPanelClosed
        , refreshMessage = Just <| PersonMessage PersonDetailsRefreshRequested
        }
        (Just
            { pageSize = model.personR.relationsPageSize
            , currentPage = model.personR.relationsCurrentPage
            , maxPage =
                get (personRA << personA << SaveData.try << relationsA) model
                    |> SaveData.withDefault []
                    |> maxPage model.personR.relationsPageSize
            , pageChangedMessage = PersonMessage << RelationsPageChanged
            }
        )
        relationsContent
        model


{-| Content of relations panel
-}
relationsContent : FullModel -> List (Html Msg)
relationsContent model =
    [ Grid.row []
        [ Grid.col []
            [ Table.table
                { options =
                    [ Table.small
                    , Table.hover
                    , Table.attr <| id "relations-table"
                    ]
                , thead =
                    Table.simpleThead
                        [ Table.th [] [ text "Name" ]
                        , Table.th [] [ text "Type" ]
                        , Table.th [] [ text "Opinion" ]
                        ]
                , tbody =
                    Table.tbody []
                        (get (personRA << personA << SaveData.try << relationsA) model
                            |> SaveData.withDefault []
                            |> List.sortWith relationOrdering
                            |> List.map relationEntry
                        )
                }
            ]
        ]
    ]


relationOrdering : Ordering RelationLink
relationOrdering a b =
    listOrdering relationTypeOrdering a.types b.types
        |> Ordering.ifStillTiedThen (personNameOrdering a.name b.name)


relationEntry : RelationLink -> Table.Row Msg
relationEntry link =
    Table.tr []
        [ Table.td [] [ a [ href (PersonR link.id) ] [ text <| personName link.name link.shortTitle ] ]
        , Table.td []
            [ List.map relationTypeToString link.types
                |> String.join ", "
                |> text
            ]
        , Table.td [] [ displayOpinion link.opinion ]
        ]


{-| Panel showing demesne
-}
demesnePanel : FullModel -> List (Html Msg)
demesnePanel model =
    infoPanel
        { title = "Demesne"
        , currentStatus = model.personR.demesneStatus
        , openingMessage = PersonMessage <| DemesneStatusChanged InfoPanelOpen
        , closingMessage = PersonMessage <| DemesneStatusChanged InfoPanelClosed
        , refreshMessage = Just <| PersonMessage <| DemesneRefreshRequested
        }
        (Just
            { pageSize = model.personR.demesnePageSize
            , currentPage = model.personR.demesneCurrentPage
            , maxPage =
                model.personR.demesne
                    |> RemoteData.withDefault []
                    |> maxPage model.personR.demesnePageSize
            , pageChangedMessage = PersonMessage << DemesnePageChanged
            }
        )
        demesneContent
        model


{-| Content of demesne panel
-}
demesneContent : FullModel -> List (Html Msg)
demesneContent model =
    [ Table.table
        { options =
            [ Table.small
            , Table.hover
            , Table.attr <| id "relations-table"
            ]
        , thead =
            Table.simpleThead
                [ Table.th [] [ text "Name" ]
                , Table.th [] [ text "Type" ]
                ]
        , tbody =
            Table.tbody []
                (model.personR.demesne
                    |> RemoteData.withDefault []
                    |> List.sortWith demesneSorter
                    |> List.map demesneEntry
                )
        }
    ]


{-| Single demesne entry in demesne panel
-}
demesneEntry : DemesneShortInfo -> Table.Row Msg
demesneEntry entry =
    let
        name =
            formalName entry
                |> unDemesneName

        link =
            case entry of
                PlanetDemesneShort report ->
                    a [ href (PlanetR report.planetId) ] [ text name ]

                StarSystemDemesneShort report ->
                    a [ href (StarSystemR report.starSystemId) ] [ text name ]
    in
    Table.tr []
        [ Table.td [] [ link ]
        , Table.td [] [ text <| demesneType entry ]
        ]


{-| Sort demesne report by type and then by name
-}
demesneSorter : DemesneShortInfo -> DemesneShortInfo -> Order
demesneSorter a b =
    case a of
        PlanetDemesneShort pa ->
            case b of
                PlanetDemesneShort pb ->
                    compare (unPlanetName pa.name) (unPlanetName pb.name)

                StarSystemDemesneShort _ ->
                    GT

        StarSystemDemesneShort sa ->
            case b of
                PlanetDemesneShort _ ->
                    LT

                StarSystemDemesneShort sb ->
                    compare (unStarSystemName sa.name) (unStarSystemName sb.name)


demesneType : DemesneShortInfo -> String
demesneType info =
    case info of
        PlanetDemesneShort _ ->
            "Planet"

        StarSystemDemesneShort _ ->
            "Star system"


{-| Panel showing traits
-}
traitsPanel : FullModel -> List (Html Msg)
traitsPanel model =
    infoPanel
        { title = "Traits"
        , currentStatus = model.personR.traitsStatus
        , openingMessage = PersonMessage <| TraitsStatusChanged InfoPanelOpen
        , closingMessage = PersonMessage <| TraitsStatusChanged InfoPanelClosed
        , refreshMessage = Just <| PersonMessage <| PersonDetailsRefreshRequested
        }
        (Just
            { pageSize = model.personR.traitsPageSize
            , currentPage = model.personR.traitsCurrentPage
            , maxPage =
                get (personRA << personA << SaveData.try << traitsA) model
                    |> SaveData.withDefault (Just [])
                    |> Maybe.withDefault []
                    |> maxPage model.personR.traitsPageSize
            , pageChangedMessage = PersonMessage << TraitsPageChanged
            }
        )
        traitsContent
        model


traitsContent : FullModel -> List (Html Msg)
traitsContent model =
    let
        traits =
            get (personRA << personA << SaveData.try << traitsA) model
                |> SaveData.withDefault (Just [])
                |> Maybe.withDefault []
                |> List.sortWith traitOrdering

        blockSize =
            List.length traits // 3

        blockLeftOvers =
            remainderBy 3 (List.length traits)

        leftCount =
            if blockLeftOvers > 0 then
                blockSize + 1

            else
                blockSize

        leftTraits =
            List.take leftCount traits
                |> List.map Just

        middleCount =
            if blockLeftOvers > 1 then
                blockSize + 1

            else
                blockSize

        middleTraits =
            List.drop leftCount traits
                |> List.take middleCount
                |> List.map Just
                |> (\xs -> List.append xs (List.repeat (leftCount - middleCount) Nothing))

        rightCount =
            List.length traits - leftCount - middleCount

        rightTraits =
            List.drop (leftCount + middleCount) traits
                |> List.map Just
                |> (\xs -> List.append xs (List.repeat (leftCount - rightCount) Nothing))

        groupedTraits =
            List.map3 (\a b c -> ( a, b, c )) leftTraits middleTraits rightTraits
    in
    [ Table.table
        { options =
            [ Table.small
            , Table.hover
            , Table.attr <| id "traits-table"
            ]
        , thead = Table.thead [] []
        , tbody =
            Table.tbody []
                (List.map traitRow groupedTraits)
        }
    ]


traitRow : ( Maybe Trait, Maybe Trait, Maybe Trait ) -> Table.Row Msg
traitRow ( a, b, c ) =
    Table.tr []
        [ traitEntry2 a
        , traitEntry2 b
        , traitEntry2 c
        ]


traitEntry2 : Maybe Trait -> Table.Cell Msg
traitEntry2 trait =
    case trait of
        Nothing ->
            Table.td [] []

        Just x ->
            Table.td [ Table.cellAttr (title <| unTraitDescription x.description) ]
                [ text <| unTraitName x.name ]


{-| Handle messages specific to this page
-}
update : PersonRMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PersonDetailsReceived (RData NotAsked) ->
            ( model
            , Cmd.none
            )

        PersonDetailsReceived (RData Loading) ->
            ( model
            , Cmd.none
            )

        PersonDetailsReceived (Saving _) ->
            ( model
            , Cmd.none
            )

        PersonDetailsReceived (RData (Success person)) ->
            ( setFullModel (personRA << personA) (RData (Success person)) model
            , Cmd.none
            )

        PersonDetailsReceived (RData (Failure err)) ->
            ( setFullModel (personRA << personA) (RData (Failure err)) model
                |> over errorsA (\errors -> error err "Failed to load person details" :: errors)
            , Cmd.none
            )

        DemesneReceived NotAsked ->
            ( model
            , Cmd.none
            )

        DemesneReceived Loading ->
            ( model
            , Cmd.none
            )

        DemesneReceived (Success demesne) ->
            ( setFullModel (personRA << demesneA) (Success demesne) model
            , Cmd.none
            )

        DemesneReceived (Failure err) ->
            ( setFullModel (personRA << demesneA) (Failure err) model
                |> over errorsA (\errors -> error err "Failed to load demesne" :: errors)
            , Cmd.none
            )

        PersonDetailsStatusChanged status ->
            ( setFullModel (personRA << personDetailsStatusA) status model
            , Cmd.none
            )

        PersonDetailsRefreshRequested ->
            withFullModel
                model
                ( model, Cmd.none )
                (\m ->
                    get (personRA << personA << SaveData.try << idA) m
                        |> SaveData.map
                            (\pId ->
                                ( setFullModel (personRA << personA) (RData Loading) model
                                , getPersonDetails (PersonMessage << PersonDetailsReceived) pId
                                )
                            )
                        |> SaveData.withDefault ( model, Cmd.none )
                )

        StatsStatusChanged status ->
            ( setFullModel (personRA << statsStatusA) status model
            , Cmd.none
            )

        DemesneStatusChanged status ->
            ( setFullModel (personRA << demesneStatusA) status model
            , Cmd.none
            )

        DemesneRefreshRequested ->
            withFullModel
                model
                ( model, Cmd.none )
                (\m ->
                    get (personRA << personA << SaveData.try << idA) m
                        |> SaveData.map
                            (\pId ->
                                ( setFullModel (personRA << demesneA) Loading model
                                , getDemesne (PersonMessage << DemesneReceived) pId
                                )
                            )
                        |> SaveData.withDefault ( model, Cmd.none )
                )

        DemesnePageChanged pageNumber ->
            let
                lastPgNumber =
                    withFullModel
                        model
                        0
                        (\m ->
                            m.personR.demesne
                                |> RemoteData.withDefault []
                                |> maxPage m.personR.demesnePageSize
                        )

                setPage target _ =
                    if target < 0 then
                        0

                    else if target > lastPgNumber then
                        lastPgNumber

                    else
                        target
            in
            ( overFullModel (personRA << demesneCurrentPageA) (setPage pageNumber) model
            , Cmd.none
            )

        RelationsStatusChanged status ->
            ( setFullModel (personRA << relationsStatusA) status model
            , Cmd.none
            )

        RelationsPageChanged pageNumber ->
            let
                lastPgNumber =
                    withFullModel
                        model
                        0
                        (\m ->
                            get (personRA << personA << SaveData.try << relationsA) m
                                |> SaveData.withDefault []
                                |> maxPage m.personR.relationsPageSize
                        )

                setPage target _ =
                    if target < 0 then
                        0

                    else if target > lastPgNumber then
                        lastPgNumber

                    else
                        target
            in
            ( overFullModel (personRA << relationsCurrentPageA) (setPage pageNumber) model
            , Cmd.none
            )

        LifeFocusModalVisibilityChanged visibility ->
            ( setFullModel (personRA << lifeFocusModalA << dialogStatusA) visibility model
                |> setFullModel (personRA << lifeFocusModalA << focusA)
                    (withFullModel model
                        Nothing
                        (\m ->
                            SaveData.map .lifeFocus m.personR.person
                                |> SaveData.withDefault Nothing
                        )
                    )
            , Cmd.none
            )

        LifeFocusChanged focus ->
            ( setFullModel (personRA << lifeFocusModalA << focusA) (Just focus) model
                |> setFullModel (personRA << lifeFocusModalA << explanationA) (lifeFocusToExplanation focus)
            , Cmd.none
            )

        LifeFocusEnter focus ->
            ( setFullModel (personRA << lifeFocusModalA << explanationA) (lifeFocusToExplanation focus) model
            , Cmd.none
            )

        LifeFocusConfirmed person focus ->
            ( setFullModel (personRA << personA) (Saving person) model
                |> setFullModel (personRA << lifeFocusModalA << dialogStatusA) Modal.hidden
                |> setFullModel (personRA << lifeFocusModalA << focusA) (Just focus)
            , changeLifeFocus (PersonMessage << PersonDetailsReceived) person.id (Just focus)
            )

        TraitsStatusChanged status ->
            ( setFullModel (personRA << traitsStatusA) status model
            , Cmd.none
            )

        TraitsPageChanged pageNumber ->
            let
                lastPgNumber =
                    withFullModel
                        model
                        0
                        (\m ->
                            get (personRA << personA << SaveData.try << traitsA) m
                                |> SaveData.withDefault Nothing
                                |> Maybe.withDefault []
                                |> maxPage m.personR.traitsPageSize
                        )

                setPage target _ =
                    if target < 0 then
                        0

                    else if target > lastPgNumber then
                        lastPgNumber

                    else
                        target
            in
            ( overFullModel (personRA << traitsCurrentPageA) (setPage pageNumber) model
            , Cmd.none
            )


isLoading : FullModel -> Bool
isLoading model =
    let
        vm =
            model.personR
    in
    SaveData.isLoading vm.person
        || RemoteData.isLoading vm.demesne
