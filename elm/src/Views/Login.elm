module Views.Login exposing
    ( init
    , isLoading
    , page
    , update
    )

import Accessors exposing (set)
import Api.User exposing (login, postUser)
import Bootstrap.Button as Button
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Data.Accessors
    exposing
        ( loginRA
        , loginResponseA
        , modeA
        , newUserResponseA
        , password2A
        , passwordA
        , userNameA
        )
import Data.Common exposing (Route(..), UserId(..))
import Data.Model
    exposing
        ( ApiMsg(..)
        , LoginModel
        , Model
        , Msg(..)
        , SubModel(..)
        )
import Data.User exposing (Password(..), UserName(..))
import Html exposing (Html, h2, h4, p, text)
import Html.Attributes exposing (class, for, id)
import RemoteData exposing (RemoteData(..))
import SaveData exposing (SaveData(..))
import ViewModels.Login exposing (LoginMode(..), LoginRMsg(..))


page : LoginModel -> Html Msg
page model =
    case model.loginR.mode of
        LogIn ->
            loginPage model

        CreateUser ->
            createUserPage model


loginPage : LoginModel -> Html Msg
loginPage model =
    let
        loginDisabled =
            String.isEmpty model.loginR.userName
                || String.isEmpty model.loginR.password
                || SaveData.isLoading model.loginResponse
                || SaveData.isLoading model.newUserResponse
    in
    Grid.containerFluid []
        [ Grid.row [ Row.centerXs, Row.attrs [ class "login-content" ] ]
            [ Grid.col [ Col.md4, Col.attrs [ class "login-info" ] ]
                [ h4 [ class "logo-title text-center" ]
                    [ text "Deep Sky" ]
                ]
            , Grid.col [ Col.md8, Col.xs12, Col.sm12, Col.attrs [ class "login-form" ] ]
                [ Grid.containerFluid []
                    [ Grid.row []
                        [ Grid.col [ Col.attrs [ class "text-center" ] ]
                            [ h2 [] [ text "Log In" ] ]
                        ]
                    , Grid.row []
                        [ Grid.col []
                            [ Form.group []
                                [ Form.label [ for "UserName" ] [ text "Username" ]
                                , Input.text
                                    [ Input.id "UserName"
                                    , Input.onInput (LoginMessage << UserNameChanged)
                                    , Input.value model.loginR.userName
                                    ]
                                ]
                            , Form.group []
                                [ Form.label [ for "Password" ] [ text "Password" ]
                                , Input.password
                                    [ Input.id "Password"
                                    , Input.onInput (LoginMessage << PasswordChanged)
                                    , Input.value model.loginR.password
                                    ]
                                ]
                            , Button.button
                                [ Button.primary
                                , Button.onClick (LoginMessage <| LoginRequested model.loginR.userName model.loginR.password)
                                , Button.attrs
                                    [ id "btn-submit"
                                    , class "login-button"
                                    ]
                                , Button.disabled loginDisabled
                                ]
                                [ text "Submit" ]
                            ]
                        ]
                    , Grid.row []
                        [ Grid.col []
                            [ p []
                                [ text "Don't have account? "
                                , Button.button
                                    [ Button.roleLink
                                    , Button.onClick (LoginMessage <| ModeChanged CreateUser)
                                    , Button.attrs [ id "btn-signup" ]
                                    ]
                                    [ text "Sign up here." ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]


createUserPage : LoginModel -> Html Msg
createUserPage model =
    let
        ( pwd1Ok, pwd1Option, pwd1Msg ) =
            passwordFeedBack model.loginR.password model.loginR.password2

        ( pwd2Ok, pwd2Option, pwd2Msg ) =
            passwordFeedBack model.loginR.password2 model.loginR.password

        ( userNameOk, userNameOption, userNameMsg ) =
            usernameFeedBack model.loginR.userName

        buttonDisabled =
            (pwd1Ok == False)
                || (pwd2Ok == False)
                || (userNameOk == False)
                || SaveData.isLoading model.newUserResponse
    in
    Grid.containerFluid []
        [ Grid.row [ Row.centerXs, Row.attrs [ class "login-content" ] ]
            [ Grid.col [ Col.md4, Col.attrs [ class "login-info" ] ]
                [ h4 [ class "logo-title text-center" ]
                    [ text "Deep Sky" ]
                ]
            , Grid.col [ Col.md8, Col.xs12, Col.sm12, Col.attrs [ class "login-form" ] ]
                [ Grid.containerFluid []
                    [ Grid.row []
                        [ Grid.col [ Col.attrs [ class "text-center" ] ]
                            [ h2 [] [ text "Sign up" ] ]
                        ]
                    , Grid.row []
                        [ Grid.col []
                            [ Form.group []
                                [ Form.label [ for "UserName" ] [ text "Username" ]
                                , Input.text
                                    [ Input.id "UserName"
                                    , Input.onInput (LoginMessage << UserNameChanged)
                                    , Input.value model.loginR.userName
                                    , userNameOption
                                    ]
                                , userNameMsg
                                ]
                            , Form.group []
                                [ Form.label [ for "Password" ] [ text "Password" ]
                                , Input.password
                                    [ Input.id "Password"
                                    , Input.onInput (LoginMessage << PasswordChanged)
                                    , Input.value model.loginR.password
                                    , pwd1Option
                                    ]
                                , pwd1Msg
                                , Form.help [] [ text "Password must be at least 8 characters" ]
                                ]
                            , Form.group []
                                [ Form.label [ for "Password2" ] [ text "Password again" ]
                                , Input.password
                                    [ Input.id "Password2"
                                    , Input.onInput (LoginMessage << Password2Changed)
                                    , Input.value model.loginR.password2
                                    , pwd2Option
                                    ]
                                , pwd2Msg
                                ]
                            , Button.button
                                [ Button.primary
                                , Button.onClick (LoginMessage <| NewUserRequested model.loginR.userName model.loginR.password)
                                , Button.attrs
                                    [ id "btn-submit"
                                    , class "login-button"
                                    ]
                                , Button.disabled buttonDisabled
                                ]
                                [ text "Submit" ]
                            ]
                        ]
                    , Grid.row []
                        [ Grid.col []
                            [ p []
                                [ text "Already have an account? "
                                , Button.button
                                    [ Button.roleLink
                                    , Button.onClick (LoginMessage <| ModeChanged LogIn)
                                    ]
                                    [ text "Login here." ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]


usernameFeedBack : String -> ( Bool, Input.Option Msg, Html Msg )
usernameFeedBack userName =
    if String.isEmpty userName then
        ( False, Input.danger, Form.invalidFeedback [] [ text "Username can't be empty" ] )

    else
        ( True, Input.success, Form.validFeedback [] [] )


passwordFeedBack : String -> String -> ( Bool, Input.Option Msg, Html Msg )
passwordFeedBack pwd pwd2 =
    if String.isEmpty pwd then
        ( False, Input.danger, Form.invalidFeedback [] [ text "Password can't be empty" ] )

    else if String.length pwd < 8 then
        ( False, Input.danger, Form.invalidFeedback [] [ text "Password must be at least 8 characters" ] )

    else if pwd /= pwd2 then
        ( False, Input.danger, Form.invalidFeedback [] [ text "Passwords don't match" ] )

    else
        ( True, Input.success, Form.validFeedback [] [] )


update : LoginRMsg -> LoginModel -> ( LoginModel, Cmd Msg )
update msg model =
    case msg of
        UserNameChanged s ->
            ( set (loginRA << userNameA) s model
            , Cmd.none
            )

        PasswordChanged s ->
            ( set (loginRA << passwordA) s model
            , Cmd.none
            )

        Password2Changed s ->
            ( set (loginRA << password2A) s model
            , Cmd.none
            )

        LoginRequested userName pwd ->
            ( set loginResponseA (RData Loading) model
            , login userName pwd (ApiMsgCompleted << AvatarReceived)
            )

        NewUserRequested userName pwd ->
            ( set newUserResponseA (RData Loading) model
            , postUser
                { id = UserId 0
                , name = UserName userName
                , password = Just <| Password pwd
                , avatar = Nothing
                }
                (ApiMsgCompleted << NewUserCreated)
            )

        ModeChanged mode ->
            ( set (loginRA << modeA) mode model
                |> set (loginRA << userNameA) ""
                |> set (loginRA << passwordA) ""
                |> set (loginRA << password2A) ""
            , Cmd.none
            )


isLoading : Model -> Bool
isLoading model =
    case model.subModel of
        NotLoggedIn m ->
            SaveData.isLoading m.loginResponse
                || SaveData.isLoading m.newUserResponse

        _ ->
            False


init : Model -> Cmd Msg
init _ =
    Cmd.none
