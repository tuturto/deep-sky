module Views.Home exposing
    ( init
    , isLoading
    , page
    )

import Data.Model
    exposing
        ( FullModel
        , Model
        , Msg(..)
        )
import Html
    exposing
        ( Html
        , div
        , text
        )


page : Model -> Html Msg
page _ =
    div [] [ text "Hello from Home" ]


init : Model -> Cmd Msg
init _ =
    Cmd.none


isLoading : FullModel -> Bool
isLoading _ =
    False
