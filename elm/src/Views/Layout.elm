module Views.Layout exposing (view)

import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Navbar as Navbar
import Browser
import Data.Common
    exposing
        ( ErrorMessage(..)
        , Route(..)
        , unPlanetName
        , unStarSystemName
        )
import Data.Model
    exposing
        ( AvatarSelectionModel
        , FullModel
        , LoginModel
        , Model
        , Msg(..)
        , SubModel(..)
        , avatarAvailable
        , withFullModel
        )
import Data.PersonNames exposing (displayName)
import Data.User exposing (Role(..))
import Data.Vehicles exposing (Unit(..), unShipName, unVehicleName)
import Html
    exposing
        ( Attribute
        , Html
        , a
        , div
        , i
        , li
        , nav
        , ol
        , text
        , ul
        )
import Html.Attributes exposing (class, id, style)
import Html.Events exposing (onClick)
import MessageToast
import Navigation exposing (parseLocation)
import RemoteData exposing (RemoteData(..))
import SaveData
import Url exposing (Url)
import Views.Admin.Main
import Views.Admin.People.Add
import Views.Admin.People.Edit
import Views.Admin.People.List
import Views.Admin.Status
import Views.Avatar
import Views.Bases
import Views.Construction
import Views.Designer
import Views.Fleet
import Views.Helpers
    exposing
        ( biologicalsToText
        , chemicalsToText
        , href
        , mechanicalsToText
        , starDateToText
        )
import Views.Home
import Views.Login
import Views.Messages
import Views.Person
import Views.Planet
import Views.Profile
import Views.Research
import Views.StarSystem
import Views.StarSystems
import Views.Unit


menuBar : Model -> List Role -> Html Msg
menuBar m _ =
    case m.subModel of
        NotLoggedIn _ ->
            Navbar.config NavbarMsg
                |> Navbar.withAnimation
                |> Navbar.collapseMedium
                |> Navbar.brand
                    []
                    [ text "Deep Sky" ]
                |> Navbar.view m.navbarState

        UserWithoutAvatar model ->
            let
                currentLocation =
                    parseLocation m.url

                mClass =
                    menuClass currentLocation

                menuButton route msg =
                    li [ mClass route ] [ a [ href route ] [ text msg ] ]
            in
            Navbar.config NavbarMsg
                |> Navbar.withAnimation
                |> Navbar.collapseMedium
                |> Navbar.brand
                    []
                    [ text "Deep Sky" ]
                -- TODO: logo
                |> Navbar.items
                    [ Navbar.itemLink
                        [ id "logout"
                        , onClick LogoutRequested
                        ]
                        [ a [ href HomeR ] [ text "Logout" ] ]
                    ]
                |> Navbar.view m.navbarState

        UserWithAvatar model ->
            let
                currentLocation =
                    parseLocation m.url

                mClass =
                    menuClass currentLocation

                menuButton route msg =
                    li [ mClass route ] [ a [ href route ] [ text msg ] ]
            in
            Navbar.config NavbarMsg
                |> Navbar.withAnimation
                |> Navbar.collapseMedium
                |> Navbar.brand
                    []
                    [ text "Deep Sky" ]
                -- TODO: logo
                |> Navbar.items
                    [ Navbar.itemLink
                        []
                        [ a [ href HomeR ] [ text "Home" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href ProfileR ] [ text "Profile" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href StarSystemsR ] [ text "Star Systems" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href BasesR ] [ text "Bases" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href FleetR ] [ text "Fleet" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href DesignerR ] [ text "Designer" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href ResearchR ] [ text "Research" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href ConstructionR ] [ text "Construction" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href MessagesR ] [ text "Messages" ] ]
                    , Navbar.itemLink
                        []
                        [ a [ href AdminR ] [ text "Admin" ] ]
                    , Navbar.itemLink
                        [ id "logout"
                        , onClick LogoutRequested
                        ]
                        [ a [ href HomeR ] [ text "Logout" ] ]
                    ]
                |> Navbar.view m.navbarState


infoBar : Model -> Html Msg
infoBar m =
    case m.subModel of
        NotLoggedIn _ ->
            ul [ class "infoitem" ]
                ([ li []
                    [ i [ class "fas fa-clock" ] []
                    , starDateToText <| RemoteData.toMaybe m.currentTime
                    ]
                 ]
                    ++ (if isLoading m then
                            [ li [ id "loading-indicator", class "float-right" ] [ text "loading..." ] ]

                        else
                            []
                       )
                )

        UserWithoutAvatar model ->
            ul [ class "infoitem" ]
                ([ li []
                    [ i [ class "fas fa-clock" ] []
                    , starDateToText <| RemoteData.toMaybe m.currentTime
                    ]
                 ]
                    ++ (if isLoading m then
                            [ li [ id "loading-indicator", class "float-right" ] [ text "loading..." ] ]

                        else
                            []
                       )
                )

        UserWithAvatar model ->
            ul [ class "infoitem" ]
                ([ li []
                    [ i [ class "fas fa-clock" ] []
                    , starDateToText <| RemoteData.toMaybe m.currentTime
                    ]
                 , li [] <|
                    biologicalsToText <|
                        RemoteData.toMaybe model.resources
                 , li [] <|
                    mechanicalsToText <|
                        RemoteData.toMaybe model.resources
                 , li [] <|
                    chemicalsToText <|
                        RemoteData.toMaybe model.resources
                 ]
                    ++ (if isLoading m then
                            [ li [ id "loading-indicator", class "float-right" ] [ text "loading..." ] ]

                        else
                            []
                       )
                )


isLoading : Model -> Bool
isLoading m =
    case m.subModel of
        NotLoggedIn model ->
            SaveData.isLoading model.loginResponse

        UserWithoutAvatar _ ->
            RemoteData.isLoading m.currentTime
                || RemoteData.isLoading m.logoutResponse

        UserWithAvatar model ->
            isModuleLoading m.url model
                || RemoteData.isLoading model.resources
                || RemoteData.isLoading m.currentTime
                || SaveData.isLoading model.avatar
                || RemoteData.isLoading m.logoutResponse


isModuleLoading : Url -> FullModel -> Bool
isModuleLoading url model =
    case parseLocation url of
        AdminR ->
            Views.Admin.Main.isLoading model

        AdminListPeopleR ->
            Views.Admin.People.List.isLoading model

        AdminPersonR _ ->
            Views.Admin.People.Edit.isLoading model

        AdminNewPersonR ->
            Views.Admin.People.Add.isLoading model

        AdminStatusR ->
            Views.Admin.Status.isLoading model

        BasesR ->
            False

        ConstructionR ->
            False

        DesignerR ->
            Views.Designer.isLoading model

        FleetR ->
            False

        HomeR ->
            Views.Home.isLoading model

        MessagesR ->
            Views.Messages.isLoading model

        ProfileR ->
            False

        ResearchR ->
            Views.Research.isLoading model

        StarSystemR _ ->
            Views.StarSystem.isLoading model

        StarSystemsR ->
            Views.StarSystems.isLoading model

        PlanetR _ ->
            Views.Planet.isLoading model

        PersonR _ ->
            Views.Person.isLoading model

        UnitR _ ->
            Views.Unit.isLoading model


errorBar : Model -> Html Msg
errorBar model =
    div [ class "container", onClick ClearErrors ] <|
        List.map errorRow model.errors


errorRow : ErrorMessage -> Html Msg
errorRow (ErrorMessage error) =
    div [ class "row error-bar" ]
        [ div [ class "col-lg-12" ] [ text error ] ]


menuClass : Route -> Route -> Attribute Msg
menuClass current checked =
    if similarRoutes current checked then
        class "active"

    else
        class ""


{-| Compare routes and deduce if they should be considired similar for purposes
of active menu item selection. First route is currently active route
(ie. the route that browser is currently displaying). Second route should be
one of the top level routes (ie. ones that are displayed at the top menu bar).
-}
similarRoutes : Route -> Route -> Bool
similarRoutes current checked =
    case checked of
        StarSystemsR ->
            case current of
                StarSystemsR ->
                    True

                StarSystemR _ ->
                    True

                PlanetR _ ->
                    True

                _ ->
                    False

        AdminR ->
            case current of
                AdminR ->
                    True

                AdminListPeopleR ->
                    True

                AdminPersonR _ ->
                    True

                AdminNewPersonR ->
                    True

                AdminStatusR ->
                    True

                _ ->
                    False

        FleetR ->
            case current of
                FleetR ->
                    True

                UnitR _ ->
                    True

                _ ->
                    False

        _ ->
            current == checked


breadcrumbPath : Model -> Html Msg
breadcrumbPath model =
    case model.subModel of
        NotLoggedIn _ ->
            div [] []

        UserWithoutAvatar _ ->
            div [] []

        UserWithAvatar _ ->
            div []
                [ ol [ class "breadcrumb" ]
                    (breadcrumb model True <|
                        parseLocation model.url
                    )
                ]


{-| Given model and route, build segment of breadcrumb path as a triple.
First element of tuple is text that should be displayed in the breadcrumb
path. Second element is possible id for Html. Third element is possible
parent element of the route. For example HomeR is parent of StarSystemsR,
which in turn is parent of StarSystemR 1. Model can be used to compute
dynamic text to be displayed in breadcrumb path, for example a planet or
person name.
-}
segment : Model -> Route -> ( String, Maybe (Attribute Msg), Maybe Route )
segment m route =
    case route of
        AdminR ->
            ( "Admin", Nothing, Just HomeR )

        AdminListPeopleR ->
            ( "People", Nothing, Just AdminR )

        AdminPersonR _ ->
            let
                name =
                    withFullModel m
                        ""
                        (\model ->
                            RemoteData.map (\x -> displayName x.name) model.adminR.adminEditPersonR.person
                                |> RemoteData.withDefault "-"
                        )
            in
            ( name, Just (id "breadcrumb-person-name"), Just AdminListPeopleR )

        AdminNewPersonR ->
            ( "Add person", Nothing, Just AdminListPeopleR )

        AdminStatusR ->
            ( "Satus", Nothing, Just AdminR )

        BasesR ->
            ( "Bases", Nothing, Just HomeR )

        ConstructionR ->
            ( "Constructions", Nothing, Just HomeR )

        DesignerR ->
            ( "Designs", Nothing, Just HomeR )

        FleetR ->
            ( "Fleet", Nothing, Just HomeR )

        HomeR ->
            ( "Home", Nothing, Nothing )

        MessagesR ->
            ( "Messages", Nothing, Just HomeR )

        ProfileR ->
            ( "Profile", Nothing, Just HomeR )

        ResearchR ->
            ( "Research", Nothing, Just HomeR )

        StarSystemR _ ->
            let
                starSystemName =
                    withFullModel m
                        ""
                        (\model ->
                            model.starSystemR.starSystem
                                |> RemoteData.map (\x -> unStarSystemName x.name)
                                |> RemoteData.withDefault "Unknown star system"
                        )
            in
            ( starSystemName, Just (id "breadcrumb-system-name"), Just StarSystemsR )

        StarSystemsR ->
            ( "Star systems", Nothing, Just HomeR )

        PlanetR _ ->
            withFullModel m
                ( "-", Just (id "breadcrumb-planet-name"), Just HomeR )
                (\model ->
                    RemoteData.map
                        (\planet ->
                            ( unPlanetName planet.name
                            , Just (id "breadcrumb-planet-name")
                            , Just (StarSystemR planet.systemId)
                            )
                        )
                        model.planetR.planet
                        |> RemoteData.withDefault ( "-", Just (id "breadcrumb-planet-name"), Just HomeR )
                )

        PersonR _ ->
            withFullModel m
                ( "-", Just (id "breadcrumb-person-name"), Just HomeR )
                (\model ->
                    let
                        personName =
                            SaveData.map (\person -> displayName person.name) model.personR.person
                                |> SaveData.withDefault "-"
                    in
                    ( personName, Just (id "breadcrumb-person-name"), Just HomeR )
                )

        UnitR _ ->
            withFullModel m
                ( "-", Just (id "breadcrumb-unit-name"), Just FleetR )
                (\model ->
                    let
                        unitName =
                            RemoteData.map
                                (\x ->
                                    case x of
                                        Ship details ->
                                            unShipName details.name

                                        Vehicle details ->
                                            unVehicleName details.name
                                )
                                model.unitR.unit
                                |> RemoteData.withDefault "-"
                    in
                    ( unitName, Just (id "breadcrumb-unit-name"), Just FleetR )
                )


breadcrumb : Model -> Bool -> Route -> List (Html Msg)
breadcrumb model topLevel route =
    let
        ( linkText, linkId, linkParent ) =
            segment model route

        textEntry =
            if topLevel then
                text <| linkText

            else
                a [ href route ] [ text linkText ]

        entryClass =
            if topLevel then
                case linkId of
                    Nothing ->
                        [ class "breadcrumb-item active" ]

                    Just x ->
                        [ class "breadcrumb-item active", x ]

            else
                case linkId of
                    Nothing ->
                        [ class "breadcrumb-item" ]

                    Just x ->
                        [ x, class "breadcrumb-item" ]
    in
    case linkParent of
        Nothing ->
            [ li entryClass [ textEntry ] ]

        Just x ->
            breadcrumb model False x ++ [ li entryClass [ textEntry ] ]


view : Model -> Browser.Document Msg
view model =
    { title = "Deep Sky"
    , body =
        [ Grid.container []
            [ menuBar model [ AdminRole, PlayerRole ]
            , breadcrumbPath model
            , infoBar model
            , errorBar model
            , currentPage model
            ]
        , div [ style "width" "100vw", style "height" "100vh" ]
            [ -- Only need to pass the proper MessageToast
              MessageToast.view model.messageToast
            ]
        ]
    }


currentPage : Model -> Html Msg
currentPage model =
    case model.subModel of
        NotLoggedIn lModel ->
            Views.Login.page lModel

        UserWithoutAvatar _ ->
            Views.Avatar.page model

        UserWithAvatar _ ->
            case parseLocation model.url of
                AdminR ->
                    Views.Admin.Main.page model

                AdminListPeopleR ->
                    Views.Admin.People.List.page model

                AdminPersonR _ ->
                    Views.Admin.People.Edit.page model

                AdminNewPersonR ->
                    Views.Admin.People.Add.page model

                AdminStatusR ->
                    Views.Admin.Status.page model

                BasesR ->
                    Views.Bases.page model

                ConstructionR ->
                    Views.Construction.page model

                DesignerR ->
                    Views.Designer.page model

                FleetR ->
                    Views.Fleet.page model

                HomeR ->
                    Views.Home.page model

                MessagesR ->
                    Views.Messages.page model

                ProfileR ->
                    Views.Profile.page model

                ResearchR ->
                    Views.Research.page model

                StarSystemR systemId ->
                    Views.StarSystem.page systemId model

                StarSystemsR ->
                    Views.StarSystems.page model

                PlanetR planetId ->
                    Views.Planet.page planetId model

                PersonR _ ->
                    Views.Person.page model

                UnitR _ ->
                    Views.Unit.page model
