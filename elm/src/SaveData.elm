module SaveData exposing
    ( SaveData(..)
    , andMap
    , fromResult
    , isLoading
    , map
    , map2
    , succeed
    , toMaybe
    , toWebData
    , try
    , tryRemote
    , withDefault
    )

import Accessors exposing (Relation, makeOneToN)
import Http exposing (Error)
import RemoteData exposing (RemoteData(..), WebData)


tryRemote : Relation elem sub wrap -> Relation (RemoteData err elem) sub (RemoteData err wrap)
tryRemote =
    makeOneToN
        RemoteData.map
        RemoteData.map


try : Relation elem sub wrap -> Relation (SaveData elem) sub (SaveData wrap)
try =
    makeOneToN
        map
        map


{-| Data that can be saved and retained on client while save is in progress
-}
type SaveData a
    = RData (WebData a)
    | Saving a


{-| Is data being loaded currently. In case it is being saved, return True
Otherwise defaults to RemoteData.isLoading behaviour.
-}
isLoading : SaveData a -> Bool
isLoading d =
    case d of
        RData details ->
            RemoteData.isLoading details

        Saving _ ->
            True


{-| Map function over SaveData
-}
map : (a -> b) -> SaveData a -> SaveData b
map f d =
    case d of
        RData details ->
            RData <| RemoteData.map f details

        Saving details ->
            Saving <| f details


map2 : (a -> b -> c) -> SaveData a -> SaveData b -> SaveData c
map2 f a b =
    map f a
        |> andMap b


andMap : SaveData a -> SaveData (a -> b) -> SaveData b
andMap a f =
    case ( a, f ) of
        ( RData (Success value), RData (Success fn) ) ->
            RData <| Success (fn value)

        ( RData (Failure error), _ ) ->
            RData (Failure error)

        ( _, RData (Failure error) ) ->
            RData (Failure error)

        ( Saving value, fn ) ->
            case fn of
                RData (Success fn2) ->
                    Saving (fn2 value)

                RData (Failure err) ->
                    RData (Failure err)

                RData NotAsked ->
                    RData NotAsked

                RData Loading ->
                    RData Loading

                Saving fn2 ->
                    Saving (fn2 value)

        ( value, Saving fn2 ) ->
            case value of
                RData (Success value2) ->
                    Saving (fn2 value2)

                RData (Failure err) ->
                    RData (Failure err)

                RData NotAsked ->
                    RData NotAsked

                RData Loading ->
                    RData Loading

                Saving value2 ->
                    Saving (fn2 value2)

        ( RData Loading, _ ) ->
            RData Loading

        ( _, RData Loading ) ->
            RData Loading

        ( RData NotAsked, _ ) ->
            RData NotAsked

        ( _, RData NotAsked ) ->
            RData NotAsked


{-| Turn Result into SaveData
-}
fromResult : Result Error a -> SaveData a
fromResult res =
    RData <| RemoteData.fromResult res


{-| Lift data into SaveData
-}
succeed : a -> SaveData a
succeed d =
    RData <| RemoteData.succeed d


withDefault : a -> SaveData a -> a
withDefault def d =
    case d of
        RData details ->
            RemoteData.withDefault def details

        Saving details ->
            details


toMaybe : SaveData a -> Maybe a
toMaybe d =
    case d of
        RData details ->
            RemoteData.toMaybe details

        Saving details ->
            Just details



-- | coerce SaveData to WebData
-- Saving a is coerced to NotAsked


toWebData : SaveData a -> WebData a
toWebData d =
    case d of
        RData r ->
            r

        Saving _ ->
            NotAsked
