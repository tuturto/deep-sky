module Api.Admin exposing
    ( addPerson
    , getPeople
    , getPerson
    , getSimulationStatus
    , putPerson
    , putSimulationStatus
    , reseedDatabase
    , simulationSummary
    , wipeDatabase
    )

import Api.Common
    exposing
        ( dynastyIdDecoder
        , dynastyIdEncoder
        , encodeMaybe
        , get
        , pagedDecoder
        , planetIdDecoder
        , planetIdEncoder
        , post
        , put
        , starDateDecoder
        , starDateEncoder
        , starSystemIdDecoder
        , starSystemIdEncoder
        )
import Api.Endpoints exposing (Endpoint(..))
import Api.People
    exposing
        ( ageEncoder
        , genderDecoder
        , genderEncoder
        , personIdDecoder
        , personIdEncoder
        , personNameDecoder
        , personNameEncoder
        , sexDecoder
        , sexEncoder
        , statDecoder
        , statEncoder
        )
import Api.User exposing (factionIdDecoder, factionIdEncoder)
import Data.Admin
    exposing
        ( AgeOptions(..)
        , Count(..)
        , Person
        , PersonOptions
        , Simulation
        , SimulationSummary
        , SystemStatus(..)
        )
import Data.Common exposing (PagedResult, PersonId)
import Data.Model exposing (ApiMsg(..), Msg(..))
import Http
import Json.Decode as Decode
    exposing
        ( andThen
        , fail
        , field
        , int
        , maybe
        , string
        , succeed
        )
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import RemoteData exposing (WebData)
import SaveData exposing (SaveData)


{-| Command for loading current simulation status
-}
getSimulationStatus : (WebData Simulation -> Msg) -> Cmd Msg
getSimulationStatus msg =
    Http.send (RemoteData.fromResult >> msg) (get ApiAdminSimulationStatus simulationDecoder)


{-| Command for loading current simulation summary
-}
simulationSummary : (SaveData SimulationSummary -> Msg) -> Cmd Msg
simulationSummary msg =
    Http.send (SaveData.fromResult >> msg) (get ApiAdminSummary simulationSummaryDecoder)


{-| Update simulation status
-}
putSimulationStatus : (WebData Simulation -> Msg) -> Simulation -> Cmd Msg
putSimulationStatus msg simulation =
    Http.send (RemoteData.fromResult >> msg) (put ApiAdminSimulationStatus (simulationEncoder simulation) simulationDecoder)


{-| Get list of people
Results are paged, first parameter indicates amount of records to skip, second parameter amount
of records to take. In case Nothing is supplied, server default is used.
-}
getPeople : (WebData (PagedResult Person) -> Msg) -> Maybe Int -> Maybe Int -> Cmd Msg
getPeople msg skip take =
    Http.send (RemoteData.fromResult >> msg) (get (ApiAdminPeople skip take) (pagedDecoder personDecoder))


{-| Get details of single person
-}
getPerson : (WebData Person -> Msg) -> PersonId -> Cmd Msg
getPerson msg personId =
    Http.send (RemoteData.fromResult >> msg) (get (ApiAdminPerson personId) personDecoder)


putPerson : (WebData Person -> Msg) -> PersonId -> Person -> Cmd Msg
putPerson msg pId person =
    Http.send (RemoteData.fromResult >> msg) (put (ApiAdminPerson pId) (personEncoder person) personDecoder)


{-| Request creation of new person
-}
addPerson : (WebData Person -> Msg) -> PersonOptions -> Cmd Msg
addPerson msg opt =
    Http.send (RemoteData.fromResult >> msg) (post ApiAdminAddPerson (personOptionsEncoder opt) personDecoder)


wipeDatabase : (SaveData SimulationSummary -> Msg) -> Cmd Msg
wipeDatabase msg =
    Http.send (SaveData.fromResult >> msg) (post ApiAdminWipe (Encode.string "") simulationSummaryDecoder)


reseedDatabase : (SaveData SimulationSummary -> Msg) -> Cmd Msg
reseedDatabase msg =
    Http.send (SaveData.fromResult >> msg) (post ApiAdminReseed (Encode.string "") simulationSummaryDecoder)


simulationSummaryDecoder : Decode.Decoder SimulationSummary
simulationSummaryDecoder =
    succeed SimulationSummary
        |> andMap (field "Users" countDecoder)
        |> andMap (field "Factions" countDecoder)
        |> andMap (field "Planets" countDecoder)
        |> andMap (field "StarSystems" countDecoder)


countDecoder : Decode.Decoder (Count a)
countDecoder =
    succeed Count
        |> andMap int


{-| Decoder for Simulation
-}
simulationDecoder : Decode.Decoder Simulation
simulationDecoder =
    succeed Simulation
        |> andMap (field "currentTime" starDateDecoder)
        |> andMap (field "status" systemStatusDecoder)


{-| Encoder for Simulation
-}
simulationEncoder : Simulation -> Encode.Value
simulationEncoder simulation =
    Encode.object
        [ ( "currentTime", starDateEncoder simulation.time )
        , ( "status", systemStatusEncoder simulation.status )
        ]


{-| Decoder for system status
-}
systemStatusDecoder : Decode.Decoder SystemStatus
systemStatusDecoder =
    string
        |> andThen stringToSystemStatus


stringToSystemStatus : String -> Decode.Decoder SystemStatus
stringToSystemStatus s =
    case s of
        "Offline" ->
            succeed Offline

        "Maintenance" ->
            succeed Maintenance

        "Online" ->
            succeed Online

        "ProcessingTurn" ->
            succeed ProcessingTurn

        _ ->
            fail "unknown value"


{-| Encoder for system status
-}
systemStatusEncoder : SystemStatus -> Encode.Value
systemStatusEncoder status =
    Encode.string <|
        case status of
            Offline ->
                "Offline"

            Maintenance ->
                "Maintenance"

            Online ->
                "Online"

            ProcessingTurn ->
                "ProcessingTurn"


personDecoder : Decode.Decoder Person
personDecoder =
    succeed Person
        |> andMap (field "id" personIdDecoder)
        |> andMap (field "name" personNameDecoder)
        |> andMap (field "sex" sexDecoder)
        |> andMap (field "gender" genderDecoder)
        |> andMap (field "dateOfBirth" starDateDecoder)
        |> andMap (field "diplomacy" statDecoder)
        |> andMap (field "learning" statDecoder)
        |> andMap (field "martial" statDecoder)
        |> andMap (field "intrigue" statDecoder)
        |> andMap (field "stewardship" statDecoder)
        |> andMap (field "factionId" (maybe factionIdDecoder))
        |> andMap (field "planetTitle" (maybe planetIdDecoder))
        |> andMap (field "starSystemTitle" (maybe starSystemIdDecoder))
        |> andMap (field "dynastyId" (maybe dynastyIdDecoder))


personEncoder : Person -> Encode.Value
personEncoder person =
    Encode.object
        [ ( "id", personIdEncoder person.id )
        , ( "name", personNameEncoder person.name )
        , ( "sex", sexEncoder person.sex )
        , ( "gender", genderEncoder person.gender )
        , ( "dateOfBirth", starDateEncoder person.dateOfBirth )
        , ( "diplomacy", statEncoder person.diplomacy )
        , ( "learning", statEncoder person.learning )
        , ( "martial", statEncoder person.martial )
        , ( "intrigue", statEncoder person.intrigue )
        , ( "stewardship", statEncoder person.stewardship )
        , ( "factionId", encodeMaybe factionIdEncoder person.factionId )
        , ( "planetTitle", encodeMaybe planetIdEncoder person.planetTitle )
        , ( "starSystemTitle", encodeMaybe starSystemIdEncoder person.starSystemTitle )
        , ( "dynastyId", encodeMaybe dynastyIdEncoder person.dynastyId )
        ]


personOptionsEncoder : PersonOptions -> Encode.Value
personOptionsEncoder opt =
    Encode.object
        [ ( "Age", encodeMaybe ageOptionsEncoder opt.age ) ]


ageOptionsEncoder : AgeOptions -> Encode.Value
ageOptionsEncoder opt =
    case opt of
        AgeBracket start end ->
            Encode.object
                [ ( "tag", Encode.string "AgeBracket" )
                , ( "contents", Encode.list ageEncoder [ start, end ] )
                ]

        ExactAge age ->
            Encode.object
                [ ( "tag", Encode.string "ExactAge" )
                , ( "contents", ageEncoder age )
                ]
