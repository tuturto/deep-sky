module Api.People exposing
    ( PatchPersonRequest
    , ageEncoder
    , changeLifeFocus
    , crewPositionDecoder
    , crewPositionEncoder
    , dynastyLinkDecoder
    , dynastyLinkEncoder
    , genderDecoder
    , genderEncoder
    , getDemesne
    , getPersonDetails
    , lifeFocusDecoder
    , lifeFocusEncoder
    , lifeFocusStatusDecoder
    , lifeFocusStatusEncoder
    , longTitleDecoder
    , longTitleEncoder
    , onUnitDecoder
    , onUnitEncoder
    , opinionFeelingDecoder
    , opinionFeelingEncoder
    , opinionIntelDecoder
    , opinionIntelEncoder
    , opinionReasonDecoder
    , opinionReasonEncoder
    , opinionReportDecoder
    , opinionReportEncoder
    , opinionScoreDecoder
    , opinionScoreEncoder
    , patchPerson
    , patchPersonRequestEncoder
    , personDecoder
    , personEncoder
    , personIdDecoder
    , personIdEncoder
    , personIntelDecoder
    , personIntelEncoder
    , personNameDecoder
    , personNameEncoder
    , petIdDecoder
    , petIdEncoder
    , petTypeDecoder
    , petTypeEncoder
    , queryPeople
    , relationLinkDecoder
    , relationLinkEncoder
    , relationTypeDecoder
    , relationTypeEncoder
    , relationVisibilityDecoder
    , relationVisibilityEncoder
    , sexDecoder
    , sexEncoder
    , shortTitleDecoder
    , shortTitleEncoder
    , statDecoder
    , statEncoder
    , statsDecoder
    , statsEncoder
    , traitDecoder
    , traitDescriptionDecoder
    , traitDescriptionEncoder
    , traitEncoder
    , traitNameDecoder
    , traitNameEncoder
    , traitTypeDecoder
    , traitTypeEncoder
    , unitNameDecoder
    , unitNameEncoder
    )

import Api.Common
    exposing
        ( dynastyIdDecoder
        , dynastyIdEncoder
        , dynastyNameDecoder
        , dynastyNameEncoder
        , encodeMaybe
        , get
        , is
        , pagedDecoder
        , patch
        , planetIdDecoder
        , planetIdEncoder
        , planetNameDecoder
        , planetNameEncoder
        , starDateDecoder
        , starDateEncoder
        , starSystemIdDecoder
        , starSystemIdEncoder
        , starSystemNameDecoder
        , unitIdDecoder
        , unitIdEncoder
        )
import Api.Endpoints exposing (Endpoint(..), NPCSearch)
import Data.Common
    exposing
        ( DemesneName(..)
        , PagedResult
        , PersonId(..)
        , PetId(..)
        )
import Data.Model exposing (Msg(..))
import Data.People
    exposing
        ( Age(..)
        , DemesneShortInfo(..)
        , DynastyLink
        , Gender(..)
        , LifeFocus(..)
        , LifeFocusStatus(..)
        , OnPlanetData
        , OnUnitData
        , OpinionFeeling(..)
        , OpinionIntel(..)
        , OpinionReason(..)
        , OpinionReport(..)
        , OpinionScore(..)
        , Person
        , PersonIntel(..)
        , PersonLocation(..)
        , PetType(..)
        , PlanetDemesneReportShort
        , RelationLink
        , RelationType(..)
        , RelationVisibility(..)
        , Sex(..)
        , StarSystemDemesneReportShort
        , StatValue(..)
        , StatValues
        , Trait
        , TraitDescription(..)
        , TraitName(..)
        , TraitType(..)
        , unStatValue
        )
import Data.PersonNames
    exposing
        ( Cognomen(..)
        , FamilyName(..)
        , FirstName(..)
        , LongTitle(..)
        , PersonName(..)
        , RegnalNumber(..)
        , ShortTitle(..)
        , unLongTitle
        , unShortTitle
        )
import Data.Vehicles exposing (CrewPosition(..), UnitName(..))
import Http
import Json.Decode as Decode
    exposing
        ( andThen
        , bool
        , fail
        , field
        , int
        , list
        , maybe
        , oneOf
        , string
        , succeed
        )
import Json.Decode.Extra exposing (andMap, when)
import Json.Encode as Encode
import RemoteData exposing (WebData)
import SaveData exposing (SaveData)


getPersonDetails : (SaveData Person -> Msg) -> PersonId -> Cmd Msg
getPersonDetails msg pId =
    Http.send (SaveData.fromResult >> msg) (get (ApiSinglePerson pId) personDecoder)


queryPeople : (WebData (PagedResult Person) -> Msg) -> Maybe Int -> Maybe Int -> Maybe NPCSearch -> Cmd Msg
queryPeople msg skip take npc =
    Http.send (RemoteData.fromResult >> msg) (get (ApiPeople skip take npc) (pagedDecoder personDecoder))


getDemesne : (WebData (List DemesneShortInfo) -> Msg) -> PersonId -> Cmd Msg
getDemesne msg pId =
    Http.send (RemoteData.fromResult >> msg) (get (ApiDemesne pId) (list demesneReportShortDecoder))


changeLifeFocus : (SaveData Person -> Msg) -> PersonId -> Maybe LifeFocus -> Cmd Msg
changeLifeFocus msg pId focus =
    let
        request =
            { lifeFocus = focus }
    in
    patchPerson msg pId request


patchPerson : (SaveData Person -> Msg) -> PersonId -> PatchPersonRequest -> Cmd Msg
patchPerson msg pId details =
    Http.send (SaveData.fromResult >> msg) (patch (ApiSinglePerson pId) (patchPersonRequestEncoder details) personDecoder)


personNameDecoder : Decode.Decoder PersonName
personNameDecoder =
    oneOf
        [ when nameType (is "RegularName") regularNameDecoder
        , when nameType (is "SimpleName") simpleNameDecoder
        , when nameType (is "RegalName") regalNameDecoder
        ]


{-| Decoder for name type string
-}
nameType : Decode.Decoder String
nameType =
    field "Tag" string


regularNameDecoder : Decode.Decoder PersonName
regularNameDecoder =
    succeed RegularName
        |> andMap (field "FirstName" firstNameDecoder)
        |> andMap (field "FamilyName" familyNameDecoder)
        |> andMap (field "Cognomen" (maybe cognomenDecoder))


simpleNameDecoder : Decode.Decoder PersonName
simpleNameDecoder =
    succeed SimpleName
        |> andMap (field "FirstName" firstNameDecoder)
        |> andMap (field "Cognomen" (maybe cognomenDecoder))


regalNameDecoder : Decode.Decoder PersonName
regalNameDecoder =
    succeed RegalName
        |> andMap (field "FirstName" firstNameDecoder)
        |> andMap (field "FamilyName" familyNameDecoder)
        |> andMap (field "RegnalNumber" regnalNumberDecoder)
        |> andMap (field "Cognomen" (maybe cognomenDecoder))


personIdDecoder : Decode.Decoder PersonId
personIdDecoder =
    succeed PersonId
        |> andMap int


personIdEncoder : PersonId -> Encode.Value
personIdEncoder (PersonId n) =
    Encode.int n


firstNameDecoder : Decode.Decoder FirstName
firstNameDecoder =
    succeed FirstName
        |> andMap string


firstNameEncoder : FirstName -> Encode.Value
firstNameEncoder (FirstName name) =
    Encode.string name


familyNameDecoder : Decode.Decoder FamilyName
familyNameDecoder =
    succeed FamilyName
        |> andMap string


familyNameEncoder : FamilyName -> Encode.Value
familyNameEncoder (FamilyName name) =
    Encode.string name


regnalNumberDecoder : Decode.Decoder RegnalNumber
regnalNumberDecoder =
    succeed RegnalNumber
        |> andMap int


regnalNumberEncoder : RegnalNumber -> Encode.Value
regnalNumberEncoder (RegnalNumber n) =
    Encode.int n


cognomenDecoder : Decode.Decoder Cognomen
cognomenDecoder =
    succeed Cognomen
        |> andMap string


cognomenEncoder : Cognomen -> Encode.Value
cognomenEncoder (Cognomen name) =
    Encode.string name


personNameEncoder : PersonName -> Encode.Value
personNameEncoder name =
    case name of
        SimpleName a b ->
            Encode.object
                [ ( "Tag", Encode.string "SimpleName" )
                , ( "FirstName", firstNameEncoder a )
                , ( "Cognomen", encodeMaybe cognomenEncoder b )
                ]

        RegularName a b c ->
            Encode.object
                [ ( "Tag", Encode.string "RegularName" )
                , ( "FirstName", firstNameEncoder a )
                , ( "FamilyName", familyNameEncoder b )
                , ( "Cognomen", encodeMaybe cognomenEncoder c )
                ]

        RegalName a b c d ->
            Encode.object
                [ ( "Tag", Encode.string "RegalName" )
                , ( "FirstName", firstNameEncoder a )
                , ( "FamilyName", familyNameEncoder b )
                , ( "RegnalNumber", regnalNumberEncoder c )
                , ( "Cognomen", encodeMaybe cognomenEncoder d )
                ]


personDecoder : Decode.Decoder Person
personDecoder =
    succeed Person
        |> andMap (field "Id" personIdDecoder)
        |> andMap (field "Avatar" bool)
        |> andMap (field "Name" personNameDecoder)
        |> andMap (field "ShortTitle" (maybe shortTitleDecoder))
        |> andMap (field "LongTitle" (maybe longTitleDecoder))
        |> andMap (field "Stats" (maybe statsDecoder))
        |> andMap (field "Sex" sexDecoder)
        |> andMap (field "Gender" genderDecoder)
        |> andMap (field "Age" ageDecoder)
        |> andMap (field "Relations" (list relationLinkDecoder))
        |> andMap (field "IntelTypes" (list personIntelDecoder))
        |> andMap (field "Dynasty" (maybe dynastyLinkDecoder))
        |> andMap (field "Traits" (maybe (list traitDecoder)))
        |> andMap (field "AvatarOpinion" opinionReportDecoder)
        |> andMap (field "OpinionOfAvatar" opinionReportDecoder)
        |> andMap (field "Location" personLocationDecoder)
        |> andMap (field "LifeFocus" (maybe lifeFocusDecoder))
        |> andMap (field "LifeFocusStatus" lifeFocusStatusDecoder)


personEncoder : Person -> Encode.Value
personEncoder person =
    Encode.object
        [ ( "Id", personIdEncoder person.id )
        , ( "Avatar", Encode.bool person.avatar )
        , ( "Name", personNameEncoder person.name )
        , ( "ShortTitle", encodeMaybe shortTitleEncoder person.shortTitle )
        , ( "LongTitle", encodeMaybe longTitleEncoder person.longTitle )
        , ( "Stats", encodeMaybe statsEncoder person.stats )
        , ( "Sex", sexEncoder person.sex )
        , ( "Gender", genderEncoder person.gender )
        , ( "Age", ageEncoder person.age )
        , ( "Relations", Encode.list relationLinkEncoder person.relations )
        , ( "IntelTypes", Encode.list personIntelEncoder person.intelTypes )
        , ( "Dynasty", encodeMaybe dynastyLinkEncoder person.dynasty )
        , ( "Traits", encodeMaybe (Encode.list traitEncoder) person.traits )
        , ( "AvatarOpinion", opinionReportEncoder person.avatarOpinion )
        , ( "OpinionOfAvatar", opinionReportEncoder person.opinionOfAvatar )
        , ( "Location", personLocationEncoder person.location )
        , ( "LifeFocus", encodeMaybe lifeFocusEncoder person.lifeFocus )
        , ( "LifeFocusStatus", lifeFocusStatusEncoder person.lifeFocusStatus )
        ]


statDecoder : Decode.Decoder StatValue
statDecoder =
    succeed StatValue
        |> andMap int


statEncoder : StatValue -> Encode.Value
statEncoder stat =
    Encode.int <| unStatValue stat


statsDecoder : Decode.Decoder StatValues
statsDecoder =
    succeed StatValues
        |> andMap (field "Diplomacy" statDecoder)
        |> andMap (field "Learning" statDecoder)
        |> andMap (field "Martial" statDecoder)
        |> andMap (field "Intrigue" statDecoder)
        |> andMap (field "Stewardship" statDecoder)


statsEncoder : StatValues -> Encode.Value
statsEncoder stats =
    Encode.object
        [ ( "Diplomacy", statEncoder stats.diplomacy )
        , ( "Learning", statEncoder stats.learning )
        , ( "Martial", statEncoder stats.martial )
        , ( "Intrigue", statEncoder stats.intrigue )
        , ( "Stewardship", statEncoder stats.stewardship )
        ]


stringToSex : String -> Decode.Decoder Sex
stringToSex s =
    case s of
        "Male" ->
            succeed Male

        "Female" ->
            succeed Female

        "Intersex" ->
            succeed Intersex

        _ ->
            fail "failed to deserialize"


sexDecoder : Decode.Decoder Sex
sexDecoder =
    string |> andThen stringToSex


sexEncoder : Sex -> Encode.Value
sexEncoder sex =
    case sex of
        Male ->
            Encode.string "Male"

        Female ->
            Encode.string "Female"

        Intersex ->
            Encode.string "Intersex"


stringToGender : String -> Decode.Decoder Gender
stringToGender s =
    case s of
        "Man" ->
            succeed Man

        "Woman" ->
            succeed Woman

        "Agender" ->
            succeed Agender

        "Nonbinary" ->
            succeed Nonbinary

        _ ->
            fail "Failed to deserialize"


genderEncoder : Gender -> Encode.Value
genderEncoder gender =
    case gender of
        Man ->
            Encode.string "Man"

        Woman ->
            Encode.string "Woman"

        Agender ->
            Encode.string "Agender"

        Nonbinary ->
            Encode.string "Nonbinary"


genderDecoder : Decode.Decoder Gender
genderDecoder =
    string |> andThen stringToGender


ageDecoder : Decode.Decoder Age
ageDecoder =
    succeed Age
        |> andMap int


ageEncoder : Age -> Encode.Value
ageEncoder (Age n) =
    Encode.int n


demesneReportShortDecoder : Decode.Decoder DemesneShortInfo
demesneReportShortDecoder =
    oneOf
        [ when demesneType (is "Planet") planetDemesneReportShortDecoder
        , when demesneType (is "StarSystem") starSystemDemesneReportShortDecoder
        ]


{-| Decoder for demesne report type string
-}
demesneType : Decode.Decoder String
demesneType =
    field "Tag" string


planetDemesneReportShortDecoder : Decode.Decoder DemesneShortInfo
planetDemesneReportShortDecoder =
    let
        decoder =
            succeed PlanetDemesneReportShort
                |> andMap (field "PlanetId" planetIdDecoder)
                |> andMap (field "StarSystemId" starSystemIdDecoder)
                |> andMap (field "Name" planetNameDecoder)
                |> andMap (field "FormalName" demesneNameDecoder)
                |> andMap (field "Date" starDateDecoder)
    in
    succeed PlanetDemesneShort
        |> andMap decoder


starSystemDemesneReportShortDecoder : Decode.Decoder DemesneShortInfo
starSystemDemesneReportShortDecoder =
    let
        decoder =
            succeed StarSystemDemesneReportShort
                |> andMap (field "StarSystemId" starSystemIdDecoder)
                |> andMap (field "Name" starSystemNameDecoder)
                |> andMap (field "FormalName" demesneNameDecoder)
                |> andMap (field "Date" starDateDecoder)
    in
    succeed StarSystemDemesneShort
        |> andMap decoder


demesneNameDecoder : Decode.Decoder DemesneName
demesneNameDecoder =
    succeed DemesneName
        |> andMap string


shortTitleDecoder : Decode.Decoder ShortTitle
shortTitleDecoder =
    succeed ShortTitle
        |> andMap string


shortTitleEncoder : ShortTitle -> Encode.Value
shortTitleEncoder title =
    Encode.string <| unShortTitle title


longTitleDecoder : Decode.Decoder LongTitle
longTitleDecoder =
    succeed LongTitle
        |> andMap string


longTitleEncoder : LongTitle -> Encode.Value
longTitleEncoder title =
    Encode.string <| unLongTitle title


relationLinkDecoder : Decode.Decoder RelationLink
relationLinkDecoder =
    succeed RelationLink
        |> andMap (field "Id" personIdDecoder)
        |> andMap (field "Name" personNameDecoder)
        |> andMap (field "ShortTitle" (maybe shortTitleDecoder))
        |> andMap (field "LongTitle" (maybe longTitleDecoder))
        |> andMap (field "Types" (list relationTypeDecoder))
        |> andMap (field "Opinion" opinionReportDecoder)


relationLinkEncoder : RelationLink -> Encode.Value
relationLinkEncoder link =
    Encode.object
        [ ( "Id", personIdEncoder link.id )
        , ( "Name", personNameEncoder link.name )
        , ( "ShortTitle", encodeMaybe shortTitleEncoder link.shortTitle )
        , ( "LongTitle", encodeMaybe longTitleEncoder link.longTitle )
        , ( "Types", Encode.list relationTypeEncoder link.types )
        , ( "Opinion", opinionReportEncoder link.opinion )
        ]


relationTypeDecoder : Decode.Decoder RelationType
relationTypeDecoder =
    string |> andThen stringToRelationType


stringToRelationType : String -> Decode.Decoder RelationType
stringToRelationType s =
    case s of
        "Parent" ->
            succeed Parent

        "Child" ->
            succeed Child

        "Sibling" ->
            succeed Sibling

        "StepParent" ->
            succeed StepParent

        "StepChild" ->
            succeed StepChild

        "StepSibling" ->
            succeed StepSibling

        "Spouse" ->
            succeed Spouse

        "ExSpouse" ->
            succeed ExSpouse

        "Widow" ->
            succeed Widow

        "Lover" ->
            succeed Lover

        "ExLover" ->
            succeed ExLover

        "Friend" ->
            succeed Friend

        "Rival" ->
            succeed Rival

        _ ->
            fail "unknown type"


relationTypeEncoder : RelationType -> Encode.Value
relationTypeEncoder relationType =
    case relationType of
        Parent ->
            Encode.string "Parent"

        Child ->
            Encode.string "Child"

        Sibling ->
            Encode.string "Sibling"

        StepParent ->
            Encode.string "StepParent"

        StepChild ->
            Encode.string "StepChild"

        StepSibling ->
            Encode.string "StepSibling"

        Spouse ->
            Encode.string "Spouse"

        ExSpouse ->
            Encode.string "ExSpouse"

        Widow ->
            Encode.string "Widow"

        Lover ->
            Encode.string "Lover"

        ExLover ->
            Encode.string "ExLover"

        Friend ->
            Encode.string "Friend"

        Rival ->
            Encode.string "Rival"


personIntelDecoder : Decode.Decoder PersonIntel
personIntelDecoder =
    oneOf
        [ when tagType (is "Stats") (succeed Stats)
        , when tagType (is "Demesne") (succeed Demesne)
        , when tagType (is "FamilyRelations") (succeed FamilyRelations)
        , when tagType (is "SecretRelations") (succeed SecretRelations)
        , when tagType (is "Traits") (succeed Traits)
        , when tagType
            (is "Opinions")
            (succeed Opinions
                |> andMap (field "contents" opinionIntelDecoder)
            )
        , when tagType (is "Location") (succeed Location)
        , when tagType (is "Activity") (succeed Activity)
        ]


personIntelEncoder : PersonIntel -> Encode.Value
personIntelEncoder intel =
    case intel of
        Stats ->
            Encode.object [ ( "tag", Encode.string "Stats" ) ]

        Demesne ->
            Encode.object [ ( "tag", Encode.string "Demesne" ) ]

        FamilyRelations ->
            Encode.object [ ( "tag", Encode.string "FamilyRelations" ) ]

        SecretRelations ->
            Encode.object [ ( "tag", Encode.string "SecretRelations" ) ]

        Traits ->
            Encode.object [ ( "tag", Encode.string "Traits" ) ]

        Location ->
            Encode.object [ ( "tag", Encode.string "Location" ) ]

        Activity ->
            Encode.object [ ( "tag", Encode.string "Activity" ) ]

        Opinions details ->
            Encode.object
                [ ( "tag", Encode.string "Opinions" )
                , ( "contents", opinionIntelEncoder details )
                ]


opinionIntelEncoder : OpinionIntel -> Encode.Value
opinionIntelEncoder intel =
    case intel of
        BaseOpinionIntel visibility ->
            Encode.object
                [ ( "tag", Encode.string "BaseOpinionIntel" )
                , ( "contents", relationVisibilityEncoder visibility )
                ]

        ReasonsForOpinions visibility ->
            Encode.object
                [ ( "tag", Encode.string "ReasonsForOpinions" )
                , ( "contents", relationVisibilityEncoder visibility )
                ]

        DetailedOpinions visibility ->
            Encode.object
                [ ( "tag", Encode.string "DetailedOpinions" )
                , ( "contents", relationVisibilityEncoder visibility )
                ]


relationVisibilityEncoder : RelationVisibility -> Encode.Value
relationVisibilityEncoder visiblity =
    case visiblity of
        SecretRelation ->
            Encode.string "SecretRelation"

        FamilyRelation ->
            Encode.string "FamilyRelation"

        PublicRelation ->
            Encode.string "PublicRelation"


tagType : Decode.Decoder String
tagType =
    field "tag" string


dynastyLinkDecoder : Decode.Decoder DynastyLink
dynastyLinkDecoder =
    succeed DynastyLink
        |> andMap (field "Id" dynastyIdDecoder)
        |> andMap (field "Name" dynastyNameDecoder)


dynastyLinkEncoder : DynastyLink -> Encode.Value
dynastyLinkEncoder dynasty =
    Encode.object
        [ ( "Id", dynastyIdEncoder dynasty.id )
        , ( "Name", dynastyNameEncoder dynasty.name )
        ]


opinionIntelDecoder : Decode.Decoder OpinionIntel
opinionIntelDecoder =
    oneOf
        [ when tagType
            (is "BaseOpinionIntel")
            (succeed BaseOpinionIntel
                |> andMap (field "contents" relationVisibilityDecoder)
            )
        , when tagType
            (is "ReasonsForOpinions")
            (succeed ReasonsForOpinions
                |> andMap (field "contents" relationVisibilityDecoder)
            )
        , when tagType
            (is "DetailedOpinions")
            (succeed DetailedOpinions
                |> andMap (field "contents" relationVisibilityDecoder)
            )
        ]


relationVisibilityDecoder : Decode.Decoder RelationVisibility
relationVisibilityDecoder =
    string |> andThen stringToRelationVisibility


stringToRelationVisibility : String -> Decode.Decoder RelationVisibility
stringToRelationVisibility s =
    case s of
        "PublicRelation" ->
            succeed PublicRelation

        "FamilyRelation" ->
            succeed FamilyRelation

        "SecretRelation" ->
            succeed SecretRelation

        _ ->
            fail "failed to decode"


traitDecoder : Decode.Decoder Trait
traitDecoder =
    succeed Trait
        |> andMap (field "Name" traitNameDecoder)
        |> andMap (field "Type" traitTypeDecoder)
        |> andMap (field "Description" traitDescriptionDecoder)
        |> andMap (field "ValidUntil" (maybe starDateDecoder))


traitEncoder : Trait -> Encode.Value
traitEncoder trait =
    Encode.object
        [ ( "Name", traitNameEncoder trait.name )
        , ( "Type", traitTypeEncoder trait.traitType )
        , ( "Description", traitDescriptionEncoder trait.description )
        , ( "ValidUntil", encodeMaybe starDateEncoder trait.validUntil )
        ]


traitNameDecoder : Decode.Decoder TraitName
traitNameDecoder =
    succeed TraitName
        |> andMap string


traitNameEncoder : TraitName -> Encode.Value
traitNameEncoder (TraitName s) =
    Encode.string s


traitTypeDecoder : Decode.Decoder TraitType
traitTypeDecoder =
    succeed TraitType
        |> andMap string


traitTypeEncoder : TraitType -> Encode.Value
traitTypeEncoder (TraitType s) =
    Encode.string s


traitDescriptionDecoder : Decode.Decoder TraitDescription
traitDescriptionDecoder =
    succeed TraitDescription
        |> andMap string


traitDescriptionEncoder : TraitDescription -> Encode.Value
traitDescriptionEncoder (TraitDescription s) =
    Encode.string s


opinionFeelingDecoder : Decode.Decoder OpinionFeeling
opinionFeelingDecoder =
    string |> andThen stringToOpinionFeeling


stringToOpinionFeeling : String -> Decode.Decoder OpinionFeeling
stringToOpinionFeeling s =
    case s of
        "PositiveFeeling" ->
            succeed PositiveFeeling

        "NeutralFeeling" ->
            succeed NeutralFeeling

        "NegativeFeeling" ->
            succeed NegativeFeeling

        _ ->
            fail "failed to deserialize"


opinionFeelingEncoder : OpinionFeeling -> Encode.Value
opinionFeelingEncoder feeling =
    case feeling of
        PositiveFeeling ->
            Encode.string "PositiveFeeling"

        NeutralFeeling ->
            Encode.string "NeutralFeeling"

        NegativeFeeling ->
            Encode.string "NegativeFeeling"


opinionReasonDecoder : Decode.Decoder OpinionReason
opinionReasonDecoder =
    succeed OpinionReason
        |> andMap string


opinionReasonEncoder : OpinionReason -> Encode.Value
opinionReasonEncoder (OpinionReason s) =
    Encode.string s


opinionScoreDecoder : Decode.Decoder OpinionScore
opinionScoreDecoder =
    succeed OpinionScore
        |> andMap int


opinionScoreEncoder : OpinionScore -> Encode.Value
opinionScoreEncoder (OpinionScore n) =
    Encode.int n


opinionReportDecoder : Decode.Decoder OpinionReport
opinionReportDecoder =
    oneOf
        [ when opinionReportType
            (is "BaseOpinionReport")
            (succeed BaseOpinionReport
                |> andMap (field "Feeling" opinionFeelingDecoder)
            )
        , when opinionReportType
            (is "OpinionReasonReport")
            (succeed OpinionReasonReport
                |> andMap (field "Feeling" opinionFeelingDecoder)
                |> andMap (field "Reasons" (list opinionReasonDecoder))
            )
        , when opinionReportType
            (is "DetailedOpinionReport")
            (succeed DetailedOpinionReport
                |> andMap (field "Score" opinionScoreDecoder)
                |> andMap (field "Reasons" (list opinionReasonDecoder))
            )
        ]


opinionReportEncoder : OpinionReport -> Encode.Value
opinionReportEncoder report =
    case report of
        BaseOpinionReport feeling ->
            Encode.object
                [ ( "Tag", Encode.string "BaseOpinionReport" )
                , ( "Feeling", opinionFeelingEncoder feeling )
                ]

        OpinionReasonReport feeling reasons ->
            Encode.object
                [ ( "Tag", Encode.string "OpinionReasonReport" )
                , ( "Feeling", opinionFeelingEncoder feeling )
                , ( "Reasons", Encode.list opinionReasonEncoder reasons )
                ]

        DetailedOpinionReport score reasons ->
            Encode.object
                [ ( "Tag", Encode.string "DetailedOpinionReport" )
                , ( "Score", opinionScoreEncoder score )
                , ( "Reasons", Encode.list opinionReasonEncoder reasons )
                ]


opinionReportType : Decode.Decoder String
opinionReportType =
    field "Tag" string


petIdDecoder : Decode.Decoder PetId
petIdDecoder =
    succeed PetId
        |> andMap int


petIdEncoder : PetId -> Encode.Value
petIdEncoder (PetId n) =
    Encode.int n


petTypeDecoder : Decode.Decoder PetType
petTypeDecoder =
    string |> andThen stringToPetType


stringToPetType : String -> Decode.Decoder PetType
stringToPetType s =
    case s of
        "Cat" ->
            succeed Cat

        "Rat" ->
            succeed Rat

        _ ->
            fail "failed to deserialize"


petTypeEncoder : PetType -> Encode.Value
petTypeEncoder t =
    case t of
        Cat ->
            Encode.string "Cat"

        Rat ->
            Encode.string "Rat"


personLocationDecoder : Decode.Decoder PersonLocation
personLocationDecoder =
    oneOf
        [ when locationTag
            (is "OnPlanet")
            (succeed OnPlanet
                |> andMap (field "Contents" onPlanetDecoder)
            )
        , when locationTag
            (is "OnUnit")
            (succeed OnUnit
                |> andMap (field "Contents" onUnitDecoder)
            )
        , when locationTag
            (is "UnknownLocation")
            (succeed UnknownLocation)
        ]


personLocationEncoder : PersonLocation -> Encode.Value
personLocationEncoder location =
    case location of
        OnPlanet details ->
            Encode.object
                [ ( "Tag", Encode.string "OnPlanet" )
                , ( "Contents", onPlanetEncoder details )
                ]

        OnUnit details ->
            Encode.object
                [ ( "Tag", Encode.string "OnUnit" )
                , ( "Contents", onUnitEncoder details )
                ]

        UnknownLocation ->
            Encode.object
                [ ( "Tag", Encode.string "UnknownLocation" )
                ]


locationTag : Decode.Decoder String
locationTag =
    field "Tag" string


onPlanetDecoder : Decode.Decoder OnPlanetData
onPlanetDecoder =
    succeed OnPlanetData
        |> andMap (field "PlanetId" planetIdDecoder)
        |> andMap (field "StarSystemId" starSystemIdDecoder)
        |> andMap (field "PlanetName" planetNameDecoder)


onPlanetEncoder : OnPlanetData -> Encode.Value
onPlanetEncoder planet =
    Encode.object
        [ ( "PlanetId", planetIdEncoder planet.planetId )
        , ( "StarSystemId", starSystemIdEncoder planet.starSystemId )
        , ( "PlanetName", planetNameEncoder planet.planetName )
        ]


onUnitDecoder : Decode.Decoder OnUnitData
onUnitDecoder =
    succeed OnUnitData
        |> andMap (field "UnitId" unitIdDecoder)
        |> andMap (field "UnitName" unitNameDecoder)
        |> andMap (field "CrewPosition" (maybe crewPositionDecoder))


onUnitEncoder : OnUnitData -> Encode.Value
onUnitEncoder unit =
    Encode.object
        [ ( "UnitId", unitIdEncoder unit.unitId )
        , ( "UnitName", unitNameEncoder unit.unitName )
        , ( "CrewPosition", encodeMaybe crewPositionEncoder unit.position )
        ]


unitNameDecoder : Decode.Decoder UnitName
unitNameDecoder =
    succeed UnitName
        |> andMap string


unitNameEncoder : UnitName -> Encode.Value
unitNameEncoder (UnitName s) =
    Encode.string s


crewPositionDecoder : Decode.Decoder CrewPosition
crewPositionDecoder =
    string |> andThen stringToCrewPosition


stringToCrewPosition : String -> Decode.Decoder CrewPosition
stringToCrewPosition s =
    case s of
        "Commander" ->
            succeed Commander

        "Navigator" ->
            succeed Navigator

        "Signaler" ->
            succeed Signaler

        "SensorOperator" ->
            succeed SensorOperator

        "Gunner" ->
            succeed Gunner

        "Doctor" ->
            succeed Doctor

        "Nurse" ->
            succeed Nurse

        "Driver" ->
            succeed Driver

        "Helmsman" ->
            succeed Helmsman

        "Artificer" ->
            succeed Artificer

        "Crew" ->
            succeed Crew

        "Passenger" ->
            succeed Passenger

        _ ->
            fail "failed to deserialize"


crewPositionEncoder : CrewPosition -> Encode.Value
crewPositionEncoder pos =
    case pos of
        Commander ->
            Encode.string "Commander"

        Navigator ->
            Encode.string "Navigator"

        Signaler ->
            Encode.string "Signaler"

        SensorOperator ->
            Encode.string "SensorOperator"

        Gunner ->
            Encode.string "Gunner"

        Doctor ->
            Encode.string "Doctor"

        Nurse ->
            Encode.string "Nurse"

        Driver ->
            Encode.string "Driver"

        Helmsman ->
            Encode.string "Helmsman"

        Artificer ->
            Encode.string "Artificer"

        Crew ->
            Encode.string "Crew"

        Passenger ->
            Encode.string "Passenger"


lifeFocusDecoder : Decode.Decoder LifeFocus
lifeFocusDecoder =
    string |> andThen stringToLifeFocus


stringToLifeFocus : String -> Decode.Decoder LifeFocus
stringToLifeFocus s =
    case s of
        "RulershipFocus" ->
            succeed RulershipFocus

        "BusinessFocus" ->
            succeed BusinessFocus

        "SeductionFocus" ->
            succeed SeductionFocus

        "IntrigueFocus" ->
            succeed IntrigueFocus

        "HuntingFocus" ->
            succeed HuntingFocus

        "WarFocus" ->
            succeed WarFocus

        "CarousingFocus" ->
            succeed CarousingFocus

        "FamilyFocus" ->
            succeed FamilyFocus

        "ScholarshipFocus" ->
            succeed ScholarshipFocus

        "TheologyFocus" ->
            succeed TheologyFocus

        _ ->
            fail "failed to deserialize"


lifeFocusEncoder : LifeFocus -> Encode.Value
lifeFocusEncoder focus =
    case focus of
        RulershipFocus ->
            Encode.string "RulershipFocus"

        BusinessFocus ->
            Encode.string "BusinessFocus"

        SeductionFocus ->
            Encode.string "SeductionFocus"

        IntrigueFocus ->
            Encode.string "IntrigueFocus"

        HuntingFocus ->
            Encode.string "HuntingFocus"

        WarFocus ->
            Encode.string "WarFocus"

        CarousingFocus ->
            Encode.string "CarousingFocus"

        FamilyFocus ->
            Encode.string "FamilyFocus"

        ScholarshipFocus ->
            Encode.string "ScholarshipFocus"

        TheologyFocus ->
            Encode.string "TheologyFocus"


lifeFocusStatusDecoder : Decode.Decoder LifeFocusStatus
lifeFocusStatusDecoder =
    string |> andThen stringToLifeFocusStatus


stringToLifeFocusStatus : String -> Decode.Decoder LifeFocusStatus
stringToLifeFocusStatus s =
    case s of
        "CanChangeLifeFocus" ->
            succeed CanChangeLifeFocus

        "CanNotChangeLifeFocus" ->
            succeed CanNotChangeLifeFocus

        _ ->
            fail "Failed to deserialize data"


lifeFocusStatusEncoder : LifeFocusStatus -> Encode.Value
lifeFocusStatusEncoder status =
    case status of
        CanChangeLifeFocus ->
            Encode.string "CanChangeLifeFocus"

        CanNotChangeLifeFocus ->
            Encode.string "CanNotChangeLifeFocus"


type alias PatchPersonRequest =
    { lifeFocus : Maybe LifeFocus
    }


patchPersonRequestEncoder : PatchPersonRequest -> Encode.Value
patchPersonRequestEncoder request =
    Encode.object
        [ ( "LifeFocus", encodeMaybe lifeFocusEncoder request.lifeFocus )
        ]
