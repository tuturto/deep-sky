module Api.User exposing
    ( factionIdDecoder
    , factionIdEncoder
    , getAvatar
    , getUser
    , login
    , logout
    , passwordDecoder
    , passwordEncoder
    , postUser
    , putUser
    , userDecoder
    , userEncoder
    , userIdDecoder
    , userIdEncoder
    , userNameDecoder
    , userNameEncoder
    )

import Api.Common
    exposing
        ( encodeMaybe
        , get
        , post
        , put
        )
import Api.Endpoints exposing (Endpoint(..))
import Api.People exposing (personDecoder, personIdDecoder, personIdEncoder)
import Data.Common exposing (FactionId(..), PersonId(..), UserId(..))
import Data.Login exposing (LoginResponse)
import Data.Model exposing (Msg(..))
import Data.People exposing (Person)
import Data.User exposing (Password(..), User, UserName(..))
import Http exposing (Error(..))
import Json.Decode as Decode
    exposing
        ( field
        , int
        , maybe
        , string
        , succeed
        )
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import RemoteData exposing (RemoteData(..), WebData)
import SaveData exposing (SaveData(..))


getUser : (SaveData User -> Msg) -> Cmd Msg
getUser msg =
    Http.send (SaveData.fromResult >> msg) (get ApiUser userDecoder)


{-| Retrieve currently logged in user's avatar
-}
getAvatar : (SaveData (Maybe Person) -> Msg) -> Cmd Msg
getAvatar msg =
    Http.send (SaveData.fromResult >> msg) (get ApiAvatar (maybe personDecoder))


{-| Update currecntly logged in user's details
-}
putUser : User -> (SaveData User -> Msg) -> Cmd Msg
putUser user msg =
    Http.send (SaveData.fromResult >> msg) (put ApiUser (userEncoder user) userDecoder)


{-| Create a completely new user
-}
postUser : User -> (SaveData User -> Msg) -> Cmd Msg
postUser user msg =
    Http.send (SaveData.fromResult >> msg) (post ApiUser (userEncoder user) userDecoder)


login : String -> String -> (SaveData LoginResponse -> Msg) -> Cmd Msg
login user pwd msg =
    Http.send (SaveData.fromResult >> msg) (post ApiLogin (loginEncoder user pwd) loginDecoder)


logout : (WebData String -> Msg) -> Cmd Msg
logout msg =
    Http.send (RemoteData.fromResult >> msg) (post ApiLogout Encode.null logoutDecoder)


factionIdDecoder : Decode.Decoder FactionId
factionIdDecoder =
    succeed FactionId
        |> andMap int


factionIdEncoder : FactionId -> Encode.Value
factionIdEncoder (FactionId x) =
    Encode.int x


userNameDecoder : Decode.Decoder UserName
userNameDecoder =
    succeed UserName
        |> andMap string


userNameEncoder : UserName -> Encode.Value
userNameEncoder (UserName s) =
    Encode.string s


userIdDecoder : Decode.Decoder UserId
userIdDecoder =
    succeed UserId
        |> andMap int


userIdEncoder : UserId -> Encode.Value
userIdEncoder (UserId x) =
    Encode.int x


passwordDecoder : Decode.Decoder Password
passwordDecoder =
    succeed Password
        |> andMap string


passwordEncoder : Password -> Encode.Value
passwordEncoder (Password s) =
    Encode.string s


userDecoder : Decode.Decoder User
userDecoder =
    succeed User
        |> andMap (field "id" userIdDecoder)
        |> andMap (field "ident" userNameDecoder)
        |> andMap (field "password" (maybe passwordDecoder))
        |> andMap (field "avatar" (maybe personIdDecoder))


userEncoder : User -> Encode.Value
userEncoder user =
    Encode.object
        [ ( "id", userIdEncoder user.id )
        , ( "ident", userNameEncoder user.name )
        , ( "password", encodeMaybe passwordEncoder user.password )
        , ( "avatar", encodeMaybe personIdEncoder user.avatar )
        ]


loginDecoder : Decode.Decoder LoginResponse
loginDecoder =
    succeed LoginResponse
        |> andMap (field "User" userDecoder)
        |> andMap (field "Avatar" (maybe personDecoder))


loginEncoder : String -> String -> Encode.Value
loginEncoder username pwd =
    Encode.object
        [ ( "UserName", Encode.string username )
        , ( "Password", Encode.string pwd )
        ]


logoutDecoder : Decode.Decoder String
logoutDecoder =
    Decode.string
