module ViewModels.Person exposing
    ( LifeFocusModal
    , PersonRMsg(..)
    , PersonViewModel
    , init
    )

import Bootstrap.Modal as Modal
import Data.Common exposing (InfoPanelStatus(..))
import Data.People exposing (DemesneShortInfo, LifeFocus(..), Person)
import RemoteData exposing (RemoteData(..), WebData)
import SaveData exposing (SaveData(..))


type PersonRMsg
    = PersonDetailsReceived (SaveData Person)
    | DemesneReceived (WebData (List DemesneShortInfo))
    | PersonDetailsStatusChanged InfoPanelStatus
    | PersonDetailsRefreshRequested
    | StatsStatusChanged InfoPanelStatus
    | DemesneStatusChanged InfoPanelStatus
    | DemesneRefreshRequested
    | DemesnePageChanged Int
    | RelationsStatusChanged InfoPanelStatus
    | RelationsPageChanged Int
    | TraitsStatusChanged InfoPanelStatus
    | TraitsPageChanged Int
    | LifeFocusModalVisibilityChanged Modal.Visibility
    | LifeFocusChanged LifeFocus
    | LifeFocusEnter LifeFocus
    | LifeFocusConfirmed Person LifeFocus


type alias PersonViewModel =
    { person : SaveData Person
    , demesne : WebData (List DemesneShortInfo)
    , personDetailsStatus : InfoPanelStatus
    , statsStatus : InfoPanelStatus
    , demesneStatus : InfoPanelStatus
    , demesnePageSize : Int
    , demesneCurrentPage : Int
    , relationsStatus : InfoPanelStatus
    , relationsPageSize : Int
    , relationsCurrentPage : Int
    , traitsStatus : InfoPanelStatus
    , traitsPageSize : Int
    , traitsCurrentPage : Int
    , lifeFocusModal : LifeFocusModal
    }


type alias LifeFocusModal =
    { dialogStatus : Modal.Visibility
    , focus : Maybe LifeFocus
    , explanation : String
    }


init : PersonViewModel
init =
    { person = RData Loading
    , demesne = Loading
    , personDetailsStatus = InfoPanelOpen
    , statsStatus = InfoPanelOpen
    , demesneStatus = InfoPanelOpen
    , demesnePageSize = 10
    , demesneCurrentPage = 0
    , relationsStatus = InfoPanelOpen
    , relationsPageSize = 10
    , relationsCurrentPage = 0
    , traitsStatus = InfoPanelOpen
    , traitsPageSize = 10
    , traitsCurrentPage = 0
    , lifeFocusModal =
        { dialogStatus = Modal.hidden
        , focus = Nothing
        , explanation = ""
        }
    }
