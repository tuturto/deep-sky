module ViewModels.Admin.Status exposing
    ( AdminStatusRMsg(..)
    , AdminStatusViewModel
    , StatusAction(..)
    , init
    )

import Data.Admin exposing (Person, SimulationSummary)
import Data.Common exposing (PagedResult, PersonId)
import Dict exposing (Dict)
import RemoteData exposing (RemoteData(..), WebData)
import SaveData exposing (SaveData(..))


{-| Messages view model may emit
-}
type AdminStatusRMsg
    = WipeRequested
    | WipeConfirmed
    | WipeCancelled
    | DatabaseActionCompleted (SaveData SimulationSummary)
    | ReseedRequested
    | ReseedConfirmed
    | ReseedCancelled
    | BogusDialog
    | SummaryReceived (SaveData SimulationSummary)


{-| Current state of view model
-}
type alias AdminStatusViewModel =
    { action : Maybe StatusAction
    , summary : SaveData SimulationSummary
    }


type StatusAction
    = ConfirmingWipe
    | WipeInProgress
    | ConfirmingReseed
    | ReseedInProgress


{-| Create initial view model
-}
init : AdminStatusViewModel
init =
    { action = Nothing
    , summary = RData NotAsked
    }
