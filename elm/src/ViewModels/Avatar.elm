module ViewModels.Avatar exposing
    ( AvatarRMsg(..)
    , AvatarViewModel
    , QuerySettings
    , defaultQuery
    , init
    )

import Api.Endpoints exposing (NPCSearch(..))
import Bootstrap.Modal as Modal
import Data.Common exposing (InfoPanelStatus(..), PagedResult)
import Data.People exposing (Person)
import Data.User exposing (User)
import Dict exposing (Dict)
import RemoteData exposing (RemoteData(..), WebData)
import SaveData exposing (SaveData(..))


type alias AvatarViewModel =
    { availableAvatars : Dict Int (WebData (List Person))
    , avatarsPage : Int
    , avatarsPageSize : Int
    , avatarListStatus : InfoPanelStatus
    , avatar : SaveData Person
    , dialogStatus : Modal.Visibility
    }


type AvatarRMsg
    = AvailableAvatarsReceived (WebData (PagedResult Person))
    | AvatarListStatusChanged InfoPanelStatus
    | AvatarListUpdatedRequested
    | AvatarListPageChanged Int
    | AvatarSelected Person
    | AvatarConfirmed Person
    | AvatarCancelled
    | UserReceived (SaveData User)


init : AvatarViewModel
init =
    { availableAvatars =
        Dict.fromList
            [ ( 0, Loading )
            , ( 1, Loading )
            ]
    , avatarsPage = defaultQuery.avatarsPage
    , avatarsPageSize = defaultQuery.avatarsPageSize
    , avatarListStatus = InfoPanelOpen
    , avatar = RData NotAsked
    , dialogStatus = Modal.hidden
    }


type alias QuerySettings =
    { avatarsPage : Int
    , avatarsPageSize : Int
    , personType : NPCSearch
    }


defaultQuery : QuerySettings
defaultQuery =
    { avatarsPage = 0
    , avatarsPageSize = 10
    , personType = OnlyNPCs
    }
