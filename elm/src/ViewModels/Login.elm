module ViewModels.Login exposing
    ( LoginMode(..)
    , LoginRMsg(..)
    , LoginViewModel
    , init
    )

import RemoteData exposing (RemoteData(..))


type LoginRMsg
    = UserNameChanged String
    | PasswordChanged String
    | Password2Changed String
    | LoginRequested String String
    | NewUserRequested String String
    | ModeChanged LoginMode


type LoginMode
    = LogIn
    | CreateUser


type alias LoginViewModel =
    { userName : String
    , password : String
    , password2 : String
    , mode : LoginMode
    }


init : LoginViewModel
init =
    { userName = ""
    , password = ""
    , password2 = ""
    , mode = LogIn
    }
