---
layout: post
title:  "Circle of life"
date:   2021-11-27 05:52:00 +0300
categories: plans
---

I have been writing down some thought and ideas of how I would like to approach
one of the major parts of the game: life. Specifically I have been thinking
how people born, grow up, form a family and eventually die. It's lots to tackle
and I think will take quite a long time to have even the basic skeleton down.
Even figuring out where to start has proven to be somewhat a problem.

Beginning
---------
To be born, one needs parents, at least to a some degree. And they need to be
in somewhere same area for a chance of pregnancy to occur. As usual, there
probably is going to be some bonuses and maluses in action here. For example,
it's harder to get pregnant the older you are. And easier to get pregnant if
you're madly in love.

As interesting as it would be to model political intrigue and powerplay around
unborn heirs, I'm going to leave it to another time. Same with genes and
such. There's plans, but they are going to be implemented way, way later.

Early years
-----------
First two or three years are probably pretty boring for the player to play.
Babies don't really have agency in the grand scheme of things, so if there's
anything to do, it's in the form of special events.

Childhood
---------
At the age of three, most of the playing is still in form of special events
that shape childs personality, traits and skills. If such a young child is
ruler, there's going to be regency in place. Player can try to affect things,
giving orders and making demands, but they're still mostly in the mercy of
whims of their regent.

Guardians
---------
Around age of six starts the formal training. Child is assigned teacher, who is
in charge of their training. Choice of the teacher will affect on what kind of
special events will trigger in this phase and also affect on personality of the
child.

Adulthood
---------
Depending on the culture and local laws, around 16 to 21 years of age the
adulthood begins. This is where the person has full agency and can make their
own choices. They might stay in their parents' court (if nobility), workship
(if craftsmen) or similar, or they might decide to search for their fortune and
start adventuring.

This is also the phase where they might gain their own holdings and start to
rule over them.

Marriage
--------
Marriages can be either arranged by parents or sought out by the person itself.
Betrothal is powerful diplomatic tool, so especially in case of the nobels the
parents often arrange the marriage. In any case, when couple is married, a
question of where to live comes into play. It could be place of either one's
parents or either one's own holding. In extreme cases (both are equal level
rulers), they might choose to live in their respective capitols.

When marriage is formed, decision is made in whose dynasty the possible
children will be part of. This affects on future inheritance and succession and
thus is important part to decide.

The end
-------
Eventually the person will die. It might be of natural causes or it might be
a violent one. In any case, when person dies, their holdings, titles and
earthly possessions must be given to someone. This process is governed by
succession and inheritance laws. Primamy heir of the person will be the next
person player will take control of when their own character dies. So arranging
laws and succession to your advantage is big part of the game.

In closing
----------
List of things to design and implement is pretty long. In truth, it's quite
scary amount of work to do, but also very interesting. I probably will start
from the marriage part and continue from there, trying to build a very 
barebones skeleton that can be fleshed out later.
