---
layout: post
title:  "Welcome to Deep Sky"
date:   2021-07-01 20:43:00 +0300
categories: misc
---

Deep Sky is my little toy project, where I can experiment with various things
and see how they work. Backend is written Haskell, while frontend is written
in Elm. Premise of the game is simple: each player is a ruler of a faction and
they try to make sure their dynasty survives as long as possible. If one thinks
of Crusader Kings 2, but with Romans and set in space, they aren't far off.

I try to keep posting here little posts now and then about how the game
progresses and what I'm currently working. As of writing this, I have finally
finished implementing a little bit smoother start up. Administrators don't
have to run scripts manually, but everything can be managed from the within
the game.
