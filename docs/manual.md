---
layout: page
title: Manual
---

Deep Sky
========

Toy simulation by Tuula Turto

Contents
--------

* [How to play](/howtoplay)
  * [How to join a game](/joining)
  * [What are reports?](/reports)
  * [Viewing star systems](/starsystem)
  * [Managing planets](/planet)
  * [Examining people](/person)
  * [Doing research](/research)
  * [Designing vehicles](/designer)
  * [News archive](/messages)
* [How to administer game](/admin/admin)

Released under MIT license
