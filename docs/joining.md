---
layout: page
---

How to join a game
==================

Deep Sky is a multiplayer game, so first step is to find a server where you
can play. You can also set up your own server for trying out the game or
playing with your friends. For more information about how to set up a server,
have a look at [admin section](/admin/admin)

Creating an account and logging in
----------------------------------

When you first open the game on your browser, you're greeted with login page.

![Login page](/img/login.png)

Since you don't yet have an account, click the bottom left link that reads
"Sign up here" and a different page is displayed.

![Sign up page](/img/signup.png)

Here you can select your desired username and enter password of your choice
twice. After signing up, you're redirected back to login page where you can
log in.

Avatar selection
----------------

Every player has their own avatar, that is their in-game persona. After
logging in, you are presented with a list of available avatars. You can
browse them and select one you think is suitable for your. After confirming
the selection, you are ready to play.

![Avatar selection](/img/avatar.png)
