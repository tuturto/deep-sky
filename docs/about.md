---
layout: page
title: About
permalink: /about/
---

I'm Tuula Turto and this little site is my development blog and living manual
for Deep Sky. Deep Sky isn't supposed to be a particularly serious project.
Mostly it exists to serve as a project, where I can experiment with various
things and try out how they work.