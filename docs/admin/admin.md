---
layout: page
---

Game mastering
==============

One or more people are needed for administering the game. Common tasks include
things like setting up the game, configuring various options and fine tuning
the simulation. For example, sometimes human is required to step in and create
a star system or two somewhere for new players to have a place to start out.

**Note:** The first player ever to create an account on a freshly installed
server is automatically promoted to an administrator.

After initial setup these tasks can be performed in admin interface, that is
available only for logged in user which special privileges.

Admin interface is divided into separate views that each handle a specific
group of tasks. These tasks are:

* [Simulation status](/admin/status)
* [People](/admin/people)
