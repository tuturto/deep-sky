# Changelog
Notable changes to the project are documented here. Latest ones are on top.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- `apiNotImplemented` for returning http 501
- People can now have life focus[32]
### Changed
- Cleaned up warnings from server side code
- [Server side now uses lenses everywhere][27]
- [`ErrorCode` has been broken to smaller types][31]
### Removed
### Fixed
### Known bugs
- [Disallow dismissing special events][1]
- [Directly opening a subpage doesn't run init][24]

## [0.3.0] - 2021-07-15
### Added
- `SystemStatus` and related checks for API endpoints
- People section for admin panel
- `Creators.Person` module for generating new people
- interface for accessing unit info
- units report their status to faction of owner
- Robot Framework and Selenium taken into use for testing
- `pushUrl` function for type safe url manipulation
- `SaveData` to track when data is being saved
- generic tab control on the client side
- Admin functionality to wipe and reseed the database
- `Count a` for representing counts on client side
- `Validate` module on server side for common validation checks
- Account creation and avatar selection
- [Common error handling][9]
- [Create static website for manual and maybe sort of blog][23]
- [Saving a design now indicates when it was completed][25]
- [Client side state handling restructured][20]
### Changed
- Admin interface has been rewritten in Elm
- `Age` changed from `Int` to `Natural`
- `Vehicles` module renamed to `Units`
- url of planet changed from `/starsystem/sId/pId` to `planet/pId`
- type of design name changed from `Text` to `DesignName`
- `PersonName` and related types and functions moved from `Data.Person` to `Data.PersonName` on client
- Data loaded from server is now handled with `RemoteData` library
- `intrique` changed to `intrigue`
- All login systems were replaced with username + password
- [Login / Logout now use new system with nicer layout][19]
### Removed
### Fixed
- [Old planet details are shown][6]
- [Stats of generated person should follow normal distribution][12]
- [No people are shown in admin view][13]
- [Submitting user written news fails][14]
### Known bugs
- [Disallow dismissing special events][1]
- [Directly opening a subpage doesn't run init][24]

## [0.2.0] - 2019-10-19
### Added
- Players have avatar that they're playing as
- `Person` to represent people in detailed way
- `HumanIntelligence` to represent level of person intel
- `Relation` to represent relations between people
- `Dynasty` to represent dynasties
- `Marriage` to represent engagements and marriages
- `Errors` module for common error codes and returning them to client
- Started using `Data.Either.Validation` for message validation
- People can have pets
- Added immediate events that are resolved as soon as choice has been made
- Added `Markov` module for working with markov chains
- Added `Names` module for generating names with markov chains
- Added `Data.Aeson.Lens` as a dependency
- `PlanetName`, `StarName` and `StarSystemName` taken into use
- People can be in different planets or assigned to a unit
- Designs have crew requirements displayed
### Changed
- `News` can now be targeted to faction or specific person
- planet can have ruler
- planet report JSON result has all keys starting with uppercase
- star system can have ruler
- Database table `time` replaced with `simulation`
- `StarDate` used instead of `Time` everywhere
- Level of component changed from `Int` to `ComponentLevel`
- Amount of components changed from `Int` to `ComponentAmount`
- Component name changed from `String` to `ComponentName`
- Component description changed from `String` to `ComponentDescription`
- Players are not directly members of faction, but via their avatar
- `requireFaction` returns also user avatar
- `apiRequireFaction` returns also user avatar
- User submitted news have player avatar as originator
- Faction resources changed from `Int` to `RawResource a`
- Faction name changed from `Text` to `FactionName`
- `SpectralType`, `LuminosityClass`, `PlanetaryStatus` and `Coordinates` moved to `Space.Data`
- Designs must alloce enough living quarters for their crews
### Removed
- `maybeFaction` function removed
### Fixed
- [Empty construction queue on a planet causes error during simulation][4]
- [Remove widget files][5]
- [Designer shows only 6 components][10]
### Known bugs
- [Disallow dismissing special events][1]
- [Old planet details are shown][6]

## [0.1.0] - 2019-05-11
### Added
- List of known star systems
- Details of kown star system
- Details of known planets
- Initial system for resource production on planets
- System for constructing buildings on planets
- System for performing research
- List of received messages
- Users may submit their own news
- Special events system
- [User manual](https://tuturto.github.io/deep-sky/)
- Vehicle designer
### Known bugs
- [Disallow dismissing special events][1]

[Unreleased]: https://codeberg.org/tuturto/deep-sky/compare/0.3.0...HEAD

[0.1.0]: https://codeberg.org/tuturto/deep-sky/releases/tag/0.1.0
[0.2.0]: https://codeberg.org/tuturto/deep-sky/releases/tag/0.2.0
[0.3.0]: https://codeberg.org/tuturto/deep-sky/releases/tag/0.3.0

[1]: https://codeberg.org/tuturto/deep-sky/issues/1
[4]: https://codeberg.org/tuturto/deep-sky/issues/4
[5]: https://codeberg.org/tuturto/deep-sky/issues/5
[6]: https://codeberg.org/tuturto/deep-sky/issues/6
[9]: https://codeberg.org/tuturto/deep-sky/issues/9
[10]: https://codeberg.org/tuturto/deep-sky/issues/10
[12]: https://codeberg.org/tuturto/deep-sky/issues/12
[13]: https://codeberg.org/tuturto/deep-sky/issues/13
[14]: https://codeberg.org/tuturto/deep-sky/issues/14
[19]: https://codeberg.org/tuturto/deep-sky/issues/19
[20]: https://codeberg.org/tuturto/deep-sky/issues/20
[23]: https://codeberg.org/tuturto/deep-sky/issues/23
[24]: https://codeberg.org/tuturto/deep-sky/issues/24
[25]: https://codeberg.org/tuturto/deep-sky/issues/25
[27]: https://codeberg.org/tuturto/deep-sky/issues/27
[31]: https://codeberg.org/tuturto/deep-sky/issues/31
[32]: https://codeberg.org/tuturto/deep-sky/issues/32
