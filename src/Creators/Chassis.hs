{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}


module Creators.Chassis ( chassisDefinitions, bawleyChassis, bawleyComponents
                        , bilanderChassis, bilanderComponents, yawlChassis
                        , yawlComponents, cogChassis, cogComponents
                        , freighterChassis, freighterComponents, craneShipChassis
                        , craneShipComponents, cruiseLinerChassis
                        , cruiseLinerComponents, satelliteLayerChassis
                        , satelliteLayerComponents, flyboatChassis
                        , flyboatComponents, brigantineChassis
                        , brigantineComponents, schoonerChassis
                        , schoonerComponents, blackwallFrigateChassis
                        , blackwallFrigateComponents, clipperChassis
                        , clipperComponents, caravelChassis, caravelComponents
                        , corvetteChassis, corvetteComponents, frigateChassis
                        , frigateComponents, galleonChassis, galleonComponents
                        , manowarChassis, manowarComponents, suvChassis
                        , suvComponents
                        )
    where

import Import

import Research.Data ( Technology(..) )
import Units.Data ( CrewSpaceReq(..) )
import Units.Components ( ChassisType(..), ComponentAmount(..), ComponentLevel(..)
                        , ComponentType(..) )


chassisDefinitions :: [(Chassis, (Key Chassis) -> [RequiredComponent])]
chassisDefinitions =
    [ (bawleyChassis, bawleyComponents)
    , (bilanderChassis, bilanderComponents)
    , (yawlChassis, yawlComponents)
    , (cogChassis, cogComponents)
    , (freighterChassis, freighterComponents)
    , (craneShipChassis, craneShipComponents)
    , (cruiseLinerChassis, cruiseLinerComponents)
    , (satelliteLayerChassis, satelliteLayerComponents)
    , (flyboatChassis, flyboatComponents)
    , (brigantineChassis, brigantineComponents)
    , (schoonerChassis, schoonerComponents)
    , (blackwallFrigateChassis, blackwallFrigateComponents)
    , (clipperChassis, clipperComponents)
    , (caravelChassis, caravelComponents)
    , (corvetteChassis, corvetteComponents)
    , (frigateChassis, frigateComponents)
    , (galleonChassis, galleonComponents)
    , (manowarChassis, manowarComponents)
    , (suvChassis, suvComponents)
    ]


bawleyChassis :: Chassis
bawleyChassis =
    Chassis
        { _chassisName = "Bawley"
        , _chassisTonnage = 100
        , _chassisType = SpaceShip
        , _chassisTechnology = Just BawleyHulls
        , _chassisArmourSlots = 2
        , _chassisInnerSlots = 4
        , _chassisOuterSlots = 4
        , _chassisSensorSlots = 2
        , _chassisWeaponSlots = 2
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 2
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


bawleyComponents :: Key Chassis -> [RequiredComponent]
bawleyComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


yawlChassis :: Chassis
yawlChassis =
    Chassis
        { _chassisName = "Yawl"
        , _chassisTonnage = 120
        , _chassisType = SpaceShip
        , _chassisTechnology = Just YawlHulls
        , _chassisArmourSlots = 2
        , _chassisInnerSlots = 6
        , _chassisOuterSlots = 6
        , _chassisSensorSlots = 2
        , _chassisWeaponSlots = 2
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 2
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


yawlComponents :: Key Chassis -> [RequiredComponent]
yawlComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


bilanderChassis :: Chassis
bilanderChassis =
    Chassis
        { _chassisName = "Bilander"
        , _chassisTonnage = 400
        , _chassisType = SpaceShip
        , _chassisTechnology = Just BilanderHulls
        , _chassisArmourSlots = 3
        , _chassisInnerSlots = 8
        , _chassisOuterSlots = 6
        , _chassisSensorSlots = 2
        , _chassisWeaponSlots = 4
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 2
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


bilanderComponents :: Key Chassis -> [RequiredComponent]
bilanderComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


cogChassis :: Chassis
cogChassis =
    Chassis
        { _chassisName = "Cog"
        , _chassisTonnage = 600
        , _chassisType = SpaceShip
        , _chassisTechnology = Just CogHulls
        , _chassisArmourSlots = 4
        , _chassisInnerSlots = 10
        , _chassisOuterSlots = 8
        , _chassisSensorSlots = 2
        , _chassisWeaponSlots = 4
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 2
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


cogComponents :: Key Chassis -> [RequiredComponent]
cogComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


freighterChassis :: Chassis
freighterChassis =
    Chassis
        { _chassisName = "Freighter"
        , _chassisTonnage = 2000
        , _chassisType = SpaceShip
        , _chassisTechnology = Just FreighterHulls
        , _chassisArmourSlots = 8
        , _chassisInnerSlots = 20
        , _chassisOuterSlots = 16
        , _chassisSensorSlots = 3
        , _chassisWeaponSlots = 6
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 4
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


freighterComponents :: Key Chassis -> [RequiredComponent]
freighterComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


craneShipChassis :: Chassis
craneShipChassis =
    Chassis
        { _chassisName = "Crane ship"
        , _chassisTonnage = 800
        , _chassisType = SpaceShip
        , _chassisTechnology = Just CraneShipHulls
        , _chassisArmourSlots = 2
        , _chassisInnerSlots = 8
        , _chassisOuterSlots = 16
        , _chassisSensorSlots = 3
        , _chassisWeaponSlots = 1
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 2
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


craneShipComponents :: Key Chassis -> [RequiredComponent]
craneShipComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


cruiseLinerChassis :: Chassis
cruiseLinerChassis =
    Chassis
        { _chassisName = "Cruise liner"
        , _chassisTonnage = 2000
        , _chassisType = SpaceShip
        , _chassisTechnology = Just CruiseLinerHulls
        , _chassisArmourSlots = 4
        , _chassisInnerSlots = 40
        , _chassisOuterSlots = 40
        , _chassisSensorSlots = 4
        , _chassisWeaponSlots = 0
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 4
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


cruiseLinerComponents :: Key Chassis -> [RequiredComponent]
cruiseLinerComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 4)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 2)
    ]


satelliteLayerChassis :: Chassis
satelliteLayerChassis =
    Chassis
        { _chassisName = "Satellite layer"
        , _chassisTonnage = 400
        , _chassisType = SpaceShip
        , _chassisTechnology = Just SatelliteLayerHulls
        , _chassisArmourSlots = 2
        , _chassisInnerSlots = 20
        , _chassisOuterSlots = 10
        , _chassisSensorSlots = 4
        , _chassisWeaponSlots = 1
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 2
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


satelliteLayerComponents :: Key Chassis -> [RequiredComponent]
satelliteLayerComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


flyboatChassis :: Chassis
flyboatChassis =
    Chassis
        { _chassisName = "Flyboat"
        , _chassisTonnage = 75
        , _chassisType = SpaceShip
        , _chassisTechnology = Just FlyboatHulls
        , _chassisArmourSlots = 1
        , _chassisInnerSlots = 5
        , _chassisOuterSlots = 2
        , _chassisSensorSlots = 2
        , _chassisWeaponSlots = 1
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 2
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


flyboatComponents :: Key Chassis -> [RequiredComponent]
flyboatComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


brigantineChassis :: Chassis
brigantineChassis =
    Chassis
        { _chassisName = "Brigantine"
        , _chassisTonnage = 100
        , _chassisType = SpaceShip
        , _chassisTechnology = Just BrigantineHulls
        , _chassisArmourSlots = 2
        , _chassisInnerSlots = 7
        , _chassisOuterSlots = 4
        , _chassisSensorSlots = 2
        , _chassisWeaponSlots = 1
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 2
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


brigantineComponents :: Key Chassis -> [RequiredComponent]
brigantineComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


schoonerChassis :: Chassis
schoonerChassis =
    Chassis
        { _chassisName = "Schooner"
        , _chassisTonnage = 400
        , _chassisType = SpaceShip
        , _chassisTechnology = Just SchoonerHulls
        , _chassisArmourSlots = 4
        , _chassisInnerSlots = 15
        , _chassisOuterSlots = 10
        , _chassisSensorSlots = 3
        , _chassisWeaponSlots = 2
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 5
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


schoonerComponents :: Key Chassis -> [RequiredComponent]
schoonerComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


blackwallFrigateChassis :: Chassis
blackwallFrigateChassis =
    Chassis
        { _chassisName = "Blackwall frigate"
        , _chassisTonnage = 600
        , _chassisType = SpaceShip
        , _chassisTechnology = Just BlackwallFrigateHulls
        , _chassisArmourSlots = 4
        , _chassisInnerSlots = 20
        , _chassisOuterSlots = 15
        , _chassisSensorSlots = 3
        , _chassisWeaponSlots = 3
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 3
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


blackwallFrigateComponents :: Key Chassis -> [RequiredComponent]
blackwallFrigateComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 3)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


clipperChassis :: Chassis
clipperChassis =
    Chassis
        { _chassisName = "Clipper"
        , _chassisTonnage = 700
        , _chassisType = SpaceShip
        , _chassisTechnology = Just ClipperHulls
        , _chassisArmourSlots = 4
        , _chassisInnerSlots = 20
        , _chassisOuterSlots = 15
        , _chassisSensorSlots = 3
        , _chassisWeaponSlots = 3
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 5
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


clipperComponents :: Key Chassis -> [RequiredComponent]
clipperComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 4)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


caravelChassis :: Chassis
caravelChassis =
    Chassis
        { _chassisName = "Caravel"
        , _chassisTonnage = 100
        , _chassisType = SpaceShip
        , _chassisTechnology = Just CaravelHulls
        , _chassisArmourSlots = 1
        , _chassisInnerSlots = 5
        , _chassisOuterSlots = 5
        , _chassisSensorSlots = 3
        , _chassisWeaponSlots = 4
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 3
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


caravelComponents :: Key Chassis -> [RequiredComponent]
caravelComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


corvetteChassis :: Chassis
corvetteChassis =
    Chassis
        { _chassisName = "Corvette"
        , _chassisTonnage = 250
        , _chassisType = SpaceShip
        , _chassisTechnology = Just CorvetteHulls
        , _chassisArmourSlots = 3
        , _chassisInnerSlots = 6
        , _chassisOuterSlots = 6
        , _chassisSensorSlots = 3
        , _chassisWeaponSlots = 8
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 3
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


corvetteComponents :: Key Chassis -> [RequiredComponent]
corvetteComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


frigateChassis :: Chassis
frigateChassis =
    Chassis
        { _chassisName = "Frigate"
        , _chassisTonnage = 500
        , _chassisType = SpaceShip
        , _chassisTechnology = Just FrigateHulls
        , _chassisArmourSlots = 6
        , _chassisInnerSlots = 10
        , _chassisOuterSlots = 10
        , _chassisSensorSlots = 4
        , _chassisWeaponSlots = 16
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 3
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


frigateComponents :: Key Chassis -> [RequiredComponent]
frigateComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 3)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


galleonChassis :: Chassis
galleonChassis =
    Chassis
        { _chassisName = "Galleon"
        , _chassisTonnage = 1000
        , _chassisType = SpaceShip
        , _chassisTechnology = Just GalleonHulls
        , _chassisArmourSlots = 12
        , _chassisInnerSlots = 15
        , _chassisOuterSlots = 15
        , _chassisSensorSlots = 4
        , _chassisWeaponSlots = 32
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 4
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


galleonComponents :: Key Chassis -> [RequiredComponent]
galleonComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 3)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


manowarChassis :: Chassis
manowarChassis =
    Chassis
        { _chassisName = "Man-of-war"
        , _chassisTonnage = 1500
        , _chassisType = SpaceShip
        , _chassisTechnology = Just ManOfWarHulls
        , _chassisArmourSlots = 20
        , _chassisInnerSlots = 20
        , _chassisOuterSlots = 20
        , _chassisSensorSlots = 4
        , _chassisWeaponSlots = 64
        , _chassisEngineSlots = 0
        , _chassisMotiveSlots = 0
        , _chassisSailSlots = 5
        , _chassisCrewSpaceRequired = CrewSpaceRequired
        }


manowarComponents :: Key Chassis -> [RequiredComponent]
manowarComponents cId =
    [ RequiredComponent cId BridgeComponent (MkComponentLevel 1) (MkComponentAmount 1)
    , RequiredComponent cId StarSailComponent (MkComponentLevel 1) (MkComponentAmount 4)
    , RequiredComponent cId SensorComponent (MkComponentLevel 1) (MkComponentAmount 2)
    , RequiredComponent cId SupplyComponent (MkComponentLevel 1) (MkComponentAmount 1)
    ]


suvChassis :: Chassis
suvChassis =
    Chassis
        { _chassisName = "SUV"
        , _chassisTonnage = 5
        , _chassisType = LandVehicle
        , _chassisTechnology = Nothing
        , _chassisArmourSlots = 0
        , _chassisInnerSlots = 2
        , _chassisOuterSlots = 1
        , _chassisSensorSlots = 1
        , _chassisWeaponSlots = 0
        , _chassisEngineSlots = 1
        , _chassisMotiveSlots = 1
        , _chassisSailSlots = 0
        , _chassisCrewSpaceRequired = CrewSpaceOptional
        }


suvComponents :: Key Chassis -> [RequiredComponent]
suvComponents cId =
    [ RequiredComponent cId MotiveComponent (MkComponentLevel 1) (MkComponentAmount 1) ]
