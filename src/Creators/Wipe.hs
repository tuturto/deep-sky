{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE FlexibleContexts           #-}


module Creators.Wipe ( wipeDatabase, reseedDatabase )
    where

import Import
import Control.Lens ( (^.), (&), to, traversed, (.~) )
import Control.Monad.Fail
import qualified Database.Esqueleto as E

import Common ( dayZero )
import Creators.Chassis ( chassisDefinitions )
import CustomTypes ( StarDate(..), SystemStatus(..), Role(..), UserIdentity(..) )
import People.Data ( Gender(..), PersonName(..), Sex(..), Cognomen(..)
                   , RegnalNumber(..), RelationType(..), RelationVisibility(..)
                   , PersonIntel(..), OpinionIntel(..), TraitType(..)
                   , MarriageStatus(..)
                   )
import Space.Data ( SpectralType(..), LuminosityClass(..) )

import Creators.Person ( generatePersonM, PersonOptions(..), AgeOptions(..) )
import Control.Monad.Random ( evalRandIO )

-- | Wipe database completely
wipeDatabase :: (BaseBackend backend ~ SqlBackend
    , BackendCompatible SqlBackend backend, PersistStoreWrite backend
    , PersistQueryRead backend, PersistQueryWrite backend, PersistUniqueWrite backend
    , PersistUniqueRead backend, MonadIO m) => ReaderT backend m ()
wipeDatabase = do
    _ <- deleteWhere ([] :: [Filter ShipConstruction])
    _ <- deleteWhere ([] :: [Filter BuildingConstruction])
    _ <- deleteWhere ([] :: [Filter News])
    _ <- deleteWhere ([] :: [Filter CurrentResearch])
    _ <- deleteWhere ([] :: [Filter AvailableResearch])
    _ <- deleteWhere ([] :: [Filter CompletedResearch])
    _ <- deleteWhere ([] :: [Filter StarSystemReport])
    _ <- deleteWhere ([] :: [Filter StarReport])
    _ <- deleteWhere ([] :: [Filter PlanetReport])
    _ <- deleteWhere ([] :: [Filter StarLaneReport])
    _ <- deleteWhere ([] :: [Filter BuildingReport])
    _ <- deleteWhere ([] :: [Filter PlanetPopulationReport])
    _ <- deleteWhere ([] :: [Filter PlanetStatusReport])
    _ <- deleteWhere ([] :: [Filter HumanIntelligence])
    _ <- deleteWhere ([] :: [Filter Relation])
    _ <- deleteWhere ([] :: [Filter Marriage])
    _ <- deleteWhere ([] :: [Filter PersonTrait])
    _ <- deleteWhere ([] :: [Filter Pet])
    _ <- deleteWhere ([] :: [Filter PersonLocation])
    _ <- deleteWhere ([] :: [Filter PersonOnUnit])
    _ <- deleteWhere ([] :: [Filter PersonOnPlanet])
    _ <- updateWhere [] [ UserAvatar =. Nothing ]
    _ <- updateWhere [] [ PersonDynastyId =. Nothing ]
    _ <- deleteWhere ([] :: [Filter Dynasty])
    _ <- deleteWhere ([] :: [Filter Star])
    _ <- deleteWhere ([] :: [Filter PlanetPopulation])
    _ <- deleteWhere ([] :: [Filter PlanetStatus])
    _ <- deleteWhere ([] :: [Filter Building])
    _ <- deleteWhere ([] :: [Filter StarLane])
    _ <- deleteWhere ([] :: [Filter UnitObservation])
    _ <- deleteWhere ([] :: [Filter UnitStatsReport])
    _ <- deleteWhere ([] :: [Filter DesignStatsReport])
    _ <- deleteWhere ([] :: [Filter CrewAssignment])
    _ <- deleteWhere ([] :: [Filter UnitCommand])
    _ <- deleteWhere ([] :: [Filter Formation])
    _ <- deleteWhere ([] :: [Filter PlannedComponent])
    _ <- deleteWhere ([] :: [Filter RequiredComponent])
    _ <- deleteWhere ([] :: [Filter InstalledComponent])
    _ <- deleteWhere ([] :: [Filter Unit])
    _ <- deleteWhere ([] :: [Filter Ship])
    _ <- deleteWhere ([] :: [Filter Vehicle])
    _ <- deleteWhere ([] :: [Filter Design])
    _ <- deleteWhere ([] :: [Filter Chassis])
    _ <- updateWhere [] [ PersonFactionId =. Nothing ]
    _ <- updateWhere [] [ PlanetOwnerId =. Nothing ]
    _ <- deleteWhere ([] :: [Filter Faction])

    _ <- updateWhere [] [ PersonPlanetTitle =. Nothing ]
    _ <- updateWhere [] [ PersonStarSystemTitle =. Nothing ]

    _ <- deleteWhere ([] :: [Filter Planet])
    _ <- deleteWhere ([] :: [Filter StarSystem])
    _ <- deleteWhere ([] :: [Filter Person])

    admins <- E.select $
            E.from $ \(user `E.LeftOuterJoin` userRole) -> do
                E.on (userRole E.?. UserRoleUserId E.==. E.just (user E.^. UserId))
                E.where_ (userRole E.?. UserRoleRole E.==. E.just (E.val RoleAdministrator))
                return user

    _ <- deleteWhere ([] :: [Filter Race])

    _ <- deleteWhere ([] :: [Filter Simulation])
    _ <- insert $ Simulation
            { _simulationCurrentTime = dayZero
            , _simulationStatus = Offline
            }

    _ <- createAdminAvatars admins
    return ()


-- | Wipe database completely and then create initial state
reseedDatabase :: (BaseBackend backend ~ SqlBackend
    , BackendCompatible SqlBackend backend, PersistStoreWrite backend
    , PersistUniqueWrite backend
    , PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m, MonadFail m) => ReaderT backend m ()
reseedDatabase = do
    _ <- wipeDatabase
    _ <- reseedStarSystems
    _ <- reseedFactions
    _ <- reseedPeople
    _ <- reseedChassis

    return ()


reseedStarSystems :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m, MonadFail m) => ReaderT backend m ()
reseedStarSystems = do
    _ <- reseedSol
    _ <- reseedAclael
    _ <- reseedBarynth
    _ <- reseedStarLanes

    return ()


reseedSol :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m) => ReaderT backend m ()
reseedSol = do
    solId <- insert $ StarSystem "Sol" 0 0 Nothing
    _ <- insert $ Star "Sun" solId G V

    _ <- insert $ Planet "Mercury" 1 solId Nothing 0.38  Nothing
    _ <- insert $ Planet "Venus"   2 solId Nothing 0.904 Nothing
    _ <- insert $ Planet "Earth"   3 solId Nothing 1.0   Nothing
    _ <- insert $ Planet "Mars"    4 solId Nothing 0.376 Nothing
    _ <- insert $ Planet "Jupiter" 6 solId Nothing 2.528 Nothing
    _ <- insert $ Planet "Saturn"  7 solId Nothing 1.065 Nothing
    _ <- insert $ Planet "Uranus"  8 solId Nothing 0.886 Nothing
    _ <- insert $ Planet "Neptune" 9 solId Nothing 1.14  Nothing

    return ()


reseedAclael :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m) => ReaderT backend m ()
reseedAclael = do
    sId <- insert $ StarSystem "Aclael" 4.2 (-1.2) Nothing
    _ <- insert $ Star "Aclael alpha" sId A II
    _ <- insert $ Star "Aclael beta"  sId M VI

    _ <- insert $ Planet "Aclael I" 1 sId Nothing 0.9 Nothing

    return ()


reseedBarynth :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m) => ReaderT backend m ()
reseedBarynth = do
    sId <- insert $ StarSystem "Barynth" 5.6 2.2 Nothing
    _ <- insert $ Star "Barynth alpha" sId L VI

    return ()


reseedStarLanes :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m, MonadFail m) => ReaderT backend m ()
reseedStarLanes = do
    solE:_ <- selectList [ StarSystemName ==. "Sol" ] []
    aclaelE:_ <- selectList [ StarSystemName ==. "Aclael" ] []
    barynthE:_ <- selectList [ StarSystemName ==. "Barynth" ] []

    _ <- insert $ StarLane (entityKey solE) (entityKey aclaelE)
    _ <- insert $ StarLane (entityKey aclaelE) (entityKey barynthE)

    return ()


reseedChassis :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m) => ReaderT backend m [Key Chassis]
reseedChassis =
    mapM (\(chassis, components) ->
                do
                    cId <- insert chassis
                    _ <- mapM insert $ components cId
                    return cId)
         chassisDefinitions


reseedFactions :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m, MonadFail m) => ReaderT backend m ()
reseedFactions = do
    solE:_ <- selectList [ StarSystemName ==. "Sol" ] []
    _ <- insert $ Faction "Terrans" (entityKey solE) 1500 1500 1500

    barynthE:_ <- selectList [ StarSystemName ==. "Barynth" ] []
    _ <- insert $ Faction "Republic of Aclael" (entityKey barynthE) 1500 1500 1500

    return ()

reseedPeople :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m, MonadFail m) => ReaderT backend m ()
reseedPeople = do
    _ <- reseedTalvela

    return ()


-- | Talvela are the current ruling family in Terra. Their faction is Terrans.
reseedTalvela :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m, MonadFail m) => ReaderT backend m ()
reseedTalvela = do
    terransE:_ <- selectList [ FactionName ==. "Terrans" ] []
    solE:_ <- selectList [ StarSystemName ==. "Sol" ] []
    earthE:_ <- selectList [ PlanetName ==. "Earth" ] []

    auroraId <- insert $ Person (RegalName "Aurora" "Talvela" 12 $ Just "the Just")
                                Male Woman (MkStarDate 19792)
                                14 11 9 11 12
                                (Just $ entityKey terransE)
                                (Just $ entityKey earthE)
                                (Just $ entityKey solE)
                                Nothing
                                Nothing Nothing

    houseId <- insert $ Dynasty "House Talvela" auroraId -- TODO: a bit more history here

    _ <- createOwnIntel auroraId
    _ <- updateWhere [ PlanetName ==. "Earth" ] [ PlanetRulerId =. Just auroraId ]
    _ <- updateWhere [ StarSystemName ==. "Sol" ] [ StarSystemRulerId =. Just auroraId ]
    _ <- updateWhere [ PersonId ==. auroraId] [ PersonDynastyId =. Just houseId ]
    _ <- insert $ PersonTrait auroraId Nothing Temperate
    _ <- insert $ PersonTrait auroraId Nothing Diligent
    _ <- insert $ PersonTrait auroraId Nothing Patient

    marsE:_ <- selectList [ PlanetName ==. "Mars" ] []

    lauraId <- insert $ Person (RegularName "Laura" "Talvela" Nothing)
                               Female Woman (MkStarDate 19816)
                               10 14 9 9 10
                               (Just $ entityKey terransE)
                               (Just $ entityKey marsE)
                               Nothing
                               (Just houseId)
                               Nothing Nothing
    _ <- createOwnIntel lauraId
    _ <- updateWhere [ PlanetName ==. "Mars" ] [ PlanetRulerId =. Just lauraId ]
    _ <- updateWhere [ PlanetName ==. "Venus" ] [ PlanetRulerId =. Just lauraId ]

    talvikkiId <- insert $ Person (RegularName "Talvikki" "Talvela" Nothing)
                                    Female Woman (MkStarDate 20062)
                                    8 8 7 8 7
                                    (Just $ entityKey terransE)
                                    Nothing
                                    Nothing
                                    (Just houseId)
                                    Nothing Nothing
    _ <- createOwnIntel talvikkiId

    _ <- partOfFamily auroraId lauraId
    _ <- partOfFamily auroraId talvikkiId
    _ <- partOfFamily lauraId auroraId
    _ <- partOfFamily lauraId talvikkiId
    _ <- partOfFamily talvikkiId lauraId
    _ <- partOfFamily talvikkiId auroraId

    _ <- insert $ Marriage auroraId lauraId auroraId Married (MkStarDate 19761) Nothing
    _ <- twoWayRelation auroraId lauraId Spouse PublicRelation
    _ <- twoWayRelation auroraId lauraId Lover PublicRelation
    
    _ <- insert $ Relation auroraId talvikkiId Parent PublicRelation
    _ <- insert $ Relation lauraId talvikkiId Parent PublicRelation
    _ <- insert $ Relation talvikkiId auroraId Child PublicRelation
    _ <- insert $ Relation talvikkiId lauraId Child PublicRelation

    _ <- reseedTalvelaCourt $ entityKey terransE
    _ <- reseedTalvelaReports $ entityKey terransE

    return ()


twoWayRelation :: (MonadIO m, PersistStoreWrite backend,
    BaseBackend backend ~ SqlBackend) =>
    Key Person -> Key Person -> RelationType -> RelationVisibility -> ReaderT backend m ()
twoWayRelation pId tId r pub = do
    _ <- insert $ Relation pId tId r pub
    _ <- insert $ Relation tId pId r pub
    return ()


partOfFamily :: (MonadIO m, PersistStoreWrite backend,
    BaseBackend backend ~ SqlBackend) =>
    Key Person -> Key Person -> ReaderT backend m ()
partOfFamily pId tId = do
    _ <- insert $ HumanIntelligence pId tId Stats
    _ <- insert $ HumanIntelligence pId tId Demesne
    _ <- insert $ HumanIntelligence pId tId FamilyRelations
    _ <- insert $ HumanIntelligence pId tId SecretRelations
    _ <- insert $ HumanIntelligence pId tId (Opinions $ DetailedOpinions PublicRelation)
    _ <- insert $ HumanIntelligence pId tId (Opinions $ DetailedOpinions FamilyRelation)
    _ <- insert $ HumanIntelligence pId tId (Opinions $ DetailedOpinions SecretRelation)
    _ <- insert $ HumanIntelligence pId tId Traits
    _ <- insert $ HumanIntelligence pId tId Location
    _ <- insert $ HumanIntelligence pId tId Activity
    return ()


reseedTalvelaReports :: (PersistQueryRead backend, MonadIO m, MonadFail m,
    PersistStoreWrite backend, BaseBackend backend ~ SqlBackend) =>
    FactionId -> ReaderT backend m ()
reseedTalvelaReports fId = do
    planetEs <- selectList [ PlanetName <-. ["Venus" , "Earth", "Mars", "Jupiter", "Saturn" ]] []
    sunE:_ <- selectList [ StarName ==. "Sun" ] []
    solE:_ <- selectList [ StarSystemName ==. "Sol" ] []

    let planetReports = fullPlanetReport fId solE <$> planetEs

    _ <- mapM insert planetReports
    _ <- insert $ fullStarReport fId sunE
    _ <- insert $ fullStarSystemReport fId solE

    return ()


fullPlanetReport:: FactionId -> Entity StarSystem -> Entity Planet -> PlanetReport
fullPlanetReport fId systemE planetE =
    PlanetReport
    { _planetReportPlanetId = planetE ^. entityKeyL
    , _planetReportOwnerId = Just fId
    , _planetReportStarSystemId = systemE ^. entityKeyL
    , _planetReportName =  planetE ^. entityValL . planetName . to Just
    , _planetReportPosition =  planetE ^. entityValL . planetPosition . to Just
    , _planetReportGravity =  planetE ^. entityValL . planetGravity . to Just
    , _planetReportFactionId = fId
    , _planetReportDate = dayZero
    , _planetReportRulerId = planetE ^. entityValL . planetRulerId
    }


fullStarSystemReport :: FactionId -> Entity StarSystem -> StarSystemReport
fullStarSystemReport fId systemE =
    StarSystemReport
    { _starSystemReportStarSystemId = systemE ^. entityKeyL
    , _starSystemReportName = systemE ^. entityValL . starSystemName . to Just
    , _starSystemReportCoordX = systemE ^. entityValL ^. starSystemCoordX
    , _starSystemReportCoordY = systemE ^. entityValL . starSystemCoordY
    , _starSystemReportFactionId = fId
    , _starSystemReportDate = dayZero
    , _starSystemReportRulerId = systemE ^. entityValL . starSystemRulerId
    }


fullStarReport :: FactionId -> Entity Star -> StarReport
fullStarReport fId starE =
    StarReport
    { _starReportStarId = starE ^. entityKeyL
    , _starReportStarSystemId = starE ^. entityValL . starStarSystemId
    , _starReportName = starE ^. entityValL . starName . to Just
    , _starReportSpectralType = starE ^. entityValL . starSpectralType . to Just
    , _starReportLuminosityClass = starE ^. entityValL . starLuminosityClass . to Just
    , _starReportFactionId = fId
    , _starReportDate = dayZero
    }


reseedTalvelaCourt :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m) =>
    FactionId -> ReaderT backend m ()
reseedTalvelaCourt fId = do
    p <- liftIO $ evalRandIO $ replicateM 40 $ generatePersonM dayZero $
            PersonOptions
                { _personOptionsAge = Just $ AgeBracket 15 35
                }
    let people = p & traversed . personFactionId .~ (Just fId) :: [Person]

    _ <- mapM insert people

    return ()


createOwnIntel :: (BaseBackend backend ~ SqlBackend
    , PersistStoreWrite backend, MonadIO m) => Key Person -> ReaderT backend m ()
createOwnIntel pid = do
    _ <- insert $ HumanIntelligence pid pid Stats
    _ <- insert $ HumanIntelligence pid pid Demesne
    _ <- insert $ HumanIntelligence pid pid FamilyRelations
    _ <- insert $ HumanIntelligence pid pid SecretRelations
    _ <- insert $ HumanIntelligence pid pid (Opinions $ DetailedOpinions PublicRelation)
    _ <- insert $ HumanIntelligence pid pid (Opinions $ DetailedOpinions FamilyRelation)
    _ <- insert $ HumanIntelligence pid pid (Opinions $ DetailedOpinions SecretRelation)
    _ <- insert $ HumanIntelligence pid pid Traits
    _ <- insert $ HumanIntelligence pid pid Location
    _ <- insert $ HumanIntelligence pid pid Activity

    return ()


-- | Create avatars for list of adminsitrator users. Each created avatar is
-- automatically part of the Grey Council faction.
createAdminAvatars :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m) => [Entity User] -> ReaderT backend m ()
createAdminAvatars admins = do
    sid <- insert $ StarSystem "Achernar" 0 0 Nothing
    _ <- insert $ Star "Achernar" sid G V
    pid <- insert $ Planet "Achernar Prime" 1 sid Nothing 1.0 Nothing

    fid <- insert $ Faction "Grey Council" sid 1500 1500 1500

    _ <- updateWhere [ PlanetId ==. pid] [ PlanetOwnerId =. Just fid ]
    _ <- mapM (createAdminAvatar fid) (zip [1.. ] admins)
    return ()


-- | Create an avatar for administrator user. Newly created avatar is automatically
-- part of the Grey Council faction. Avatar is named "Grey Councillor" with their
-- cognomen being user name of the administrator. Int parameter specifies regnal
-- number of the avatar.
createAdminAvatar :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, PersistQueryWrite backend
    , PersistUniqueRead backend, MonadIO m) => Key Faction -> (Int, Entity User) -> ReaderT backend m ()
createAdminAvatar fid (i, userE) = do
    let user = userE ^. entityValL
    let uid = userE ^. entityKeyL
    pid <- insert $ Person (RegalName "Grey" "Councillor" (MkRegnalNumber i) (user ^. userIdent . to _unUserIdentity . to MkCognomen . to Just))
                           Female Woman (MkStarDate 19792) 20 10 20 20 20 (Just fid)
                           Nothing Nothing Nothing Nothing Nothing
    _ <- updateWhere [ UserId ==. uid] [ UserAvatar =. Just pid ]
    return ()

{-
insert into faction (name, home_system, biologicals, mechanicals, chemicals) values ('Terrans', 1, 10000, 7500, 7500);
insert into faction (name, home_system, biologicals, mechanicals, chemicals) values ('Republic of Aclael', 2, 5000, 5000, 5000);

insert into building (planet_id, type, level, damage) values (3, 'SensorStation', 1, 0);
insert into building (planet_id, type, level, damage) values (3, 'ResearchComplex', 1, 0.25);
insert into building (planet_id, type, level, damage) values (3, 'ResearchComplex', 1, 0);
insert into building (planet_id, type, level, damage) values (3, 'Farm', 1, 0);
-}