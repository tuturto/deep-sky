{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE NoImplicitPrelude          #-}


-- | Catch all module for things that don't yet belong to anywhere else
module CustomTypes
    ( SpecialEventStatus(..), BuildingType(..), Role(..)
    , PercentileChance(..), RollResult(..), Bonus(..), Boostable(..)
    , StarDate(..), Age(..), UserIdentity(..), FactionName(..)
    , SystemStatus(..), age, buildingTypeName, roll, _Bonus
    , unFactionName, unUserIdentity, unAge, unStarDate, unPercentileChance
    , starDateToDisplayString, ageToDisplayString
    )
    where

import ClassyPrelude.Yesod   as Import
import Control.Lens ( makeLenses, makePrisms, makeWrapped )
import Data.Aeson ( withScientific, withText )
import Data.Aeson.TH
import Data.Scientific ( toBoundedInteger )
import qualified Data.Text as T
import Database.Persist.Sql
import Data.Monoid ()
import Numeric.Natural ( Natural )
import System.Random ( Random(..) )


-- | Some news are special events that offer users to make choice how to handle given situation
-- This data type is used to differentiate between different kinds of news
data SpecialEventStatus =
    UnhandledSpecialEvent
    | HandledSpecialEvent
    | NoSpecialEvent
    deriving (Show, Read, Eq, Enum, Bounded)
derivePersistField "SpecialEventStatus"


data BuildingType = SensorStation
    | ResearchComplex
    | Farm
    | ParticleAccelerator
    | NeutronDetector
    | BlackMatterScanner
    | GravityWaveSensor
    deriving (Show, Read, Eq, Enum, Bounded)
derivePersistField "BuildingType"


buildingTypeName :: BuildingType -> Text
buildingTypeName bt =
    case bt of
        SensorStation -> "Sensor Station"
        ResearchComplex -> "Research Complex"
        Farm -> "Farm"
        ParticleAccelerator -> "Particle Accelerator"
        NeutronDetector -> "Neutron Detector"
        BlackMatterScanner -> "Black Matter Scanner"
        GravityWaveSensor -> "Gravity Wave Sensor"


data Role = RoleUser
          | RoleAdministrator
    deriving (Show, Read, Eq, Enum, Bounded)
derivePersistField "Role"


newtype PercentileChance =
    MkPercentileChance { _unPercentileChance :: Int }
    deriving (Show, Read, Eq, Ord, Num)


data RollResult =
    Success
    | Failure
    deriving (Show, Read, Eq, Enum, Bounded)


roll :: PercentileChance -> IO RollResult
roll odds = do
    result <- randomRIO (0, 100 :: Int)
    return $ if result <= _unPercentileChance odds
        then Success
        else Failure


data Bonus = Bonus Int Double
    deriving (Show, Read, Eq)


instance Semigroup Bonus where
    (<>) (Bonus a0 p0) (Bonus a1 p1) =
        Bonus (a0 + a1) (p0 * p1)


instance Monoid Bonus where
    mempty = Bonus 0 1


-- | Thing that can be modified by a bonus
class Boostable a where

    -- | Apply bonus to a, scaling it accordingly
    applyBonus :: Bonus -> a -> a


newtype StarDate = MkStarDate { _unStarDate :: Int }
    deriving (Show, Read, Eq, Num, Ord)


instance ToJSON StarDate where
    toJSON = toJSON . _unStarDate


instance FromJSON StarDate where
    parseJSON =
        withScientific "star date"
            (\x -> case toBoundedInteger x of
                Nothing ->
                    mempty

                Just n ->
                    return $ MkStarDate n)


instance PersistField StarDate where
    toPersistValue (MkStarDate n) =
        PersistInt64 $ fromIntegral n

    fromPersistValue (PersistInt64 n) =
        Right $ MkStarDate $ fromIntegral n

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql StarDate where
    sqlType _ = SqlInt64


instance Random StarDate where
    randomR (MkStarDate lo, MkStarDate hi) g =
        (MkStarDate res, g')
        where
            (res, g') = randomR (lo, hi) g

    random g =
        (MkStarDate res, g')
        where
            (res, g') = random g

    randomRs (MkStarDate lo, MkStarDate hi) g =
        MkStarDate <$> res
        where
            res = randomRs (lo, hi) g


starDateToDisplayString :: StarDate -> Text
starDateToDisplayString (MkStarDate n) =
    let full = pack $ show n
        month = T.takeEnd 1 full
        year = T.take (T.length full - 1) full
    in
        year ++ "." ++ month


newtype Age = MkAge { _unAge :: Natural }
    deriving (Show, Read, Eq, Num, Ord)


instance ToJSON Age where
    toJSON = toJSON . _unAge


instance FromJSON Age where
    parseJSON =
        withScientific "age"
            (\x -> case toBoundedInteger x of
                Nothing ->
                    mempty

                Just n ->
                    if n >= 0 then
                        return $ MkAge $ fromIntegral (n :: Int)

                    else
                        mempty)


{- | Age (ie. difference between two star dates in whole years)
     If star is in the future from end, MkAge 0 is returned
-}
age :: StarDate -> StarDate -> Age
age (MkStarDate start) (MkStarDate end) =
    if start <= end
        then
            MkAge $ fromIntegral ((end - start) `quot` 10)

        else
            MkAge 0


ageToDisplayString :: Age -> Text
ageToDisplayString (MkAge n) =
    pack $ show n


newtype UserIdentity = MkUserIdentity { _unUserIdentity :: Text }
    deriving (Show, Read, Eq)


instance IsString UserIdentity where
    fromString = MkUserIdentity . fromString


instance ToJSON UserIdentity where
    toJSON = toJSON . _unUserIdentity


instance FromJSON UserIdentity where
    parseJSON =
        withText "user identity"
            (return . MkUserIdentity)


instance PersistField UserIdentity where
    toPersistValue (MkUserIdentity s) =
        PersistText s

    fromPersistValue (PersistText s) =
        Right $ MkUserIdentity s

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql UserIdentity where
    sqlType _ = SqlString


newtype FactionName = MkFactionName { _unFactionName :: Text }
    deriving (Show, Read, Eq)


instance IsString FactionName where
    fromString = MkFactionName . fromString


instance ToJSON FactionName where
    toJSON = toJSON . _unFactionName


instance FromJSON FactionName where
    parseJSON =
        withText "faction name"
            (return . MkFactionName)


instance PersistField FactionName where
    toPersistValue (MkFactionName s) =
        PersistText s

    fromPersistValue (PersistText s) =
        Right $ MkFactionName s

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql FactionName where
    sqlType _ = SqlString


-- | Status of the simulation
data SystemStatus =
    Offline
    | Maintenance
    | Online
    | ProcessingTurn
    deriving (Show, Read, Eq)
derivePersistField "SystemStatus"


$(deriveJSON defaultOptions ''SpecialEventStatus)
$(deriveJSON defaultOptions ''Role)
$(deriveJSON defaultOptions ''BuildingType)
$(deriveJSON defaultOptions ''SystemStatus)

makeLenses ''PercentileChance
makeWrapped ''PercentileChance
makePrisms ''Bonus
makeLenses ''StarDate
makeWrapped ''StarDate
makeLenses ''Age
makeWrapped ''Age
makeLenses ''UserIdentity
makeWrapped ''UserIdentity
makeLenses ''FactionName
makeWrapped ''FactionName
