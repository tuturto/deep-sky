{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE LambdaCase                 #-}


module Report ( createPlanetReports, createStarReports, createStarLaneReports, createSystemReport
              , collateReports, collateReport, spectralInfo, createPlanetStatusReport
              , planetStatusIconMapper
              , CollatedPlanetReport(..), CollatedStarReport(..), CollatedStarLaneReport(..)
              , CollatedBuildingReport(..), CollatedPopulationReport(..), CollatedStarSystemReport(..)
              , CollatedBaseReport(..), CollatedPlanetStatusReport(..)
              , cssrId, cssrName, cssrLocation, cssrRulerId, cssrRulerName, cssrRulerTitle
              , cssrDate, csrStarId, csrSystemId, csrName, csrSpectralType, csrLuminosityClass
              , csrDate, cprId, cprSystemId, cprOwnerId, cprName, cprPosition, cprGravity
              , cprDate, cprRulerId, cprRulerName, cprRulerTitle, cpopPlanetId, cpopRaceId
              , cpopRace, cpopPopulation, cpopDate, collatedPlanetStatusReportPlanetId
              , collatedPlanetStatusReportStatus, collatedPlanetStatusReportDate
              , planetaryStatusInfoStatus, planetaryStatusInfoDescription, planetaryStatusInfoIcon
              , cslStarLaneId, cslSystemId1, cslSystemId2, cslStarSystemName1, cslStarSystemName2
              , cslDate, cbsPlanetReport, cbsStarSystemName, cbrBuildingId, cbrPlanetId
              , cbrType, cbrLevel, cbrDamage, cbrDate
              )
    where

import Import
import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import Data.Monoid ()
import Database.Persist.Sql (toSqlKey)
import Control.Lens ( (^.), (^?), _Just, makeLenses )

import CustomTypes
import Dto.Icons ( IconMapper(..) )
import People.Data ( PersonName(..), ShortTitle )
import People.Titles ( shortTitle )
import Space.Data ( PlanetName(..), StarName(..), StarSystemName(..)
                  , PlanetaryStatus(..), LuminosityClass(..), SpectralType(..)
                  , Coordinates(..)
                  )


-- | Class to transform a report stored in db to respective collated report
class ReportTransform a b | a -> b where
    fromReport :: a -> b


-- | Class to indicate if two reports are about same entity
class Grouped a where
    sameGroup :: a -> a -> Bool


data CollatedStarSystemReport = CollatedStarSystemReport
    { _cssrId :: StarSystemId
    , _cssrName :: Maybe StarSystemName
    , _cssrLocation :: Coordinates
    , _cssrRulerId :: Maybe PersonId
    , _cssrRulerName :: Maybe PersonName
    , _cssrRulerTitle :: Maybe ShortTitle
    , _cssrDate :: StarDate
    } deriving Show


instance Semigroup CollatedStarSystemReport where
    (<>) a b = CollatedStarSystemReport
                { _cssrId = _cssrId a
                , _cssrName = _cssrName a <|> _cssrName b
                , _cssrLocation = _cssrLocation a
                , _cssrRulerId = _cssrRulerId a
                , _cssrRulerName = _cssrRulerName a
                , _cssrRulerTitle = _cssrRulerTitle a
                , _cssrDate = max (_cssrDate a) (_cssrDate b)
                }


instance Monoid CollatedStarSystemReport where
    mempty = CollatedStarSystemReport
                { _cssrId = toSqlKey 0
                , _cssrName = Nothing
                , _cssrRulerId = Nothing
                , _cssrRulerName = Nothing
                , _cssrRulerTitle = Nothing
                , _cssrLocation = Coordinates 0 0
                , _cssrDate = 0
                }


instance ReportTransform StarSystemReport CollatedStarSystemReport where
    fromReport report =
        CollatedStarSystemReport
            { _cssrId = report ^. starSystemReportStarSystemId
            , _cssrName = report ^. starSystemReportName
            , _cssrLocation = Coordinates (report ^. starSystemReportCoordX) (report ^. starSystemReportCoordY)
            , _cssrRulerId = report ^. starSystemReportRulerId
            , _cssrRulerName = Nothing
            , _cssrRulerTitle = Nothing
            , _cssrDate = report ^. starSystemReportDate
            }


instance ReportTransform (StarSystemReport, Maybe Person) CollatedStarSystemReport where
    fromReport (report, person) =
        CollatedStarSystemReport
            { _cssrId = report ^. starSystemReportStarSystemId
            , _cssrName = report ^. starSystemReportName
            , _cssrLocation = Coordinates (report ^. starSystemReportCoordX) (report ^. starSystemReportCoordY)
            , _cssrRulerId = report ^. starSystemReportRulerId
            , _cssrRulerName = person ^? _Just . personName
            , _cssrRulerTitle = person >>= shortTitle
            , _cssrDate = report ^. starSystemReportDate
            }


instance Grouped StarSystemReport where
    sameGroup a b =
        a ^. starSystemReportStarSystemId == b ^. starSystemReportStarSystemId


data CollatedStarReport = CollatedStarReport
    { _csrStarId          :: StarId
    , _csrSystemId        :: StarSystemId
    , _csrName            :: Maybe StarName
    , _csrSpectralType    :: Maybe SpectralType
    , _csrLuminosityClass :: Maybe LuminosityClass
    , _csrDate            :: StarDate
    } deriving Show


instance Semigroup CollatedStarReport where
    (<>) a b = CollatedStarReport (_csrStarId a)
                                  (_csrSystemId a)
                                  (_csrName a <|> _csrName b)
                                  (_csrSpectralType a <|> _csrSpectralType b)
                                  (_csrLuminosityClass a <|> _csrLuminosityClass b)
                                  (max (_csrDate a) (_csrDate b))


instance Monoid CollatedStarReport where
    mempty = CollatedStarReport (toSqlKey 0) (toSqlKey 0) Nothing Nothing Nothing 0


instance ReportTransform StarReport CollatedStarReport where
    fromReport report =
        CollatedStarReport (report ^. starReportStarId)
                           (report ^. starReportStarSystemId)
                           (report ^. starReportName)
                           (report ^. starReportSpectralType)
                           (report ^. starReportLuminosityClass)
                           (report ^. starReportDate)


instance Grouped StarReport where
    sameGroup a b =
        a ^. starReportStarId == b ^. starReportStarId


instance ToJSON CollatedStarReport where
  toJSON CollatedStarReport { _csrStarId = rId
                            , _csrSystemId = rSId
                            , _csrName = rName
                            , _csrSpectralType = rSpectral
                            , _csrLuminosityClass = rLuminosity
                            , _csrDate = rDate } =
    object [ "id" .= rId
           , "systemId" .= rSId
           , "name" .= rName
           , "spectralType" .= rSpectral
           , "luminosityClass" .= rLuminosity
           , "date" .= rDate ]


data CollatedPlanetReport = CollatedPlanetReport
    { _cprId :: PlanetId
    , _cprSystemId :: StarSystemId
    , _cprOwnerId :: Maybe FactionId
    , _cprName :: Maybe PlanetName
    , _cprPosition :: Maybe Int
    , _cprGravity :: Maybe Double
    , _cprDate :: StarDate
    , _cprRulerId :: Maybe PersonId
    , _cprRulerName :: Maybe PersonName
    , _cprRulerTitle :: Maybe ShortTitle
    } deriving Show


instance Semigroup CollatedPlanetReport where
    (<>) a b =
        CollatedPlanetReport
            { _cprId = _cprId a
            , _cprSystemId = _cprSystemId a
            , _cprOwnerId = _cprOwnerId a <|> _cprOwnerId b
            , _cprName = _cprName a <|> _cprName b
            , _cprPosition = _cprPosition a <|> _cprPosition b
            , _cprGravity = _cprGravity a <|> _cprGravity b
            , _cprDate = max (_cprDate a) (_cprDate b)
            , _cprRulerId = _cprRulerId a -- Ruler info is always up to date
            , _cprRulerName = _cprRulerName a
            , _cprRulerTitle = _cprRulerTitle a
            }


instance Monoid CollatedPlanetReport where
    mempty = CollatedPlanetReport
                { _cprId = toSqlKey 0
                , _cprSystemId = toSqlKey 0
                , _cprOwnerId = Nothing
                , _cprName = Nothing
                , _cprPosition = Nothing
                , _cprGravity = Nothing
                , _cprRulerId = Nothing
                , _cprRulerName = Nothing
                , _cprRulerTitle = Nothing
                , _cprDate = 0
                }


instance ReportTransform PlanetReport CollatedPlanetReport where
    fromReport report =
        CollatedPlanetReport
            { _cprId = report ^. planetReportPlanetId
            , _cprSystemId = report ^. planetReportStarSystemId
            , _cprOwnerId = report ^. planetReportOwnerId
            , _cprName = report ^. planetReportName
            , _cprPosition = report ^. planetReportPosition
            , _cprGravity = report ^. planetReportGravity
            , _cprDate = report ^. planetReportDate
            , _cprRulerId = report ^. planetReportRulerId
            , _cprRulerName = Nothing
            , _cprRulerTitle = Nothing
            }


instance ReportTransform (PlanetReport, Maybe Person) CollatedPlanetReport where
    fromReport (report, person) =
        CollatedPlanetReport
            { _cprId = report ^. planetReportPlanetId
            , _cprSystemId = report ^. planetReportStarSystemId
            , _cprOwnerId = report ^. planetReportOwnerId
            , _cprName = report ^. planetReportName
            , _cprPosition = report ^. planetReportPosition
            , _cprGravity = report ^. planetReportGravity
            , _cprDate = report ^. planetReportDate
            , _cprRulerId = report ^. planetReportRulerId
            , _cprRulerName = person ^? _Just . personName
            , _cprRulerTitle = person >>= shortTitle
            }


instance Grouped PlanetReport where
    sameGroup a b =
        a ^. planetReportPlanetId == b ^. planetReportPlanetId


data CollatedPopulationReport = CollatedPopulationReport
    { _cpopPlanetId   :: PlanetId
    , _cpopRaceId     :: Maybe RaceId
    , _cpopRace       :: Maybe Text
    , _cpopPopulation :: Maybe Int
    , _cpopDate       :: StarDate
    } deriving Show


instance ToJSON CollatedPopulationReport where
    toJSON CollatedPopulationReport { _cpopPlanetId = pId
                                    , _cpopRaceId = rRaceId
                                    , _cpopRace = rRace
                                    , _cpopPopulation = rPop
                                    , _cpopDate = rDate } =
        object [ "planetId" .= pId
               , "raceId" .= rRaceId
               , "race" .= rRace
               , "inhabitants" .= rPop
               , "date" .= rDate ]


instance Semigroup CollatedPopulationReport where
    (<>) a b = CollatedPopulationReport (_cpopPlanetId a)
                                        (_cpopRaceId a <|> _cpopRaceId b)
                                        (_cpopRace a <|> _cpopRace b)
                                        (_cpopPopulation a <|> _cpopPopulation b)
                                        (max (_cpopDate a) (_cpopDate b))


instance Monoid CollatedPopulationReport where
    mempty = CollatedPopulationReport (toSqlKey 0) Nothing Nothing Nothing 0


instance ReportTransform (PlanetPopulationReport, Maybe Race) CollatedPopulationReport where
    fromReport (report, pRace) =
        CollatedPopulationReport (report ^. planetPopulationReportPlanetId)
                                 (report ^. planetPopulationReportRaceId)
                                 (pRace ^? _Just . raceName )
                                 (report ^. planetPopulationReportPopulation)
                                 (report ^. planetPopulationReportDate)


instance Grouped (PlanetPopulationReport, Maybe Race) where
    sameGroup (a, _) (b, _) =
        a ^. planetPopulationReportPlanetId == b ^. planetPopulationReportPlanetId &&
            a ^. planetPopulationReportRaceId == b ^. planetPopulationReportRaceId


data CollatedPlanetStatusReport = CollatedPlanetStatusReport
    { _collatedPlanetStatusReportPlanetId :: !PlanetId
    , _collatedPlanetStatusReportStatus :: ![PlanetaryStatusInfo]
    , _collatedPlanetStatusReportDate :: !StarDate
    }
    deriving (Show, Read, Eq)


instance Semigroup CollatedPlanetStatusReport where
    (<>) a _ = a


instance Monoid CollatedPlanetStatusReport where
        mempty = CollatedPlanetStatusReport
                    { _collatedPlanetStatusReportPlanetId = toSqlKey 0
                    , _collatedPlanetStatusReportStatus = []
                    , _collatedPlanetStatusReportDate = 0
                    }


instance Grouped (PlanetStatusReport, IconMapper PlanetaryStatus) where
    sameGroup (a, _) (b, _) =
        a ^. planetStatusReportPlanetId == b ^. planetStatusReportPlanetId


instance ReportTransform (PlanetStatusReport, IconMapper PlanetaryStatus) CollatedPlanetStatusReport where
    fromReport (report, icons) =
        CollatedPlanetStatusReport
            { _collatedPlanetStatusReportPlanetId = report ^. planetStatusReportPlanetId
            , _collatedPlanetStatusReportStatus = statusToInfo icons <$> (report ^. planetStatusReportStatus)
            , _collatedPlanetStatusReportDate = report ^. planetStatusReportDate
            }


statusToInfo :: IconMapper PlanetaryStatus -> PlanetaryStatus -> PlanetaryStatusInfo
statusToInfo icons status =
    PlanetaryStatusInfo
        { _planetaryStatusInfoStatus = status
        , _planetaryStatusInfoDescription = statusDescription status
        , _planetaryStatusInfoIcon = runIconMapper icons status
        }


data PlanetaryStatusInfo = PlanetaryStatusInfo
    { _planetaryStatusInfoStatus :: !(PlanetaryStatus)
    , _planetaryStatusInfoDescription :: !Text
    , _planetaryStatusInfoIcon :: !Text
    }
    deriving (Show, Read, Eq)


data CollatedStarLaneReport = CollatedStarLaneReport
    { _cslStarLaneId      :: StarLaneId
    , _cslSystemId1       :: StarSystemId
    , _cslSystemId2       :: StarSystemId
    , _cslStarSystemName1 :: Maybe StarSystemName
    , _cslStarSystemName2 :: Maybe StarSystemName
    , _cslDate            :: StarDate
    } deriving Show


instance Semigroup CollatedStarLaneReport where
    (<>) a b = CollatedStarLaneReport
                { _cslStarLaneId = _cslStarLaneId a
                , _cslSystemId1 = _cslSystemId1 a
                , _cslSystemId2 = _cslSystemId2 a
                , _cslStarSystemName1 = _cslStarSystemName1 a <|> _cslStarSystemName1 b
                , _cslStarSystemName2 = _cslStarSystemName2 a <|> _cslStarSystemName2 b
                , _cslDate = max (_cslDate a) (_cslDate b)
                }


instance Monoid CollatedStarLaneReport where
    mempty = CollatedStarLaneReport
                { _cslStarLaneId = toSqlKey 0
                , _cslSystemId1 = toSqlKey 0
                , _cslSystemId2 = toSqlKey 0
                , _cslStarSystemName1 = Nothing
                , _cslStarSystemName2 = Nothing
                , _cslDate = 0
                }


instance ReportTransform StarLaneReport CollatedStarLaneReport where
    fromReport report = CollatedStarLaneReport
                { _cslStarLaneId = report ^. starLaneReportStarLaneId
                , _cslSystemId1 = report ^. starLaneReportStarSystem1
                , _cslSystemId2 = report ^. starLaneReportStarSystem2
                , _cslStarSystemName1 = report ^. starLaneReportStarSystemName1
                , _cslStarSystemName2 = report ^. starLaneReportStarSystemName2
                , _cslDate = report ^. starLaneReportDate
                }


instance Grouped StarLaneReport where
    sameGroup a b =
        a ^. starLaneReportStarSystem1 == b ^. starLaneReportStarSystem1 &&
        a ^. starLaneReportStarSystem2 == b ^. starLaneReportStarSystem2


data CollatedBaseReport = CollatedBaseReport {
      _cbsPlanetReport   :: CollatedPlanetReport
    , _cbsStarSystemName :: StarSystemName
} deriving Show


data CollatedBuildingReport = CollatedBuildingReport
    { _cbrBuildingId   :: BuildingId
    , _cbrPlanetId     :: PlanetId
    , _cbrType         :: Maybe BuildingType
    , _cbrLevel        :: Maybe Int
    , _cbrDamage       :: Maybe Double
    , _cbrDate         :: StarDate
    } deriving Show


instance Semigroup CollatedBuildingReport where
    (<>) a b = CollatedBuildingReport
                { _cbrBuildingId = _cbrBuildingId a
                , _cbrPlanetId = _cbrPlanetId a
                , _cbrType = _cbrType a <|> _cbrType b
                , _cbrLevel = _cbrLevel a <|> _cbrLevel b
                , _cbrDamage = _cbrDamage a <|> _cbrDamage b
                , _cbrDate = max (_cbrDate a) (_cbrDate b)
                }


instance Monoid CollatedBuildingReport where
    mempty = CollatedBuildingReport
                { _cbrBuildingId = toSqlKey 0
                , _cbrPlanetId = toSqlKey 0
                , _cbrType = Nothing
                , _cbrLevel = Nothing
                , _cbrDamage = Nothing
                , _cbrDate = 0
                }


instance ReportTransform BuildingReport CollatedBuildingReport where
    fromReport report =
        CollatedBuildingReport
                { _cbrBuildingId = report ^. buildingReportBuildingId
                , _cbrPlanetId = report ^. buildingReportPlanetId
                , _cbrType = report ^. buildingReportType
                , _cbrLevel = report ^. buildingReportLevel
                , _cbrDamage = report ^. buildingReportDamage
                , _cbrDate = report ^. buildingReportDate
                }


instance Grouped BuildingReport where
    sameGroup a b =
        a ^. buildingReportBuildingId == b ^. buildingReportBuildingId


instance ToJSON CollatedBuildingReport where
  toJSON CollatedBuildingReport { _cbrBuildingId = bId
                                , _cbrPlanetId = pId
                                , _cbrType = rType
                                , _cbrLevel = rLevel
                                , _cbrDamage = rDamage
                                , _cbrDate = rDate
                                } =
    object [ "id" .= bId
           , "planetId" .= pId
           , "type" .= rType
           , "level" .= rLevel
           , "damage" .= rDamage
           , "date" .= rDate ]


spectralInfo :: Maybe SpectralType -> Maybe LuminosityClass -> Text
spectralInfo Nothing Nothing     = ""
spectralInfo (Just st) Nothing   = pack $ show st
spectralInfo Nothing (Just lc)   = pack $ show lc
spectralInfo (Just st) (Just lc) = pack $ show st ++ show lc


-- | Combine list of reports and form a single collated report
--   Resulting report will have facts from the possibly partially empty reports
--   If a fact is not present for a given field, Nothing is left there
collateReport :: (Monoid a, ReportTransform b a) => [b] -> a
collateReport reports = mconcat $ fmap fromReport reports


-- | Combine list of reports and form a list of collated reports
--   Each reported entity is given their own report
collateReports :: (Grouped b, Monoid a, ReportTransform b a) => [b] -> [a]
collateReports [] = []
collateReports s@(x:_) = collateReport itemsOfKind : collateReports restOfItems
    where split = span (sameGroup x) s
          itemsOfKind = fst split
          restOfItems = snd split


createStarReports :: (BaseBackend backend ~ SqlBackend,
    PersistQueryRead backend, MonadIO m) =>
    StarSystemId -> FactionId -> ReaderT backend m [CollatedStarReport]
createStarReports systemId factionId = do
    loadedStarReports <- selectList [ StarReportStarSystemId ==. systemId
                                    , StarReportFactionId ==. factionId ] [ Asc StarReportId
                                                                          , Asc StarReportDate ]
    return $ collateReports $ fmap entityVal loadedStarReports


createSystemReport :: (BaseBackend backend ~ SqlBackend, MonadIO m,
    PersistQueryRead backend) =>
    StarSystemId -> FactionId -> ReaderT backend m CollatedStarSystemReport
createSystemReport systemId factionId = do
    systemReports <- selectList [ StarSystemReportStarSystemId ==. systemId
                                , StarSystemReportFactionId ==. factionId ] [ Asc StarSystemReportDate ]
    return $ collateReport $ fmap entityVal systemReports


createPlanetReports :: (BaseBackend backend ~ SqlBackend,
    MonadIO m, PersistQueryRead backend) =>
    StarSystemId -> FactionId -> ReaderT backend m [CollatedPlanetReport]
createPlanetReports systemId factionId = do
    planets <- selectList [ PlanetStarSystemId ==. systemId ] []
    loadedPlanetReports <-  selectList [ PlanetReportPlanetId <-. fmap entityKey planets
                                       , PlanetReportFactionId ==. factionId ] [ Asc PlanetReportPlanetId
                                                                               , Asc PlanetReportDate ]
    return $ collateReports $ fmap entityVal loadedPlanetReports


createPlanetStatusReport :: ( BaseBackend backend ~ SqlBackend
                            , MonadIO m, PersistQueryRead backend) =>
                            (Route App -> Text) -> PlanetId -> FactionId -> ReaderT backend m [CollatedPlanetStatusReport]
createPlanetStatusReport render planetId factionId = do
    statuses <- selectList [ PlanetStatusReportPlanetId ==. planetId
                           , PlanetStatusReportFactionId ==. factionId ]
                           [ Asc PlanetStatusReportDate ]
    let icons = planetStatusIconMapper render
    return $ collateReports $ fmap (\x -> (entityVal x, icons)) statuses


createStarLaneReports :: (BaseBackend backend ~ SqlBackend,
    MonadIO m, PersistQueryRead backend) =>
    StarSystemId -> FactionId -> ReaderT backend m [CollatedStarLaneReport]
createStarLaneReports systemId factionId = do
    loadedLaneReports <- selectList ([ StarLaneReportStarSystem1 ==. systemId
                                     , StarLaneReportFactionId ==. factionId ]
                                 ||. [ StarLaneReportStarSystem2 ==. systemId
                                     , StarLaneReportFactionId ==. factionId ]) []
    return $ rearrangeStarLanes systemId $ collateReports $ fmap entityVal loadedLaneReports


rearrangeStarLanes :: StarSystemId -> [CollatedStarLaneReport] -> [CollatedStarLaneReport]
rearrangeStarLanes systemId = fmap arrangeStarLane
    where arrangeStarLane starLane = if systemId == _cslSystemId1 starLane
                                        then starLane
                                        else CollatedStarLaneReport (toSqlKey 0)
                                                                    (_cslSystemId2 starLane)
                                                                    (_cslSystemId1 starLane)
                                                                    (_cslStarSystemName2 starLane)
                                                                    (_cslStarSystemName1 starLane)
                                                                    (_cslDate starLane)


planetStatusIconMapper :: (Route App -> Text) -> IconMapper PlanetaryStatus
planetStatusIconMapper render =
    IconMapper $ \case
            GoodHarvest ->
                render $ StaticR images_statuses_wheat_up_png

            PoorHarvest ->
                render $ StaticR images_statuses_wheat_down_png

            GoodMechanicals ->
                render $ StaticR images_statuses_cog_up_png

            PoorMechanicals ->
                render $ StaticR images_statuses_cog_down_png

            GoodChemicals ->
                render $ StaticR images_statuses_droplets_up_png

            PoorChemicals ->
                render $ StaticR images_statuses_droplets_down_png

            KragiiAttack ->
                render $ StaticR images_statuses_hydra_png


statusDescription :: PlanetaryStatus -> Text
statusDescription status =
    case status of
        GoodHarvest -> "Harvest is plentiful"
        PoorHarvest -> "Harvest is poor"
        GoodMechanicals -> "Mech industry is booming"
        PoorMechanicals -> "Downturn in mech industry"
        GoodChemicals -> "Chem industry is booming"
        PoorChemicals -> "Downturn in chem industry"
        KragiiAttack -> "Kragii infestation in planet!"



$(deriveJSON defaultOptions { fieldLabelModifier = drop 5 } ''CollatedStarSystemReport)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 4 } ''CollatedPlanetReport)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 27 } ''CollatedPlanetStatusReport)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 20 } ''PlanetaryStatusInfo)

makeLenses ''CollatedStarSystemReport
makeLenses ''CollatedStarReport
makeLenses ''CollatedPlanetReport
makeLenses ''CollatedPopulationReport
makeLenses ''CollatedPlanetStatusReport
makeLenses ''PlanetaryStatusInfo
makeLenses ''CollatedStarLaneReport
makeLenses ''CollatedBaseReport
makeLenses ''CollatedBuildingReport
