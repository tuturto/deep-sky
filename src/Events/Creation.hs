{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE TemplateHaskell            #-}

module Events.Creation
    ( EventCreation(..), _NamingPet )
    where

import Import
import Control.Lens ( makePrisms )


data EventCreation
    = NamingPet PersonId PetId
    deriving (Show, Read, Eq)

makePrisms ''EventCreation
