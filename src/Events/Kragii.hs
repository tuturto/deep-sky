{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE FlexibleInstances          #-}

module Events.Kragii
    ( KragiiWormsEvent(..), KragiiWormsChoice(..), KragiiResults(..)
    , KragiiNews(..), kragiiWormsPlanetId, kragiiWormsPlanetName, kragiiWormsSystemId
    , kragiiWormsSystemName, kragiiWormsFactionId, kragiiWormsDate, _CropsDestroyed
    , kragiiNewsPlanetId, kragiiNewsPlanetName, kragiiNewsSystemId, kragiiNewsSystemName
    , kragiiNewsExplanation, kragiiNewsFactionId, kragiiNewsDate
    )

    where


import Import
import Control.Lens ( (^.), (^?), _Just, makeLenses, makePrisms, to )
import Control.Monad.Trans.Maybe ( MaybeT(..), runMaybeT )
import Control.Monad.Trans.Writer ( WriterT, runWriterT, tell )
import Data.Aeson.TH

import Common ( ToDto(..), FromDto(..) )
import CustomTypes ( PercentileChance(..), RollResult(..), StarDate(..), roll )
import Dto.News ( KragiiWormsChoiceDto(..), UserOptionDto(..), KragiiNewsDto(..) )
import Events.Creation ( EventCreation(..) )
import Events.Import ( SpecialEvent(..), EventRemoval(..), UserOption(..)
                     , EventResolveType(..), userOptionChoice, userOptionTitle
                     , userOptionExplanation
                     )
import Resources ( RawResource(..), Biological(..) )
import Space.Data ( PlanetName(..), StarSystemName(..), PlanetaryStatus(..) )


-- | Data for kragii worms attack
data KragiiWormsEvent = KragiiWormsEvent
    { _kragiiWormsPlanetId :: !PlanetId
    , _kragiiWormsPlanetName :: !PlanetName
    , _kragiiWormsSystemId :: !StarSystemId
    , _kragiiWormsSystemName :: !StarSystemName
    , _kragiiWormsFactionId :: !FactionId
    , _kragiiWormsDate :: !StarDate
    }
    deriving (Show, Read, Eq)


-- | User choices for kragii worms attack
data KragiiWormsChoice = EvadeWorms
    | AttackWorms
    | TameWorms
    deriving (Show, Read, Eq, Enum, Bounded)


-- | Results of resolving kragii attack
data KragiiResults =
    WormsStillPresent
    | WormsRemoved
    | WormsTamed
    | CropsDestroyed (RawResource Biological)
    | FarmersInjured
    deriving (Show, Read, Eq)


-- | data for kragii attack resolution
data KragiiNews = KragiiNews
    { _kragiiNewsPlanetId :: !PlanetId
    , _kragiiNewsPlanetName :: !PlanetName
    , _kragiiNewsSystemId :: !StarSystemId
    , _kragiiNewsSystemName :: !StarSystemName
    , _kragiiNewsExplanation :: !Text
    , _kragiiNewsFactionId :: !FactionId
    , _kragiiNewsDate :: !StarDate
    }
    deriving (Show, Read, Eq)


instance SpecialEvent KragiiWormsEvent KragiiWormsChoice KragiiResults where

    eventOptions _ = [ UserOption { _userOptionTitle = "Avoid the worms"
                                  , _userOptionExplanation = [ "Keep using fields, while avoiding the worms and hope they'll eventually leave."
                                                            , "50 units of biologicals lost"
                                                            , "25% chance of worms leaving"
                                                            ]
                                  , _userOptionChoice = EvadeWorms
                                  }
                     , UserOption { _userOptionTitle = "Attack worms and drive them away"
                                  , _userOptionExplanation = [ "Agresively spray fields with chemicals and drive worms away while losing some harvest."
                                                            , "75% chance of driving worms away"
                                                            , "25% chance of serious injuries to farmers"
                                                            , "20 units of biologicals lost"]
                                  , _userOptionChoice = AttackWorms
                                  }
                     , UserOption { _userOptionTitle = "Try and tame them"
                                  , _userOptionExplanation = [ "Try to use worms in your advantage in farming."
                                                            , "25% chance of success"
                                                            , "75% chance of serious injuries to farmers"
                                                            ]
                                  , _userOptionChoice = TameWorms
                                  }
                     ]

    resolveType _ =
        DelayedEvent

    resolveEvent keyEventPair (Just choice) =
        runWriterT . runMaybeT $
            case choice of
                    EvadeWorms ->
                        chooseToAvoid keyEventPair

                    AttackWorms ->
                        chooseToAttack keyEventPair

                    TameWorms ->
                        chooseToTame keyEventPair

    resolveEvent keyEventPair Nothing =
        runWriterT . runMaybeT $ noChoice keyEventPair


-- | Avoiding kragii worms means of trying to work only part of the fields, while
-- the worms have free reign on other parts. It's least dangerous option to people
-- involved and will lead to somewhat smaller crop output while worms are present.
-- Given enough time, they should naturally move on.
chooseToAvoid :: (MonadIO m, PersistQueryWrite backend, BaseBackend backend ~ SqlBackend) =>
                 (NewsId, KragiiWormsEvent)
                 -> MaybeT (WriterT [KragiiResults] (ReaderT backend m)) (EventRemoval, [EventCreation])
chooseToAvoid (_, event) = do
    faction <- getFaction event
    (cost, bioLeft) <- calculateNewBio (MkRawResource 50) (entityVal faction)
    _ <- destroyCrops faction cost bioLeft
    removeNews event $ MkPercentileChance 25


-- | Attacking and driving kragii worms away from the fields is unpleasant and
-- somewhat unsafe option. It's best option though, if one just wants to quickly get
-- rid of them. Some of the crops might be destroyed as a result of chemicals used
-- in the attack.
chooseToAttack :: (MonadIO m, PersistQueryWrite backend, BaseBackend backend ~ SqlBackend) =>
                  (NewsId, KragiiWormsEvent)
                  -> MaybeT (WriterT [KragiiResults] (ReaderT backend m)) (EventRemoval, [EventCreation])
chooseToAttack (_, event) = do
    faction <- getFaction event
    (cost, bioLeft) <- calculateNewBio (MkRawResource 20) (entityVal faction)
    -- TODO: injured farmers
    _ <- destroyCrops faction cost bioLeft
    removeNews event $ MkPercentileChance 75


-- | Kragii worms can't be tamed in the usual sense. However, it is possible to
-- try to corral them and have them working on improving soil of the fields they
-- attacked. While this is potentially dangerous to people involved, it can
-- yield much more nutrient soil and thus higher crop output
chooseToTame :: (MonadIO m, PersistQueryWrite backend, BaseBackend backend ~ SqlBackend) =>
                (NewsId, KragiiWormsEvent)
                -> MaybeT (WriterT [KragiiResults] (ReaderT backend m)) (EventRemoval, [EventCreation])
chooseToTame (_, event) = do
    -- TODO: proper implementation
    faction <- getFaction event
    (cost, bioLeft) <- calculateNewBio (MkRawResource 50) (entityVal faction)
    _ <- destroyCrops faction cost bioLeft
    removeNews event $ MkPercentileChance 25


-- | If left unchecked, kragii worms will eat crops from fields
-- They prefer potatoe plants and will not touch beans or paprikas
-- Different crop types aren't currently modeled in the game, so we just
-- use up 100 units of biological resources. There's chance that worms
-- will eventually leave all by themselves.
noChoice :: (MonadIO m, PersistQueryWrite backend, BaseBackend backend ~ SqlBackend) =>
            (NewsId, KragiiWormsEvent)
            -> MaybeT (WriterT [KragiiResults] (ReaderT backend m)) (EventRemoval, [EventCreation])
noChoice (_, event) = do
    faction <- getFaction event
    (cost, bioLeft) <- calculateNewBio (MkRawResource 100) (entityVal faction)
    _ <- destroyCrops faction cost bioLeft
    removeNews event $ MkPercentileChance 10


-- | retrieve current owner (faction) of planet where kragii attack is in progress
getFaction :: ( MonadIO m, PersistStoreRead backend, BaseBackend backend ~ SqlBackend ) =>
              KragiiWormsEvent
              -> MaybeT (WriterT [KragiiResults] (ReaderT backend m)) (Entity Faction)
getFaction event = MaybeT $ do
    planet <- lift $ get $ _kragiiWormsPlanetId event
    let owner = planet ^? _Just . planetOwnerId . _Just
    res <- lift $ mapM getEntity owner
    return $ join res


-- | Amount of biological resources left after consuming given amount
-- first element of the tuple is cost and second one amount left after applying cost
calculateNewBio :: Monad m =>
                   RawResource Biological
                   -> Faction
                   -> MaybeT (WriterT [KragiiResults] m) (RawResource Biological, RawResource Biological)
calculateNewBio cost faction = MaybeT $ do
    let currentBio = faction ^. factionBiologicals
    return $ if currentBio > 0
                then Just ( cost
                          , max 0 (currentBio - cost))
                else Nothing


-- | Update biologicals stockpile of the faction that is target of kragii attack
destroyCrops :: ( MonadIO m, PersistQueryWrite backend, BaseBackend backend ~ SqlBackend ) =>
                Entity Faction -> RawResource Biological
                -> RawResource Biological
                -> MaybeT (WriterT [KragiiResults] (ReaderT backend m)) ()
destroyCrops faction cost bioLeft = MaybeT $ do
    _ <- lift $ updateWhere [ FactionId ==. entityKey faction ]
                            [ FactionBiologicals =. bioLeft ]
    tell [ CropsDestroyed cost ]
    return $ Just ()


-- | Roll a die and see if kragii worms are removed from play
removeNews :: ( PersistStoreWrite backend, PersistQueryWrite backend, MonadIO m
              , BaseBackend backend ~ SqlBackend ) =>
              KragiiWormsEvent
              -> PercentileChance
              -> MaybeT (WriterT [KragiiResults] (ReaderT backend m)) (EventRemoval, [EventCreation])
removeNews event odds = MaybeT $ do
    res <- liftIO $ roll odds
    case res of
        Success -> do
            _ <- lift $ deleteWhere [ PlanetStatusPlanetId ==. _kragiiWormsPlanetId event
                                    , PlanetStatusStatus ==. KragiiAttack
                                    ]
            _ <- tell [ WormsRemoved ]
            return $ Just (RemoveOriginalEvent, [])
        Failure -> do
            _ <- tell [ WormsStillPresent ]
            return $ Just (KeepOriginalEvent, [])


instance ToDto KragiiWormsChoice KragiiWormsChoiceDto where
    toDto choice =
        case choice of
            EvadeWorms -> EvadeWormsDto
            AttackWorms -> AttackWormsDto
            TameWorms -> TameWormsDto


instance FromDto KragiiWormsChoice KragiiWormsChoiceDto where
    fromDto dto =
        case dto of
            EvadeWormsDto -> EvadeWorms
            AttackWormsDto -> AttackWorms
            TameWormsDto -> TameWorms


instance ToDto (UserOption KragiiWormsChoice) (UserOptionDto KragiiWormsChoiceDto) where
    toDto option =
        UserOptionDto
            { _userOptionDtoTitle = option ^. userOptionTitle
            , _userOptionDtoExplanation = option ^. userOptionExplanation
            , _userOptionDtoChoice = option ^. userOptionChoice . to toDto
            }


instance ToDto KragiiNews KragiiNewsDto where
    toDto event =
        KragiiNewsDto
            { _kragiiNewsDtoPlanetId = _kragiiNewsPlanetId event
            , _kragiiNewsDtoPlanetName = _kragiiNewsPlanetName event
            , _kragiiNewsDtoSystemId = _kragiiNewsSystemId event
            , _kragiiNewsDtoSystemName = _kragiiNewsSystemName event
            , _kragiiNewsDtoResolution = _kragiiNewsExplanation event
            , _kragiiNewsDtoFactionId = _kragiiNewsFactionId event
            , _kragiiNewsDtoDate = _kragiiNewsDate event
            }


instance FromDto KragiiNews KragiiNewsDto where
    fromDto dto =
        KragiiNews
        { _kragiiNewsPlanetId = _kragiiNewsDtoPlanetId dto
        , _kragiiNewsPlanetName = _kragiiNewsDtoPlanetName dto
        , _kragiiNewsSystemId = _kragiiNewsDtoSystemId dto
        , _kragiiNewsSystemName = _kragiiNewsDtoSystemName dto
        , _kragiiNewsExplanation = _kragiiNewsDtoResolution dto
        , _kragiiNewsFactionId = _kragiiNewsDtoFactionId dto
        , _kragiiNewsDate = _kragiiNewsDtoDate dto
        }


$(deriveJSON defaultOptions ''KragiiWormsEvent)
$(deriveJSON defaultOptions ''KragiiWormsChoice)
$(deriveJSON defaultOptions ''KragiiNews)

makeLenses ''KragiiWormsEvent
makePrisms ''KragiiResults
makeLenses ''KragiiNews