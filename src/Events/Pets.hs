{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE LambdaCase                 #-}

module Events.Pets
    ( ScurryingSoundsEvent(..), ScurryingSoundsChoice(..)
    , ScurryingSoundsResult(..), ScurryingSoundsNews(..)
    , NamingPetEvent(..), NamingPetChoice(..), NamingPetResult(..)
    , NamingPetNews(..), namingPetNewsExplanation, namingPetNewsPetId
    , namingPetNewsPetType, namingPetNewsDate, scurryingSoundsEventPersonId
    , scurryingSoundsEventDate, _PetObtained, _TooManyPets, _CrittersRemoved
    , _SoundsStoppedByThemselves, scurryingSoundsNewsExplanation, scurryingSoundsNewsPetId
    , scurryingSoundsNewsPetType, scurryingSoundsNewsDate, namingPetEventPersonId
    , namingPetEventPetId, namingPetEventPetType, namingPetEventDate, namingPetNameOptions
    )
    where


import Import
import Control.Lens ( (^.), to, makeLenses, makePrisms )
import Control.Monad.Trans.Maybe ( MaybeT(..), runMaybeT )
import Control.Monad.Trans.Writer ( WriterT, runWriterT, tell )
import Control.Monad.Random ( evalRand )
import Data.Aeson.TH
import System.Random ( newStdGen )
import Common ( ToDto(..), FromDto(..) )
import CustomTypes ( StarDate, RollResult(..), PercentileChance(..), roll )
import Dto.News ( ScurryingSoundsNewsDto(..), ScurryingSoundsChoiceDto(..)
                , UserOptionDto(..), NamingPetNewsDto(..)
                , NamingPetChoiceDto(..) )
import Events.Creation ( EventCreation(..) )
import Events.Import ( SpecialEvent(..), EventRemoval(..), UserOption(..)
                     , EventResolveType(..), userOptionChoice, userOptionExplanation
                     , userOptionTitle
                     )
import MenuHelpers ( starDate )
import Names ( petNameM )
import People.Data ( PetType(..), PetName(..) )


-- | Character hears scurrying sounds inside their walls
data ScurryingSoundsEvent = ScurryingSoundsEvent
    { _scurryingSoundsEventPersonId :: !PersonId
    , _scurryingSoundsEventDate :: !StarDate
    } deriving (Show, Read, Eq)


-- | Options for dealing with scurrying sounds
data ScurryingSoundsChoice
    = GetCat
    | TameRat
    | GetRidSomehowElse
    deriving (Show, Read, Eq)


instance FromDto ScurryingSoundsChoice ScurryingSoundsChoiceDto where
    fromDto = \case
                GetCatDto ->
                    GetCat

                TameRatDto ->
                    TameRat

                GetRidSomehowElseDto ->
                    GetRidSomehowElse


instance ToDto ScurryingSoundsChoice ScurryingSoundsChoiceDto where
    toDto = \case
                GetCat ->
                    GetCatDto

                TameRat ->
                    TameRatDto

                GetRidSomehowElse ->
                    GetRidSomehowElseDto


instance ToDto (UserOption ScurryingSoundsChoice) (UserOptionDto ScurryingSoundsChoiceDto) where
    toDto option =
        UserOptionDto
            { _userOptionDtoTitle = option ^. userOptionTitle
            , _userOptionDtoExplanation = option ^. userOptionExplanation
            , _userOptionDtoChoice = option ^. userOptionChoice . to toDto
            }


-- | End result for scurrying sounds
data ScurryingSoundsResult
    = PetObtained PetType PetId
    | TooManyPets
    | CrittersRemoved
    | SoundsStoppedByThemselves
    deriving (Show, Read, Eq)


-- | News article explaining end resolution of scurrying sounds
data ScurryingSoundsNews = ScurryingSoundsNews
    { _scurryingSoundsNewsExplanation :: !Text
    , _scurryingSoundsNewsPetId :: !(Maybe PetId)
    , _scurryingSoundsNewsPetType :: !(Maybe PetType)
    , _scurryingSoundsNewsDate :: !StarDate
    } deriving (Show, Read, Eq)


instance ToDto ScurryingSoundsNews ScurryingSoundsNewsDto where
    toDto ScurryingSoundsNews{..} =
        ScurryingSoundsNewsDto
            { _scurryingSoundsNewsDtoExplanation = _scurryingSoundsNewsExplanation
            , _scurryingSoundsNewsDtoPetId = _scurryingSoundsNewsPetId
            , _scurryingSoundsNewsDtoPetType = _scurryingSoundsNewsPetType
            , _scurryingSoundsNewsDtoDate = _scurryingSoundsNewsDate
            }


instance FromDto ScurryingSoundsNews ScurryingSoundsNewsDto where
    fromDto ScurryingSoundsNewsDto{..} =
        ScurryingSoundsNews
            { _scurryingSoundsNewsExplanation = _scurryingSoundsNewsDtoExplanation
            , _scurryingSoundsNewsPetId = _scurryingSoundsNewsDtoPetId
            , _scurryingSoundsNewsPetType = _scurryingSoundsNewsDtoPetType
            , _scurryingSoundsNewsDate = _scurryingSoundsNewsDtoDate
            }


instance SpecialEvent ScurryingSoundsEvent ScurryingSoundsChoice ScurryingSoundsResult where
    eventOptions _ = [ UserOption { _userOptionTitle = "Get a cat"
                                  , _userOptionExplanation = [ "Get a cat and hope it will get rid of critters making sounds." ]
                                  , _userOptionChoice = GetCat
                                  }
                     , UserOption { _userOptionTitle = "Tame critter making sounds"
                                  , _userOptionExplanation = [ "See if you can tame the animal that is making the sounds." ]
                                  , _userOptionChoice = TameRat
                                  }
                     , UserOption { _userOptionTitle = "Let someone else dispose of the noises"
                                  , _userOptionExplanation = [ "Let someone else deal with the problem." ]
                                  , _userOptionChoice = GetRidSomehowElse
                                  }
                     ]

    resolveType _ =
        DelayedEvent

    resolveEvent keyEventPair (Just choice) =
        runWriterT . runMaybeT $
            case choice of
                    GetCat ->
                        chooseToGetCat keyEventPair

                    TameRat ->
                        chooseToTame keyEventPair

                    GetRidSomehowElse ->
                        chooseToGetRidOf

    resolveEvent keyEventPair Nothing =
        runWriterT . runMaybeT $ noChoice keyEventPair


-- | Person has chosen to get a cat
-- this will stop the noises and person will acquire a new pet, if maximum
-- amount of pets hasn't been exceeded
chooseToGetCat :: (MonadIO m, PersistQueryWrite backend
    , BaseBackend backend ~ SqlBackend) =>
    (NewsId, ScurryingSoundsEvent)
    -> MaybeT (WriterT [ScurryingSoundsResult] (ReaderT backend m)) (EventRemoval, [EventCreation])
chooseToGetCat (_, event) = do
    date <- (lift . lift) starDate
    person <- getPerson event
    peId <- addPet person date Cat
    let cEvent = NamingPet <$> Just (entityKey person)
                           <*> peId
    return (RemoveOriginalEvent, maybeToList cEvent)


-- | Person has chosen to tame the rate
-- this will stop the noises and person will acquire a new pet, if maximum
-- amount of pets hasn't been exceeded
chooseToTame :: (MonadIO m, PersistQueryWrite backend
    , BaseBackend backend ~ SqlBackend) =>
    (NewsId, ScurryingSoundsEvent)
    -> MaybeT (WriterT [ScurryingSoundsResult] (ReaderT backend m)) (EventRemoval, [EventCreation])
chooseToTame (_, event) = do
    date <- (lift . lift) starDate
    person <- getPerson event
    peId <- addPet person date Rat
    let cEvent = NamingPet <$> Just (entityKey person)
                           <*> peId
    return (RemoveOriginalEvent, maybeToList cEvent)


-- | Person has chosen to get rid of critters. Noises will stop.
chooseToGetRidOf :: (MonadIO m, PersistQueryWrite backend
    , BaseBackend backend ~ SqlBackend) =>
    MaybeT (WriterT [ScurryingSoundsResult] (ReaderT backend m)) (EventRemoval, [EventCreation])
chooseToGetRidOf = do
    lift $ tell [ CrittersRemoved ]
    return (RemoveOriginalEvent, [])


-- | Handle case where person did not made any choice
-- there's small chance that noises stop by themselves and amount of existing
-- pets adds to this chance
noChoice :: (MonadIO m, PersistQueryWrite backend, PersistQueryRead backend
    , BaseBackend backend ~ SqlBackend) =>
    (NewsId, ScurryingSoundsEvent)
    -> MaybeT (WriterT [ScurryingSoundsResult] (ReaderT backend m)) (EventRemoval, [EventCreation])
noChoice (_, event) = do
    pets <- (lift . lift) $ selectList [ PetOwnerId ==. (_scurryingSoundsEventPersonId event)
                                       , PetDateOfDeath ==. Nothing
                                       ] []
    let odds = MkPercentileChance $ 15 * (length pets) + 15
    res <- liftIO $ roll odds
    case res of
        Success -> do
            lift $ tell [ SoundsStoppedByThemselves ]
            return (RemoveOriginalEvent, [])
        Failure -> do
            return (KeepOriginalEvent, [])


-- | Get person information from database
getPerson :: ( MonadIO m, PersistStoreRead backend
    , BaseBackend backend ~ SqlBackend ) =>
    ScurryingSoundsEvent
    -> MaybeT (WriterT [ScurryingSoundsResult] (ReaderT backend m)) (Entity Person)
getPerson event = MaybeT $ do
    person <- lift $ getEntity $ _scurryingSoundsEventPersonId event
    -- TODO: check that person isn't dead
    return person


-- | Tries to create a new pet and give to a person
-- if person already has maximum amount of pets, pet is not given
addPet :: (PersistQueryRead backend, MonadIO m,
    PersistStoreWrite backend, BaseBackend backend ~ SqlBackend) =>
    Entity Person
    -> StarDate
    -> PetType
    -> MaybeT (WriterT [ScurryingSoundsResult] (ReaderT backend m)) (Maybe PetId)
addPet person date pType = MaybeT $ do
    pets <- lift $ selectList [ PetOwnerId ==. (entityKey person)
                              , PetDateOfDeath ==. Nothing
                              ] []
    if (length pets) >= 5
        then do
            tell [ TooManyPets ]
            return (Just Nothing)
        else do
            pId <- lift $ insert $ Pet { _petType = pType
                                       , _petName = petTypeToName pType
                                       , _petDateOfBirth = date - 6
                                       , _petDateOfDeath = Nothing
                                       , _petOwnerId = entityKey person
                                       }
            tell [ PetObtained pType pId ]

            return $ Just $ Just pId


petTypeToName :: PetType -> PetName
petTypeToName Cat = "Cat"
petTypeToName Rat = "Rat"


data NamingPetEvent = NamingPetEvent
    { _namingPetEventPersonId :: !PersonId
    , _namingPetEventPetId :: !PetId
    , _namingPetEventPetType :: !PetType
    , _namingPetEventDate :: !StarDate
    , _namingPetNameOptions :: ![PetName]
    } deriving (Show, Read, Eq)


data NamingPetChoice
    = GiveName PetName
    | LetSomeoneElseDecide
    deriving (Show, Read, Eq)


instance FromDto NamingPetChoice NamingPetChoiceDto where
    fromDto =
        \case
            (GiveNameDto name) ->
                GiveName name

            LetSomeoneElseDecideDto ->
                LetSomeoneElseDecide


instance ToDto NamingPetChoice NamingPetChoiceDto where
    toDto =
        \case
            (GiveName name) ->
                GiveNameDto name

            LetSomeoneElseDecide ->
                LetSomeoneElseDecideDto


-- TODO: deduplicate
instance ToDto (UserOption NamingPetChoice) (UserOptionDto NamingPetChoiceDto) where
    toDto option =
        UserOptionDto
            { _userOptionDtoTitle = option ^. userOptionTitle
            , _userOptionDtoExplanation = option ^. userOptionExplanation
            , _userOptionDtoChoice = option ^. userOptionChoice . to toDto
            }


data NamingPetResult
    = PetNamed PetId PetName
    | RandomNameGiven PetId PetName
    deriving (Show, Read, Eq)


data NamingPetNews = NamingPetNews
    { _namingPetNewsExplanation :: !Text
    , _namingPetNewsPetId :: !PetId
    , _namingPetNewsPetType :: !PetType
    , _namingPetNewsDate :: !StarDate
    } deriving (Show, Read, Eq)


instance ToDto NamingPetNews NamingPetNewsDto where
    toDto NamingPetNews{..} =
        NamingPetNewsDto
            { _namingPetNewsDtoExplanation = _namingPetNewsExplanation
            , _namingPetNewsDtoPetId = _namingPetNewsPetId
            , _namingPetNewsDtoPetType = _namingPetNewsPetType
            , _namingPetNewsDtoDate = _namingPetNewsDate
            }


instance FromDto NamingPetNews NamingPetNewsDto where
    fromDto NamingPetNewsDto{..} =
        NamingPetNews
            { _namingPetNewsExplanation = _namingPetNewsDtoExplanation
            , _namingPetNewsPetId = _namingPetNewsDtoPetId
            , _namingPetNewsPetType = _namingPetNewsDtoPetType
            , _namingPetNewsDate = _namingPetNewsDtoDate
            }


instance SpecialEvent NamingPetEvent NamingPetChoice NamingPetResult where
    eventOptions event =
        --in this particular case, options available to user are stored in event
        (petNameOption <$> _namingPetNameOptions event)
            ++ [ UserOption { _userOptionTitle = "Let someone else decide"
                            , _userOptionExplanation = [ "Your pet will get a random name" ]
                            , _userOptionChoice = LetSomeoneElseDecide
                            }
               ]

    resolveType _ =
        ImmediateEvent

    resolveEvent keyEventPair (Just choice) =
        runWriterT . runMaybeT $ chooseToGiveName keyEventPair choice

    resolveEvent keyEventPair Nothing =
        runWriterT . runMaybeT $ chooseToGiveName keyEventPair LetSomeoneElseDecide


chooseToGiveName :: (MonadIO m, PersistQueryWrite backend
    , BaseBackend backend ~ SqlBackend) =>
    (NewsId, NamingPetEvent)
    -> NamingPetChoice
    -> MaybeT (WriterT [NamingPetResult] (ReaderT backend m)) (EventRemoval, [EventCreation])
chooseToGiveName (_, event) (GiveName name) = do
    pets <- (lift . lift) $ selectList [ PetName ==. name
                                       , PetOwnerId ==. _namingPetEventPersonId event
                                       ] []
    let pId = _namingPetEventPetId event

    if null pets
        then do
            lift $ tell [ PetNamed pId name ]
            (lift . lift) $ update pId [ PetName =. name ]
            return (RemoveOriginalEvent, [])
        else do
            g <- liftIO newStdGen
            lift $ tell [ RandomNameGiven pId (evalRand petNameM g) ]
            return (RemoveOriginalEvent, [])

chooseToGiveName (_, event) LetSomeoneElseDecide = do
    let pId = _namingPetEventPetId event
    g <- liftIO newStdGen
    lift $ tell [ RandomNameGiven pId (evalRand petNameM g) ]
    return (RemoveOriginalEvent, [])


-- | Map between pet name and user option
petNameOption :: PetName -> UserOption NamingPetChoice
petNameOption name =
    UserOption { _userOptionTitle = _unPetName name
               , _userOptionExplanation = [ "Your pet will be named to " ++ (_unPetName name) ]
               , _userOptionChoice = GiveName name
               }


$(deriveJSON defaultOptions ''ScurryingSoundsEvent)
$(deriveJSON defaultOptions ''ScurryingSoundsChoice)
$(deriveJSON defaultOptions ''ScurryingSoundsNews)
$(deriveJSON defaultOptions ''NamingPetEvent)
$(deriveJSON defaultOptions ''NamingPetChoice)
$(deriveJSON defaultOptions ''NamingPetNews)

makeLenses ''NamingPetNews
makeLenses ''ScurryingSoundsEvent
makePrisms ''ScurryingSoundsResult
makeLenses ''ScurryingSoundsNews
makeLenses ''NamingPetEvent