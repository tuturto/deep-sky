{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell            #-}

module Buildings
    ( BuildingInfo(..), buildingInfoType, buildingInfoName
    , buildingInfoCost, buildingInfoDescription, buildingInfoLevel
    , BuildingLevel(..), unBuildingLevel, building
    )
where

import Control.Lens ( makeLenses, makeWrapped )
import CustomTypes (BuildingType(..))
import Resources (RawResources(..), RawResource(..), ResourceCost)
import ClassyPrelude.Yesod   as Import

-- Types

data BuildingInfo = BuildingInfo
    { _buildingInfoType :: BuildingType
    , _buildingInfoName :: Text
    , _buildingInfoLevel :: BuildingLevel
    , _buildingInfoCost :: RawResources ResourceCost
    , _buildingInfoDescription :: Text
    }
    deriving (Show, Read, Eq)

newtype BuildingLevel = MkBuildingLevel { _unBuildingLevel :: Int }
    deriving (Show, Read, Eq, Ord, Num)

-- Instances

instance ToJSON BuildingInfo where
    toJSON (BuildingInfo bType bName bLevel bCost bDesc) =
        object [ "id" .= bType
               , "level" .= _unBuildingLevel bLevel
               , "name" .= bName
               , "cost" .= bCost
               , "description" .= bDesc
               ]

building :: BuildingType -> BuildingLevel -> BuildingInfo
building SensorStation level =
    BuildingInfo SensorStation "Sensor station" level (RawResources (MkRawResource 250) (MkRawResource 50) (MkRawResource 50))
        "Collection of various sensors, designed to scan the space"
building Farm level =
    BuildingInfo Farm "Farm" level (RawResources (MkRawResource 100) (MkRawResource 100) (MkRawResource 50))
        "Hydrophonic fram producing fresh food and other biological matter"
building ResearchComplex level =
    BuildingInfo ResearchComplex "Research complex" level (RawResources (MkRawResource 250) (MkRawResource 50) (MkRawResource 250))
        "High-tech research station filled with databanks and computers"
building NeutronDetector level =
    BuildingInfo NeutronDetector "Neutron detector" level (RawResources (MkRawResource 500) (MkRawResource 10) (MkRawResource 250))
        "Sensitive detected hidden deep inside a mountain, capable of detecting neutrons"
building ParticleAccelerator level =
    BuildingInfo ParticleAccelerator "Particle accelerator" level (RawResources (MkRawResource 500) (MkRawResource 10) (MkRawResource 250))
        "Large particle collider used to research origins of matter"
building BlackMatterScanner level =
    BuildingInfo BlackMatterScanner "Black matter scanner" level (RawResources (MkRawResource 750) (MkRawResource 50) (MkRawResource 500))
        "Super sensitive network of sensors and computers used to scan universe for black matter"
building GravityWaveSensor level =
    BuildingInfo GravityWaveSensor "Gravity wave sensor" level (RawResources (MkRawResource 1000) (MkRawResource 50) (MkRawResource 750))
        "Massive network of sensors used to hunt for gravity waves"


makeLenses ''BuildingLevel
makeWrapped ''BuildingLevel
makeLenses ''BuildingInfo
