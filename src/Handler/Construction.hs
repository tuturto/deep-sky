{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}

module Handler.Construction
    ( getConstructionR, getApiBuildingsR, getApiPlanetConstQueueR, getApiBuildingConstructionIdR
    , putApiBuildingConstructionIdR, deleteApiBuildingConstructionIdR, postApiBuildingConstructionR
    )
    where

import Import
import Control.Lens
    ( (^.), (^?), (&), (.~), to, folded, maximumOf, _Just, view
    , outside
    )
import Common
    ( apiRequireFaction, fromDto, apiNotFound, apiInvalidArgs, apiOk
    , apiRequireViewSimulation, apiRequireOpenSimulation
    )
import Buildings (building, BuildingLevel(..))
import CustomTypes (BuildingType(..))
import Dto.Construction
    ( buildingConstructionToDto, shipConstructionToDto, ConstructionDto(..), constructionIndex
    , bcdtoPlanet, _BuildingConstructionDto, bcdtoIndex
    )
import Construction ( unMaybeBuildingConstruction )
import Handler.Home ( getNewHomeR )


getConstructionR :: Handler Html
getConstructionR = getNewHomeR


-- | Retrieve list of buildings available for given faction as JSON
--   In case multiple levels of a building are available, all are reported
getApiBuildingsR :: Handler Value
getApiBuildingsR = do
    (uId, _, _, _) <- apiRequireFaction
    _ <- apiRequireViewSimulation uId
    -- TODO: implement properly
    let json = toJSON [ building SensorStation $ MkBuildingLevel 1
                      , building ResearchComplex $ MkBuildingLevel 1
                      , building Farm $ MkBuildingLevel 1
                      , building ParticleAccelerator $ MkBuildingLevel 1
                      , building NeutronDetector $ MkBuildingLevel 1
                      , building BlackMatterScanner $ MkBuildingLevel 1
                      , building GravityWaveSensor $ MkBuildingLevel 1
                      ]
    return json


-- | Retrieve construction queue of a given planet as JSON
getApiPlanetConstQueueR :: PlanetId -> Handler Value
getApiPlanetConstQueueR planetId = do
    (uId, _, _, _) <- apiRequireFaction
    _ <- apiRequireViewSimulation uId
    constructions <- runDB $ loadPlanetConstructionQueue planetId
    return $ toJSON constructions


-- | Retrieve details of given building construction
getApiBuildingConstructionIdR :: BuildingConstructionId -> Handler Value
getApiBuildingConstructionIdR cId = do
    (uId, _, _, _) <- apiRequireFaction
    _ <- apiRequireViewSimulation uId
    loadedConst <- runDB $ get cId
    construction <- maybe apiNotFound return loadedConst
    return $ toJSON construction


-- | Create a new building construction
--   In case this method is called to insert ship nothing will be inserted
--   Returns current construction queue of the planet after the insert
postApiBuildingConstructionR :: Handler Value
postApiBuildingConstructionR = do
    (uId, _, _, _) <- apiRequireFaction
    _ <- apiRequireOpenSimulation uId
    msg <- requireCheckJsonBody
    -- TODO: validate permissions
    _ <- runDB $ createBuildingConstruction msg
    let pId = (const (PlanetKey 0) & outside _BuildingConstructionDto .~ view bcdtoPlanet) msg
    newConstructions <- runDB $ loadPlanetConstructionQueue pId
    return $ toJSON newConstructions


-- | Update existing building construction
--   In case this method is called to update ship construction http 400 error will be returned
putApiBuildingConstructionIdR :: BuildingConstructionId -> Handler Value
putApiBuildingConstructionIdR cId = do
    (uId, _, _, _) <- apiRequireFaction
    _ <- apiRequireOpenSimulation uId
    msg <- requireCheckJsonBody
    (newIndex, oldIndex, pId) <- validatePut msg cId
    let direction = if newIndex < oldIndex
                    then 1
                    else -1
    let upperBound = max oldIndex newIndex
    let lowerBound = min oldIndex newIndex
    newConstructions <- runDB $ do
                        -- move construction to the new index
                        _ <- update cId [ BuildingConstructionIndex =. newIndex ]
                        -- move building constructions that have their index between bounds
                        _ <- updateWhere [ BuildingConstructionPlanetId ==. pId
                                         , BuildingConstructionIndex >=. lowerBound
                                         , BuildingConstructionIndex <=. upperBound
                                         , BuildingConstructionId !=. cId ]
                                         [ BuildingConstructionIndex +=. direction ]
                        -- perform same move to ship constructions
                        _ <- updateWhere [ ShipConstructionPlanetId ==. Just pId
                                         , ShipConstructionIndex >=. lowerBound
                                         , ShipConstructionIndex <=. upperBound ]
                                         [ ShipConstructionIndex +=. direction ]
                        loadPlanetConstructionQueue pId

    return $ toJSON newConstructions


-- | Delete building construction
deleteApiBuildingConstructionIdR :: BuildingConstructionId -> Handler Value
deleteApiBuildingConstructionIdR cId = do
    (uId, _, _, _) <- apiRequireFaction
    _ <- apiRequireOpenSimulation uId
    loadedConst <- runDB $ get cId
    res <- runDB $ removeBuildingConstruction cId loadedConst
    return $ toJSON res



-- | Validate update message of building construction
--   Method will return new index, old index and key for planet if everything is ok
--   In case message doesn't actually move construction anywhere (old and new index are same)
--   http 200 is returned with current construction queue as content
--   In case there is problem with the message, appropriate http error will be returned
validatePut :: ConstructionDto -> BuildingConstructionId -> HandlerFor App (Int, Int, PlanetId)
validatePut msg cId = do
    -- TODO: use common error handling
    let i = (const 0 & outside _BuildingConstructionDto .~ view bcdtoIndex) msg
    newIndex <- if i < 0
                then apiInvalidArgs [ "negative building index" ]
                else return i
    loadedConst <- case msg of
                    ShipConstructionDto _ -> apiInvalidArgs [ "body contains ship" ]
                    BuildingConstructionDto _ -> runDB $ get cId
    construction <- maybe apiNotFound return loadedConst
    let oldIndex = construction ^. buildingConstructionIndex
    _ <- if oldIndex == newIndex
         then do
            let pId = (const (PlanetKey 0) & outside _BuildingConstructionDto .~ view bcdtoPlanet) msg
            queue <- runDB $ loadPlanetConstructionQueue pId
            apiOk queue
         else return []
    let pId = construction ^. buildingConstructionPlanetId
    return (newIndex, oldIndex, pId)


-- | transform building construction dto into building construction and save is into database
createBuildingConstruction :: (PersistQueryRead backend, MonadIO m,
                               PersistStoreWrite backend, BaseBackend backend ~ SqlBackend) =>
                              ConstructionDto -> ReaderT backend m (Maybe BuildingConstructionId)
createBuildingConstruction cDto = do
    let pId = cDto ^? _BuildingConstructionDto . bcdtoPlanet
    currentConstructions <- mapM loadPlanetConstructionQueue pId
    let f = currentConstructions :: Maybe [ConstructionDto]
    let nextIndex = fromMaybe 0 $ maximumOf (_Just . folded . to constructionIndex . to (+1)) f
    mapM (\x -> insert x { _buildingConstructionIndex = nextIndex }) $ fromDto cDto ^. unMaybeBuildingConstruction


-- | load construction queue of a given planet
loadPlanetConstructionQueue :: (PersistQueryRead backend,
                                MonadIO m, BaseBackend backend ~ SqlBackend) =>
                                PlanetId -> ReaderT backend m [ConstructionDto]
loadPlanetConstructionQueue planetId = do
    loadedBuildings <- selectList [ BuildingConstructionPlanetId ==. planetId ] []
    loadedShips <- selectList [ ShipConstructionPlanetId ==. Just planetId ] []
    let buildings = map buildingConstructionToDto loadedBuildings
    let ships = map shipConstructionToDto loadedShips
    let constructions = buildings ++ ships
    return constructions


-- | Remove building construction from database and update indexes of other buildings in the queue
removeBuildingConstruction :: (MonadIO m, PersistEntity record, PersistQueryWrite backend,
                               PersistEntityBackend record ~ BaseBackend backend,
                               BaseBackend backend ~ SqlBackend) =>
                               Key record -> Maybe BuildingConstruction -> ReaderT backend m [ConstructionDto]
removeBuildingConstruction bId (Just buildingInfo) = do
    let bIndex = buildingInfo ^. buildingConstructionIndex
    let planetId = buildingInfo ^. buildingConstructionPlanetId
    _ <- delete bId
    _ <- updateWhere [ BuildingConstructionPlanetId ==. planetId
                     , BuildingConstructionIndex >. bIndex ] [ BuildingConstructionIndex -=. 1 ]
    _ <- updateWhere [ ShipConstructionPlanetId ==. Just planetId
                     , ShipConstructionIndex >. bIndex ] [ ShipConstructionIndex -=. 1 ]
    loadPlanetConstructionQueue $ buildingInfo ^. buildingConstructionPlanetId


removeBuildingConstruction _ Nothing =
    return []

-- TODO:
-- and ships
-- and general clean up of code
