{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Fleet where

import Import
import Handler.Home ( getNewHomeR )

getFleetR :: Handler Html
getFleetR = getNewHomeR
