{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE FlexibleContexts           #-}

module Handler.Home
    ( getHomeR, getNewHomeR, postApiLogoutR, postApiLoginR )
where

import qualified Prelude as P
import Yesod
import Yesod.Auth.HashDB ( validateUser )
import Control.Lens ( (.~), (^?), (^.), _1, _2, _Just )
import Database.Persist.Sql ( toSqlKey )
import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import Import
import Common ( apiOk, apiForbidden, apiRequireViewSimulation, dayZero )
import CustomTypes ( UserIdentity(..), SystemStatus(..) )
import People.Import ( PersonReport, minimalPersonReport )
import People.Queries ( queryPerson )


-- | Default start page that does not contain client
getHomeR :: Handler Html
getHomeR = getNewHomeR


-- | Serve client and let it take care of what to display
getNewHomeR :: Handler Html
getNewHomeR = do
    -- make sure there is simulation status
    status <- runDB $ selectList ([] :: [Filter Simulation]) []
    when (length status == 0) $ do
        _ <- runDB $ insert $ Simulation
                    { _simulationCurrentTime = dayZero
                    , _simulationStatus = Offline
                    }
        return ()

    -- redirect
    defaultLayout $ do
        setTitle "Deep Sky"
        addScript $ StaticR js_client_js
        addStylesheet $ StaticR css_site_css
        $(widgetFile "newhome")


{-| Perform a login with username and password passed in JSON body.
    In case login is valid, load and return details of the logged in
    user and their selected avatar.
-}
postApiLoginR :: Handler Value
postApiLoginR = do
    req <- requireCheckJsonBody
    users <- runDB $ selectList [ UserIdent ==. loginRequestUserName req ] []

    let userName = loginRequestUserName req
    let pwd = loginRequestPassword req

    case users of
        userE : _ -> do
            let user = userE ^. entityValL
            isValid <- validateUser (UniqueUser userName) pwd
            if isValid
                then do
                    _ <- apiRequireViewSimulation $ userE ^. entityKeyL
                    liftHandler $ setCreds False $ Creds "hashdb" (_unUserIdentity userName) []

                    report <- case user ^. userAvatar of
                        Nothing -> do
                            return Nothing

                        Just _ -> do
                            status <- runDB $ get $ toSqlKey 1
                            aData <- runDB $ queryPerson $ entityKey userE
                            return $ minimalPersonReport <$> status ^? _Just . simulationCurrentTime
                                                         <*> aData ^? _Just . _1
                                                         <*> aData ^? _Just . _2

                    returnJson $
                        LoginResult
                        { loginResultUser = (entityValL . userPassword) .~ Nothing $ userE
                        , loginResultAvatar = report
                        }

                else do
                    apiForbidden "Username or password does not match"

        _ -> do
            apiForbidden "Username or password does not match"


data LoginRequest =
    LoginRequest
    { loginRequestUserName :: UserIdentity
    , loginRequestPassword :: Text
    }
    deriving (Show, Read, Eq)


data LoginResult =
    LoginResult
    { loginResultUser :: Entity User
    , loginResultAvatar :: Maybe PersonReport
    }
    deriving (Show, Read, Eq)


postApiLogoutR :: Handler Value
postApiLogoutR = do
    clearCreds False
    apiOk "logout ok"


$(deriveJSON defaultOptions { fieldLabelModifier = P.drop 12 } ''LoginRequest)
$(deriveJSON defaultOptions { fieldLabelModifier = P.drop 11 } ''LoginResult)