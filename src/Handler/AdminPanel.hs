{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE FlexibleContexts           #-}

module Handler.AdminPanel
    ( getAdminPanelR, getAdminApiSimulationR, putAdminApiSimulationR
    , getAdminApiPeopleR, getAdminPeopleR, getAdminPersonR, getAdminApiPersonR
    , putAdminApiPersonR, getAdminAddPersonR, postAdminApiAddPersonR
    , getAdminStatusR, getAdminApiSummaryR, postAdminApiWipeR, postAdminApiReseedR
    )
    where

import Control.Lens ( (#), (^?), (^.), _Just, to, view )
import Control.Monad.Fail
import Control.Monad.Random ( evalRand )
import Database.Persist.Sql (toSqlKey)
import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import Data.Either.Validation ( Validation(..), _Failure, _Success )
import Data.Maybe ( fromJust )
import qualified Database.Esqueleto as E
import Import
import qualified Prelude as P
import System.Random ( newStdGen )

import Common
    ( apiRequireAdmin, pagedResult, apiNotFound, apiRequireOfflineSimulation )
import Creators.Person
    ( PersonOptions(..), AgeOptions(..), generatePersonM, personOptionsAge )
import Creators.Wipe ( wipeDatabase, reseedDatabase )
import CustomTypes ( SystemStatus(..) )
import Errors ( ECode, resourceNotFound )
import Handler.Home ( getNewHomeR )
import MenuHelpers ( starDate )
import People.Data
    ( StatScore(..), firstName, cognomen, familyName, regnalNumber, FamilyName(..)
    , FirstName(..), Cognomen(..)
    )
import People.Errors
    ( ageBracketStartIsGreaterThanEnd, regnalNumberIsLessThanZero, familyNameIsEmpty
    , cognomenIsEmpty, firstNameIsEmpty, dateOfBirthIsInFuture, couldNotConfirmDateOfBirth
    , statIsTooLow
    )
import Simulation.Errors
    ( turnProcessingAndStateChangeDisallowed, deltaTIsTooBig, simulationStatusNotFound )
import Simulation.Main ( processTurn )


-- | Get admin panel
getAdminPanelR :: Handler Html
getAdminPanelR = getNewHomeR


-- | Get people listing for admins
getAdminPeopleR :: Handler Html
getAdminPeopleR = getNewHomeR

-- | Get single person for admins
getAdminPersonR :: PersonId -> Handler Html
getAdminPersonR _ = getNewHomeR


-- | Add new person
getAdminAddPersonR :: Handler Html
getAdminAddPersonR = getNewHomeR


-- | Get admin status page
getAdminStatusR :: Handler Html
getAdminStatusR = getNewHomeR


-- | Get current state of simulation
getAdminApiSimulationR :: Handler Value
getAdminApiSimulationR = do
    _ <- apiRequireAdmin
    status <- runDB $ get $ toSqlKey 1 :: HandlerFor App (Maybe Simulation)
    when (isNothing status) $ do
        raiseIfErrors [ simulationStatusNotFound ]
    return $ toJSON status


-- | Update current state of simulation
putAdminApiSimulationR :: Handler Value
putAdminApiSimulationR = do
    _ <- apiRequireAdmin
    msg <- requireCheckJsonBody
    status <- runDB $ get $ toSqlKey 1
    _ <- raiseIfFailure $ validateSimulationPut status msg

    -- update simulation status
    -- at later point we might want to do this in a separate process
    -- and return from server immediately
    when (status ^? _Just . simulationCurrentTime /= (msg ^. simulationCurrentTime . to Just)) $ do
        runDB $ update (toSqlKey 1) [ SimulationStatus =. ProcessingTurn ]
        _ <- runDB $ processTurn
        runDB $ update (toSqlKey 1) [ SimulationStatus =. Online ]
        return ()

    when (status ^? _Just . simulationStatus /= (msg ^. simulationStatus . to Just)) $ do
        _ <- runDB $ update (toSqlKey 1) [ SimulationStatus =. (msg ^. simulationStatus) ]
        return ()

    -- load and return simulation status
    finalStatus <- runDB $ get $ toSqlKey 1 :: HandlerFor App (Maybe Simulation)
    return $ toJSON finalStatus


-- | All people in the simulation
-- supports query parameters skip and take
getAdminApiPeopleR :: Handler Value
getAdminApiPeopleR = do
    _ <- apiRequireAdmin
    skipParamM <- lookupGetParam "skip"
    takeParamM <- lookupGetParam "take"
    let skipParam = maybe 0 id (join $ fmap readMay skipParamM)
    let takeParam = maybe 60000 id (join $ fmap readMay takeParamM)

    people <- runDB $ selectList [] [ Asc PersonId
                                    , OffsetBy skipParam
                                    , LimitTo takeParam
                                    ]

    return $ toJSON $ pagedResult skipParam takeParam people


-- | Details of single person
getAdminApiPersonR :: PersonId -> Handler Value
getAdminApiPersonR pId = do
    _ <- apiRequireAdmin
    person <- runDB $ get pId
    when (isNothing person) apiNotFound

    return $ toJSON (Entity pId $ fromJust person)


-- | Update details of single person
putAdminApiPersonR :: PersonId -> Handler Value
putAdminApiPersonR pId = do
    _ <- apiRequireAdmin
    msg <- requireCheckJsonBody
    errors <- runDB $ updatePerson pId msg
    _ <- raiseIfErrors errors
    returnJson (Entity pId msg)


-- | Create new person
postAdminApiAddPersonR :: Handler Value
postAdminApiAddPersonR = do
    _ <- apiRequireAdmin
    msg <- requireCheckJsonBody
    date <- runDB $ starDate
    _ <- raiseIfFailure $ validateAddPerson msg
    g <- liftIO newStdGen
    let person = evalRand (generatePersonM date msg) g
    pId <- runDB $ insert person
    returnJson (Entity pId person)


-- | Get summary of simulation status
getAdminApiSummaryR :: Handler Value
getAdminApiSummaryR = do
    _ <- apiRequireAdmin
    res <- runDB $ getSimulationSummary
    returnJson(res)


-- | Request database wipe
postAdminApiWipeR :: Handler Value
postAdminApiWipeR = do
    _ <- apiRequireAdmin
    _ <- apiRequireOfflineSimulation
    _ <- runDB $ wipeDatabase
    res <- runDB $ getSimulationSummary
    returnJson(res)


-- | Request reseed of database
postAdminApiReseedR :: Handler Value
postAdminApiReseedR = do
    _ <- apiRequireAdmin
    _ <- apiRequireOfflineSimulation
    _ <- runDB $ reseedDatabase
    res <- runDB $ getSimulationSummary
    returnJson(res)


getSimulationSummary :: (MonadIO m, MonadFail m,
   BackendCompatible SqlBackend backend,
   PersistQueryRead backend,
   PersistUniqueRead backend,
   BaseBackend backend ~ SqlBackend)
   => ReaderT backend m SimulationSummary
getSimulationSummary = do
    [E.Value planets] <- E.select $
                E.from $ \(_ :: E.SqlExpr (Entity Planet)) -> do
                    return E.countRows

    [E.Value users] <- E.select $
                E.from $ \(_ :: E.SqlExpr (Entity User)) -> do
                    return E.countRows

    [E.Value factions] <- E.select $
                E.from $ \(_ :: E.SqlExpr (Entity Faction)) -> do
                    return E.countRows

    [E.Value starSystems] <- E.select $
                E.from $ \(_ :: E.SqlExpr (Entity StarSystem)) -> do
                    return E.countRows

    return $ SimulationSummary
                { simulationSummaryUsers = users
                , simulationSummaryFactions = factions
                , simulationSummaryPlanets = planets
                , simulationSummaryStarSystems = starSystems
                }


-- | Temporary place for now
data SimulationSummary = SimulationSummary
    { simulationSummaryUsers :: Int
    , simulationSummaryFactions :: Int
    , simulationSummaryPlanets :: Int
    , simulationSummaryStarSystems :: Int
     }
    deriving (Show, Read, Eq)


updatePerson :: (MonadIO m,
    PersistUniqueRead backend, PersistStoreWrite backend,
    BaseBackend backend ~ SqlBackend)
    => PersonId -> Person -> ReaderT backend m [ECode]
updatePerson pId msg = do
    person <- get pId
    simulation <- get $ toSqlKey 1
    let errors = validatePersonPut simulation person msg
    when (P.null $ concat $ errors ^? _Failure) $ do
        replace pId msg
    return $ concat $ errors ^? _Failure


-- | Given simulation status loaded from database, validate new simulation status
validateSimulationPut :: Maybe Simulation -> Simulation -> Validation [ECode] Simulation
validateSimulationPut old new =
    case old of
        Nothing ->
            _Failure # [ simulationStatusNotFound ]

        Just oldStatus ->
            pure new
                <* timeDifferenceIsNotTooBig oldStatus new
                <* onlyTimeOrStatusChanges oldStatus new


-- | Time difference between two steps in simulation should be exactly one
timeDifferenceIsNotTooBig :: Simulation -> Simulation -> Validation [ECode] Simulation
timeDifferenceIsNotTooBig old new =
    if dt == 0 || dt == 1
        then
            _Success # new
        else
            _Failure # [ deltaTIsTooBig ]
    where
        dt = new ^. simulationCurrentTime - old ^. simulationCurrentTime


-- | I's not allowed to process turn and change system status in one go
onlyTimeOrStatusChanges :: Simulation -> Simulation -> Validation [ECode] Simulation
onlyTimeOrStatusChanges old new =
    if dt /= 0 && (old ^. simulationStatus /= new ^. simulationStatus)
        then
            _Failure # [ turnProcessingAndStateChangeDisallowed ]
        else
            _Success # new
    where
        dt = new ^. simulationCurrentTime - old ^. simulationCurrentTime


-- | Validate updating person
validatePersonPut :: Maybe Simulation -> Maybe Person -> Person -> Validation [ECode] Person
validatePersonPut simulation old new =
    case old of
        Nothing ->
            _Failure # [ resourceNotFound ]

        Just _ ->
            pure new
                <* statsAreValid new
                <* dateOfBirthIsNotInFuture simulation new
                <* nameIsValid new


-- | Validate that all stats are ok
statsAreValid :: Person -> Validation [ECode] Person
statsAreValid person =
    pure person
        <* statIsZeroOrGreater (view personDiplomacy) "diplomacy" person
        <* statIsZeroOrGreater (view personMartial) "martial" person
        <* statIsZeroOrGreater (view personStewardship) "stewardship" person
        <* statIsZeroOrGreater (view personLearning) "learning" person
        <* statIsZeroOrGreater (view personIntrigue) "intrigue" person


-- | Stat should be zero or greater
statIsZeroOrGreater :: (Person -> StatScore a) -> Text -> Person -> Validation [ECode] Person
statIsZeroOrGreater stat name new =
    if stat new >= 0
        then
            _Success # new
        else
            _Failure # [ statIsTooLow name ]


-- | Person's date of birth should not be in future
dateOfBirthIsNotInFuture :: Maybe Simulation -> Person -> Validation [ECode] Person
dateOfBirthIsNotInFuture Nothing _ =
    _Failure # [ couldNotConfirmDateOfBirth ]

dateOfBirthIsNotInFuture (Just simulation) person =
    if person ^. personDateOfBirth <= simulation ^. simulationCurrentTime
        then
            _Success # person
        else
            _Failure # [ dateOfBirthIsInFuture ]


-- | Validate various aspects of person's name
nameIsValid :: Person -> Validation [ECode] Person
nameIsValid person =
    pure person
        <* firstNameIsNotEmpty person
        <* cognomenIsNotEmpty person
        <* familyNameIsNotEmpty person
        <* regnalNumberIsNotNegative person


-- | First name can't be empty text
firstNameIsNotEmpty :: Person -> Validation [ECode] Person
firstNameIsNotEmpty person =
    if person ^. personName . to (not . null . _unFirstName . firstName)
        then
            _Success # person
        else
            _Failure # [ firstNameIsEmpty ]


-- | Cognomen can't be empty text
cognomenIsNotEmpty :: Person -> Validation [ECode] Person
cognomenIsNotEmpty person =
    case person ^. personName . to cognomen of
        Nothing ->
            _Success # person

        Just s ->
            if (not . null . _unCognomen) s
                then
                    _Success # person
                else
                    _Failure # [ cognomenIsEmpty ]


-- | Family name can't be empty text
familyNameIsNotEmpty :: Person -> Validation [ECode] Person
familyNameIsNotEmpty person =
    case person ^. personName . to familyName of
        Nothing ->
            _Success # person

        Just s ->
            if (not . null . _unFamilyName) s
                then
                    _Success # person
                else
                    _Failure # [ familyNameIsEmpty ]


-- | Regnal number should be zero or greater
regnalNumberIsNotNegative :: Person -> Validation [ECode] Person
regnalNumberIsNotNegative person =
    case person ^. personName . to regnalNumber of
        Nothing ->
            _Success # person

        Just n ->
            if n >= 0
                then
                    _Success # person
                else
                    _Failure # [ regnalNumberIsLessThanZero ]


-- | Validate person add message
validateAddPerson :: PersonOptions -> Validation [ECode] PersonOptions
validateAddPerson opt =
        pure opt
            <* validateAgeOptions opt


validateAgeOptions :: PersonOptions -> Validation [ECode] PersonOptions
validateAgeOptions opt =
    case opt ^. personOptionsAge of
        Nothing ->
            _Success # opt

        Just (AgeBracket a b) ->
            if a <= b
                then _Success # opt
                else _Failure # [ ageBracketStartIsGreaterThanEnd ]

        Just (ExactAge _) ->
            _Success # opt


$(deriveJSON defaultOptions { fieldLabelModifier = drop 17 } ''SimulationSummary)
