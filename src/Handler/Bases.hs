{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}

module Handler.Bases
    ( getBaseR, getBasesR )
where

import Import
import Handler.Home ( getNewHomeR )


getBasesR :: Handler Html
getBasesR = getNewHomeR


getBaseR :: PlanetId -> Handler Html
getBaseR _ = getNewHomeR
