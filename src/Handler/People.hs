{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE FlexibleContexts           #-}

module Handler.People
    ( getApiPersonR, getPersonR, getPeopleR, getApiDemesneR, getApiPeopleR
    , getApiAvatarR, patchApiPersonR, validatePatchApiPersonR, PatchPersonRequest(..)
    , patchPersonRequestLifeFocus
    )
    where

import Import
import Control.Lens ( (^.), (^?), (.~), (&), _Just, to, makeLenses )
import Data.Maybe ( fromJust )
import Data.Either.Validation ( Validation(..) )
import Database.Persist.Sql ( toSqlKey )

import Data.Aeson.TH
    ( deriveJSON, defaultOptions, fieldLabelModifier, omitNothingFields )

import Common
    ( apiRequireFaction, apiNotFound, apiRequireViewSimulation, apiRequireAuthPair
    , pagedResult, apiRequireOpenSimulation
    )
import CustomTypes ( StarDate, Age(..), age )
import Errors
    ( ECode, insufficientRights )
import MenuHelpers ( starDate )
import People.Data ( LifeFocus )
import People.Errors ( canNotChangeLifeFocusSoSoon, canNotReselectSameLifeFocus )
import People.Import
    ( PersonReport(..), personReport, demesneReport, minimalPersonReport )
import People.Queries
    ( queryPerson, GetApiPeopleQuery(..), RulerParam(..), fromBool
    , validateGetApiPeopleR, freeAvatarInfo, queryPeople
    )
import Handler.Home ( getNewHomeR )
import Simulation.Errors ( simulationStatusNotFound )


-- | serve client program and have it started showing person details
getPersonR :: PersonId -> Handler Html
getPersonR _ = getNewHomeR


-- | serve client program and have it started showing database
getPeopleR :: Handler Html
getPeopleR = getNewHomeR


-- | Information of single person, taking intel level into account
getApiPersonR :: PersonId -> HandlerFor App Value
getApiPersonR pId = do
    (uId, _, avatar, fId) <- apiRequireFaction
    _ <- apiRequireViewSimulation uId
    target <- runDB $ get pId
    when (isNothing target) apiNotFound

    report <- runDB $ personReport avatar fId (Entity pId (fromJust target))

    returnJson report


{- | API for updating person details.

    This method excepts 'PatchPersonRequest' as a body of the request and
    returns 'People.Import.PersonReport' in the body of the response.

    Player is allowed to update only their own data.
-}
patchApiPersonR :: PersonId -> HandlerFor App Value
patchApiPersonR pId = do
    (uId, user, _, fId) <- apiRequireFaction
    _ <- apiRequireOpenSimulation uId
    today <- runDB $ starDate
    msg <- requireCheckJsonBody
    target <- runDB $ get pId
    when (isNothing target) apiNotFound
    let avatarE = Entity pId (fromJust target)

    _ <- raiseIfFailure $ validatePatchApiPersonR (uId, user, avatarE, msg, today)

    let newAvatar = updatePersonData today avatarE msg
    _ <- runDB $ replace (newAvatar ^. entityKeyL) $ newAvatar ^. entityValL

    target' <- runDB $ get pId
    let avatarE' = Entity pId (fromJust target')

    report <- runDB $ personReport avatarE' fId avatarE'
    returnJson report


{- | Validate PATCH request for person.
     Key User and User pair define player who wants to do the update
     Entity Person is the person being updated
     PatchPersonRequest contains details of the update
     StarDate is current star date
-}
validatePatchApiPersonR :: (Key User, User, Entity Person, PatchPersonRequest, StarDate)
    -> Validation [ECode] (Key User, User, Entity Person, PatchPersonRequest, StarDate)
validatePatchApiPersonR params = do
    pure params
        *> canOnlyUpdateOwnAvatar params
        *> focusCanChangeEveryFiveYears params
        *> canNotSelectSameFocus params


canOnlyUpdateOwnAvatar :: (Key User, User, Entity Person, PatchPersonRequest, StarDate)
    -> Validation [ECode] (Key User, User, Entity Person, PatchPersonRequest, StarDate)
canOnlyUpdateOwnAvatar params@(_, user, avatarE, _, _) =
    case user ^? userAvatar . _Just . to (\x -> x == avatarE ^. entityKeyL) of
        Nothing ->
            Failure [ insufficientRights ]

        Just False ->
            Failure [ insufficientRights ]

        Just True ->
            Success params


{- | A person can change their life focus every five years. Sooner than that is not allowed. -}
focusCanChangeEveryFiveYears :: (Key User, User, Entity Person, PatchPersonRequest, StarDate)
    -> Validation [ECode] (Key User, User, Entity Person, PatchPersonRequest, StarDate)
focusCanChangeEveryFiveYears params@(_, _, avatarE, msg, today) =
    case _patchPersonRequestLifeFocus msg of
        Nothing ->
            Success params

        Just _ ->
            case (avatarE ^. entityValL . personLifeFocusChosen) of
                Nothing ->
                    Success params

                Just changeDate ->
                    if age changeDate today >= MkAge 5
                        then
                            Success params

                        else
                            Failure [ canNotChangeLifeFocusSoSoon changeDate today ]


canNotSelectSameFocus :: (Key User, User, Entity Person, PatchPersonRequest, StarDate)
    -> Validation [ECode] (Key User, User, Entity Person, PatchPersonRequest, StarDate)
canNotSelectSameFocus params@(_, _, avatarE, msg, _) =
    case _patchPersonRequestLifeFocus msg of
        Nothing ->
            Success params

        Just newFocus ->
            if avatarE ^. entityValL . personLifeFocus == newFocus
                then
                    Failure [ canNotReselectSameLifeFocus newFocus ]
                else
                    Success params


data PatchPersonRequest = PatchPersonRequest
    { _patchPersonRequestLifeFocus :: Maybe (Maybe LifeFocus)
    }
    deriving (Show, Read, Eq)


updatePersonData :: StarDate -> Entity Person -> PatchPersonRequest -> Entity Person
updatePersonData today personE request =
    personE & entityValL . personLifeFocus .~ focus
            & entityValL . personLifeFocusChosen .~ focusChangeDate
    where
        (focus, focusChangeDate) =
            case _patchPersonRequestLifeFocus request of
                Nothing ->
                    ( personE ^. entityValL . personLifeFocus
                    , personE ^. entityValL . personLifeFocusChosen
                    )

                Just Nothing ->
                    ( Nothing
                    , Just today
                    )

                Just value ->
                    ( value
                    , Just today
                    )


queryAvatarReport :: (YesodPersist site,
    BackendCompatible SqlBackend (YesodPersistBackend site),
    PersistQueryRead (YesodPersistBackend site),
    PersistUniqueRead (YesodPersistBackend site),
    BaseBackend (YesodPersistBackend site) ~ SqlBackend) =>
    Key User -> User -> HandlerFor site (Maybe PersonReport)
queryAvatarReport uId user = do
    case user ^. userAvatar of
        Nothing -> do
            return Nothing

        Just _ -> do
            status <- runDB $ get $ toSqlKey 1
            aData <- runDB $ queryPerson uId
            return $ minimalPersonReport <$> status ^? _Just . simulationCurrentTime
                                         <*> (fst <$> aData)
                                         <*> (snd <$> aData)


-- | Information of avatar of currently logged in user
getApiAvatarR :: HandlerFor App Value
getApiAvatarR = do
    (uId, user) <- apiRequireAuthPair
    _ <- apiRequireViewSimulation uId

    report <- queryAvatarReport uId user

    returnJson report


{-| Query people
    Supports following query parameters:
    - skip :: Natural - how many results to skip over
    - take :: Natural - how many results to take
    - npc :: Bool - search only for NPCs
    - ruler :: Bool - true = search only rulers, false = search only not rulers, missing = search both
-}
getApiPeopleR :: HandlerFor App Value
getApiPeopleR = do
    (uId, user) <- apiRequireAuthPair
    _ <- apiRequireViewSimulation uId

    skipParamM <- lookupGetParam "skip"
    takeParamM <- lookupGetParam "take"
    npcParamM <- lookupGetParam "npc"
    rulerParamM <- lookupGetParam "ruler"

    let skipParam = fromMaybe 0 $ skipParamM >>= readMay
    let takeParam = fromMaybe 0 $ takeParamM >>= readMay

    let params = GetApiPeopleQuery
                    { getApiPeopleQuerySkip = skipParam
                    , getApiPeopleQueryTake = takeParam
                    , getApiPeopleQueryNpc = npcParamM >>= readMay >>= liftM fromBool
                    , getApiPeopleQueryRulers = fromMaybe BothRulersAndNonRulers $ rulerParamM >>= readMay
                    , getApiPeopleQueryUser = user
                    }

    _ <- raiseIfFailure $ validateGetApiPeopleR params

    status <- runDB $ get $ toSqlKey 1
    when (isNothing status) $ do
        raiseIfErrors [ simulationStatusNotFound ]
    let t = fromJust $ status ^? _Just . simulationCurrentTime

    -- are we searching for potential avatars (user doesn't have avatar)?
    -- or are we searching people we have info on (user already has an avatar)?
    -- this affects on the data we want to report back
    avatars <- case user ^. userAvatar of
                    Nothing -> do
                        aData <- runDB $ freeAvatarInfo params
                        return $ (\x -> minimalPersonReport t (fst x) (snd x)) <$> aData

                    Just _ -> do
                        aData <- runDB $ queryPeople params
                        return $ (\x -> minimalPersonReport t (fst x) (snd x)) <$> aData

    returnJson $ pagedResult (fromIntegral skipParam) (fromIntegral takeParam) avatars


-- | Demesne of given character, according to intelligence level
getApiDemesneR :: PersonId -> HandlerFor App Value
getApiDemesneR pId = do
    (uId, _, avatar, _) <- apiRequireFaction
    _ <- apiRequireViewSimulation uId
    today <- runDB $ starDate
    person <- runDB $ get pId
    when (isNothing person) apiNotFound
    intel <- runDB $ selectList [ HumanIntelligencePersonId ==. pId
                                , HumanIntelligenceOwnerId ==. entityKey avatar
                                ] []
    planets <- runDB $ selectList [ PlanetRulerId ==. Just pId ] []
    systems <- runDB $ selectList [ StarSystemRulerId ==. Just pId ] []
    let report = demesneReport today (fromJust person) systems planets (entityVal <$> intel)
    return $ toJSON report


$(deriveJSON defaultOptions { fieldLabelModifier = drop 19
                            , omitNothingFields = True
                            } ''PatchPersonRequest)

makeLenses ''PatchPersonRequest
