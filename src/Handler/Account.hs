{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE FlexibleContexts           #-}

module Handler.Account
    ( getApiUserR, putApiUserR, postApiUserR )
    where

import Import
import Control.Lens ( (.~), (#), (^.), (&), to )
import Data.Either.Validation ( Validation(..), _Failure, _Success )
import Yesod.Auth.HashDB ( setPassword )

import Common ( apiRequireViewSimulation, apiRequireAuthPair
              , apiRequireOpenSimulation, apiInternalError )
import CustomTypes ( Role(..), StarDate(..), UserIdentity(..) )
import People.Data ( Gender(..), Sex(..), PersonName(..) )
import Errors ( ECode, insufficientRights, changingAvatarIsNotAllowed )
import Validation ( shouldNotBeNull )
import Space.Data ( SpectralType(..), LuminosityClass(..) )

{-| Get details of currently logged in user
-}
getApiUserR :: HandlerFor App Value
getApiUserR = do
    (uId, user) <- apiRequireAuthPair
    _ <- apiRequireViewSimulation uId

    returnJson $
        Entity uId user
            & (entityValL . userPassword) .~ Nothing


{-| Update details of currently logged in user
-}
putApiUserR :: HandlerFor App Value
putApiUserR = do
    (uId, user) <- apiRequireAuthPair
    _ <- apiRequireOpenSimulation uId

    msg <- requireCheckJsonBody
    _ <- raiseIfFailure $ validateUserPut (Entity uId user) msg

    new <- case msg ^. entityValL . userPassword of
            Nothing -> pure $
                        user
                            & userIdent .~ (msg ^. entityValL . userIdent)
                            & userAvatar .~ (msg ^. entityValL . userAvatar)

            Just s ->
                setPassword s $
                    user
                        & userIdent .~ (msg ^. entityValL . userIdent)
                        & userAvatar .~ (msg ^. entityValL . userAvatar)

    _ <- runDB $ replace uId new
    returnJson $ Entity uId (new & userPassword .~ Nothing)


{-| Create new user
-}
postApiUserR :: HandlerFor App Value
postApiUserR = do
    -- TODO: require open simulation?
    msg <- requireCheckJsonBody

    -- TODO: validate user post
    user <- case msg ^. userPassword of
                Nothing ->
                    apiInternalError
                Just s ->
                    setPassword s msg

    admins <- runDB $ selectList [ UserRoleRole ==. RoleAdministrator ] []
    uId <- if length admins < 1
            then do
                fid <- runDB $ createAchernar
                uId <- runDB $ createFounder fid user

                return uId
            else do
                runDB $ insert user

    newUser <- runDB $ get uId
    case newUser of
        Just new ->
            returnJson $ Entity uId (userPassword .~ Nothing $ new)

        Nothing ->
            apiInternalError


-- | Given user loaded from database, validate new user data
validateUserPut :: Entity User -> Entity User -> Validation [ECode] (Entity User)
validateUserPut old new =
    pure new
        <* canNotClaimDifferentAvatar old new
        <* canNotChangeOtherUsersData old new
        <* shouldNotBeNull (new ^. entityValL . userIdent . to _unUserIdentity) "User name" new


-- | User can not change their avatar
canNotClaimDifferentAvatar :: Entity User -> Entity User -> Validation [ECode] (Entity User)
canNotClaimDifferentAvatar old new =
    case (old ^. entityValL . userAvatar) of
        Nothing ->
            _Success # new

        Just _ ->
            if new ^. entityValL . userAvatar == old ^. entityValL . userAvatar
                then _Success # new
                else _Failure # [ changingAvatarIsNotAllowed ]


-- | Regular user can change only their own details
canNotChangeOtherUsersData :: Entity User -> Entity User -> Validation [ECode] (Entity User)
canNotChangeOtherUsersData old new =
    if old ^. entityKeyL == new ^. entityKeyL
        then _Success # new
        else _Failure # [ insufficientRights ]


createAchernar :: ReaderT SqlBackend (HandlerFor App) (Key Faction)
createAchernar = do
    sid <- insert $ StarSystem "Achernar" 0 0 Nothing
    _ <- insert $ Star "Achernar" sid G V
    pid <- insert $ Planet "Achernar Prime" 1 sid Nothing 1.0 Nothing
    fid <- insert $ Faction "Grey Council" sid 1500 1500 1500
    _ <- updateWhere [ PlanetId ==. pid] [ PlanetOwnerId =. Just fid ]

    return fid


createFounder :: (MonadIO m, PersistStoreWrite backend,
    BaseBackend backend ~ SqlBackend) =>
    Key Faction -> User -> ReaderT backend m (Key User)
createFounder fid user = do
    perId <- insert $ Person (RegalName "Grey" "Councillor" 1 (Just $ "the Founder"))
                                     Female Woman (MkStarDate 19792) 20 10 20 20 20 (Just fid)
                                     Nothing Nothing Nothing
                                     Nothing Nothing
    uId <- insert $ user & userAvatar .~ Just perId
    _ <- insert $ UserRole uId RoleAdministrator

    return uId
