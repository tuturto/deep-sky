{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}


module Validation
    ( shouldBeGreaterThan, shouldNotBeNull )
where

import Import
import Control.Lens ( (#) )
import qualified Data.Text
import Data.Either.Validation ( Validation(..), _Failure, _Success )
import Errors ( ECode, argumentOutOfRange )


-- | Validate that the numeric value is greater than another value
shouldBeGreaterThan :: (Num a, Ord a) => a -> a -> Text -> b -> Validation [ECode] b
shouldBeGreaterThan value anotherValue name result =
    if value > anotherValue
        then
            _Success # result
        else
            _Failure # [ argumentOutOfRange $ "'" ++ name ++ "' needs to be greater than zero" ]


-- | Validate that text is not null
shouldNotBeNull :: Text -> Text -> b -> Validation [ECode] b
shouldNotBeNull value name result =
    if not $ Data.Text.null value
        then
            _Success # result
        else
            _Failure # [ argumentOutOfRange $ "'" ++ name ++ "' should not be empty" ]
