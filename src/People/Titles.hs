{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}

module People.Titles ( shortTitle, longTitle )
    where

import Import
import People.Data ( ShortTitle(..), LongTitle(..) )
import Control.Lens ( (^.), to )

shortTitle :: Person -> Maybe ShortTitle
shortTitle person
    | person ^. personStarSystemTitle . to isJust = Just "Procurator"
    | person ^. personPlanetTitle . to isJust = Just "Praetor"
    | otherwise       = Nothing


longTitle :: Person -> Maybe LongTitle
longTitle _ =
    Just "unimplemented"