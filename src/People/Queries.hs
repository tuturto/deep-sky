{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE TypeFamilies               #-}

module People.Queries
    ( PersonLocationSum(..), OnPlanetData(..), OnUnitData(..)
    , getPersonLocation, getPlanetReport, queryPerson, _OnPlanet, _OnUnit
    , _UnknownLocation, onPlanetDataPersonId, onPlanetDataPlanetId, onPlanetDataStarSystemId
    , onUnitDataPersonId, onUnitDataUnitId, onUnitDataCrewPosition
    , GetApiPeopleQuery(..), RulerParam(..), NPCParam(..), fromBool, validateGetApiPeopleR
    , freeAvatarInfo, queryPeople
    )
    where

import Import
import Control.Lens ( makeLenses, makePrisms )
import Data.Aeson ( withObject )
import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import qualified Database.Esqueleto as E
import Control.Lens ( (^.), (^?), _Just )
import Common ( safeHead )
import Report ( CollatedPlanetReport(..), collateReport )
import Units.Data ( CrewPosition(..) )

import Control.Lens ( (#), view )
import Data.Either.Validation ( Validation(..), _Failure, _Success )
import Numeric.Natural ( Natural )

import Errors ( ECode, incorrectQueryParameter )
import Validation ( shouldBeGreaterThan )

-- | Domain object for person location
data PersonLocationSum =
    OnPlanet OnPlanetData
    | OnUnit OnUnitData
    | UnknownLocation
    deriving (Show, Read, Eq)


data OnPlanetData = OnPlanetData
    { _onPlanetDataPersonId :: !PersonId
    , _onPlanetDataPlanetId :: !PlanetId
    , _onPlanetDataStarSystemId :: !StarSystemId
    }
    deriving (Show, Read, Eq)


data OnUnitData = OnUnitData
    { _onUnitDataPersonId :: !PersonId
    , _onUnitDataUnitId :: !UnitId
    , _onUnitDataCrewPosition :: !CrewPosition
    }
    deriving (Show, Read, Eq)


instance ToJSON PersonLocationSum where
    toJSON (OnPlanet details) =
        object [ "Tag" .= ("OnPlanet" :: Text)
               , "Contents" .= details
               ]

    toJSON (OnUnit details) =
        object [ "Tag" .= ("OnUnit" :: Text)
               , "Contents" .= details
               ]

    toJSON (UnknownLocation) =
        object [ "Tag" .= ("UnknownLocation" :: Text) ]


instance FromJSON PersonLocationSum where
    parseJSON = withObject "contents" $ \o -> do
        tag <- o .: "Tag"
        asum [ do
                guard (tag == ("OnPlanet" :: Text))
                contents <- o .: "Contents"
                return $ OnPlanet contents
             , do
                guard (tag == ("OnUnit" :: Text))
                contents <- o .: "Contents"
                return $ OnUnit contents
             , do
                guard (tag == ("UnknownLocation" :: Text))
                return UnknownLocation
             ]


-- | Get person location and activity from database
-- in case information is not found, return UnknownLocation
getPersonLocation :: (MonadIO m, BaseBackend backend ~ SqlBackend,
    BackendCompatible SqlBackend backend,
    PersistQueryRead backend, PersistUniqueRead backend) =>
    PersonId -> ReaderT backend m PersonLocationSum
getPersonLocation pId = do
    res <- E.select $
            E.from $ \(personLocation
                        `E.LeftOuterJoin` personOnPlanet
                        `E.LeftOuterJoin` personOnUnit
                        `E.LeftOuterJoin` crewAssignment) -> do
                E.on (crewAssignment E.?. CrewAssignmentPersonId E.==. (E.val $ Just pId))
                E.on (personOnUnit E.?. PersonOnUnitId E.==. personLocation E.?. PersonLocationOnUnit)
                E.on (personOnPlanet E.?. PersonOnPlanetId E.==. personLocation E.?. PersonLocationOnPlanet)
                E.where_ (personOnUnit E.?. PersonOnUnitPersonId E.==. (E.val $ Just pId)
                         E.||. personOnPlanet E.?. PersonOnPlanetPersonId E.==. (E.val $ Just pId))
                return (personLocation, personOnPlanet, personOnUnit, crewAssignment)

    let planetId = getPlanetId res
    planet <- mapM get planetId

    return $ locationResToLocationSum res (planet ^? _Just . _Just . planetStarSystemId)


{-| Map joined entitites into possible planet id
-}
getPlanetId :: [(Maybe (Entity PersonLocation), Maybe (Entity PersonOnPlanet), Maybe (Entity PersonOnUnit), Maybe (Entity CrewAssignment))]
    -> Maybe PlanetId
getPlanetId ((_, personOnPlanet, Nothing, Nothing):[]) =
    personOnPlanet ^? _Just . entityValL . personOnPlanetPlanetId

getPlanetId _ =
    Nothing


-- | Retrieve current planet report for faction
getPlanetReport :: (MonadIO m, BaseBackend backend ~ SqlBackend,
    BackendCompatible SqlBackend backend,
    PersistQueryRead backend, PersistUniqueRead backend) =>
    FactionId -> PlanetId -> ReaderT backend m CollatedPlanetReport
getPlanetReport fId pId = do -- TODO: move somewhere else
    pReps <- selectList [ PlanetReportFactionId ==. fId
                        , PlanetReportPlanetId ==. pId ] [ Desc PlanetReportDate ]

    return $ collateReport $ fmap entityVal pReps


-- | Translate data loaded from database into Maybe PersonLocationSum
-- makes assumption that only zero or one entries are given
-- multiple entries in the list will result UnknownLocation
locationResToLocationSum :: [(Maybe (Entity PersonLocation), Maybe (Entity PersonOnPlanet), Maybe (Entity PersonOnUnit), Maybe (Entity CrewAssignment))]
    -> Maybe StarSystemId
    -> PersonLocationSum
locationResToLocationSum ((Just _, Just personOnPlanet, Nothing, Nothing):[]) (Just sId)=
    OnPlanet details
    where
        details = OnPlanetData
                    { _onPlanetDataPersonId = poPlanet ^. personOnPlanetPersonId
                    , _onPlanetDataPlanetId = poPlanet ^. personOnPlanetPlanetId
                    , _onPlanetDataStarSystemId = sId
                    }
        poPlanet = entityVal personOnPlanet

locationResToLocationSum ((Just _, Nothing, Just personOnUnit, Just assignment):[]) _ =
    OnUnit details
    where
        details = OnUnitData
                    { _onUnitDataPersonId = poUnit ^. personOnUnitPersonId
                    , _onUnitDataUnitId = poUnit ^. personOnUnitUnitId
                    , _onUnitDataCrewPosition = assignment ^. entityValL . crewAssignmentPosition
                    }
        poUnit = entityVal personOnUnit

locationResToLocationSum _ _ =
    UnknownLocation


{- Load Person and their possible dynasty based on user Id.
   This will return Nothing, if specified user has not selected their avatar.
-}
queryPerson :: (MonadIO m, BackendCompatible SqlBackend backend,
    PersistQueryRead backend, PersistUniqueRead backend) =>
    Key User -> ReaderT backend m (Maybe (Entity Person, Maybe (Entity Dynasty)))
queryPerson uId  = do
    res <- E.select $
        E.from $ \(user `E.LeftOuterJoin` avatar `E.LeftOuterJoin` dynasty) -> do
            E.on (dynasty E.?. DynastyId E.==. avatar E.^. PersonDynastyId)
            E.on (E.just (avatar E.^. PersonId) E.==. (user E.^. UserAvatar))
            E.where_ (user E.^. UserId E.==. (E.val uId))
            return (avatar, dynasty)

    return $ safeHead res


data GetApiPeopleQuery = GetApiPeopleQuery
    { getApiPeopleQuerySkip :: Natural
    , getApiPeopleQueryTake :: Natural
    , getApiPeopleQueryNpc :: Maybe NPCParam
    , getApiPeopleQueryRulers :: RulerParam
    , getApiPeopleQueryUser :: User
    }


data RulerParam =
    OnlyRulers
    | OnlyNonRulers
    | BothRulersAndNonRulers
    deriving (Show, Read, Eq)


data NPCParam =
    OnlyNPCs
    | NoNPCFiltering
    deriving (Show, Read, Eq)


fromBool :: Bool -> NPCParam
fromBool True = OnlyNPCs
fromBool False = NoNPCFiltering


-- | Validate query parameters of getApiPeopleR
validateGetApiPeopleR :: GetApiPeopleQuery -> Validation [ECode] GetApiPeopleQuery
validateGetApiPeopleR params =
    pure params
        <* userWithoutAvatarCanSearchNPCs params
        <* shouldBeGreaterThan (getApiPeopleQueryTake params) 0 "take" params


-- | User can search for NPC avatars only when they don't have their own avatar
-- This typically happens in the beginning, when new user has created their account
-- and is searching for suitable in-world person to play as.
-- After user has selected their avatar, they are not allowed to filter their search
-- results based on NPC status. This is to stop human players from targeting NPCs.
userWithoutAvatarCanSearchNPCs :: GetApiPeopleQuery -> Validation [ECode] GetApiPeopleQuery
userWithoutAvatarCanSearchNPCs params =
    case (view userAvatar . getApiPeopleQueryUser) params of
        Nothing ->
            _Success # params
        Just _ ->
            case getApiPeopleQueryNpc params of
                Nothing ->
                    _Success # params

                Just NoNPCFiltering ->
                    _Success # params

                Just OnlyNPCs ->
                    _Failure # [ incorrectQueryParameter "NPC filtering not allowed for user with an avatar" ]


freeAvatarInfo :: (MonadIO m, BackendCompatible SqlBackend backend,
    PersistQueryRead backend, PersistUniqueRead backend) =>
    GetApiPeopleQuery -> ReaderT backend m [(Entity Person, Maybe (Entity Dynasty))]
freeAvatarInfo params = do
    res <- E.select $
        E.from $ \(avatar `E.LeftOuterJoin` dynasty) -> do
            E.on (dynasty E.?. DynastyId E.==. avatar E.^. PersonDynastyId)
            E.where_ $
                ((E.notExists $
                    E.from $ \(user) -> do
                        E.where_ (user E.^. UserAvatar E.==. E.just (avatar E.^. PersonId)))
                E.&&. (filterByRulerStatus avatar $ getApiPeopleQueryRulers params)
                )
            E.limit (fromInteger . toInteger $ getApiPeopleQueryTake params)
            E.offset (fromInteger . toInteger $ getApiPeopleQuerySkip params)
            return (avatar, dynasty)

    return res


queryPeople :: (MonadIO m, BackendCompatible SqlBackend backend,
    PersistQueryRead backend, PersistUniqueRead backend) =>
    GetApiPeopleQuery -> ReaderT backend m [(Entity Person, Maybe (Entity Dynasty))]
queryPeople params = do
    res <- E.select $
        E.from $ \(avatar `E.LeftOuterJoin` dynasty) -> do
            E.on (dynasty E.?. DynastyId E.==. avatar E.^. PersonDynastyId)
            E.where_ (filterByRulerStatus avatar $ getApiPeopleQueryRulers params)
            E.limit (fromInteger . toInteger $ getApiPeopleQueryTake params)
            E.offset (fromInteger . toInteger $ getApiPeopleQuerySkip params)
            return (avatar, dynasty)

    return res


filterByRulerStatus :: E.SqlExpr (Entity Person)
    -> RulerParam -> E.SqlExpr (E.Value Bool)
filterByRulerStatus avatar param =
    case param of
        BothRulersAndNonRulers ->
            (E.val (1 :: Int) E.==. E.val 1)

        OnlyRulers ->
            (avatar E.^. PersonPlanetTitle E.!=. E.val Nothing
             E.||. avatar E.^. PersonStarSystemTitle E.!=. E.val Nothing)

        OnlyNonRulers ->
            (avatar E.^. PersonPlanetTitle E.==. E.val Nothing
             E.&&. avatar E.^. PersonStarSystemTitle E.==. E.val Nothing)


$(deriveJSON defaultOptions { fieldLabelModifier = drop 13 } ''OnPlanetData)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 11 } ''OnUnitData)

makePrisms ''PersonLocationSum
makeLenses ''OnPlanetData
makeLenses ''OnUnitData
