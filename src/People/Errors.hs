{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE TemplateHaskell            #-}

module People.Errors
    ( PersonCreationError(..), statIsTooLow, couldNotConfirmDateOfBirth, dateOfBirthIsInFuture
    , firstNameIsEmpty, familyNameIsEmpty, cognomenIsEmpty, regnalNumberIsLessThanZero
    , ageBracketStartIsGreaterThanEnd, personCreationFailed, canNotChangeLifeFocusSoSoon
    , canNotReselectSameLifeFocus
    )
    where

import Import
import Data.Aeson.TH
import CustomTypes
    ( StarDate(..), Age(..), age, starDateToDisplayString, ageToDisplayString )
import Errors ( ErrorCodeClass(..), ECode(..) )
import People.Data ( LifeFocus(..) )

statIsTooLow :: Text -> ECode
statIsTooLow s = ECode $ StatIsTooLow s

couldNotConfirmDateOfBirth :: ECode
couldNotConfirmDateOfBirth = ECode CouldNotConfirmDateOfBirth

dateOfBirthIsInFuture :: ECode
dateOfBirthIsInFuture = ECode DateOfBirthIsInFuture

firstNameIsEmpty :: ECode
firstNameIsEmpty = ECode FirstNameIsEmpty

familyNameIsEmpty :: ECode
familyNameIsEmpty = ECode FamilyNameIsEmpty

cognomenIsEmpty :: ECode
cognomenIsEmpty = ECode CognomenIsEmpty

regnalNumberIsLessThanZero :: ECode
regnalNumberIsLessThanZero = ECode RegnalNumberIsLessThanZero

ageBracketStartIsGreaterThanEnd :: ECode
ageBracketStartIsGreaterThanEnd = ECode AgeBracketStartIsGreaterThanEnd

personCreationFailed :: ECode
personCreationFailed = ECode PersonCreationFailed


{-| Error codes relating to creation of a new person
-}
data PersonCreationError =
    StatIsTooLow Text
    | CouldNotConfirmDateOfBirth
    | DateOfBirthIsInFuture
    | FirstNameIsEmpty
    | FamilyNameIsEmpty
    | CognomenIsEmpty
    | RegnalNumberIsLessThanZero
    | AgeBracketStartIsGreaterThanEnd
    | PersonCreationFailed
    deriving (Show, Read, Eq)

instance ErrorCodeClass PersonCreationError where
    httpStatusCode = \case
        StatIsTooLow _ -> 400
        CouldNotConfirmDateOfBirth -> 500
        DateOfBirthIsInFuture -> 400
        FirstNameIsEmpty -> 400
        FamilyNameIsEmpty -> 400
        CognomenIsEmpty -> 400
        RegnalNumberIsLessThanZero -> 400
        AgeBracketStartIsGreaterThanEnd -> 400
        PersonCreationFailed -> 500

    description = \case
        StatIsTooLow s ->
            "Stat " ++ s ++ " is too low"

        CouldNotConfirmDateOfBirth ->
            "Could not confirm date of birth is later than current stardate"

        DateOfBirthIsInFuture ->
            "Date of birth is in future"

        FirstNameIsEmpty ->
            "First name is empty"

        FamilyNameIsEmpty ->
            "Family name is empty"

        CognomenIsEmpty ->
            "Cognomen is empty"

        RegnalNumberIsLessThanZero ->
            "Regnal number is less than zero"

        AgeBracketStartIsGreaterThanEnd ->
            "Age bracket start is greater than end"

        PersonCreationFailed ->
            "Person creation failed"


canNotChangeLifeFocusSoSoon :: StarDate -> StarDate -> ECode
canNotChangeLifeFocusSoSoon changed today =
    ECode $ CanNotChangeLifeFocusSoSoon changed (age changed today)


canNotReselectSameLifeFocus :: Maybe LifeFocus -> ECode
canNotReselectSameLifeFocus focus =
    ECode $ CanNotReselectSameLifeFocus focus


{-| Error codes relating to people in general
-}
data PersonError =
    CanNotChangeLifeFocusSoSoon StarDate Age
    | CanNotReselectSameLifeFocus (Maybe LifeFocus)
    deriving (Show, Read, Eq)


instance ErrorCodeClass PersonError where
    httpStatusCode = \case
        CanNotChangeLifeFocusSoSoon _ _ -> 400
        CanNotReselectSameLifeFocus _ -> 400

    description = \case
        CanNotChangeLifeFocusSoSoon changed _age ->
            -- TODO: better error message
            "Life focus can be changed only every five years. You last changed life focus on: "
                ++ starDateToDisplayString changed ++ ". It has been only " ++ ageToDisplayString _age ++ " years since."

        CanNotReselectSameLifeFocus _ ->
            "Reselecting same life focus is not allowed."


$(deriveJSON defaultOptions ''PersonCreationError)
$(deriveJSON defaultOptions ''PersonError)
