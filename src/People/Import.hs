{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances          #-}

module People.Import
    ( PersonReport(..), StatReport(..), RelationLink(..), TraitReport(..)
    , TraitDescription(..), TraitName(..), PersonLocationReport(..), personReport
    , demesneReport
    , relationsReport, knownLink, traitName, traitDescription
    , flipRelation, flipRelationType, locationReport
    , minimalPersonReport, personReportId, personReportName, personReportShortTitle
    , personReportLongTitle, personReportSex, personReportGender, personReportAge
    , personReportStats, personReportRelations, personReportIntelTypes
    , personReportDynasty, personReportAvatarOpinion, personReportOpinionOfAvatar
    , personReportTraits, personReportAvatar, personReportLocation, statReportDiplomacy
    , statReportMartial, statReportStewardship, statReportIntrigue, statReportLearning
    , _PlanetDemesne, _StarSystemDemesne, planetDemesneReportPlanetId
    , planetDemesneReportStarSystemId, planetDemesneReportName, planetDemesneReportFormalName
    , planetDemesneReport, starSystemDemesneReportStarSystemId, starSystemDemesneReportName
    , starSystemDemesneReportFormalName, starSystemDemesneReportDate, relationLinkName
    , relationLinkShortTitle, relationLinkLongTitle, relationLinkTypes, relationLinkId
    , relationLinkOpinion, dynastyReportId, dynastyReportName, traitReportName
    , traitReportDescription, traitReportType, traitReportValidUntil, unTraitName
    , unTraitDescription, _OnPlanetReport, _OnUnitReport, _UnknownLocationReport
    , onPlanetReportDataPlanetId, onPlanetReportDataStarSystemId, onPlanetReportDataPlanetName
    , onUnitReportDataUnitId, onUnitReportDataCrewPosition, onUnitReportDataUnitName
    , personReportLifeFocus, personReportLifeFocusStatus
    )
    where

import Import
import Control.Lens
    ( (&), (.~), (%~), _Just, (^..), (^?), (^.), to, folded, makeLenses
    , makePrisms, makeWrapped, ix
    )
import Data.Aeson ( Object, withObject, withText,  )
import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import Data.Aeson.Types ( Parser )
import Data.List ( nub )
import Common ( mkUniq )
import CustomTypes ( StarDate, Age(..), age )
import MenuHelpers ( starDate )
import People.Data
    ( PersonIntel(..), Diplomacy, Martial, Stewardship, Intrigue, Learning, StatScore
    , PersonName, Sex, Gender, DemesneName(..), ShortTitle(..), LongTitle(..)
    , RelationVisibility(..), RelationType(..), DynastyName(..), TraitType(..)
    , LifeFocus(..), LifeFocusStatus(..)
    )
import People.Titles ( shortTitle, longTitle )
import People.Opinion ( OpinionReport(..), OpinionFeeling(..), opinionReport )
import People.Queries
    ( PersonLocationSum(..), getPersonLocation, getPlanetReport, onUnitDataCrewPosition
    , onUnitDataUnitId, onPlanetDataStarSystemId, onPlanetDataPlanetId
    )
import Queries
    ( PersonRelationData(..), PersonDataLink(..), personDataLinkOriginatorIntelligence
    , personRelations, personDataLinkTargetIntelligence, personRelationDataPerson
    , personDataLinkRelation, personDataLinkPerson, personRelationDataLinks
    )
import Report ( cprName )
import Space.Data ( PlanetName(..), StarSystemName(..), unStarSystemName, unPlanetName )
import Units.Data ( UnitName(..), CrewPosition(..) )
import Units.Lenses ( unitNameL )
import Units.Queries ( getUnit )


data PersonReport = PersonReport
    { _personReportId :: !PersonId
    , _personReportName :: !PersonName
    , _personReportShortTitle :: !(Maybe ShortTitle)
    , _personReportLongTitle :: !(Maybe LongTitle)
    , _personReportSex :: !Sex
    , _personReportGender :: !Gender
    , _personReportAge :: !Age
    , _personReportStats :: !(Maybe StatReport)
    , _personReportRelations :: ![RelationLink]
    , _personReportIntelTypes :: ![PersonIntel]
    , _personReportDynasty :: !(Maybe DynastyReport)
    , _personReportAvatarOpinion :: !OpinionReport
    , _personReportOpinionOfAvatar :: !OpinionReport
    , _personReportTraits :: !(Maybe [TraitReport])
    , _personReportAvatar :: !Bool
    , _personReportLocation :: !PersonLocationReport
    , _personReportLifeFocus :: !(Maybe LifeFocus)
    , _personReportLifeFocusStatus :: !LifeFocusStatus
    } deriving (Show, Read, Eq)


data StatReport = StatReport
    { _statReportDiplomacy :: !(StatScore Diplomacy)
    , _statReportMartial :: !(StatScore Martial)
    , _statReportStewardship :: !(StatScore Stewardship)
    , _statReportIntrigue :: !(StatScore Intrigue)
    , _statReportLearning :: !(StatScore Learning)
    } deriving (Show, Read, Eq)


data DemesneReport =
    PlanetDemesne PlanetDemesneReport
    | StarSystemDemesne StarSystemDemesneReport
    deriving (Show, Read, Eq)


data PlanetDemesneReport = PlanetDemesneReport
    { _planetDemesneReportPlanetId :: !PlanetId
    , _planetDemesneReportStarSystemId :: !StarSystemId
    , _planetDemesneReportName :: !PlanetName
    , _planetDemesneReportFormalName :: !DemesneName
    , _planetDemesneReport :: !StarDate
    } deriving (Show, Read, Eq)


data StarSystemDemesneReport = StarSystemDemesneReport
    { _starSystemDemesneReportStarSystemId :: !StarSystemId
    , _starSystemDemesneReportName :: !StarSystemName
    , _starSystemDemesneReportFormalName :: !DemesneName
    , _starSystemDemesneReportDate :: !StarDate
    } deriving (Show, Read, Eq)


instance ToJSON DemesneReport where
    toJSON (PlanetDemesne report) =
        object [ "Tag" .= ("Planet" :: Text)
               , "PlanetId" .= _planetDemesneReportPlanetId report
               , "StarSystemId" .= _planetDemesneReportStarSystemId report
               , "Name" .= _planetDemesneReportName report
               , "FormalName" .= _planetDemesneReportFormalName report
               , "Date" .= _planetDemesneReport report
               ]

    toJSON (StarSystemDemesne report) =
        object [ "Tag" .= ("StarSystem" :: Text)
               , "StarSystemId" .= _starSystemDemesneReportStarSystemId report
               , "Name" .= _starSystemDemesneReportName report
               , "FormalName" .= _starSystemDemesneReportFormalName report
               , "Date" .= _starSystemDemesneReportDate report
               ]


-- | Build demesne report listing all holdings of a person, according to
-- given intelligence level
demesneReport :: StarDate
    -> Person
    -> [Entity StarSystem]
    -> [Entity Planet]
    -> [HumanIntelligence]
    -> [DemesneReport]
demesneReport today person systems planets intel =
    (planetReport today <$> incPlanets) ++ (systemReport today <$> incSystems)
    where
        incPlanets = filter (planetIncluded intel person) planets
        incSystems = filter (starSystemIncluded intel person) systems


-- | Shoudl planet be included according to HUMINT and primary title
-- if Demesne is included in intelligence, planet is included
-- otherwise it's included if it's either primary title or star system where
-- the planet resides is primary title
planetIncluded :: [HumanIntelligence] -> Person -> Entity Planet -> Bool
planetIncluded intel person planet =
        fullIntel
        || isPrimary
        || isRelatedTitle
    where
        fullIntel = any (\x -> x ^. humanIntelligenceLevel == Demesne) intel
        isPrimary = person ^. personPlanetTitle == planet ^. entityKeyL . to Just
        isRelatedTitle = person ^. personStarSystemTitle == planet ^. entityValL . planetStarSystemId . to Just


-- | Should a star system be included according to HUMINT and primary title
-- if Demesne is included in intelligence, star system is included
-- otherwise it's included if star system is set a primary title
starSystemIncluded :: [HumanIntelligence] -> Person -> Entity StarSystem -> Bool
starSystemIncluded intel person starSystem =
        fullIntel
        || isPrimary
    where
        fullIntel = any (\x -> x ^. humanIntelligenceLevel == Demesne) intel
        isPrimary = person ^. personStarSystemTitle == starSystem ^. entityKeyL . to Just


-- | Demesne report of given planet with timestamp
planetReport :: StarDate -> Entity Planet -> DemesneReport
planetReport date planet =
    PlanetDemesne $ PlanetDemesneReport
                        { _planetDemesneReportPlanetId = planet ^. entityKeyL
                        , _planetDemesneReportStarSystemId = planet ^. entityValL . planetStarSystemId
                        , _planetDemesneReportName = name
                        , _planetDemesneReportFormalName = MkDemesneName ("Colony of " ++ name ^. unPlanetName )
                        , _planetDemesneReport = date
                        }
    where
        name = planet ^. entityValL . planetName


-- | Demesne report of given star system with timestamp
systemReport :: StarDate -> Entity StarSystem -> DemesneReport
systemReport date system =
    StarSystemDemesne $ StarSystemDemesneReport
                            { _starSystemDemesneReportStarSystemId = system ^. entityKeyL
                            , _starSystemDemesneReportName = name
                            , _starSystemDemesneReportFormalName = MkDemesneName ("Province of " ++ name ^. unStarSystemName)
                            , _starSystemDemesneReportDate = date
                            }
    where
        name = system ^. entityValL . starSystemName


minimalPersonReport :: StarDate -> Entity Person -> Maybe (Entity Dynasty) -> PersonReport
minimalPersonReport today personE dynastyE =
    PersonReport { _personReportId = personE ^. entityKeyL
                 , _personReportAvatar = False
                 , _personReportName = person ^. personName
                 , _personReportShortTitle = shortTitle person
                 , _personReportLongTitle = longTitle person
                 , _personReportSex = person ^. personSex
                 , _personReportGender = person ^. personGender
                 , _personReportAge = age (person ^. personDateOfBirth) today
                 , _personReportStats = statReport person [minBound..]
                 , _personReportRelations = []
                 , _personReportIntelTypes = []
                 , _personReportDynasty =  dynastyReport <$> dynastyE
                 , _personReportTraits = Nothing
                 , _personReportAvatarOpinion = BaseOpinionReport NeutralFeeling
                 , _personReportOpinionOfAvatar = BaseOpinionReport NeutralFeeling
                 , _personReportLocation = UnknownLocationReport
                 , _personReportLifeFocus = Nothing
                 , _personReportLifeFocusStatus = CanNotChangeLifeFocus
                 }
    where
        person = entityVal personE


-- | Person report of given person and taking HUMINT level into account
createPersonReport :: StarDate
    -> Maybe (Entity Dynasty)
    -> [PersonTrait]
    -> [PersonIntel]
    -> [Relation]
    -> PersonLocationReport
    -> PersonId
    -> PersonRelationData
    -> PersonReport
createPersonReport today dynasty allTraits targetIntel relations location avatarId info =
    PersonReport { _personReportId = pId
                 , _personReportAvatar = pId == avatarId
                 , _personReportName = person ^. personName
                 , _personReportShortTitle = shortTitle person
                 , _personReportLongTitle = longTitle person
                 , _personReportSex = person ^. personSex
                 , _personReportGender = person ^. personGender
                 , _personReportAge = age (person ^. personDateOfBirth) today
                 , _personReportStats = statReport person targetIntelTypes
                 , _personReportRelations = relationsReport targetTraitTypes allTraits $ info ^. personRelationDataLinks
                 , _personReportIntelTypes = targetIntelTypes
                 , _personReportDynasty =  dynastyReport <$> dynasty
                 , _personReportTraits = if Traits `elem` targetIntel
                                            then Just $ traitReport <$> targetTraits
                                            else Nothing
                 , _personReportAvatarOpinion = opinionReport
                                                (mkUniq $ avatarTraits ^.. folded . personTraitType)
                                                [minBound..]
                                                targetTraitTypes
                                                targetRelIntelTypes
                                                targetRelations
                 , _personReportOpinionOfAvatar = opinionReport
                                                    targetTraitTypes
                                                    targetRelIntelTypes
                                                    (mkUniq $ avatarTraits ^.. folded . personTraitType)
                                                    [minBound..]
                                                    avatarRelations
                 , _personReportLocation = location
                 , _personReportLifeFocus  = if activityKnown (pId == avatarId) targetIntel
                                                then
                                                    person ^. personLifeFocus
                                                else
                                                    Nothing
                 , _personReportLifeFocusStatus = canChangeFocus (pId == avatarId) today $ person ^. personLifeFocusChosen
                 }
                 where
                    person = info ^. personRelationDataPerson . entityValL
                    pId = info ^. personRelationDataPerson . entityKeyL
                    targetIntelTypes = mkUniq targetIntel
                    targetRelIntelTypes = mkUniq $ info ^.. personRelationDataLinks
                                                    . folded . personDataLinkTargetIntelligence
                                                    . _Just . humanIntelligenceLevel
                    avatarRelations = flipRelation <$> targetRelations
                    targetRelations = filter (\x -> x ^. relationTargetId == avatarId) relations
                    avatarTraits = filter (\x -> x ^. personTraitPersonId == avatarId) allTraits
                    targetTraits = filter (\x -> x ^. personTraitPersonId == pId) allTraits
                    targetTraitTypes = mkUniq $ targetTraits ^.. folded. personTraitType


{- | Only own avatar's life focus can be changed and only every five years
-}
canChangeFocus :: Bool -> StarDate -> Maybe StarDate -> LifeFocusStatus
canChangeFocus avatar currentDate changeDate =
    if avatar
        then
            case changeDate of
                Nothing ->
                    CanChangeLifeFocus

                Just date ->
                    if age date currentDate >= MkAge 5
                        then
                            CanChangeLifeFocus
                        else
                            CanNotChangeLifeFocus
        else
            CanNotChangeLifeFocus

{- | Person's activity is know in two cases. Either the person is avatar of the
     player or there is enough PersonIntel about them (namely Activity).
-}
activityKnown :: Bool -> [PersonIntel] -> Bool
activityKnown avatar intel =
    avatar || Activity `elem` intel


-- | Relations of a specific person
-- public relations are always known
-- family relations are known if intel includes family relations or secret relations
-- secret relations are known only if intel includes secret relations
relationsReport :: [TraitType] -> [PersonTrait] -> [PersonDataLink] -> [RelationLink]
relationsReport originatorTraits allTraits links =
    mapMaybe (relationLink originatorTraits allTraits) grouped
    where
        known = filter knownLink links
        grouped = groupBy (\x y -> x ^. personDataLinkPerson . entityKeyL == y ^. personDataLinkPerson . entityKeyL) known


-- | Is this link known based on available human intelligence
knownLink :: PersonDataLink -> Bool
knownLink item =
    case item ^. personDataLinkRelation .relationVisibility of
        PublicRelation ->
            True

        FamilyRelation ->
            FamilyRelations `elem` intel
            || SecretRelations `elem` intel

        SecretRelation ->
            SecretRelations `elem` intel
    where
        intel = catMaybes [ item ^? personDataLinkTargetIntelligence . _Just . humanIntelligenceLevel
                          , item ^? personDataLinkOriginatorIntelligence . _Just . humanIntelligenceLevel
                          ]


-- | Collect all relations of given person and report them
relationLink :: [TraitType] -> [PersonTrait] -> [PersonDataLink] -> Maybe RelationLink
relationLink originatorTraits allTraits links =
    RelationLink <$> person ^? _Just . personName
                 <*> (shortTitle <$> person)
                 <*> (longTitle <$> person)
                 <*> Just types
                 <*> pId
                 <*> Just (opinionReport originatorTraits originatorIntel targetTraits targetIntel targetRelations)
    where
        person = links ^? ix 0 . personDataLinkPerson . entityValL :: Maybe Person
        pId = links ^? ix 0 . personDataLinkPerson . entityKeyL
        types = mkUniq $ links ^.. folded . personDataLinkRelation . relationType
        targetTraits = mkUniq $ mapMaybe (\x -> if x ^. personTraitPersonId . to Just == pId
                                                    then x ^. personTraitType . to Just
                                                    else Nothing)
                                         allTraits
        originatorIntel = mkUniq $ links ^.. traverse . personDataLinkOriginatorIntelligence . _Just . humanIntelligenceLevel
        targetIntel = mkUniq $ links ^.. traverse . personDataLinkTargetIntelligence . _Just . humanIntelligenceLevel
        targetRelations = nub $ links ^.. folded . personDataLinkRelation


-- | Stat report of given person and taking HUMINT level into account
-- if Stats level isn't available, no report is given
-- if Stats level is available, full report is given without errors
statReport :: Person -> [PersonIntel] -> Maybe StatReport
statReport person intel =
    if available
        then Just StatReport { _statReportDiplomacy = person ^. personDiplomacy
                             , _statReportMartial = person ^. personMartial
                             , _statReportStewardship = person ^. personStewardship
                             , _statReportIntrigue = person ^. personIntrigue
                             , _statReportLearning = person ^. personLearning
                             }
        else Nothing
    where
        available = Stats `elem` intel


data RelationLink = RelationLink
    { _relationLinkName :: !PersonName
    , _relationLinkShortTitle :: !(Maybe ShortTitle)
    , _relationLinkLongTitle :: !(Maybe LongTitle)
    , _relationLinkTypes :: ![RelationType]
    , _relationLinkId :: !PersonId
    , _relationLinkOpinion :: !OpinionReport
    } deriving (Show, Read, Eq)


data DynastyReport = DynastyReport
    { _dynastyReportId :: !DynastyId
    , _dynastyReportName :: !DynastyName
    } deriving (Show, Read, Eq)


dynastyReport :: Entity Dynasty -> DynastyReport
dynastyReport dynasty =
    DynastyReport
        { _dynastyReportId = dynasty ^. entityKeyL
        , _dynastyReportName = dynasty ^. entityValL . dynastyName
        }


data TraitReport = TraitReport
    { _traitReportName :: !TraitName
    , _traitReportDescription :: !TraitDescription
    , _traitReportType :: !TraitType
    , _traitReportValidUntil :: !(Maybe StarDate)
    } deriving (Show, Read, Eq)


newtype TraitName = MkTraitName { _unTraitName :: Text }
    deriving (Show, Read, Eq)


instance IsString TraitName where
    fromString = MkTraitName . fromString


instance ToJSON TraitName where
    toJSON = toJSON . _unTraitName


instance FromJSON TraitName where
    parseJSON =
        withText "trait name"
            (return . MkTraitName)


newtype TraitDescription = MkTraitDescription { _unTraitDescription :: Text }
    deriving (Show, Read, Eq)


instance IsString TraitDescription where
    fromString = MkTraitDescription . fromString


instance ToJSON TraitDescription where
    toJSON = toJSON . _unTraitDescription


instance FromJSON TraitDescription where
    parseJSON =
        withText "trait description"
            (return . MkTraitDescription)


traitReport :: PersonTrait -> TraitReport
traitReport trait =
    TraitReport
        { _traitReportName = trait ^. personTraitType . to traitName
        , _traitReportDescription = trait ^. personTraitType . to traitDescription
        , _traitReportType = trait ^. personTraitType
        , _traitReportValidUntil = trait ^. personTraitValidUntil
        }


traitName :: TraitType -> TraitName
traitName Brave = "Brave"
traitName Coward = "Coward"
traitName Chaste = "Chaste"
traitName Temperate = "Temperate"
traitName Charitable = "Charitable"
traitName Diligent = "Diligent"
traitName Patient = "Patient"
traitName Kind = "Kind"
traitName Humble = "Humble"
traitName Lustful = "Lustful"
traitName Gluttonous = "Gluttonous"
traitName Greedy = "Greedy"
traitName Slothful = "Slothful"
traitName Wroth = "Wroth"
traitName Envious = "Envious"
traitName Proud = "Proud"
traitName Ambitious = "Ambitious"
traitName Content = "Content"
traitName Cruel = "Cruel"
traitName Cynical = "Cynical"
traitName Deceitful = "Deceitful"
traitName Honest = "Honest"
traitName Shy = "Shy"


traitDescription :: TraitType -> TraitDescription
traitDescription Brave = "Brave person doesn't shy away from confrontation"
traitDescription Coward = "Cowardly person shyes away from confrontation"
traitDescription Chaste = "Virtuous or pure person refrains from unlawful sexual activity"
traitDescription Temperate = "Temperate person avoids extremeties in all matters"
traitDescription Charitable = "Charitable person willingly gives to those in need"
traitDescription Diligent = "Work in itself is good and sufficient rewards for one's efforts"
traitDescription Patient = "Steadfast can face long term difficulties and not lose their will"
traitDescription Kind = "Benevolent person treating others with respect"
traitDescription Humble = "Although we are all special, nobody is truly special"
traitDescription Lustful = "Lustful person chases carnal pleasures in excess"
traitDescription Gluttonous = "Lack of moderation in regards to food and drink"
traitDescription Greedy = "Person showing selfish desire for wealth and fortune"
traitDescription Slothful = "Laziness and lack of work has made this character indifferent"
traitDescription Wroth = "Uncontrolled feeling of anger, rage and hathred"
traitDescription Envious = "Coveting what's not yours"
traitDescription Proud = "Placing oneself above others"
traitDescription Ambitious = "Ambitious person has set their goals high"
traitDescription Content = "Content person is happy how things are currently"
traitDescription Cruel = "Indifference of suffering of others"
traitDescription Cynical = "This person has strong distrust on motives of others"
traitDescription Deceitful = "For this person, cheating and deceiving has become second nature"
traitDescription Honest = "Free from fraud and deception"
traitDescription Shy = "This person is nervous or uncomfortable with other people"


-- | Flipped relation where originator and target id has been swapped
-- and relation type flipped
flipRelation :: Relation -> Relation
flipRelation relation =
    relation & relationOriginatorId .~ relation ^. relationTargetId
             & relationTargetId .~ relation ^. relationOriginatorId
             & relationType %~ flipRelationType


-- | Inverse of relation type (Parent -> Child)
flipRelationType :: RelationType -> RelationType
flipRelationType Parent = Child
flipRelationType Child = Parent
flipRelationType Sibling = Sibling
flipRelationType StepParent = StepChild
flipRelationType StepChild = StepParent
flipRelationType StepSibling = StepSibling
flipRelationType Betrothed = Betrothed
flipRelationType Spouse = Spouse
flipRelationType ExSpouse = ExSpouse
flipRelationType Widow = Widow
flipRelationType Lover = Lover
flipRelationType ExLover = ExLover
flipRelationType Friend = Friend
flipRelationType Rival = Rival


data PersonLocationReport =
    OnPlanetReport OnPlanetReportData
    | OnUnitReport OnUnitReportData
    | UnknownLocationReport
    deriving (Show, Read, Eq)


data OnPlanetReportData = OnPlanetReportData
    { _onPlanetReportDataPlanetId :: !PlanetId
    , _onPlanetReportDataStarSystemId :: !StarSystemId
    , _onPlanetReportDataPlanetName :: !PlanetName
    } deriving (Show, Read, Eq)


data OnUnitReportData = OnUnitReportData
    { _onUnitReportDataUnitId :: !UnitId
    , _onUnitReportDataCrewPosition :: !(Maybe CrewPosition)
    , _onUnitReportDataUnitName :: !UnitName
    } deriving (Show, Read, Eq)


instance ToJSON PersonLocationReport where
    toJSON report =
        case report of
            OnPlanetReport details ->
                object [ "Tag" .= ("OnPlanet" :: Text)
                       , "Contents" .= toJSON details
                       ]

            OnUnitReport details ->
                object [ "Tag" .= ("OnUnit" :: Text)
                       , "Contents" .= toJSON details
                       ]

            UnknownLocationReport ->
                object [ "Tag" .= ("UnknownLocation" :: Text)
                       ]


instance FromJSON PersonLocationReport where
    parseJSON = withObject "Person location report" $ \o -> do
        tag <- o .: "Tag"
        parseReport tag o


parseReport :: Text -> Object -> Parser PersonLocationReport
parseReport "OnPlanet" o = do
    contents <- o .: "Contents"
    return $ OnPlanetReport contents

parseReport "OnUnit" o = do
    contents <- o .: "Contents"
    return $ OnUnitReport contents

parseReport "UnknownLocation" _ =
    return UnknownLocationReport

parseReport _ _ = mempty


personReport :: (MonadIO m, BaseBackend backend ~ SqlBackend,
    BackendCompatible SqlBackend backend,
    PersistQueryRead backend, PersistUniqueRead backend) =>
    Entity Person -> FactionId -> Entity Person -> ReaderT backend m PersonReport
personReport avatar fId target = do
    today <- starDate
    let avatarId = avatar ^. entityKeyL
    let pId = target ^. entityKeyL

    info <- personRelations target (avatar ^. entityKeyL)
    let dId = info ^. personRelationDataPerson . entityValL . personDynastyId
    dynasty <- mapM getEntity dId
    intel <- selectList [ HumanIntelligencePersonId ==. pId
                        , HumanIntelligenceOwnerId ==. avatarId ] []
    let intelTypes = mkUniq $ intel ^.. folded . entityValL . humanIntelligenceLevel
    targetRelations <- selectList [ RelationOriginatorId ==. pId ] []
    let pIds = pId : avatarId : mkUniq (targetRelations ^.. folded . entityValL . relationTargetId)
    allTraits <- selectList ( (PersonTraitPersonId <-. pIds) :
                                ( [PersonTraitValidUntil <=. Just today]
                                ||. [PersonTraitValidUntil ==. Nothing])
                            ) []

    location <- locationReport fId pId intelTypes

    let report = createPersonReport today
                                    (join dynasty)
                                    (allTraits ^.. folded . entityValL)
                                    intelTypes
                                    (targetRelations ^.. folded . entityValL)
                                    location
                                    avatarId
                                    info
    return report


locationReport :: (MonadIO m, BackendCompatible SqlBackend backend,
    PersistQueryRead backend, PersistUniqueRead backend,
    BaseBackend backend ~ SqlBackend) =>
    FactionId
    -> PersonId -> [PersonIntel] -> ReaderT backend m PersonLocationReport
locationReport fId pId intel = do
    location <- getPersonLocation pId
    if Location `elem` intel then (do
        case location of
            -- TODO: break into smaller pieces
            OnPlanet details -> do
                planet <- getPlanetReport fId $ details ^. onPlanetDataPlanetId
                return $
                    OnPlanetReport OnPlanetReportData
                        { _onPlanetReportDataPlanetId = details ^. onPlanetDataPlanetId
                        , _onPlanetReportDataStarSystemId = details ^. onPlanetDataStarSystemId
                        , _onPlanetReportDataPlanetName = fromMaybe "Unknown planet" $ planet ^. cprName
                        }

            OnUnit details -> do
                unit <- getUnit $ details ^. onUnitDataUnitId
                return $
                    OnUnitReport OnUnitReportData
                        { _onUnitReportDataUnitId = details ^. onUnitDataUnitId
                        , _onUnitReportDataCrewPosition = if Activity `elem` intel
                                                            then Just $ details ^. onUnitDataCrewPosition
                                                            else Nothing
                        , _onUnitReportDataUnitName = fromMaybe "Unknown unit" $ unit ^? ( _Just . unitNameL )
                        }

            UnknownLocation ->
                return UnknownLocationReport) else (do
        return UnknownLocationReport)


$(deriveJSON defaultOptions { fieldLabelModifier = drop 19 } ''OnPlanetReportData)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 17 } ''OnUnitReportData)

$(deriveJSON defaultOptions { fieldLabelModifier = drop 13 } ''PersonReport)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 11 } ''StatReport)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 13 } ''RelationLink)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 14 } ''DynastyReport)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 12 } ''TraitReport)

makeLenses ''PersonReport
makeLenses ''StatReport
makePrisms ''DemesneReport
makeLenses ''PlanetDemesneReport
makeLenses ''StarSystemDemesneReport
makeLenses ''RelationLink
makeLenses ''DynastyReport
makeLenses ''TraitReport
makeLenses ''TraitName
makeWrapped ''TraitName
makeLenses ''TraitDescription
makeWrapped ''TraitDescription
makePrisms ''PersonLocationReport
makeLenses ''OnPlanetReportData
makeLenses ''OnUnitReportData
