{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE NoImplicitPrelude          #-}

module Research.Import ( availableForResearch, isAvailable, researchOutput, researchReady
                       , antecedentsAvailable  )
    where


import Import
import Control.Lens ( (^.), (^..), folded, allOf, filtered, elemOf
                    , anyOf )
import CustomTypes ( BuildingType(..) )
import Research.Data ( TechTree(..), Research(..), TotalResearchScore(..)
                     , ResearchProduction(..), ResearchScore(..), ResearchCategory(..)
                     , Technology(..), researchAntecedents, unTechTree
                     , researchType, researchCategory, researchCost, unResearchScore
                     , totalResearchScoreSocial, totalResearchScoreNatural
                     , totalResearchScoreEngineering
                     )
import Research.Tree ( techMap )


-- | All research that has not yet been done, but is available based
-- on the given tech tree
availableForResearch :: TechTree -> [CompletedResearch] -> [Research]
availableForResearch tree completed =
    tree ^.. unTechTree . folded . filtered antecedentsDone
    where
        completedTech = completed ^.. folded . completedResearchType
        antecedentsDone tech = isAvailable completed tech
                            && notElem (tech ^. researchType) completedTech


-- | is given research available based on completed research
isAvailable :: [CompletedResearch] -> Research -> Bool
isAvailable completed research =
    allOf (researchAntecedents . folded) isCompleted research
    where
        isCompleted x =
            anyOf (folded . completedResearchType) (\y -> x == y) completed


-- | Research output of a building, takes account type and level
-- of the building.
researchOutput :: Building -> TotalResearchScore ResearchProduction
researchOutput b =
    case b ^. buildingType of
        ResearchComplex ->
            MkTotalResearchScore
                { _totalResearchScoreEngineering = MkResearchScore 10
                , _totalResearchScoreNatural = MkResearchScore 10
                , _totalResearchScoreSocial = MkResearchScore 10
                }

        ParticleAccelerator ->
            MkTotalResearchScore
                { _totalResearchScoreEngineering = MkResearchScore 15
                , _totalResearchScoreNatural = MkResearchScore 15
                , _totalResearchScoreSocial = MkResearchScore 0
                }

        _ ->
            mempty


-- | Is current research completed
researchReady :: CurrentResearch -> Bool
researchReady r =
    case research ^. researchCategory of
        Engineering _ ->
            completed (cost ^. totalResearchScoreEngineering) (r ^. currentResearchProgress)

        NaturalScience _ ->
            completed (cost ^. totalResearchScoreNatural) (r ^. currentResearchProgress)

        SocialScience _ ->
            completed (cost ^. totalResearchScoreSocial) (r ^. currentResearchProgress)

    where
        research = techMap $ r ^. currentResearchType
        cost = research ^. researchCost
        completed c done = c ^. unResearchScore <= done


-- | Is given list of known technology enough to satisfy antecedents of research
antecedentsAvailable :: [Technology] -> Research -> Bool
antecedentsAvailable known research =
    allOf (researchAntecedents . folded) (\x -> elemOf folded x known) research
