{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE NoImplicitPrelude          #-}

module Research.Data ( ResearchCategory(..), EngineeringSubField(..), NaturalScienceSubField(..)
                     , SocialScienceSubField(..), ResearchScore(..), ResearchTier(..)
                     , Technology(..), Research(..), TechTree(..), EngineeringCost(..)
                     , SocialScienceCost(..), NaturalScienceCost(..), TotalResearchScore(..)
                     , ResearchProduction(..), ResearchCost(..), ResearchLimit(..)
                     , TopResearchCategory(..), ResearchLeft(..), ResearchProgress(..)
                     , isEngineering, isSocialScience, isNaturalScience
                     , sameTopCategory, topCategory
                     , totalResearchScoreEngineering, totalResearchScoreNatural
                     , totalResearchScoreSocial, researchName, researchType, researchCategory
                     , researchAntecedents, researchCost, researchTier, researchProgressResearch
                     , researchProgressProgress, unResearchScore, unResearchTier, unTechTree
                     , unResearchLimit, _Engineering, _SocialScience, _NaturalScience
                     )
    where


import Control.Lens ( makeLenses, makePrisms, makeWrapped )
import Data.Aeson ( withScientific )
import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import Data.Scientific ( toBoundedInteger )
import Database.Persist.TH
import ClassyPrelude.Yesod   as Import


data ResearchCategory =
    Engineering EngineeringSubField
    | NaturalScience NaturalScienceSubField
    | SocialScience SocialScienceSubField
    deriving (Show, Read, Eq)


data TopResearchCategory =
    Eng
    | NatSci
    | SocSci
    deriving (Show, Read, Eq, Ord)


topCategory :: ResearchCategory -> TopResearchCategory
topCategory cat =
    case cat of
        (Engineering _) ->
            Eng

        (NaturalScience _) ->
            NatSci

        (SocialScience _) ->
            SocSci


isEngineering :: ResearchCategory -> Bool
isEngineering (Engineering _) = True
isEngineering _ = False


isNaturalScience :: ResearchCategory -> Bool
isNaturalScience (NaturalScience _) = True
isNaturalScience _ = False


isSocialScience :: ResearchCategory -> Bool
isSocialScience (SocialScience _) = True
isSocialScience _ = False


sameTopCategory :: ResearchCategory -> ResearchCategory -> Bool
sameTopCategory a b =
    topCategory a == topCategory b


data EngineeringSubField =
    Industry
    | Materials
    | Propulsion
    | FieldManipulation
    deriving (Show, Read, Eq)


data NaturalScienceSubField =
    Computing
    | EnergyManipulation
    | Particles
    | Biology
    deriving (Show, Read, Eq)


data SocialScienceSubField =
    MilitaryTheory
    | Statecraft
    | ShadowOps
    deriving (Show, Read, Eq)


newtype ResearchScore a = MkResearchScore { _unResearchScore :: Int }
    deriving (Show, Read, Eq, Ord, Num)


instance Semigroup (ResearchScore a) where
    (<>) (MkResearchScore b) (MkResearchScore c) = MkResearchScore (b + c)


instance Monoid (ResearchScore a) where
    mempty = MkResearchScore 0


data EngineeringCost = EngineeringCost
    deriving (Show, Read, Eq)


data NaturalScienceCost = NaturalScienceCost
    deriving (Show, Read, Eq)


data SocialScienceCost = SocialScienceCost
    deriving (Show, Read, Eq)


data ResearchCost = ResearchCost
    deriving (Show, Read, Eq)


data ResearchProduction = ResearchProduction
    deriving (Show, Read, Eq)


data ResearchLeft = ResearchLeft
    deriving (Show, Read, Eq)


data TotalResearchScore a = MkTotalResearchScore
    { _totalResearchScoreEngineering :: ResearchScore EngineeringCost
    , _totalResearchScoreNatural :: ResearchScore NaturalScienceCost
    , _totalResearchScoreSocial :: ResearchScore SocialScienceCost
    }
    deriving (Show, Read, Eq)


instance Semigroup (TotalResearchScore a) where
    (<>) (MkTotalResearchScore a0 b0 c0) (MkTotalResearchScore a1 b1 c1) =
        MkTotalResearchScore (a0 <> a1) (b0 <> b1) (c0 <> c1)


instance Monoid (TotalResearchScore a) where
    mempty = MkTotalResearchScore mempty mempty mempty


newtype ResearchTier = MkResearchTier { _unResearchTier :: Int }
    deriving (Show, Read, Eq, Ord, Num)


data Technology =
    HighSensitivitySensors -- various sensors
    | SideChannelSensors
    | HighTensileMaterials -- heavy armour
    | SatelliteTechnology
    -- hulls
    | BawleyHulls
    | YawlHulls
    | BilanderHulls
    | CogHulls
    | FreighterHulls
    | CraneShipHulls
    | CruiseLinerHulls
    | SatelliteLayerHulls
    | FlyboatHulls
    | BrigantineHulls
    | SchoonerHulls
    | BlackwallFrigateHulls
    | ClipperHulls
    | CaravelHulls
    | CorvetteHulls
    | FrigateHulls
    | GalleonHulls
    | ManOfWarHulls
    | MobileBaseHulls
    | StationHulls
    -- general military theory
    | ExplorerCorps
    -- motive systems
    | HoverCrafts
    deriving (Show, Read, Eq, Enum, Bounded, Ord)


data Research = MkResearch
    { _researchName :: Text
    , _researchType :: Technology
    , _researchCategory :: ResearchCategory
    , _researchAntecedents :: [Technology]
    , _researchCost :: TotalResearchScore ResearchCost
    , _researchTier :: ResearchTier
    }
    deriving (Show, Read, Eq)


data ResearchProgress = MkResearchProgress
    { _researchProgressResearch :: Research
    , _researchProgressProgress :: ResearchScore ResearchLeft
    }
    deriving (Show, Read, Eq)


newtype TechTree = MkTechTree { _unTechTree :: [Research] }
    deriving (Show, Read, Eq)


newtype ResearchLimit = MkResearchLimit { _unResearchLimit :: Int }
    deriving (Show, Read, Eq, Ord, Num)


instance ToJSON ResearchTier where
  toJSON = toJSON . _unResearchTier


instance FromJSON ResearchTier where
    parseJSON =
        withScientific "research tier"
                       (\x -> case toBoundedInteger x of
                            Nothing ->
                                return $ MkResearchTier 1

                            Just n ->
                                return $ MkResearchTier n)


instance ToJSON (ResearchScore a) where
    toJSON = toJSON . _unResearchScore


instance FromJSON (ResearchScore a) where
    parseJSON =
        withScientific "research score"
                       (\x -> case toBoundedInteger x of
                            Nothing ->
                                return $ MkResearchScore 1

                            Just n ->
                                return $ MkResearchScore n)


derivePersistField "ResearchCategory"
derivePersistField "Technology"
derivePersistField "EngineeringSubField"
derivePersistField "NaturalScienceSubField"
derivePersistField "SocialScienceSubField"
derivePersistField "TopResearchCategory"


$(deriveJSON defaultOptions ''ResearchCategory)
$(deriveJSON defaultOptions ''Technology)
$(deriveJSON defaultOptions ''EngineeringSubField)
$(deriveJSON defaultOptions ''NaturalScienceSubField)
$(deriveJSON defaultOptions ''SocialScienceSubField)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 19 } ''TotalResearchScore)
$(deriveJSON defaultOptions ''ResearchCost)
$(deriveJSON defaultOptions ''ResearchProduction)
$(deriveJSON defaultOptions ''EngineeringCost)
$(deriveJSON defaultOptions ''NaturalScienceCost)
$(deriveJSON defaultOptions ''SocialScienceCost)
$(deriveJSON defaultOptions ''ResearchLeft)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 9 } ''Research)
$(deriveJSON defaultOptions ''TopResearchCategory)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 17 } ''ResearchProgress)

makeLenses ''ResearchCategory
makePrisms ''ResearchCategory
makeLenses ''TopResearchCategory
makePrisms ''TopResearchCategory
makeLenses ''EngineeringSubField
makePrisms ''EngineeringSubField
makeLenses ''NaturalScienceSubField
makePrisms ''NaturalScienceSubField
makeLenses ''SocialScienceSubField
makePrisms ''SocialScienceSubField
makeLenses ''ResearchScore
makeWrapped ''ResearchScore
makeLenses ''ResearchTier
makeWrapped ''ResearchTier
makeLenses ''TotalResearchScore
makeLenses ''Technology
makePrisms ''Technology
makeLenses ''Research
makeLenses ''ResearchProgress
makeLenses ''TechTree
makeWrapped ''TechTree
makeLenses ''ResearchLimit
makeWrapped ''ResearchLimit
