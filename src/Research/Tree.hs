{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}

module Research.Tree ( techTree, techMap )
    where

import Research.Data ( Research(..), ResearchCategory(..), EngineeringSubField(..)
                     , SocialScienceSubField(..), TechTree(..), TotalResearchScore(..)
                     , ResearchTier(..), Technology(..), ResearchScore(..)
                     )

techTree :: TechTree
techTree =
    MkTechTree $ fmap techMap [minBound ..]


techMap :: Technology -> Research
techMap HighSensitivitySensors =
    MkResearch { _researchName = "High-gain sensors"
             , _researchType = HighSensitivitySensors
             , _researchCategory = Engineering FieldManipulation
             , _researchAntecedents = []
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

techMap SideChannelSensors =
    MkResearch { _researchName = "Side channel sensors"
             , _researchType = SideChannelSensors
             , _researchCategory = Engineering FieldManipulation
             , _researchAntecedents = [ HighSensitivitySensors ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

techMap SatelliteTechnology =
    MkResearch { _researchName = "Satellites"
             , _researchType = SatelliteTechnology
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

-- various hulls
-- working line
techMap BawleyHulls =
    MkResearch { _researchName = "Bawley hulls"
             , _researchType = BawleyHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

techMap YawlHulls =
    MkResearch { _researchName = "Yawl hulls"
             , _researchType = YawlHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ BawleyHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 750
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

techMap BilanderHulls =
    MkResearch { _researchName = "Bilander hulls"
             , _researchType = BilanderHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ YawlHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 1000
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 2
             }

techMap CogHulls =
    MkResearch { _researchName = "Cog hulls"
             , _researchType = CogHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ BilanderHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 1500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 2
             }

techMap FreighterHulls =
    MkResearch { _researchName = "Freighter hulls"
             , _researchType = FreighterHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ CogHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 2500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 3
             }

techMap CraneShipHulls =
    MkResearch { _researchName = "Crane ship hulls"
             , _researchType = CraneShipHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ CogHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 2500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 3
             }

techMap CruiseLinerHulls =
    MkResearch { _researchName = "Cruise liner hulls"
             , _researchType = CruiseLinerHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ CogHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 2500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 3
             }

techMap SatelliteLayerHulls =
    MkResearch { _researchName = "Satellite layer hulls"
             , _researchType = SatelliteLayerHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ CogHulls
                                     , SatelliteTechnology ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 2500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 3
             }

-- fast line
techMap FlyboatHulls =
    MkResearch { _researchName = "Flyboat hulls"
             , _researchType = FlyboatHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

techMap BrigantineHulls =
    MkResearch { _researchName = "Brigantine hulls"
             , _researchType = BrigantineHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ FlyboatHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 750
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

techMap SchoonerHulls =
    MkResearch { _researchName = "Schooner hulls"
             , _researchType = SchoonerHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ BrigantineHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 1000
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 2
             }

techMap BlackwallFrigateHulls =
    MkResearch { _researchName = "Blackwall frigate hulls"
             , _researchType = BlackwallFrigateHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ SchoonerHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 1500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 2
             }

techMap ClipperHulls =
    MkResearch { _researchName = "Clipper hulls"
             , _researchType = ClipperHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ BlackwallFrigateHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 2500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 3
             }

-- war line
techMap CaravelHulls =
    MkResearch { _researchName = "Caravel hulls"
             , _researchType = CaravelHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

techMap CorvetteHulls =
    MkResearch { _researchName = "Corvette hulls"
             , _researchType = CorvetteHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ CaravelHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 750
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

techMap FrigateHulls =
    MkResearch { _researchName = "Frigate hulls"
             , _researchType = FrigateHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ CorvetteHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 1000
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 2
             }

techMap GalleonHulls =
    MkResearch { _researchName = "Galleon hulls"
             , _researchType = GalleonHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ FrigateHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 1500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 2
             }

techMap ManOfWarHulls =
    MkResearch { _researchName = "Man-of-War hulls"
             , _researchType = ManOfWarHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ GalleonHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 2500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 3
             }

-- misc hulls
techMap StationHulls =
    MkResearch { _researchName = "Station hulls"
             , _researchType = StationHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = []
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 2500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 2
             }

techMap MobileBaseHulls =
    MkResearch { _researchName = "Mobilebase hulls"
             , _researchType = MobileBaseHulls
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ StationHulls ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 2500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 3
             }

techMap ExplorerCorps =
    MkResearch { _researchName = "Explorer corps"
             , _researchType = ExplorerCorps
             , _researchCategory = SocialScience MilitaryTheory
             , _researchAntecedents = [ ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 0
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 500
                                }
             , _researchTier = MkResearchTier 1
             }

-- armour plating
techMap HighTensileMaterials =
    MkResearch { _researchName = "High tensile materials"
             , _researchType = HighTensileMaterials
             , _researchCategory = Engineering Materials
             , _researchAntecedents = [ ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }

-- motive systems
techMap HoverCrafts =
    MkResearch { _researchName = "Hovercrafts"
             , _researchType = HoverCrafts
             , _researchCategory = Engineering Propulsion
             , _researchAntecedents = [ ]
             , _researchCost = MkTotalResearchScore
                                { _totalResearchScoreEngineering = MkResearchScore 500
                                , _totalResearchScoreNatural = MkResearchScore 0
                                , _totalResearchScoreSocial = MkResearchScore 0
                                }
             , _researchTier = MkResearchTier 1
             }
