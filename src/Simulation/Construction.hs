{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE ExplicitForAll             #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE InstanceSigs               #-}
{-# LANGUAGE FlexibleContexts           #-}

module Simulation.Construction ( handleFactionConstruction, queueCostReq, planetConstructionSpeed
                               , overallConstructionSpeed )
    where

import Import
import Control.Lens ( (^.), (^?), _Just, to )
import qualified Queries ( planetConstructionQueue )
import Resources ( RawResources(..), RawResource(..), ResourceCost, ConstructionSpeed
                 , ResourcesAvailable, unRawResource, ccdChemicalCost, ccdBiologicalCost
                 , ccdMechanicalCost )
import Common ( safeHead )
import Construction ( Constructable(..), constructionLeft, ConstructionSpeedCoeff(..)
                    , OverallConstructionSpeed(..), speedLimitedByOverallSpeed, resourceScaledBySpeed
                    , constructionWillFinish, speedLimitedByWorkLeft, overallSpeedBiological
                    , overallSpeedChemical, overallSpeedMechanical
                    )
import CustomTypes ( StarDate )
import MenuHelpers ( getScore )
import Buildings ( BuildingLevel(..), building, buildingInfoCost )
import News.Import ( buildingConstructionFinishedNews )
import Data.Maybe ( fromJust )


-- | Process through all construction queues of a faction and update them
--   New buildings will be constructed when applicable, resources are spent
--   and overall resource consumption is taken into account when selecting the speed
--   of the construction
handleFactionConstruction :: (BaseBackend backend ~ SqlBackend,
                              BackendCompatible SqlBackend backend,
                              PersistQueryWrite backend,
                              PersistStoreWrite backend, PersistQueryRead backend, PersistUniqueRead backend,
                              MonadIO m) =>
                              StarDate -> Entity Faction -> ReaderT backend m ()
handleFactionConstruction date factionE = do
    let faction = entityVal factionE
    planets <- selectList [ PlanetOwnerId ==. Just (entityKey factionE)] []
    queues <- mapM (Queries.planetConstructionQueue . entityKey) planets
    let totalCost = mconcat $ map (queueCostReq . toPlainObjects) queues
    let availableResources = getScore $ Just faction
    let consSpeed = overallConstructionSpeed totalCost availableResources
    _ <- updateWhere [ FactionId ==. entityKey factionE ]
                     [ FactionBiologicals -=. resourceScaledBySpeed (totalCost ^. ccdBiologicalCost) (consSpeed ^. overallSpeedBiological)
                     , FactionMechanicals -=. resourceScaledBySpeed (totalCost ^. ccdMechanicalCost) (consSpeed ^. overallSpeedMechanical)
                     , FactionChemicals -=. resourceScaledBySpeed (totalCost ^. ccdChemicalCost) (consSpeed ^. overallSpeedChemical)
                     ]
    mapM_ (doPlanetConstruction (entityKey factionE) date consSpeed . planetAndFirstConstruction) queues


-- | Select construction from queue with the smallest construction index
planetAndFirstConstruction :: (Maybe (Entity Planet), [Entity BuildingConstruction]) -> (Maybe (Entity Planet), Maybe (Entity BuildingConstruction))
planetAndFirstConstruction (planet, queue@(_:_)) =
    (planet, safeHead sortedQ)
    where
        sortedQ = sortBy sorter queue
        sorter a b = compare ((cIndex . entityVal) a) ((cIndex . entityVal) b)

planetAndFirstConstruction (planet, []) =
    (planet, Nothing)


-- | Perform construction on a planet at given speed
doPlanetConstruction :: (PersistQueryRead backend, PersistQueryWrite backend,
                        MonadIO m, BaseBackend backend ~ SqlBackend) =>
                        FactionId -> StarDate -> OverallConstructionSpeed -> (Maybe (Entity Planet), Maybe (Entity BuildingConstruction))
                        -> ReaderT backend m ()
doPlanetConstruction fId date speed (Just planetE, Just bConsE) = do
    let bCons = bConsE ^. entityValL
    let realSpeed = speedLimitedByOverallSpeed speed $ planetConstructionSpeed $ planetE ^. entityValL
    let modelBuilding = building (bCons ^. buildingConstructionType) (bCons ^. buildingConstructionLevel . to MkBuildingLevel)
    let cToDo = speedLimitedByWorkLeft realSpeed bCons (modelBuilding ^. buildingInfoCost)
    _ <- if constructionWillFinish realSpeed (cProgress bCons) (modelBuilding ^. buildingInfoCost)
         then finishConstruction fId date bConsE
         else workOnConstruction cToDo bConsE
    return ()
doPlanetConstruction _ _ _ _ =
    return ()


-- | Finish construction by removing it from construction queue, updating indecies and
--   placing the newly constructed building on planet. Respective reports and news entries
---  are created for the faction.
finishConstruction :: (PersistQueryRead backend, PersistQueryWrite backend,
                       MonadIO m, BaseBackend backend ~ SqlBackend) =>
                       FactionId -> StarDate -> Entity BuildingConstruction -> ReaderT backend m ()
finishConstruction fId date bConsE = do
    let bCons = bConsE ^. entityValL
    let bConsId = bConsE ^. entityKeyL
    let planetId = bCons ^. buildingConstructionPlanetId
    _ <- delete bConsId
    _ <- updateWhere [ BuildingConstructionPlanetId ==. planetId
                     , BuildingConstructionId !=. bConsId ] [ BuildingConstructionIndex -=. 1 ]
    _ <- updateWhere [ ShipConstructionPlanetId ==. Just planetId ] [ ShipConstructionIndex -=. 1 ]
    let newBuilding = Building { _buildingPlanetId = planetId
                               , _buildingType = bCons ^. buildingConstructionType
                               , _buildingLevel = bCons ^. buildingConstructionLevel
                               , _buildingDamage = 0.0
                               }
    bId <- insert newBuilding
    let report = BuildingReport { _buildingReportBuildingId = bId
                                , _buildingReportPlanetId = planetId
                                , _buildingReportType = bCons ^. buildingConstructionType . to Just
                                , _buildingReportLevel = bCons ^. buildingConstructionLevel . to Just
                                , _buildingReportDamage = Just 0.0
                                , _buildingReportFactionId = fId
                                , _buildingReportDate = date
                                }
    _ <- insert report
    -- TODO: rather messy piece, clean up this
    planet <- get planetId
    let starSystemId = planet ^? _Just . planetStarSystemId :: Maybe StarSystemId
    starSystem <- mapM get starSystemId
    let news = buildingConstructionFinishedNews (Entity planetId $ fromJust planet) (Entity (fromJust starSystemId) (fromJust $ fromJust starSystem) )
                                                (Entity bId newBuilding) date fId
    _ <- insert news
    return ()


-- | Update construction in queue by increasing its progress fields by given speed
workOnConstruction :: (PersistQueryWrite backend,
                      BaseBackend backend ~ SqlBackend, MonadIO m) =>
                      RawResources ConstructionSpeed -> Entity BuildingConstruction -> ReaderT backend m ()
workOnConstruction speed bConsE = do
    let bConsId = entityKey bConsE
    _ <- update bConsId [ BuildingConstructionProgressBiologicals +=. speed ^. ccdBiologicalCost . unRawResource
                        , BuildingConstructionProgressMechanicals +=. speed ^. ccdMechanicalCost . unRawResource
                        , BuildingConstructionProgressChemicals +=. speed ^. ccdChemicalCost . unRawResource
                        ]
    return ()


-- | Turn entities into plain objects
toPlainObjects :: (Maybe (Entity Planet), [Entity BuildingConstruction]) -> (Maybe Planet, [BuildingConstruction])
toPlainObjects (planet, constructions) =
    (entityVal <$> planet, map entityVal constructions)


-- | Total requirement of cost for a construction queue for a turn
--   Take into account speed the planet can construct buildings
queueCostReq :: (Maybe Planet, [BuildingConstruction]) -> RawResources ResourceCost
queueCostReq (Just planet, construction:_) =
    RawResources (min (planetSpeed ^. ccdMechanicalCost) (constLeft ^. ccdMechanicalCost))
                 (min (planetSpeed ^. ccdBiologicalCost) (constLeft ^. ccdBiologicalCost))
                 (min (planetSpeed ^. ccdChemicalCost) (constLeft ^. ccdChemicalCost))
    where
        modelBuilding = building (construction ^. buildingConstructionType)
                                 (construction ^. buildingConstructionLevel . to MkBuildingLevel)
        planetSpeed = planetConstructionSpeed planet
        constLeft = constructionLeft (modelBuilding ^. buildingInfoCost)
                                     (cProgress construction)
queueCostReq (_, _) = mempty


-- | Speed that a planet can build constructions
planetConstructionSpeed :: Planet -> RawResources ConstructionSpeed
planetConstructionSpeed _ =
    RawResources (MkRawResource 50) (MkRawResource 50) (MkRawResource 50)


-- | Overall speed coefficient with given total cost and total resources
--   Used to scale all construction of a faction, so they don't end up using
--   more resources than they have
overallConstructionSpeed :: RawResources ResourceCost -> RawResources ResourcesAvailable -> OverallConstructionSpeed
overallConstructionSpeed cost res =
    OverallConstructionSpeed
        { _overallSpeedBiological = bioSpeed
        , _overallSpeedMechanical = mechSpeed
        , _overallSpeedChemical = chemSpeed
        }
    where
        bioSpeed = speedPerResource (cost ^. ccdBiologicalCost) (res ^. ccdBiologicalCost)
        mechSpeed = speedPerResource (cost ^. ccdMechanicalCost) (res ^. ccdMechanicalCost)
        chemSpeed = speedPerResource (cost ^. ccdChemicalCost) (res ^. ccdChemicalCost)


-- | Speed that consumes at most available amount of resources or finishes the construction
speedPerResource :: RawResource t -> RawResource t -> ConstructionSpeedCoeff t
speedPerResource cost res =
    if res >= cost
        then
            NormalConstructionSpeed
        else
            LimitedConstructionSpeed $
                fromIntegral (res ^. unRawResource ) / fromIntegral (cost ^. unRawResource)
