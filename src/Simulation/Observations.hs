{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE ExplicitForAll             #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE InstanceSigs               #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE TemplateHaskell            #-}

module Simulation.Observations
    ( handleFactionObservations, ObservationCandidate(..), ObservationType(..), groupStarReports
    , groupPlanetReports, groupStarLaneReports, buildOCStarList, buildOCPlanetList
    , buildOCStarLaneList, needsObservation, _OCStar, _OCPlanet, _OCStarLane, _OCStarSystem
    )
    where

import Import
import Control.Lens ( (^.), (^..), folded, to, makePrisms )
import Data.Aeson.Text ( encodeToLazyText )
import System.Random

import Common
import CustomTypes
import News.Import ( planetFoundNews, starFoundNews )
import Report ( createPlanetReports, createStarLaneReports, createStarReports
              , CollatedPlanetReport(..), CollatedStarLaneReport(..)
              , CollatedStarReport(..), CollatedStarSystemReport(..)
              , cssrRulerId, cssrLocation, cssrName, cprGravity, cprPosition
              , cprName, cprOwnerId, csrName, csrLuminosityClass, csrSpectralType
              , cslSystemId1, cslSystemId1, cslSystemId2, cprId, csrStarId
              )
import Space.Data ( Coordinates(..) )
import Units.Data ( UnitObservationDetailsJSON(..) )
import Units.Queries ( Unit'(..), getFactionUnits )
import Units.Reports ( UnitObservationDetails(..), unitLocation )


-- | Generate reports for all kinds of things faction can currently observe
--   Currently these include forces on faction's own planets (population that is),
--   sensor stations on planets, forces on space (space ships and satellites) and
--   ground forces on other faction's planets
handleFactionObservations :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend,
    PersistUniqueRead backend,
    BackendCompatible SqlBackend backend, MonadIO m) =>
    StarDate -> Entity Faction -> ReaderT backend m ()
handleFactionObservations date faction = do
    -- observations by on faction's planets
    -- observations of space by planetary sensor arrays (SensorStation and such)
    -- observations by space forces
    -- observations by ground forces
    factionPlanets <- selectList [ PlanetOwnerId ==. Just (entityKey faction)] []
    populations <- selectList [ PlanetPopulationPlanetId <-. map entityKey factionPlanets
                              , PlanetPopulationPopulation >. 0 ] []
    units <- getFactionUnits $ faction ^. entityKeyL
    let populatedPlanets = filter isPopulated factionPlanets
                            where isPopulated planet = (planet ^. entityKeyL) `elem`
                                    (populations ^.. folded . entityValL . planetPopulationPlanetId)
    mapM_ (doPlanetObservation date faction) populatedPlanets
    mapM_ (doSensorStationObservations date faction) populatedPlanets
    mapM_ (doUnitReport date faction) units


-- | Let units do reports on their current location, status and such
doUnitReport :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, BackendCompatible SqlBackend backend, MonadIO m) =>
    StarDate -> Entity Faction -> (UnitId, Unit') -> ReaderT backend m ()
doUnitReport date faction (uId, unit) = do
    _ <- insert obs
    return ()
    where
        obs = UnitObservation
                { _unitObservationUnitId = uId
                , _unitObservationContent = json
                , _unitObservationOwnerId = faction ^. entityKeyL
                , _unitObservationDate = date
                }
        json = MkUnitObservationDetailsJSON $ toStrict (encodeToLazyText details)
        details = UnitObservationDetails
                    { _unitObservationDetailsLocation = unitLocation unit
                    }


-- | Let forces on populated planet observe their surroundings and write reports
doPlanetObservation :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, MonadIO m) =>
    StarDate -> Entity Faction -> Entity Planet -> ReaderT backend m ()
doPlanetObservation date faction planet = do
    let p = planet ^. entityValL
    let observation = PlanetReport
                      { _planetReportPlanetId = planet ^. entityKeyL
                      , _planetReportOwnerId = p ^. planetOwnerId
                      , _planetReportStarSystemId = p ^. planetStarSystemId
                      , _planetReportName = p ^. planetName . to Just
                      , _planetReportPosition = p ^. planetPosition . to Just
                      , _planetReportGravity = p ^. planetGravity . to Just
                      , _planetReportFactionId = faction ^. entityKeyL
                      , _planetReportDate = date
                      , _planetReportRulerId = p ^. planetRulerId
                      }
    _ <- insert observation
    pops <- selectList [ PlanetPopulationPlanetId ==. entityKey planet] []
    let popObservations = map (\pop -> PlanetPopulationReport (planet ^. entityKeyL) (pop ^. entityValL. planetPopulationRaceId . to Just)
                                                              (pop ^. entityValL . planetPopulationPopulation . to Just)
                                                              (faction ^. entityKeyL) date) pops
    _ <- mapM insert popObservations
    buildings <- selectList [ BuildingPlanetId ==. planet ^. entityKeyL] []
    let bObservations = map (\b -> BuildingReport (b ^. entityKeyL) (planet ^. entityKeyL) (b ^. entityValL . buildingType . to Just)
                                                  (b ^. entityValL . buildingLevel . to Just)
                                                  (b ^. entityValL . buildingDamage . to Just)
                                                  (faction ^. entityKeyL) date) buildings
    _ <- mapM insert bObservations
    statuses <- selectList [ PlanetStatusPlanetId ==. planet ^. entityKeyL ] []
    let statusReport = PlanetStatusReport { _planetStatusReportPlanetId = planet ^. entityKeyL
                                          , _planetStatusReportStatus = mkUniq $ statuses ^.. folded . entityValL . planetStatusStatus
                                          , _planetStatusReportFactionId = faction ^. entityKeyL
                                          , _planetStatusReportDate = date
                                          }
    _ <- insert statusReport
    systemDb <- get $ p ^. planetStarSystemId
    let systemReport = (\system ->
            StarSystemReport
                { _starSystemReportStarSystemId = p ^. planetStarSystemId
                , _starSystemReportName = system ^. starSystemName . to Just
                , _starSystemReportCoordX = system ^. starSystemCoordX
                , _starSystemReportCoordY = system ^. starSystemCoordY
                , _starSystemReportFactionId = faction ^. entityKeyL
                , _starSystemReportDate = date
                , _starSystemReportRulerId = system ^. starSystemRulerId
                }) <$> systemDb
    _ <- mapM insert systemReport
    return ()


-- | Let sensor stations on planet observe surrounding space
doSensorStationObservations :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, MonadIO m) =>
    StarDate -> Entity Faction -> Entity Planet -> ReaderT backend m ()
doSensorStationObservations date faction planet = do
    let p = planet ^. entityValL
    buildings <- selectList [ BuildingPlanetId ==. planet ^. entityKeyL
                            , BuildingType ==. SensorStation
                            , BuildingDamage <. 0.5 ] []
    stars <- selectList [ StarStarSystemId ==. p ^. planetStarSystemId ] []
    starReports <- createStarReports (p ^. planetStarSystemId) $ faction ^. entityKeyL
    let starGroups = groupStarReports stars starReports
    planets <- selectList [ PlanetStarSystemId ==. p ^. planetStarSystemId
                          , PlanetId !=. planet ^. entityKeyL ] []
    planetReports <- createPlanetReports (p ^. planetStarSystemId) $ faction ^. entityKeyL
    let planetGroups = groupPlanetReports planets planetReports
    starLanes <- selectList ([ StarLaneStarSystem1 ==. p ^. planetStarSystemId ]
                         ||. [ StarLaneStarSystem2 ==. p ^. planetStarSystemId ]) []
    starLaneReports <- createStarLaneReports (p ^. planetStarSystemId) $ faction ^. entityKeyL
    let starLaneGroups = groupStarLaneReports starLanes starLaneReports
    let candidates = buildOCStarList starGroups ++ buildOCPlanetList planetGroups ++ buildOCStarLaneList starLaneGroups
    mapM_ (observeRandomTarget date faction candidates) buildings


-- | Observe random target on given list of observation candidates
observeRandomTarget :: (BaseBackend backend ~ SqlBackend,
    PersistStoreWrite backend, PersistQueryRead backend, MonadIO m) =>
    StarDate -> Entity Faction -> [ObservationCandidate] -> Entity Building -> ReaderT backend m ()
observeRandomTarget date faction candidates building = do
    res <- liftIO $ randomRIO (0, length candidates - 1)
    let target = maybeGet res candidates
    _ <- observeTarget date faction target building
    return ()


-- | Observe given target
observeTarget :: ( BaseBackend backend ~ SqlBackend
                 , PersistStoreWrite backend, PersistQueryRead backend, MonadIO m ) =>
                 StarDate -> Entity Faction -> Maybe ObservationCandidate -> Entity Building -> ReaderT backend m ()

observeTarget _ _ Nothing _ =
    return ()

observeTarget _ _ (Just OCStarLane {}) _ =
    return ()

observeTarget date faction (Just (OCStar starEntity _ observationType)) _ = do
    let star = starEntity ^. entityValL
    let sid = starEntity ^. entityKeyL
    aLuminosityClass <- liftIO $ chooseOne (star ^. starSpectralType . to Just) Nothing
    aSpectralType <- liftIO $ chooseOne (star ^. starLuminosityClass . to Just) Nothing
    let res = StarReport sid (star ^. starStarSystemId) (star ^. starName . to Just)
                         aLuminosityClass aSpectralType
                         (faction ^. entityKeyL) date
    system <- get (star ^. starStarSystemId)
    let news = case observationType of
                NewObservation ->
                    (\s -> starFoundNews star (Entity (star ^. starStarSystemId) s) date (faction ^. entityKeyL)) <$> system
                UpdatedObservation ->
                    Nothing
    _ <- mapM insert news
    _ <- insert res
    return ()

observeTarget date faction (Just (OCPlanet planetEntity _ observationType)) _ = do
    let planet = planetEntity ^. entityValL
    let pid = planetEntity ^. entityKeyL
    aGravity <- liftIO $ chooseOne (Just (planet ^. planetGravity)) Nothing
    -- current ruler of the planet is always deduced while observing
    let res = PlanetReport
                { _planetReportPlanetId = pid
                , _planetReportOwnerId = planet ^. planetOwnerId
                , _planetReportStarSystemId = planet ^. planetStarSystemId
                , _planetReportName = planet ^. planetName . to Just
                , _planetReportPosition = planet ^. planetPosition . to Just
                , _planetReportGravity = aGravity
                , _planetReportFactionId = faction ^. entityKeyL
                , _planetReportDate = date
                , _planetReportRulerId = planet ^. planetRulerId
                }
    system <- get (planet ^. planetStarSystemId)
    let news = case observationType of
                NewObservation ->
                    (\s -> planetFoundNews planetEntity s date (faction ^. entityKeyL)) <$> system
                UpdatedObservation ->
                    Nothing
    _ <- mapM insert news
    _ <- insert res
    return ()

observeTarget date faction (Just (OCStarSystem starSystemEntity _)) _ = do
    let system = starSystemEntity ^. entityValL
    let sId = starSystemEntity ^. entityKeyL
    -- current ruler of star system is always deduced while observing
    let res = StarSystemReport
                { _starSystemReportStarSystemId = sId
                , _starSystemReportName = system ^. starSystemName . to Just
                , _starSystemReportCoordX = system ^. starSystemCoordX
                , _starSystemReportCoordY = system ^. starSystemCoordY
                , _starSystemReportFactionId = faction ^. entityKeyL
                , _starSystemReportDate = date
                , _starSystemReportRulerId = system ^. starSystemRulerId
                }
    _ <- insert res
    return ()


data ObservationCandidate = OCStar (Entity Star) (Maybe CollatedStarReport) ObservationType
                          | OCPlanet (Entity Planet) (Maybe CollatedPlanetReport) ObservationType
                          | OCStarLane (Entity StarLane) (Maybe CollatedStarLaneReport)
                          | OCStarSystem (Entity StarSystem) (Maybe CollatedStarSystemReport)
    deriving Show


data ObservationType =
    NewObservation
    | UpdatedObservation
    deriving (Show, Read, Eq)


-- | given list of stars and collated reports, build list of pairs with star and maybe respective report
groupStarReports :: [Entity Star] -> [CollatedStarReport] -> [(Entity Star, Maybe CollatedStarReport)]
groupStarReports stars reports =
    map fn stars
        where fn star = (star, matchingReport star)
              matchingReport star = find (\a -> a ^. csrStarId == entityKey star) reports


-- | given list of planets and collated reports, build list of pairs with planet and maybe respective report
groupPlanetReports :: [Entity Planet] -> [CollatedPlanetReport] -> [(Entity Planet, Maybe CollatedPlanetReport)]
groupPlanetReports planets reports =
    map fn planets
        where fn planet = (planet, matchingReport planet)
              matchingReport planet = find (\a -> a ^. cprId == entityKey planet) reports


-- | given list of starlanes and collated reports, build list of pairs with starlane and maybe respective report
groupStarLaneReports :: [Entity StarLane] -> [CollatedStarLaneReport] -> [(Entity StarLane, Maybe CollatedStarLaneReport)]
groupStarLaneReports lanes reports =
    map fn lanes
        where fn lane = (lane, matchingReport lane)
              matchingReport lane = find (\a -> (a ^. cslSystemId1 == lane ^. entityValL . starLaneStarSystem1
                                                   && a ^. cslSystemId2 == lane ^. entityValL . starLaneStarSystem2)
                                                || (a ^. cslSystemId2 == lane ^. entityValL . starLaneStarSystem1
                                                    && (a ^. cslSystemId1 == lane ^. entityValL . starLaneStarSystem2))) reports


-- | Map list of star entities and respective reports to observation candidates and filter out fully observed items
buildOCStarList :: [(Entity Star, Maybe CollatedStarReport)] -> [ObservationCandidate]
buildOCStarList reports = filter needsObservation $ map combineFn reports
    where combineFn (star, report) = OCStar star report (observationType report)
          observationType Nothing = NewObservation
          observationType _ = UpdatedObservation


-- | Map list of planet entities and respective reports to observation candidates and filter out fully observed items
buildOCPlanetList :: [(Entity Planet, Maybe CollatedPlanetReport)] -> [ObservationCandidate]
buildOCPlanetList reports =
    filter needsObservation $ map combineFn reports
    where combineFn (planet, report) = OCPlanet planet report (observationType report)
          observationType Nothing = NewObservation
          observationType _ = UpdatedObservation


-- | Map list of starlane entities and respective reports to observation candidates and filter out fully observed items
buildOCStarLaneList :: [(Entity StarLane, Maybe CollatedStarLaneReport)] -> [ObservationCandidate]
buildOCStarLaneList reports = filter needsObservation $ map combineFn reports
    where combineFn (lane, report) = OCStarLane lane report


-- | Does given observation candidate need observing?
--   If given collated report contains different information than the object it's about, return True
needsObservation :: ObservationCandidate -> Bool
needsObservation (OCStar _ Nothing _) = True
needsObservation (OCPlanet _ Nothing _) = True
needsObservation (OCStarSystem _ Nothing) = True
needsObservation OCStarLane {} = False

needsObservation (OCStar entity (Just report) _) =
    report ^. csrSpectralType /= (star ^. starSpectralType . to Just)
    || report ^. csrLuminosityClass /= (star ^. starLuminosityClass . to Just)
    || report ^. csrName /= (star ^. starName . to Just)
    where
        star = entityVal entity

needsObservation (OCPlanet entity (Just report) _) =
    report ^. cprOwnerId /= planet ^. planetOwnerId
    || report ^. cprName /= (planet ^. planetName . to Just)
    || report ^. cprPosition /= (planet ^. planetPosition . to Just)
    || report ^. cprGravity /= (planet ^. planetGravity . to Just)
    where
        planet = entity ^. entityValL

needsObservation (OCStarSystem (Entity _ starSystem) (Just report)) =
    report ^. cssrName /= (starSystem ^. starSystemName . to Just)
    || report ^. cssrLocation /= Coordinates (starSystem ^. starSystemCoordX) (starSystem ^. starSystemCoordY)
    || report ^. cssrRulerId /= starSystem ^. starSystemRulerId


makePrisms ''ObservationCandidate
