{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE FlexibleContexts           #-}

module Simulation.Research ( handleFactionResearch )
    where

import Import
import Control.Lens ( (+~), (^.), to, folded, (^..), filtered, elemOf, view )
import System.Random
import Common ( mkUniq, getR )
import CustomTypes ( StarDate )
import News.Import ( researchCompleted )
import Queries ( factionBuildings )
import Research.Data ( TotalResearchScore(..), ResearchProduction(..), ResearchCategory(..)
                     , Research(..), ResearchLimit(..)
                     , isEngineering, isNaturalScience, isSocialScience, topCategory
                     , researchCategory, unTechTree, researchType, unResearchLimit
                     , totalResearchScoreSocial, totalResearchScoreEngineering
                     , totalResearchScoreNatural, unResearchScore
                     )
import Research.Import ( researchOutput, researchReady, antecedentsAvailable )
import Research.Tree ( techTree, techMap )



-- | Process all research that faction can do
handleFactionResearch :: (MonadIO m,
    BackendCompatible SqlBackend backend, PersistUniqueRead backend,
    PersistQueryWrite backend, BaseBackend backend ~ SqlBackend) =>
    StarDate -> Entity Faction -> ReaderT backend m ()
handleFactionResearch date faction = do
    production <- totalProduction $ entityKey faction
    current <- selectList [ CurrentResearchFactionId ==. entityKey faction ] []
    let updated = updateProgress production <$> current
    _ <- updateUnfinished updated
    _ <- handleCompleted date updated $ entityKey faction
    _ <- updateAvailableResearch $ entityKey faction
    return ()


-- | total research production of a faction
totalProduction :: (MonadIO m,
    BackendCompatible SqlBackend backend, PersistQueryRead backend,
    PersistUniqueRead backend) =>
    FactionId -> ReaderT backend m (TotalResearchScore ResearchProduction)
totalProduction fId = do
    pnbs <- factionBuildings fId
    let buildings = snd =<< pnbs
    return $ mconcat $ researchOutput . entityVal <$> buildings


-- | removed research that has been completed from current research and
-- insert respective details into completed research. Also remove respective
-- entry from available researches. Create new article for each completed research.
handleCompleted :: (MonadIO m, PersistQueryWrite backend,
    BaseBackend backend ~ SqlBackend) =>
    StarDate -> [Entity CurrentResearch] -> FactionId -> ReaderT backend m ()
handleCompleted date updated fId = do
    let finished = updated ^.. folded . filtered (researchReady . entityVal)
    let finishedTech = finished ^.. folded . entityValL . currentResearchType
    insertMany_ $ finished ^.. folded . entityValL . to (currentToCompleted date)
    insertMany_ $ finished ^.. folded . entityValL . currentResearchType . to (researchCompleted date fId)
    deleteWhere [ CurrentResearchId <-. fmap entityKey finished ]
    deleteWhere [ AvailableResearchType <-. finishedTech
                , AvailableResearchFactionId ==. fId ]


-- | Map current research into completed research
currentToCompleted :: StarDate -> CurrentResearch -> CompletedResearch
currentToCompleted date research =
    CompletedResearch
        { _completedResearchType = research ^. currentResearchType
        , _completedResearchLevel = 1
        , _completedResearchFactionId = research ^. currentResearchFactionId
        , _completedResearchDate = date
        }


-- | Current research after given amount of research has been done
updateProgress :: TotalResearchScore ResearchProduction -> Entity CurrentResearch -> Entity CurrentResearch
updateProgress prod curr =
    case research ^. researchCategory of
        Engineering _ -> -- TODO: clean up after ResearchScore has lenses
            entityValL . currentResearchProgress +~ engResearch $ curr

        NaturalScience _ ->
            entityValL . currentResearchProgress +~ natResearch $ curr

        SocialScience _ ->
            entityValL . currentResearchProgress +~ socResearch $ curr
    where
        research = curr ^. entityValL . currentResearchType . to techMap
        engResearch = prod ^. totalResearchScoreEngineering . unResearchScore
        natResearch = prod ^. totalResearchScoreNatural . unResearchScore
        socResearch = prod ^. totalResearchScoreSocial . unResearchScore


-- | Update current research in database for unfinished research
updateUnfinished :: (MonadIO m, PersistStoreWrite backend,
    Traversable t, IsSequence (t (Entity record)), PersistEntity record,
    Element (t (Entity record)) ~ Entity CurrentResearch,
    PersistEntityBackend record ~ BaseBackend backend) =>
    t (Entity record) -> ReaderT backend m (t ())
updateUnfinished updated = do
    let unfinished = filter (not . researchReady . entityVal) updated
    mapM (\x -> replace (entityKey x) (entityVal x)) unfinished


-- | Add new available research when required
updateAvailableResearch :: (MonadIO m, PersistQueryWrite backend,
    BaseBackend backend ~ SqlBackend) =>
    FactionId -> ReaderT backend m ()
updateAvailableResearch fId = do
    available <- selectList [ AvailableResearchFactionId ==. fId ] []
    completed <- selectList [ CompletedResearchFactionId ==. fId ] []
    g <- liftIO newStdGen
    let maxAvailable = MkResearchLimit 3
    -- reusing same g should not have adverse effect here
    let engCand = getR g (maxAvailable ^. unResearchLimit) $ newAvailableResearch isEngineering maxAvailable available completed
    let natCand = getR g (maxAvailable ^. unResearchLimit) $ newAvailableResearch isNaturalScience maxAvailable available completed
    let socCand = getR g (maxAvailable ^. unResearchLimit) $ newAvailableResearch isSocialScience maxAvailable available completed
    rewriteAvailableResearch fId $ engCand <> natCand <> socCand


-- | Remove old available research and insert new ones
-- | Research categories of existing researches are used to determine which
-- | available researches should be removed from the database and replaced
-- | with new ones
rewriteAvailableResearch :: (MonadIO m, PersistQueryWrite backend,
    BaseBackend backend ~ SqlBackend) =>
    FactionId -> [Research] -> ReaderT backend m ()
rewriteAvailableResearch fId res = do
    let cats = mkUniq $ res ^.. folded . researchCategory . to topCategory
    unless (null cats) $ do
        deleteWhere [ AvailableResearchFactionId ==. fId
                    , AvailableResearchCategory <-. cats ]
        insertMany_ $ researchToAvailable fId <$> res


researchToAvailable :: FactionId -> Research -> AvailableResearch
researchToAvailable fId res =
    AvailableResearch
        { _availableResearchType = res ^. researchType
        , _availableResearchCategory =  res ^. researchCategory . to topCategory
        , _availableResearchFactionId = fId
        }


-- | Figure out if new set of research should be made available to a player. Function
-- factors in current research limit, currently available research and research that has
-- already been completed.
newAvailableResearch :: (ResearchCategory -> Bool) -> ResearchLimit -> [Entity AvailableResearch]
    -> [Entity CompletedResearch] -> [Research]
newAvailableResearch selector limit available completed =
    if MkResearchLimit (length specificCategory) >= limit
        then []
        else candidates
    where
        specificCategory = available ^.. folded . filtered (availableResearchFilter selector)
        knownTech = completed ^.. folded . entityValL . completedResearchType
        unlockedResearch = techTree ^.. unTechTree . folded . filtered (antecedentsAvailable knownTech)
        unlockedAndUnresearched = unlockedResearch ^.. folded . filtered (\x -> not $ elemOf folded (x ^. researchType) knownTech)
        candidates = unlockedAndUnresearched ^.. folded . filtered (selector . view researchCategory)


-- | Is given available research entity of certain research category
availableResearchFilter :: (ResearchCategory -> Bool) -> Entity AvailableResearch -> Bool
availableResearchFilter f x =
    x ^. entityValL . availableResearchType . to techMap . researchCategory . to f
