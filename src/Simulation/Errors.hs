{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE TemplateHaskell            #-}

module Simulation.Errors
    ( SimulationError(..), simulationStatusNotFound, deltaTIsTooBig, turnProcessingAndStateChangeDisallowed
    , simulationNotOpenForCommands, simulationNotOpenForBrowsing, simulationNotOffline
    )
    where

import Import
import Data.Aeson.TH
import Errors ( ErrorCodeClass(..), ECode(..) )


simulationStatusNotFound :: ECode
simulationStatusNotFound = ECode SimulationStatusNotFound

deltaTIsTooBig :: ECode
deltaTIsTooBig = ECode DeltaTIsTooBig

turnProcessingAndStateChangeDisallowed :: ECode
turnProcessingAndStateChangeDisallowed = ECode TurnProcessingAndStateChangeDisallowed

simulationNotOpenForCommands :: ECode
simulationNotOpenForCommands = ECode SimulationNotOpenForCommands

simulationNotOpenForBrowsing :: ECode
simulationNotOpenForBrowsing = ECode SimulationNotOpenForBrowsing

simulationNotOffline :: ECode
simulationNotOffline = ECode SimulationNotOffline

{-| Error codes relating to simulation status or turn processing
-}
data SimulationError =
    SimulationStatusNotFound
    | DeltaTIsTooBig
    | TurnProcessingAndStateChangeDisallowed
    | SimulationNotOpenForCommands
    | SimulationNotOpenForBrowsing
    | SimulationNotOffline
    deriving (Show, Read, Eq)

instance ErrorCodeClass SimulationError where
    httpStatusCode = \case
        SimulationStatusNotFound -> 500
        DeltaTIsTooBig -> 400
        TurnProcessingAndStateChangeDisallowed -> 400
        SimulationNotOpenForCommands -> 500
        SimulationNotOpenForBrowsing -> 500
        SimulationNotOffline -> 500

    description = \case
        SimulationStatusNotFound ->
            "Simulation status seems to be missing"

        DeltaTIsTooBig ->
            "Time difference is too big"

        TurnProcessingAndStateChangeDisallowed ->
            "Can't change system state and process turn at the same time"

        SimulationNotOpenForCommands ->
            "Simulation not currently available for playing"

        SimulationNotOpenForBrowsing ->
            "Simulation not currently available for browsing"

        SimulationNotOffline ->
            "Simulation is not offline"


$(deriveJSON defaultOptions ''SimulationError)
