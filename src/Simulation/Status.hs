{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE FlexibleContexts           #-}

module Simulation.Status ( removeExpiredStatuses )
    where

import Import
import Control.Lens ( (^?), (^..), folded, to, filtered, _Just )
import Data.Maybe ( fromJust )
import CustomTypes ( StarDate )
import News.Data ( ProductionChangedNews(..), NewsArticle(..), mkFactionNews )
import Resources ( ResourceType(..) )
import Space.Data ( PlanetaryStatus(..) )


-- | Remove expired statuses from database and save respective news
-- items signaling ending of statuses.
removeExpiredStatuses :: (MonadIO m, PersistQueryWrite backend,
    BaseBackend backend ~ SqlBackend) =>
    StarDate -> ReaderT backend m [News]
removeExpiredStatuses date = do
    planetNews <- processPlanetStatus date
    insertMany_ planetNews
    return planetNews


-- | Remove expired planet statuses from database and create respective
-- news articles
processPlanetStatus :: (MonadIO m, PersistQueryWrite backend,
    BaseBackend backend ~ SqlBackend) =>
    StarDate -> ReaderT backend m [News]
processPlanetStatus date = do
    loaded <- selectList [ PlanetStatusExpiration <=. Just date ] []
    deleteWhere [ PlanetStatusExpiration <=. Just date ]
    let planetIds = loaded ^.. folded . entityValL . planetStatusPlanetId
    planets <- selectList [ PlanetId <-. planetIds ] []
    let systemIds = planets ^.. folded . entityValL . planetStarSystemId
    systems <- selectList [ StarSystemId <-. systemIds ] []
    let res = loaded ^.. folded . entityValL . to (expirationNews planets systems date) :: [Maybe News]
    return $ res ^.. folded . filtered isJust . to fromJust


--TODO: performance with big set of planets and star systems
-- | Create expiration news for specific planet status
-- List of planet entities and list of star system entities are used for caching data
-- These two lists should contain information that given PlanetStatus refers to, in
-- order for this function to be able to retrieve planet's and star system's name
expirationNews :: [Entity Planet] -> [Entity StarSystem] -> StarDate -> PlanetStatus -> Maybe News
expirationNews planets systems date (PlanetStatus pId GoodHarvest _) =
    boostEnded planets systems date pId BiologicalResource Boost

expirationNews planets systems date (PlanetStatus pId PoorHarvest _) =
    boostEnded planets systems date pId BiologicalResource Slowdown

expirationNews planets systems date (PlanetStatus pId GoodMechanicals _) =
    boostEnded planets systems date pId MechanicalResource Boost

expirationNews planets systems date (PlanetStatus pId PoorMechanicals _) =
    boostEnded planets systems date pId MechanicalResource Slowdown

expirationNews planets systems date (PlanetStatus pId GoodChemicals _) =
    boostEnded planets systems date pId ChemicalResource Boost

expirationNews planets systems date (PlanetStatus pId PoorChemicals _) =
    boostEnded planets systems date pId ChemicalResource Slowdown

expirationNews _ _ _ (PlanetStatus _ KragiiAttack _) =
    Nothing


-- | News entry about boost ending
-- Will return Nothing when one of the components (planet or starsystem)
-- isn't found in provided lists or if planet doesn't currently have an
-- owner.
boostEnded :: (SemiSequence seq1, SemiSequence seq2,
    Element seq1 ~ Entity Planet, Element seq2 ~ Entity StarSystem) =>
    seq1 -> seq2 -> StarDate -> PlanetId -> ResourceType -> StatusType -> Maybe News
boostEnded planets systems date pId resource sType =
        mkFactionNews <$> fId
                      <*> Just date
                      <*> case sType of
                            Boost ->
                                ProductionBoostEnded <$> content
                            Slowdown ->
                                ProductionSlowdownEnded <$> content
    where
        content =  ProductionChangedNews
                        <$> planet ^? _Just . entityKeyL
                        <*> planet ^? _Just . entityValL . planetName
                        <*> system ^? _Just . entityKeyL
                        <*> system ^? _Just . entityValL . starSystemName
                        <*> Just resource
                        <*> Just date
        fId = planet ^? _Just . entityValL . planetOwnerId . _Just
        planet = find (\p -> entityKey p == pId) planets
        system = find (\s -> Just (entityKey s) == planet ^? _Just . entityValL . planetStarSystemId) systems


data StatusType =
    Boost
    | Slowdown
