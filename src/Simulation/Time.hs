{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TypeFamilies               #-}

module Simulation.Time (advanceTime)
    where

import Import
import Database.Persist.Sql (toSqlKey)
import Control.Lens ( (^?), _Just )
import CustomTypes ( StarDate(..) )

-- | advance time stored in database by one (decimal) month
advanceTime :: (BaseBackend backend ~ SqlBackend,
    PersistQueryRead backend, PersistStoreWrite backend, MonadIO m) =>
    ReaderT backend m StarDate
advanceTime = do
    _ <- update (toSqlKey 1) [SimulationCurrentTime +=. 1]
    time <- get $ toSqlKey 1

    return $ fromMaybe (MkStarDate 0) $ time ^? _Just . simulationCurrentTime
