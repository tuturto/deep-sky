{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE UndecidableInstances       #-}


module Model where

import ClassyPrelude.Yesod
import Yesod.Auth.HashDB ( HashDBUser(..) )
import Database.Persist.Quasi

import CustomTypes
import People.Data
import Research.Data
import Resources ( RawResource(..), Biological(..), Chemical(..), Mechanical(..) )
import Space.Data
import Units.Components
import Units.Data

import Control.Lens ( (&), (?~), view, makeLensesFor )

-- You can define all of your database entities in the entities file.
-- You can find more information on persistent and how to declare entities
-- at:
-- http://www.yesodweb.com/book/persistent/
share [ mkPersist sqlSettings { mpsGenerateLenses = True }
      , mkMigrate "migrateAll"
      ]
    $(persistFileWith lowerCaseSettings "config/models")


instance HashDBUser User where
    userPasswordHash = view userPassword
    setPasswordHash h u = u & userPassword ?~ h


makeLensesFor
    [ ("entityVal", "entityValL")
    , ("entityKey", "entityKeyL")
    ] ''Entity
