{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE ExplicitForAll             #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE InstanceSigs               #-}
{-# LANGUAGE FlexibleContexts           #-}

module MenuHelpers
    ( starDate, getScore, usersRoles, isAdmin, authorizeAdmin )
    where

import Model
import Import.NoFoundation
import Database.Persist.Sql (toSqlKey)
import CustomTypes
import Resources (RawResources(..), ResourcesAvailable)

import Control.Lens ( (^..), (^?), folded, _Just, (^.) )

-- | Current star date of the simulation
starDate :: (BaseBackend backend ~ SqlBackend, MonadIO m,
    PersistQueryRead backend) =>
    ReaderT backend m StarDate
starDate = do
    simulation <- get (toSqlKey 1)
    return $ fromMaybe (MkStarDate 0) $ simulation ^? _Just . simulationCurrentTime


usersRoles :: (BaseBackend backend ~ SqlBackend, MonadIO m,
    PersistQueryRead backend) =>
    UserId -> ReaderT backend m [Role]
usersRoles userId = do
    roles <- selectList [ UserRoleUserId ==. userId ] []
    return $ (entityVal <$> roles) ^.. folded . userRoleRole


isAdmin :: (BaseBackend backend ~ SqlBackend,
    PersistQueryRead backend, MonadIO m) =>
    UserId -> ReaderT backend m Bool
isAdmin userId = do
    roles <- usersRoles userId
    return $ elem RoleAdministrator roles


getScore :: Maybe Faction -> RawResources ResourcesAvailable
getScore (Just faction) =
    RawResources (faction ^. factionMechanicals) (faction ^. factionBiologicals) (faction ^. factionChemicals)
getScore _ = mempty


authorizeAdmin :: (BaseBackend (YesodPersistBackend site)
    ~
    SqlBackend,
    YesodPersist site, PersistQueryRead (YesodPersistBackend site)) =>
    Maybe UserId -> HandlerFor site AuthResult
authorizeAdmin (Just userId) = do
    checkAdmin <- runDB $ isAdmin userId
    let res = if checkAdmin then Authorized
                else Unauthorized "This part is only for administrators"
    return res
authorizeAdmin _ =
    return $ Unauthorized "This part is only for administrators"
