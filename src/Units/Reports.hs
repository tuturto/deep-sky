{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}


module Units.Reports
    ( UnitReport(..), ShipReportDetails(..), VehicleReportDetails(..), UnitLocation(..)
    , ShipLocation(..), VehicleLocation(..), UnitObservationDetails(..), ownerReport
    , otherReport, deserializeObservation, deserializeObservations, ownerShipReport
    , ownerVehicleReport, otherShipReport, otherVehicleReport, _ShipLocation, _VehicleLocation
    , shipLocation, vehicleLocation, unitLocation, _ShipReport, _VehicleReport
    , shipReportDetailsName, shipReportDetailsDesignName, shipReportDetailsStats
    , shipReportDetailsLocation, shipReportDetailsCrew, vehicleReportDetailsName
    , vehicleReportDetailsDesignName, vehicleReportDetailsStats, vehicleReportDetailsLocation
    , vehicleReportDetailsCrew, crewReportPersonId, crewReportName, crewReportPosition
    , crewReportRank, unitObservationDetailsLocation, _PlanetarySpace, _SystemSpace
    , _VehicleOnPlanet
    )
    where

import Import
import Control.Lens ( Lens', lens, (.~), (&), (^.), view, makeLenses, makePrisms )
import Data.Aeson ( decode, withObject )
import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import Data.ByteString.Builder( toLazyByteString )
import Data.Text.Encoding ( encodeUtf8Builder )
import People.Data ( PersonName(..) )
import Units.Data ( ShipName, VehicleName, Band, StatsReportDetails(..)
                  , UnitObservationDetailsJSON(..), Band(..), DesignName(..)
                  , CrewPosition(..), CrewRank(..), unUnitObservationDetailsJSON
                  )
import Units.Queries ( Unit'(..) )


data UnitReport
    = ShipReport ShipReportDetails
    | VehicleReport VehicleReportDetails
    deriving (Show, Read, Eq)


instance ToJSON UnitReport where
    toJSON obj =
        case obj of
            ShipReport details ->
                object [ "Tag" .= ("ShipReport" :: Text)
                       , "Contents" .= details
                       ]

            VehicleReport details ->
                object [ "Tag" .= ("VehicleReport" :: Text)
                       , "Contents" .= details
                       ]


data ShipReportDetails = ShipReportDetails
    { _shipReportDetailsName :: !ShipName
    , _shipReportDetailsDesignName :: !DesignName
    , _shipReportDetailsStats :: !StatsReportDetails
    , _shipReportDetailsLocation :: !(Maybe ShipLocation)
    , _shipReportDetailsCrew :: ![CrewReport]
    }
    deriving (Show, Read, Eq)


data VehicleReportDetails = VehicleReportDetails
    { _vehicleReportDetailsName :: !VehicleName
    , _vehicleReportDetailsDesignName :: !DesignName
    , _vehicleReportDetailsStats :: !StatsReportDetails
    , _vehicleReportDetailsLocation :: !(Maybe VehicleLocation)
    , _vehicleReportDetailsCrew :: ![CrewReport]
    } deriving (Show, Read, Eq)


data CrewReport = CrewReport
    { _crewReportPersonId :: !PersonId
    , _crewReportName :: !PersonName
    , _crewReportPosition :: !CrewPosition
    , _crewReportRank :: !CrewRank
    } deriving (Show, Read, Eq)


-- | Details of observed unit
-- | This is stored in database as UnitObservationDetailsJSON and
-- | then deserialized into UnitObservationDetails when constructing
-- | report on unit observations
data UnitObservationDetails = UnitObservationDetails
    { _unitObservationDetailsLocation :: !(Maybe UnitLocation) }
    deriving (Show, Read, Eq)


instance Semigroup UnitObservationDetails where
    a <> b =
        UnitObservationDetails
            { _unitObservationDetailsLocation = _unitObservationDetailsLocation a <|> _unitObservationDetailsLocation b }


instance Monoid UnitObservationDetails where
    mempty =
        UnitObservationDetails
            { _unitObservationDetailsLocation = Nothing }


data UnitLocation
    = ShipLocation ShipLocation
    | VehicleLocation VehicleLocation
    deriving (Show, Read, Eq)


instance ToJSON UnitLocation where
    toJSON loc =
        case loc of
            ShipLocation l ->
                object [ "Tag" .= ("ShipLocation" :: Text)
                       , "Contents" .= l
                       ]

            VehicleLocation l ->
                object [ "Tag" .= ("VehicleLocation" :: Text)
                       , "Contents" .= l
                       ]


instance FromJSON UnitLocation where
    parseJSON = withObject "unit location" $ \o -> do
        tag <- o .: "Tag"
        asum [ do
                guard (tag == ("ShipLocation" :: Text))
                c <- o .: "Contents"
                return $ ShipLocation c
             , do
                guard (tag == ("VehicleLocation" :: Text))
                c <- o .: "Contents"
                return $ VehicleLocation c
             ]


-- | Location of an unit
unitLocation :: Unit' -> Maybe UnitLocation
unitLocation unit =
    case unit of
        Ship' ship ->
            ShipLocation <$> ship ^. shipLocation

        Vehicle' vehicle ->
            VehicleLocation <$> vehicle ^. vehicleLocation


data ShipLocation
    = PlanetarySpace PlanetId Band
    | SystemSpace StarSystemId Band
    deriving (Show, Read, Eq)


instance ToJSON ShipLocation where
    toJSON loc =
        case loc of
            PlanetarySpace pId band ->
                object [ "Tag" .= ("PlanetarySpace" :: Text)
                       , "PlanetId" .= pId
                       , "Band" .= band
                       ]

            SystemSpace sId band ->
                object [ "Tag" .= ("SystemSpace" :: Text)
                       , "SystemId" .= sId
                       , "Band" .= band
                       ]


instance FromJSON ShipLocation where
    parseJSON = withObject "ship location" $ \o -> do
        tag <- o .: "Tag"
        asum [ do
                guard (tag == ("PlanetarySpace" :: Text))
                pId <- o .: "PlanetId"
                band <- o .: "Band"
                return $ PlanetarySpace pId band
             , do
                guard (tag == ("SystemSpace" :: Text))
                sId <- o .: "SystemId"
                band <- o .: "Band"
                return $ SystemSpace sId band
             ]


-- | Location of a ship
shipLocation :: Lens' Ship (Maybe ShipLocation)
shipLocation = lens getter setter
    where
        getter s =
            case (s ^. shipStarSystemId, (s ^. shipPlanetId, s ^. shipBand)) of
                (Nothing, (Just pId, b)) ->
                    Just $ PlanetarySpace pId b

                (Just sId, (Nothing, b)) ->
                    Just $ SystemSpace sId b

                _ ->
                    Nothing

        setter s l =
            case l of
                Just (PlanetarySpace pId b) ->
                    s & shipStarSystemId .~ Nothing
                      & shipPlanetId .~ (Just pId)
                      & shipBand .~ b

                Just (SystemSpace sId b) ->
                    s & shipStarSystemId .~ (Just sId)
                      & shipPlanetId .~ Nothing
                      & shipBand .~ b

                Nothing ->
                    s & shipStarSystemId .~ Nothing
                      & shipPlanetId .~ Nothing


data VehicleLocation
    = VehicleOnPlanet PlanetId
    deriving (Show, Read, Eq)


instance ToJSON VehicleLocation where
    toJSON loc =
        case loc of
            VehicleOnPlanet pId ->
                object [ "Tag" .= ("VehicleOnPlanet" :: Text)
                       , "PlanetId" .= pId
                       ]


instance FromJSON VehicleLocation where
    parseJSON = withObject "vehicle location" $ \o -> do
        tag <- o .: "Tag"
        asum [ do
                guard (tag == ("VehicleOnPlanet" :: Text))
                pId <- o .: "PlanetId"
                return $ VehicleOnPlanet pId
             ]


-- | Location of a vehicle
vehicleLocation :: Lens' Vehicle (Maybe VehicleLocation)
vehicleLocation = lens getter setter
    where
        getter v =
            case v ^. vehiclePlanetId of
                Just pId ->
                    Just $ VehicleOnPlanet pId

                Nothing ->
                    Nothing

        setter v l =
            case l of
                Just (VehicleOnPlanet pId) ->
                    v & vehiclePlanetId .~ Just pId

                Nothing ->
                    v & vehiclePlanetId .~ Nothing


-- | Report of unit based on data available to owner
ownerReport ::
    Unit'
    -> [StatsReportDetails]
    -> Design
    -> [(CrewAssignment, Person)]
    -> UnitReport
ownerReport unit statReports design crew =
    case unit of
        Ship' ship ->
            ShipReport $ ownerShipReport ship statReports design crew

        Vehicle' vehicle ->
            VehicleReport $ ownerVehicleReport vehicle statReports design crew


ownerShipReport ::
    Ship
    -> [StatsReportDetails]
    -> Design
    -> [(CrewAssignment, Person)]
    -> ShipReportDetails
ownerShipReport ship statReports design crew =
    ShipReportDetails
        { _shipReportDetailsName = ship ^. shipName
        , _shipReportDetailsDesignName = design ^. designName
        , _shipReportDetailsStats = mconcat statReports
        , _shipReportDetailsLocation = ship ^. shipLocation
        , _shipReportDetailsCrew = assignmentToReport <$> crew
        }


ownerVehicleReport ::
    Vehicle
    -> [StatsReportDetails]
    -> Design
    -> [(CrewAssignment, Person)]
    -> VehicleReportDetails
ownerVehicleReport vehicle statReports design crew =
    VehicleReportDetails
        { _vehicleReportDetailsName = vehicle ^. vehicleName
        , _vehicleReportDetailsDesignName = design ^. designName
        , _vehicleReportDetailsStats = mconcat statReports
        , _vehicleReportDetailsLocation = vehicle ^. vehicleLocation
        , _vehicleReportDetailsCrew = assignmentToReport <$> crew
        }


assignmentToReport :: (CrewAssignment, Person) -> CrewReport
assignmentToReport (assignment, person) =
    CrewReport
    { _crewReportPersonId = assignment ^. crewAssignmentPersonId
    , _crewReportName = person ^. personName
    , _crewReportPosition = assignment ^. crewAssignmentPosition
    , _crewReportRank = assignment ^. crewAssignmentRank
    }



-- | Report of unit based on gathered data
otherReport :: Unit' -> [StatsReportDetails] -> [UnitObservationDetails] -> Design -> UnitReport
otherReport unit statReports observations design =
    case unit of
        Ship' ship ->
            ShipReport $ otherShipReport ship statReports observations design

        Vehicle' vehicle ->
            VehicleReport $ otherVehicleReport vehicle statReports observations design


otherShipReport :: Ship -> [StatsReportDetails] -> [UnitObservationDetails] -> Design -> ShipReportDetails
otherShipReport ship statReports observations design =
    ShipReportDetails
        { _shipReportDetailsName = ship ^. shipName
        , _shipReportDetailsDesignName = design ^. designName
        , _shipReportDetailsStats = mconcat statReports
        , _shipReportDetailsLocation = loc
        , _shipReportDetailsCrew = []
        }
    where
        obs = mconcat observations
        loc = case _unitObservationDetailsLocation obs of
                Just (ShipLocation x) -> Just x
                _ -> Nothing


otherVehicleReport :: Vehicle -> [StatsReportDetails] -> [UnitObservationDetails] -> Design -> VehicleReportDetails
otherVehicleReport vehicle statReports observations design =
    VehicleReportDetails
        { _vehicleReportDetailsName = vehicle ^. vehicleName
        , _vehicleReportDetailsDesignName = design ^. designName
        , _vehicleReportDetailsStats = mconcat statReports
        , _vehicleReportDetailsLocation = loc
        , _vehicleReportDetailsCrew = []
        }
    where
        obs = mconcat observations
        loc = case _unitObservationDetailsLocation obs of
                Just (VehicleLocation x) -> Just x
                _ -> Nothing


-- | Map UnitObservationDetailsJSON to Maybe UnitObservationDetails
-- | When deserializing JSON fails, Nothing is returned
deserializeObservation :: UnitObservationDetailsJSON -> Maybe UnitObservationDetails
deserializeObservation =
    decode . toLazyByteString . encodeUtf8Builder . view unUnitObservationDetailsJSON


-- | Map list of UnitObservationDetailsJSON to list of UnitObservationDetails
-- | In cases where deserialization of JSON fails, value is discarded and
-- | only succesfull cases are returned.
deserializeObservations :: [UnitObservationDetailsJSON] -> [UnitObservationDetails]
deserializeObservations entries =
    catMaybes $ deserializeObservation <$> entries


$(deriveJSON defaultOptions { fieldLabelModifier = drop 18 } ''ShipReportDetails)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 21 } ''VehicleReportDetails)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 23 } ''UnitObservationDetails)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 11 } ''CrewReport)

makePrisms ''UnitReport
makeLenses ''VehicleReportDetails
makeLenses ''ShipReportDetails
makeLenses ''CrewReport
makeLenses ''UnitObservationDetails
makePrisms ''UnitLocation
makePrisms ''ShipLocation
makePrisms ''VehicleLocation
