{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Units.Components
    ( Component(..), ComponentSlot(..), ComponentId(..), ComponentType(..), ComponentLevel(..)
    , ComponentPower(..), Weight(..), ChassisType(..), SlotAmount(..), ChassisName(..)
    , ComponentAmount(..), ComponentName(..), ComponentDescription(..), ComponentDamage(..)
    , scaleLevel, components, requirements, componentRequirements, componentId
    , componentLevel, componentName, componentDescription, componentWeight, componentSlot
    , componentType, componentCost, componentChassisType, unComponentName, unComponentDescription
    , componentPowerLevel, componentPowerType, unComponentLevel, unComponentAmount
    , unWeight, unChassisName, unSlotAmount, unComponentDamage
    )
    where


import Control.Lens ( makeLenses, makeWrapped )
import Data.Aeson ( withScientific, withText )
import Data.Aeson.TH
    ( deriveJSON, defaultOptions, fieldLabelModifier  )
import Data.Scientific ( toBoundedInteger, toRealFloat )
import Database.Persist.TH
import Database.Persist.Sql
import ClassyPrelude.Yesod   as Import
import Research.Data ( Technology(..) )
import Resources ( RawResources(..), RawResource(..), ResourceCost )


-- Types

data Component = Component
    { _componentId :: ComponentId
    , _componentLevel :: ComponentLevel
    , _componentName :: ComponentName
    , _componentDescription :: ComponentDescription
    , _componentWeight :: Weight
    , _componentSlot :: ComponentSlot
    , _componentType :: [ ComponentPower ]
    , _componentCost :: RawResources ResourceCost
    , _componentChassisType :: ChassisType
    }
    deriving (Show, Read, Eq, Ord)


newtype ComponentName = MkComponentName { _unComponentName :: Text }
    deriving (Show, Read, Eq, Ord)


instance IsString ComponentName where
    fromString = MkComponentName . fromString


instance ToJSON ComponentName where
    toJSON = toJSON . _unComponentName


instance FromJSON ComponentName where
    parseJSON =
        withText "Component name"
            (return . MkComponentName)


newtype ComponentDescription = MkComponentDescription { _unComponentDescription :: Text }
    deriving (Show, Read, Eq, Ord)


instance IsString ComponentDescription where
    fromString = MkComponentDescription . fromString


instance ToJSON ComponentDescription where
    toJSON = toJSON . _unComponentDescription


instance FromJSON ComponentDescription where
    parseJSON =
        withText "Component name"
            (return . MkComponentDescription)


-- | Type of component and relative power of the component
data ComponentPower = ComponentPower
    { _componentPowerLevel :: ComponentLevel
    , _componentPowerType :: ComponentType
    }
    deriving (Show, Read, Eq, Ord)


-- | Relatively power of a component
newtype ComponentLevel = MkComponentLevel { _unComponentLevel :: Int }
    deriving (Show, Read, Eq, Ord, Num)


-- | Scale component power with scalar
scaleLevel :: ComponentLevel -> Int -> ComponentLevel
scaleLevel (MkComponentLevel lvl) scale =
    MkComponentLevel $ lvl * scale


instance ToJSON ComponentLevel where
    toJSON = toJSON . _unComponentLevel


instance FromJSON ComponentLevel where
    parseJSON =
        withScientific "component level"
                       (\x -> case toBoundedInteger x of
                            Nothing ->
                                mempty

                            Just n ->
                                return $ MkComponentLevel n)


instance PersistField ComponentLevel where
    toPersistValue (MkComponentLevel n) =
        PersistInt64 $ fromIntegral n

    fromPersistValue (PersistInt64 n) =
        Right $ MkComponentLevel $ fromIntegral n

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql ComponentLevel where
    sqlType _ = SqlInt64


newtype ComponentAmount = MkComponentAmount { _unComponentAmount :: Int }
    deriving (Show, Read, Eq, Ord, Num)

instance ToJSON ComponentAmount where
    toJSON = toJSON . _unComponentAmount


instance FromJSON ComponentAmount where
    parseJSON =
        withScientific "component amount"
                       (\x -> case toBoundedInteger x of
                            Nothing ->
                                mempty

                            Just n ->
                                return $ MkComponentAmount n)


instance PersistField ComponentAmount where
    toPersistValue (MkComponentAmount n) =
        PersistInt64 $ fromIntegral n

    fromPersistValue (PersistInt64 n) =
        Right $ MkComponentAmount $ fromIntegral n

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql ComponentAmount where
    sqlType _ = SqlInt64


-- | Weight of something
newtype Weight = MkWeight { _unWeight :: Int }
    deriving (Show, Read, Eq, Ord, Num)


instance ToJSON Weight where
    toJSON = toJSON . _unWeight


instance FromJSON Weight where
    parseJSON =
        withScientific "Weight"
                       (\x -> case toBoundedInteger x of
                            Nothing ->
                                mempty

                            Just n ->
                                return $ MkWeight n)


instance PersistField Weight where
    toPersistValue (MkWeight n) =
        PersistInt64 $ fromIntegral n

    fromPersistValue (PersistInt64 n) =
        Right $ MkWeight $ fromIntegral n

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql Weight where
    sqlType _ = SqlInt64


-- | Enumeration of different types of components
data ComponentType =
    BridgeComponent
    | SensorComponent
    | EngineComponent
    | StarSailComponent
    | QuartersComponent
    | InfantryBayComponent
    | SupplyComponent
    | MotiveComponent
    deriving (Show, Read, Eq, Ord)


-- | Type of chassis
data ChassisType =
    SpaceShip
    | AirShip
    | LandVehicle
    | TalosArmour
    | IcarusSuit
    deriving (Show, Read, Eq, Ord)


data ComponentId =
    ShipLongRangeSensors
    | ShipShortRangeSensors
    | ShipArmour
    | ShipHeavyArmour
    | ShipBridge
    | ShipSupplyPod
    | ShipStarSail
    | ShipSteerageQuarters
    | ShipStandardQuarters
    | ShipLuxuryQuarters
    | ShipInfantryBay
    | VehicleWheeledMotiveSystem
    | VehicleTrackedMotiveSystem
    | VehicleHoverMotiveSystem
    deriving (Show, Read, Eq, Ord, Bounded, Enum)


-- | Name of chassis
newtype ChassisName = MkChassisName { _unChassisName :: Text }
    deriving (Show, Read, Eq)


instance ToJSON ChassisName where
    toJSON = toJSON . _unChassisName


instance FromJSON ChassisName where
    parseJSON =
        withText "Chassis name"
            (return . MkChassisName)


instance PersistField ChassisName where
    toPersistValue (MkChassisName s) =
        PersistText s

    fromPersistValue (PersistText s) =
        Right $ MkChassisName s

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql ChassisName where
    sqlType _ = SqlString


instance IsString ChassisName where
    fromString = MkChassisName . fromString


-- | List defining what kind of technology is required to unlock a specific component
requirements :: [(Maybe Technology, ComponentId)]
requirements =
    fmap (\x -> (componentRequirements x, x)) [minBound..maxBound]


-- | Given a component id, which technology is required to unlock it
componentRequirements :: ComponentId -> Maybe Technology
componentRequirements ShipLongRangeSensors = Just HighSensitivitySensors
componentRequirements ShipShortRangeSensors = Nothing
componentRequirements ShipArmour = Nothing
componentRequirements ShipHeavyArmour = Just HighTensileMaterials
componentRequirements ShipBridge = Nothing
componentRequirements ShipSupplyPod = Nothing
componentRequirements ShipStarSail = Nothing
componentRequirements ShipSteerageQuarters = Nothing
componentRequirements ShipStandardQuarters = Nothing
componentRequirements ShipLuxuryQuarters = Nothing
componentRequirements ShipInfantryBay = Nothing
componentRequirements VehicleWheeledMotiveSystem = Nothing
componentRequirements VehicleTrackedMotiveSystem = Nothing
componentRequirements VehicleHoverMotiveSystem = Just HoverCrafts


-- | Given a component level and id create a component
components :: ComponentLevel -> ComponentId -> Component
components level ShipStarSail =
    Component
        { _componentId = ShipStarSail
        , _componentLevel = level
        , _componentName = "Star sails"
        , _componentDescription = "Thin and strong sails capturing star winds"
        , _componentWeight = MkWeight 15
        , _componentSlot = SailSlot
        , _componentType = [ ComponentPower level StarSailComponent ]
        , _componentCost = RawResources (MkRawResource 10) (MkRawResource 0) (MkRawResource 10)
        , _componentChassisType = SpaceShip
        }

components level ShipSteerageQuarters =
    Component
        { _componentId = ShipSteerageQuarters
        , _componentLevel = level
        , _componentName = "Steerage quarters"
        , _componentDescription = "Tightly packed quarters and associated ameneties"
        , _componentWeight = MkWeight 5
        , _componentSlot = InnerSlot
        , _componentType = [ ComponentPower (scaleLevel level 10) QuartersComponent ]
        , _componentCost = RawResources (MkRawResource 10) (MkRawResource 0) (MkRawResource 0)
        , _componentChassisType = SpaceShip
        }

components level ShipStandardQuarters =
    Component
        { _componentId = ShipStandardQuarters
        , _componentLevel = level
        , _componentName = "Standard quarters"
        , _componentDescription = "Regular quarters and associated ameneties"
        , _componentWeight = MkWeight 5
        , _componentSlot = InnerSlot
        , _componentType = [ ComponentPower (scaleLevel level 5) QuartersComponent
                          , ComponentPower (scaleLevel level 2) SupplyComponent ]
        , _componentCost = RawResources (MkRawResource 12) (MkRawResource 10) (MkRawResource 0)
        , _componentChassisType = SpaceShip
        }

components level ShipLuxuryQuarters =
    Component
        { _componentId = ShipLuxuryQuarters
        , _componentLevel = level
        , _componentName = "Luxury quarters"
        , _componentDescription = "Very fine quarters and associated ameneties"
        , _componentWeight = MkWeight 5
        , _componentSlot = InnerSlot
        , _componentType = [ ComponentPower level QuartersComponent
                          , ComponentPower level SupplyComponent ]
        , _componentCost = RawResources (MkRawResource 15) (MkRawResource 5) (MkRawResource 5)
        , _componentChassisType = SpaceShip
        }

components level ShipInfantryBay =
    Component
        { _componentId = ShipInfantryBay
        , _componentLevel = level
        , _componentName = "Infantry bay"
        , _componentDescription = "Quarters and amenities for combat troops and their gear"
        , _componentWeight = MkWeight 5
        , _componentSlot = InnerSlot
        , _componentType = [ ComponentPower level InfantryBayComponent
                          , ComponentPower (scaleLevel level 2) SupplyComponent ]
        , _componentCost = RawResources (MkRawResource 20) (MkRawResource 10) (MkRawResource 10)
        , _componentChassisType = SpaceShip
        }

components level ShipShortRangeSensors =
    Component
        { _componentId = ShipShortRangeSensors
        , _componentLevel = level
        , _componentName = "Short range sensors"
        , _componentDescription = "Various scanners and sensors for short range observation"
        , _componentWeight = MkWeight 5
        , _componentSlot = SensorSlot
        , _componentType = [ ComponentPower level SensorComponent ]
        , _componentCost = RawResources (MkRawResource 1) (MkRawResource 1) (MkRawResource 1)
        , _componentChassisType = SpaceShip
        }

components level ShipLongRangeSensors =
    Component
        { _componentId = ShipLongRangeSensors
        , _componentLevel = level
        , _componentName = "Long range sensors"
        , _componentDescription = "Various scanners and sensors for long range observation"
        , _componentWeight = MkWeight 10
        , _componentSlot = SensorSlot
        , _componentType = [ ComponentPower level SensorComponent ]
        , _componentCost = RawResources (MkRawResource 5) (MkRawResource 5) (MkRawResource 5)
        , _componentChassisType = SpaceShip
        }

components level ShipSupplyPod =
    Component
        { _componentId = ShipSupplyPod
        , _componentLevel = level
        , _componentName = "Supply pod"
        , _componentDescription = "Storage system for supplies needed by the crew and the ship"
        , _componentWeight = MkWeight 10
        , _componentSlot = InnerSlot
        , _componentType = [ ComponentPower (scaleLevel level 10) SupplyComponent ]
        , _componentCost = RawResources (MkRawResource 5) (MkRawResource 50) (MkRawResource 5)
        , _componentChassisType = SpaceShip
        }

components level ShipBridge =
    Component
        { _componentId = ShipBridge
        , _componentLevel = level
        , _componentName = "Bridge"
        , _componentDescription = "Nerve center of a ship, containing controls and instruments needed for steering the ship"
        , _componentWeight = MkWeight 10
        , _componentSlot = InnerSlot
        , _componentType = [ ComponentPower level BridgeComponent
                          , ComponentPower (scaleLevel level 5) SupplyComponent ]
        , _componentCost = RawResources (MkRawResource 10) (MkRawResource 5) (MkRawResource 10)
        , _componentChassisType = SpaceShip
        }

components level ShipArmour =
    Component
        { _componentId = ShipArmour
        , _componentLevel = level
        , _componentName = "Armour"
        , _componentDescription = "Heavy protective plating against kinetic damage"
        , _componentWeight = MkWeight 20
        , _componentSlot = ArmourSlot
        , _componentType = [ ]
        , _componentCost = RawResources (MkRawResource 20) (MkRawResource 0) (MkRawResource 0)
        , _componentChassisType = SpaceShip
        }

components level ShipHeavyArmour =
    Component
        { _componentId = ShipArmour
        , _componentLevel = level
        , _componentName = "Heavy armour"
        , _componentDescription = "Extra heavy protective plating against kinetic damage"
        , _componentWeight = MkWeight 30
        , _componentSlot = ArmourSlot
        , _componentType = [ ]
        , _componentCost = RawResources (MkRawResource 30) (MkRawResource 0) (MkRawResource 0)
        , _componentChassisType = SpaceShip
        }

components level VehicleWheeledMotiveSystem =
    Component
        { _componentId = VehicleWheeledMotiveSystem
        , _componentLevel = level
        , _componentName = "Wheeled"
        , _componentDescription = "Wheels allow fast movement on hard surfaces"
        , _componentWeight = MkWeight 0
        , _componentSlot = MotiveSlot
        , _componentType = [ ComponentPower level MotiveComponent ]
        , _componentCost = mempty
        , _componentChassisType = LandVehicle
        }

components level VehicleTrackedMotiveSystem =
    Component
        { _componentId = VehicleTrackedMotiveSystem
        , _componentLevel = level
        , _componentName = "Tracks"
        , _componentDescription = "While slower than wheeled, tracked vehicles can often travel in places where wheeled can't"
        , _componentWeight = MkWeight 0
        , _componentSlot = MotiveSlot
        , _componentType = [ ComponentPower level MotiveComponent ]
        , _componentCost = RawResources (MkRawResource 30) (MkRawResource 0) (MkRawResource 0)
        , _componentChassisType = LandVehicle
        }

components level VehicleHoverMotiveSystem =
    Component
        { _componentId = VehicleTrackedMotiveSystem
        , _componentLevel = level
        , _componentName = "Hover system"
        , _componentDescription = "Hover vehicles are able to travel both on land and over water"
        , _componentWeight = MkWeight 0
        , _componentSlot = MotiveSlot
        , _componentType = [ ComponentPower level MotiveComponent ]
        , _componentCost = RawResources (MkRawResource 30) (MkRawResource 0) (MkRawResource 0)
        , _componentChassisType = LandVehicle
        }


data ComponentSlot =
    InnerSlot
    | OuterSlot
    | ArmourSlot
    | SensorSlot
    | WeaponSlot
    | EngineSlot
    | MotiveSlot
    | SailSlot
    deriving (Show, Read, Eq, Ord)


-- | Amount of slots
newtype SlotAmount = MkSlotAmount { _unSlotAmount :: Int }
    deriving (Show, Read, Eq, Ord, Num)


instance ToJSON SlotAmount where
    toJSON = toJSON . _unSlotAmount


instance FromJSON SlotAmount where
    parseJSON =
        withScientific "Weight"
                       (\x -> case toBoundedInteger x of
                            Nothing ->
                                mempty

                            Just n ->
                                return $ MkSlotAmount n)


instance PersistField SlotAmount where
    toPersistValue (MkSlotAmount n) =
        PersistInt64 $ fromIntegral n

    fromPersistValue (PersistInt64 n) =
        Right $ MkSlotAmount $ fromIntegral n

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql SlotAmount where
    sqlType _ = SqlInt64


newtype ComponentDamage = MkComponentDamage { _unComponentDamage :: Double }
    deriving (Show, Read, Eq, Num, Ord)


instance ToJSON ComponentDamage where
    toJSON = toJSON . _unComponentDamage


instance FromJSON ComponentDamage where
    parseJSON =
        withScientific "Component damage"
            (return . MkComponentDamage . toRealFloat)


instance PersistField ComponentDamage where
    toPersistValue (MkComponentDamage n) =
        PersistDouble n

    fromPersistValue (PersistDouble n) =
        Right $ MkComponentDamage n

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql ComponentDamage where
    sqlType _ = SqlNumeric 3 2


derivePersistField "ComponentSlot"
derivePersistField "ComponentType"
derivePersistField "ComponentId"
derivePersistField "ChassisType"


$(deriveJSON defaultOptions ''ComponentType)
$(deriveJSON defaultOptions ''ComponentId)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 15 } ''ComponentPower)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 10 } ''Component)
$(deriveJSON defaultOptions ''ComponentSlot)
$(deriveJSON defaultOptions ''ChassisType)


makeLenses ''Component
makeLenses ''ComponentName
makeWrapped ''ComponentName
makeLenses ''ComponentDescription
makeWrapped ''ComponentDescription
makeLenses ''ComponentPower
makeLenses ''ComponentLevel
makeWrapped ''ComponentLevel
makeLenses ''ComponentAmount
makeWrapped ''ComponentAmount
makeLenses ''Weight
makeWrapped ''Weight
makeLenses ''ChassisName
makeWrapped ''ChassisName
makeLenses ''SlotAmount
makeWrapped ''SlotAmount
makeLenses ''ComponentDamage
makeWrapped ''ComponentDamage
