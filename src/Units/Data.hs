{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE TypeFamilies               #-}


module Units.Data ( ShipName(..), ShipType(..)
                  , CrewPosition(..), CrewSpaceReq(..), CrewRank(..)
                  , CrewAmount(..), VehicleName(..), Band(..)
                  , UnitObservationDetailsJSON(..), StatsReportDetails(..)
                  , TotalCrewSpace(..), CrewSpace(..), SteerageQuarters(..)
                  , StandardQuarters(..), LuxuryQuarters(..)
                  , CrewRequirement(..), UnitName(..), DesignName(..)
                  , unDesignName, unShipName, unVehicleName, unUnitName
                  , unCrewAmount, unUnitObservationDetailsJSON
                  , crewRequirementPosition, crewRequirementRank
                  , crewRequirementAmount, unCrewSpace, totalCrewSpaceSteerage
                  , totalCrewSpaceStandard, totalCrewSpaceLuxury, statsReportDetailsMinimumCrew
                  , statsReportDetailsNominalCrew, statsReportDetailsCrewSpace
                  , statsReportDetailsCrewSpaceRequired
)
    where

import ClassyPrelude.Yesod   as Import
import Control.Lens ( makeLenses, makeWrapped )
import Data.Aeson ( withText, withScientific, withObject )
import Data.Aeson.TH
import Data.Scientific ( toBoundedInteger )
import Database.Persist.Sql


newtype DesignName = MkDesignName { _unDesignName :: Text }
    deriving (Show, Read, Eq)


instance ToJSON DesignName where
    toJSON = toJSON . _unDesignName


instance FromJSON DesignName where
    parseJSON =
        withText "Design name"
            (return . MkDesignName)


instance PersistField DesignName where
    toPersistValue (MkDesignName s) =
        PersistText s

    fromPersistValue (PersistText s) =
        Right $ MkDesignName s

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql DesignName where
    sqlType _ = SqlString


instance IsString DesignName where
    fromString = MkDesignName . fromString


-- | Name of ship
newtype ShipName = MkShipName { _unShipName :: Text }
    deriving (Show, Read, Eq)


instance ToJSON ShipName where
    toJSON = toJSON . _unShipName


instance FromJSON ShipName where
    parseJSON =
        withText "Ship name"
            (return . MkShipName)


instance PersistField ShipName where
    toPersistValue (MkShipName s) =
        PersistText s

    fromPersistValue (PersistText s) =
        Right $ MkShipName s

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql ShipName where
    sqlType _ = SqlString


instance IsString ShipName where
    fromString = MkShipName . fromString


-- | Name of vehicle
newtype VehicleName = MkVehicleName { _unVehicleName :: Text }
    deriving (Show, Read, Eq)


instance ToJSON VehicleName where
    toJSON = toJSON . _unVehicleName


instance FromJSON VehicleName where
    parseJSON =
        withText "vehicle name"
            (return . MkVehicleName)


instance PersistField VehicleName where
    toPersistValue (MkVehicleName s) =
        PersistText s

    fromPersistValue (PersistText s) =
        Right $ MkVehicleName s

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql VehicleName where
    sqlType _ = SqlString


instance IsString VehicleName where
    fromString = MkVehicleName . fromString


-- | Name of unit
newtype UnitName = MkUnitName { _unUnitName :: Text }
    deriving (Show, Read, Eq)


instance ToJSON UnitName where
    toJSON = toJSON . _unUnitName


instance FromJSON UnitName where
    parseJSON =
        withText "unit name"
            (return . MkUnitName)


instance IsString UnitName where
    fromString = MkUnitName . fromString


data ShipType =
    Bawley
    | Bilander
    | BlackwallFrigate
    | Brigantine
    | Caravel
    | Clipper
    | Cog
    | Corvette
    | CraneShip
    | CruiseLiner
    | Freighter
    | Flyboat
    | Frigate
    | Galleon
    | ManOfWar
    | SatelliteLayer
    | Schooner
    | Yawl
    | MobileBase
    | Station
    | Satellite
    deriving (Show, Read, Eq, Bounded, Ord, Enum)
derivePersistField "ShipType"


data CrewPosition =
    Commander
    | Navigator
    | Signaler
    | SensorOperator
    | Gunner
    | Doctor
    | Nurse
    | Driver
    | Helmsman
    | Artificer
    | Crew
    | Passenger
    deriving (Show, Read, Eq, Bounded, Ord, Enum)
derivePersistField "CrewPosition"


data CrewRank =
    SecondClass
    | FirstClass
    | Senior
    | Chief
    deriving (Show, Read, Eq, Bounded, Ord, Enum)
derivePersistField "CrewRank"


-- | Does crew require quarters
data CrewSpaceReq =
    CrewSpaceRequired
    | CrewSpaceOptional
    deriving (Show, Read, Eq, Bounded, Ord, Enum)
derivePersistField "CrewSpaceReq"


newtype CrewAmount = MkCrewAmount { _unCrewAmount :: Int }
    deriving (Show, Read, Eq, Ord, Num)


instance ToJSON CrewAmount where
    toJSON = toJSON . _unCrewAmount


instance FromJSON CrewAmount where
    parseJSON =
        withScientific "crew amount"
                       (\x -> case toBoundedInteger x of
                            Nothing ->
                                mempty

                            Just n ->
                                return $ MkCrewAmount n)


instance PersistField CrewAmount where
    toPersistValue (MkCrewAmount n) =
        PersistInt64 $ fromIntegral n

    fromPersistValue (PersistInt64 n) =
        Right $ MkCrewAmount $ fromIntegral n

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql CrewAmount where
    sqlType _ = SqlInt64


-- | Band denotes distance from a planet or other celestial object
-- BandSurface indicates surface of the object.
-- BandOrbit indicates being on orbit
-- Bands A and B are at immediate vicinity. In case or asteroid fields, these
-- are inside of the field.
-- Bands C, D and E are at close proximity.
-- Bands F and G are in deep space.
data Band
    = BandSurface
    | BandOrbit
    | BandA
    | BandB
    | BandC
    | BandD
    | BandE
    | BandF
    | BandG
    deriving (Show, Read, Eq, Bounded, Ord, Enum)
derivePersistField "Band"


-- | newtype used to classify Text as JSON storing UnitObservationDetails
newtype UnitObservationDetailsJSON =
    MkUnitObservationDetailsJSON { _unUnitObservationDetailsJSON :: Text }
    deriving (Show, Read, Eq)


instance PersistField UnitObservationDetailsJSON where
    toPersistValue (MkUnitObservationDetailsJSON s) =
        PersistText s

    fromPersistValue (PersistText s) =
        Right $ MkUnitObservationDetailsJSON s

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql UnitObservationDetailsJSON where
    sqlType _ = SqlString


instance ToJSON UnitObservationDetailsJSON where
    toJSON = toJSON . _unUnitObservationDetailsJSON


instance FromJSON UnitObservationDetailsJSON where
    parseJSON =
        withText "Unit observation details json"
            (return . MkUnitObservationDetailsJSON)


data CrewRequirement = CrewRequirement
    { _crewRequirementPosition :: !CrewPosition
    , _crewRequirementRank :: !CrewRank
    , _crewRequirementAmount :: !CrewAmount
    }
    deriving (Show, Read, Eq)


-- | Space of specific quality for crew
newtype CrewSpace a = MkCrewSpace { _unCrewSpace :: CrewAmount }
    deriving (Show, Read, Eq)


instance Num (CrewSpace a) where
    (MkCrewSpace a) + (MkCrewSpace b) =
        MkCrewSpace (a + b)

    (MkCrewSpace a) * (MkCrewSpace b) =
        MkCrewSpace (a * b)

    abs (MkCrewSpace a) =
        MkCrewSpace (abs a)

    signum (MkCrewSpace n)
        | n < 0 = -1
        | n == 0 = 0
        | otherwise = 1

    fromInteger n =
        MkCrewSpace $ MkCrewAmount $ fromIntegral n

    (MkCrewSpace a) - (MkCrewSpace b) =
        MkCrewSpace (a - b)


instance Semigroup (CrewSpace a) where
    (<>) = (+)


instance Monoid (CrewSpace a) where
    mempty = 0


data SteerageQuarters = SteerageQuarters
    deriving (Show, Read, Eq)

data StandardQuarters = StandardQuarters
    deriving (Show, Read, Eq)

data LuxuryQuarters = LuxuryQuarters
    deriving (Show, Read, Eq)


-- | Total space for crew, categorized by quality of quarters
data TotalCrewSpace = TotalCrewSpace
    { _totalCrewSpaceSteerage :: !(CrewSpace SteerageQuarters)
    , _totalCrewSpaceStandard :: !(CrewSpace StandardQuarters)
    , _totalCrewSpaceLuxury :: !(CrewSpace LuxuryQuarters)
    } deriving (Show, Read, Eq)


instance Semigroup TotalCrewSpace where
    a <> b =
        TotalCrewSpace
        { _totalCrewSpaceSteerage = _totalCrewSpaceSteerage a <> _totalCrewSpaceSteerage b
        , _totalCrewSpaceStandard = _totalCrewSpaceStandard a <> _totalCrewSpaceStandard b
        , _totalCrewSpaceLuxury = _totalCrewSpaceLuxury a <> _totalCrewSpaceLuxury b
        }


instance Monoid TotalCrewSpace where
    mempty =
        TotalCrewSpace
        { _totalCrewSpaceSteerage = 0
        , _totalCrewSpaceStandard = 0
        , _totalCrewSpaceLuxury = 0
        }


instance ToJSON TotalCrewSpace where
    toJSON tSpace =
        object [ "Steerage" .= (_unCrewSpace . _totalCrewSpaceSteerage) tSpace
               , "Standard" .= (_unCrewSpace . _totalCrewSpaceStandard) tSpace
               , "Luxury" .= (_unCrewSpace . _totalCrewSpaceLuxury) tSpace
               ]


instance FromJSON TotalCrewSpace where
    parseJSON = withObject "total crew space" $ \o -> do
        steerage <- o .: "Steerage"
        standard <- o .: "Standard"
        luxury <- o .: "Luxury"
        return $ TotalCrewSpace
            { _totalCrewSpaceSteerage = MkCrewSpace steerage
            , _totalCrewSpaceStandard = MkCrewSpace standard
            , _totalCrewSpaceLuxury = MkCrewSpace luxury
            }


-- | StatsReportDetails is used to record observed stats of unit
-- | Since not all stats are always known, all fields are Maybe a
-- | where Nothing indicates that particular aspect was not observed.
data StatsReportDetails = StatsReportDetails
    { _statsReportDetailsMinimumCrew :: Maybe [CrewRequirement]
    , _statsReportDetailsNominalCrew :: Maybe [CrewRequirement]
    , _statsReportDetailsCrewSpace :: Maybe TotalCrewSpace
    , _statsReportDetailsCrewSpaceRequired :: Maybe CrewSpaceReq
    }
    deriving (Show, Read, Eq)
derivePersistField "StatsReportDetails"


-- | StatsReportDetails forms semigroup, where combining a and b will
-- always yield a if it's Just, otherwise b.
-- This is useful when collating multiple stats reports where some values
-- might not be present.
instance Semigroup StatsReportDetails where
    a <> b =
        StatsReportDetails
            { _statsReportDetailsMinimumCrew = _statsReportDetailsMinimumCrew a <|> _statsReportDetailsMinimumCrew b
            , _statsReportDetailsNominalCrew = _statsReportDetailsNominalCrew a <|> _statsReportDetailsNominalCrew b
            , _statsReportDetailsCrewSpace = _statsReportDetailsCrewSpace a <|> _statsReportDetailsCrewSpace b
            , _statsReportDetailsCrewSpaceRequired = _statsReportDetailsCrewSpaceRequired a <|> _statsReportDetailsCrewSpaceRequired b
            }


-- | StatsReportDetails forms a monoid, where zero element is report without any
-- information at all.
instance Monoid StatsReportDetails where
    mempty =
        StatsReportDetails
            { _statsReportDetailsMinimumCrew = Nothing
            , _statsReportDetailsNominalCrew = Nothing
            , _statsReportDetailsCrewSpace = Nothing
            , _statsReportDetailsCrewSpaceRequired = Nothing
            }


$(deriveJSON defaultOptions ''ShipType)
$(deriveJSON defaultOptions ''CrewPosition)
$(deriveJSON defaultOptions ''CrewSpaceReq)
$(deriveJSON defaultOptions ''CrewRank)
$(deriveJSON defaultOptions ''Band)
$(deriveJSON defaultOptions ''CrewRequirement)
$(deriveJSON defaultOptions ''CrewSpace)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 18 } ''StatsReportDetails)


makeLenses ''DesignName
makeWrapped ''DesignName
makeLenses ''ShipName
makeWrapped ''ShipName
makeLenses ''VehicleName
makeWrapped ''VehicleName
makeLenses ''UnitName
makeWrapped ''UnitName
makeLenses ''CrewAmount
makeWrapped ''CrewAmount
makeLenses ''UnitObservationDetailsJSON
makeWrapped ''UnitObservationDetailsJSON
makeLenses ''CrewRequirement
makeLenses ''CrewSpace
makeWrapped ''CrewSpace
makeLenses ''TotalCrewSpace
makeLenses ''StatsReportDetails
