{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}


module Units.Lenses ( unitNameL, _Ship, _Vehicle )
    where

import Import
import Control.Lens ( Lens', Prism', lens, prism, (.~), (&), to, (^.) )
import Units.Data ( UnitName(..), VehicleName(..), ShipName(..), unUnitName
                  , unShipName, unVehicleName
                  )
import Units.Queries ( Unit'(..) )


_Ship :: Prism' Unit' Ship
_Ship =
    prism Ship' $ \x -> case x of
        Ship' a ->
            Right a

        _ ->
            Left x


_Vehicle :: Prism' Unit' Vehicle
_Vehicle =
    prism Vehicle' $ \x -> case x of
        Vehicle' a ->
            Right a

        _ ->
            Left x


unitNameL :: Lens' Unit' UnitName
unitNameL = lens (\unit ->
                    case unit of
                        Vehicle' vehicle ->
                            vehicle ^. vehicleName . unVehicleName . to MkUnitName

                        Ship' ship ->
                             ship ^. shipName . unShipName . to MkUnitName)

                 (\unit name ->
                     case unit of
                        Ship' _ ->
                            unit & (_Ship . shipName) .~ (name ^. unUnitName . to MkShipName)

                        Vehicle' _ ->
                            unit & (_Vehicle . vehicleName) .~ (name ^. unUnitName . to MkVehicleName))
