{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE TemplateHaskell       #-}

module Dto.Construction
    ( ConstructionDto(..), buildingConstructionToDto, shipConstructionToDto
    , constructionIndex, BuildingConstructionDetailsDto(..), ShipConstructionDetailsDto(..)
    , _BuildingConstructionDto, _ShipConstructionDto, bcdtoId, bcdtoName, bcdtoIndex
    , bcdtoLevel, bcdtoType, bcdtoPlanet, bcdtoCostLeft, scdtoId, scdtoName, scdtoShipType
    , scdtoIndex
    )
where

import Import
import Control.Lens ( (^.), to, (-~), (&), view, makeLenses, makePrisms )

import Buildings ( building, BuildingLevel(..), buildingInfoCost )
import CustomTypes ( buildingTypeName, BuildingType(..))
import Resources ( RawResources(..), ConstructionLeft, ccdChemicalCost
                 , ccdBiologicalCost, ccdMechanicalCost, unRawResource
                 )
import Units.Data ( ShipType(..) )

data ConstructionDto =
  BuildingConstructionDto BuildingConstructionDetailsDto
  | ShipConstructionDto ShipConstructionDetailsDto
  deriving (Show, Read, Eq)


data BuildingConstructionDetailsDto = BuildingConstructionDetailsDto
    { _bcdtoId :: BuildingConstructionId
    , _bcdtoName :: Text
    , _bcdtoIndex :: Int
    , _bcdtoLevel :: Int
    , _bcdtoType :: BuildingType
    , _bcdtoPlanet :: PlanetId
    , _bcdtoCostLeft :: RawResources ConstructionLeft
    }
  deriving (Show, Read, Eq)


data ShipConstructionDetailsDto = ShipConstructionDetailsDto
  { _scdtoId :: ShipConstructionId
  , _scdtoName :: Text
  , _scdtoShipType :: ShipType
  , _scdtoIndex :: Int
  }
  deriving (Show, Read, Eq)


instance ToJSON ConstructionDto where
  toJSON (BuildingConstructionDto bc) =
    object [ "id" .= _bcdtoId bc
           , "name" .= _bcdtoName bc
           , "index" .= _bcdtoIndex bc
           , "level" .= _bcdtoLevel bc
           , "type" .= _bcdtoType bc
           , "planet" .= _bcdtoPlanet bc
           , "costLeft" .= _bcdtoCostLeft bc
           ]

  toJSON (ShipConstructionDto sc) =
    object [ "id" .= _scdtoId sc
           , "name" .= _scdtoName sc
           , "shipType" .= _scdtoShipType sc
           , "index" .= _scdtoIndex sc
           ]

instance FromJSON ConstructionDto where
  parseJSON (Object v) =
    asum [ BuildingConstructionDto <$>
            (BuildingConstructionDetailsDto
                                  <$> v .: "id"
                                  <*> v .: "name"
                                  <*> v .: "index"
                                  <*> v .: "level"
                                  <*> v .: "type"
                                  <*> v .: "planet"
                                  <*> v .: "costLeft")
         , ShipConstructionDto <$>
            (ShipConstructionDetailsDto
                              <$> v .: "id"
                              <*> v .: "name"
                              <*> v .: "shipType"
                              <*> v .: "index")
         ]
  parseJSON _ = mzero


-- TODO: maybe turn this into a lens?
constructionIndex :: ConstructionDto -> Int
constructionIndex c =
    case c of
      BuildingConstructionDto details ->
          _bcdtoIndex details
      ShipConstructionDto details ->
          _scdtoIndex details


buildingConstructionToDto :: Entity BuildingConstruction -> ConstructionDto
buildingConstructionToDto bce =
    BuildingConstructionDto
        BuildingConstructionDetailsDto
                            { _bcdtoId = key
                            , _bcdtoName = buildingTypeName $ bc ^. buildingConstructionType
                            , _bcdtoIndex = bc ^. buildingConstructionIndex
                            , _bcdtoLevel = bc ^. buildingConstructionLevel
                            , _bcdtoType = bc ^. buildingConstructionType
                            , _bcdtoPlanet = bc ^. buildingConstructionPlanetId
                            , _bcdtoCostLeft = RawResources mech bio chem
                            }
  where
    bc = bce ^. entityValL
    key = bce ^. entityKeyL
    template = building (bc ^. buildingConstructionType) (bc ^.buildingConstructionLevel . to MkBuildingLevel)
    cost = template ^. buildingInfoCost
    -- TODO: clean up when you have lenses for own newtypes
    mech = cost ^. ccdMechanicalCost & unRawResource -~ view buildingConstructionProgressMechanicals bc
    bio = cost ^. ccdBiologicalCost & unRawResource -~ view buildingConstructionProgressBiologicals bc
    chem = cost ^. ccdChemicalCost & unRawResource -~ view buildingConstructionProgressChemicals bc


shipConstructionToDto :: Entity ShipConstruction -> ConstructionDto
shipConstructionToDto sce =
    ShipConstructionDto $
        ShipConstructionDetailsDto key "TODO: ship name" Schooner (sc ^. shipConstructionIndex)
    where
        sc = entityVal sce
        key = entityKey sce


makePrisms ''ConstructionDto
makeLenses ''BuildingConstructionDetailsDto
makeLenses ''ShipConstructionDetailsDto
