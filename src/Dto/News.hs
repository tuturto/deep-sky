{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE TemplateHaskell       #-}

module Dto.News
    ( NewsDto(..), NewsArticleDto(..), StarFoundNewsDto(..)
    , PlanetFoundNewsDto(..), UserWrittenNewsDto(..), DesignCreatedNewsDto(..)
    , ConstructionFinishedNewsDto(..), UserNewsIconDto(..), SpecialNewsDto(..)
    , KragiiWormsEventDto(..), UserOptionDto(..), KragiiWormsChoiceDto(..)
    , KragiiNewsDto(..), ProductionChangedNewsDto(..)
    , ResearchCompletedNewsDto(..), ScurryingSoundsNewsDto(..)
    , ScurryingSoundsEventDto(..), ScurryingSoundsChoiceDto(..)
    , NamingPetNewsDto(..), NamingPetChoiceDto(..), NamingPetEventDto(..)
    , newsDtoId, newsContents, newsIcon, _StarFoundDto, _PlanetFoundDto
    , _UserWrittenDto, _DesignCreatedDto, _ConstructionFinishedDto
    , _ProductionBoostStartedDto, _ProductionSlowdownStartedDto, _ProductionBoostEndedDto
    , _ProductionSlowdownEndedDto, _ResearchCompletedDto, _KragiiDto, _ScurryingSoundsDto
    , _NamingPetDto, _SpecialDto, _KragiiEventDto, _MkScurryingSoundsEventDto
    , _MkNamingPetEventDto, kragiiWormsDtoPlanetId, kragiiWormsDtoPlanetName
    , kragiiWormsDtoSystemId, kragiiWormsDtoSystemName, kragiiWormsDtoOptions
    , kragiiWormsDtoChoice, kragiiWormsDtoFactionId, kragiiWormsDtoDate, kragiiWormsDtoResolveType
    , scurryingSoundsEventDtoDate, scurryingSoundsEventDtoPersonId, scurryingSoundsEventDtoOptions
    , scurryingSoundsEventDtoChoice, scurryingSoundsEventDtoResolveType, namingPetEventDtoPersonId
    , namingPetEventDtoPetId, namingPetEventDtoPetType, namingPetEventDtoDate
    , namingPetEventDtoOptions, namingPetEventDtoChoice, namingPetEventDtoResolveType
    , userOptionDtoTitle, userOptionDtoExplanation, userOptionDtoChoice, kragiiNewsDtoPlanetId
    , kragiiNewsDtoPlanetName, kragiiNewsDtoSystemId, kragiiNewsDtoSystemName
    , kragiiNewsDtoResolution, kragiiNewsDtoFactionId, kragiiNewsDtoDate, scurryingSoundsNewsDtoExplanation
    , scurryingSoundsNewsDtoPetId, scurryingSoundsNewsDtoPetType, scurryingSoundsNewsDtoDate
    , namingPetNewsDtoExplanation, namingPetNewsDtoPetId, namingPetNewsDtoPetType
    , namingPetNewsDtoDate, starFoundNewsDtoStarName, starFoundNewsDtoSystemName
    , starFoundNewsDtoSystemId, starFoundNewsDtoDate, planetFoundNewsDtoPlanetName
    , planetFoundNewsDtoSystemName, planetFoundNewsDtoSystemId, planetFoundNewsDtoPlanetId
    , planetFoundNewsDtoDate, userWrittenNewsDtoContent, userWrittenNewsDtoDate
    , userWrittenNewsDtoUser, userWrittenNewsDtoIcon, designCreatedNewsDtoDesignId
    , designCreatedNewsDtoName, designCreatedNewsDtoDate, constructionFinishedNewsDtoPlanetName
    , constructionFinishedNewsDtoPlanetId, constructionFinishedNewsDtoSystemName
    , constructionFinishedNewsDtoSystemId, constructionFinishedNewsDtoConstructionName
    , constructionFinishedNewsDtoBuildingId, constructionFinishedNewsDtoShipId
    , constructionFinishedNewsDtoDate, productionChangedNewsDtoPlanetId
    , productionChangedNewsDtoPlanetName, productionChangedNewsDtoSystemId
    , productionChangedNewsDtoSystemName, productionChangedNewsDtoType
    , productionChangedNewsDtoDate, researchCompletedNewsDtoTechnology
    , researchCompletedNewsDtoName, researchCompletedNewsDtoDate
    ) where

import Import
import Control.Lens ( makeLenses, makePrisms )
import Data.Aeson ( (.!=), (.:?), withObject )
import Data.Aeson.TH ( deriveJSON, defaultOptions, constructorTagModifier, fieldLabelModifier )
import Events.Import ( EventResolveType(..) )
import CustomTypes ( StarDate )
import Research.Data ( Technology )
import People.Data ( PersonName(..), PetType(..), PetName(..) )
import Resources ( ResourceType(..) )
import Space.Data ( PlanetName(..), StarName(..), StarSystemName(..) )
import Units.Data ( DesignName(..) )


-- | Data transfer object for news that mainly just wraps id and contents together
data NewsDto = NewsDto
    { _newsDtoId :: !NewsId
    , _newsContents :: !NewsArticleDto
    , _newsIcon :: !Text
    } deriving (Show, Read, Eq)


-- | data transfer object for various types of news
data NewsArticleDto =
    StarFoundDto StarFoundNewsDto
    | PlanetFoundDto PlanetFoundNewsDto
    | UserWrittenDto UserWrittenNewsDto
    | DesignCreatedDto DesignCreatedNewsDto
    | ConstructionFinishedDto ConstructionFinishedNewsDto
    | ProductionBoostStartedDto ProductionChangedNewsDto
    | ProductionSlowdownStartedDto ProductionChangedNewsDto
    | ProductionBoostEndedDto ProductionChangedNewsDto
    | ProductionSlowdownEndedDto ProductionChangedNewsDto
    | ResearchCompletedDto ResearchCompletedNewsDto
    | KragiiDto KragiiNewsDto
    | ScurryingSoundsDto ScurryingSoundsNewsDto
    | NamingPetDto NamingPetNewsDto
    | SpecialDto SpecialNewsDto
    deriving (Show, Read, Eq)


-- | data transfer object for all kinds of special news
data SpecialNewsDto =
    KragiiEventDto KragiiWormsEventDto
    | MkScurryingSoundsEventDto ScurryingSoundsEventDto
    | MkNamingPetEventDto NamingPetEventDto
    deriving (Show, Read, Eq)


-- | Data transfer object for kragii attack special event
data KragiiWormsEventDto = KragiiWormsEventDto
    { _kragiiWormsDtoPlanetId :: !PlanetId
    , _kragiiWormsDtoPlanetName :: !PlanetName
    , _kragiiWormsDtoSystemId :: !StarSystemId
    , _kragiiWormsDtoSystemName :: !StarSystemName
    , _kragiiWormsDtoOptions :: ![UserOptionDto KragiiWormsChoiceDto]
    , _kragiiWormsDtoChoice :: !(Maybe KragiiWormsChoiceDto)
    , _kragiiWormsDtoFactionId :: !FactionId
    , _kragiiWormsDtoDate :: !StarDate
    , _kragiiWormsDtoResolveType :: !(Maybe EventResolveType)
    } deriving (Show, Read, Eq)


-- | Data transfer object for scurrying sounds special event
data ScurryingSoundsEventDto = ScurryingSoundsEventDto
    { _scurryingSoundsEventDtoDate :: !StarDate
    , _scurryingSoundsEventDtoPersonId :: !PersonId
    , _scurryingSoundsEventDtoOptions :: ![UserOptionDto ScurryingSoundsChoiceDto]
    , _scurryingSoundsEventDtoChoice :: !(Maybe ScurryingSoundsChoiceDto)
    , _scurryingSoundsEventDtoResolveType :: !(Maybe EventResolveType)
    } deriving (Show, Read, Eq)


-- | data transfer object for user choice regarding kragii attack
data KragiiWormsChoiceDto =
    EvadeWormsDto
    | AttackWormsDto
    | TameWormsDto
    deriving (Show, Read, Eq)


data ScurryingSoundsChoiceDto =
    GetCatDto
    | TameRatDto
    | GetRidSomehowElseDto
    deriving (Show, Read, Eq)


data NamingPetEventDto = NamingPetEventDto
    { _namingPetEventDtoPersonId :: !PersonId
    , _namingPetEventDtoPetId :: !PetId
    , _namingPetEventDtoPetType :: !PetType
    , _namingPetEventDtoDate :: !StarDate
    , _namingPetEventDtoOptions :: ![UserOptionDto NamingPetChoiceDto]
    , _namingPetEventDtoChoice :: !(Maybe NamingPetChoiceDto)
    , _namingPetEventDtoResolveType :: !(Maybe EventResolveType)
    } deriving (Show, Read, Eq)


data NamingPetChoiceDto =
    GiveNameDto PetName
    | LetSomeoneElseDecideDto
    deriving (Show, Read, Eq)


-- | data transfer option for general user choice regarding special event
data UserOptionDto a = UserOptionDto
    { _userOptionDtoTitle :: Text
    , _userOptionDtoExplanation :: [Text]
    , _userOptionDtoChoice :: a
    } deriving (Show, Read, Eq)


-- | data transfer object for news that tell resolution of kragii attack
data KragiiNewsDto = KragiiNewsDto
    { _kragiiNewsDtoPlanetId :: !PlanetId
    , _kragiiNewsDtoPlanetName :: !PlanetName
    , _kragiiNewsDtoSystemId :: !StarSystemId
    , _kragiiNewsDtoSystemName :: !StarSystemName
    , _kragiiNewsDtoResolution :: !Text
    , _kragiiNewsDtoFactionId :: !FactionId
    , _kragiiNewsDtoDate :: !StarDate
    } deriving (Show, Read, Eq)


data ScurryingSoundsNewsDto = ScurryingSoundsNewsDto
    { _scurryingSoundsNewsDtoExplanation :: !Text
    , _scurryingSoundsNewsDtoPetId :: !(Maybe PetId)
    , _scurryingSoundsNewsDtoPetType :: !(Maybe PetType)
    , _scurryingSoundsNewsDtoDate :: !StarDate
    } deriving (Show, Read, Eq)


data NamingPetNewsDto = NamingPetNewsDto
    { _namingPetNewsDtoExplanation :: !Text
    , _namingPetNewsDtoPetId :: !PetId
    , _namingPetNewsDtoPetType :: !PetType
    , _namingPetNewsDtoDate :: !StarDate
    } deriving (Show, Read, Eq)


-- | data transfer object for star found news
data StarFoundNewsDto = StarFoundNewsDto
    { _starFoundNewsDtoStarName :: !StarName
    , _starFoundNewsDtoSystemName :: !StarSystemName
    , _starFoundNewsDtoSystemId :: !StarSystemId
    , _starFoundNewsDtoDate :: !StarDate
    } deriving (Show, Read, Eq)


-- | data transfer object for planet found news
data PlanetFoundNewsDto = PlanetFoundNewsDto
    { _planetFoundNewsDtoPlanetName :: !PlanetName
    , _planetFoundNewsDtoSystemName :: !StarSystemName
    , _planetFoundNewsDtoSystemId :: !StarSystemId
    , _planetFoundNewsDtoPlanetId :: !PlanetId
    , _planetFoundNewsDtoDate :: !StarDate
    } deriving (Show, Read, Eq)


-- | data transfer object for user written news
data UserWrittenNewsDto = UserWrittenNewsDto
    { _userWrittenNewsDtoContent :: !Text
    , _userWrittenNewsDtoDate :: !StarDate
    , _userWrittenNewsDtoUser :: !PersonName
    , _userWrittenNewsDtoIcon :: !UserNewsIconDto
    } deriving (Show, Read, Eq)


-- | Icon displayed on user written news
data UserNewsIconDto =
    GenericUserNewsDto
    | JubilationUserNewsDto
    | CatUserNewsDto
    deriving (Show, Read, Eq, Enum, Bounded)


-- | data transfer object for design created news
data DesignCreatedNewsDto = DesignCreatedNewsDto
    { _designCreatedNewsDtoDesignId :: !DesignId
    , _designCreatedNewsDtoName :: !DesignName
    , _designCreatedNewsDtoDate :: !StarDate
    } deriving (Show, Read, Eq)


-- | data transfer object for construction finished news
data ConstructionFinishedNewsDto = ConstructionFinishedNewsDto
    { _constructionFinishedNewsDtoPlanetName :: !(Maybe PlanetName)
    , _constructionFinishedNewsDtoPlanetId :: !(Maybe PlanetId)
    , _constructionFinishedNewsDtoSystemName :: !StarSystemName
    , _constructionFinishedNewsDtoSystemId :: !StarSystemId
    , _constructionFinishedNewsDtoConstructionName :: !Text
    , _constructionFinishedNewsDtoBuildingId :: !(Maybe BuildingId)
    , _constructionFinishedNewsDtoShipId :: !(Maybe ShipId)
    , _constructionFinishedNewsDtoDate :: !StarDate
    } deriving (Show, Read, Eq)


-- | Turn NewsDto into JSON
instance ToJSON NewsDto where
    toJSON NewsDto { _newsDtoId = nId
                   , _newsContents = contents
                   , _newsIcon = icon } =
        object [ "id" .= nId
               , "contents" .= contents
               , "tag" .= jsonTag contents
               , "icon" .= icon
               , "starDate" .= newsStarDate contents
               ]


-- | tag is used to distinguish different types of news articles in their JSON
-- representation. Note that this mapping doesn't correspond 1:1 with the types
-- as some DTOs (construction finished for example) will be serialized to JSON
-- differently depending on the data they contain (building vs. ship)
jsonTag :: NewsArticleDto -> Text
jsonTag news =
    case news of
        StarFoundDto _ ->
            "StarFound"

        PlanetFoundDto _ ->
            "PlanetFound"

        UserWrittenDto _ ->
            "UserWritten"

        DesignCreatedDto _ ->
            "DesignCreated"

        ConstructionFinishedDto ConstructionFinishedNewsDto { _constructionFinishedNewsDtoBuildingId = mbId} ->
            case mbId of
                Just _ ->
                    "BuildingFinished"

                Nothing ->
                    "ShipFinished"

        ProductionBoostStartedDto _ ->
            "ProductionBoostStarted"

        ProductionSlowdownStartedDto _ ->
            "ProductionSlowdownStarted"

        ProductionBoostEndedDto _ ->
            "ProductionBoostEnded"

        ProductionSlowdownEndedDto _ ->
            "ProductionSlowdownEnded"

        ResearchCompletedDto _ ->
            "ResearchCompleted"

        SpecialDto (KragiiEventDto _) ->
            "KragiiEvent"

        SpecialDto (MkScurryingSoundsEventDto _) ->
            "ScurryingSoundsEvent"

        KragiiDto _ ->
            "KragiiResolution"

        ScurryingSoundsDto _ ->
            "ScurryingSoundsResolution"

        NamingPetDto _ ->
            "NamingPetResolution"

        SpecialDto (MkNamingPetEventDto _) ->
            "NamingPetEvent"


instance ToJSON NewsArticleDto where
    toJSON news =
        case news of
            StarFoundDto dto -> toJSON dto
            PlanetFoundDto dto -> toJSON dto
            UserWrittenDto dto -> toJSON dto
            DesignCreatedDto dto -> toJSON dto
            ConstructionFinishedDto dto -> toJSON dto
            ProductionBoostStartedDto dto -> toJSON dto
            ProductionSlowdownStartedDto dto -> toJSON dto
            ProductionBoostEndedDto dto -> toJSON dto
            ProductionSlowdownEndedDto dto -> toJSON dto
            ResearchCompletedDto dto -> toJSON dto
            KragiiDto dto -> toJSON dto
            ScurryingSoundsDto dto -> toJSON dto
            NamingPetDto dto -> toJSON dto
            SpecialDto (KragiiEventDto dto) -> toJSON dto
            SpecialDto (MkScurryingSoundsEventDto dto) -> toJSON dto
            SpecialDto (MkNamingPetEventDto dto) -> toJSON dto


instance FromJSON NewsArticleDto where
    parseJSON = withObject "contents" $ \o -> do
        tag <- o .: "tag"
        asum [ do
                guard (tag == ("StarFound" :: String))
                contents <- o .: "contents"
                return $ StarFoundDto contents
             , do
                guard (tag == ("PlanetFound" :: String))
                contents <- o .: "contents"
                return $ PlanetFoundDto contents
             , do
                guard (tag == ("UserWritten" :: String))
                contents <- o .: "contents"
                return $ UserWrittenDto contents
             , do
                guard (tag == ("DesignCreated" :: String))
                contents <- o .: "contents"
                return $ DesignCreatedDto contents
             , do
                guard (tag == ("BuildingFinished" :: String)
                       || tag == ("ShipFinished" :: String))
                contents <- o .: "contents"
                return $ ConstructionFinishedDto contents
            , do
                guard (tag == ("ProductionBoostStarted" :: String))
                contents <- o .: "contents"
                return $ ProductionBoostStartedDto contents
            , do
                guard (tag == ("ProductionSlowdownStarted" :: String))
                contents <- o .: "contents"
                return $ ProductionSlowdownStartedDto contents
            , do
                guard (tag == ("ProductionBoostEnded" :: String))
                contents <- o .: "contents"
                return $ ProductionBoostEndedDto contents
            , do
                guard (tag == ("ProductionSlowdownEnded" :: String))
                contents <- o .: "contents"
                return $ ProductionSlowdownEndedDto contents
            , do
                guard (tag == ("KragiiEvent" :: String))
                contents <- o .: "contents"
                return $ SpecialDto (KragiiEventDto contents)
            , do
                guard (tag == ("ScurryingSoundsEvent" :: String))
                contents <- o .: "contents"
                return $ SpecialDto (MkScurryingSoundsEventDto contents)
            , do
                guard (tag == ("NamingPetEvent" :: String))
                contents <- o .: "contents"
                return $ SpecialDto (MkNamingPetEventDto contents)
             ]


instance ToJSON SpecialNewsDto where
    toJSON dto =
        case dto of
            KragiiEventDto x ->
                toJSON x

            MkScurryingSoundsEventDto x ->
                toJSON x

            MkNamingPetEventDto x ->
                toJSON x




instance ToJSON UserWrittenNewsDto where
    toJSON UserWrittenNewsDto { _userWrittenNewsDtoContent = content
                              , _userWrittenNewsDtoUser = userName
                              , _userWrittenNewsDtoDate = sDate
                              , _userWrittenNewsDtoIcon = icon
                              } =
        object [ "Content" .= content
               , "UserName" .= userName
               , "Date" .= sDate
               , "Icon" .= icon
               ]


instance FromJSON UserWrittenNewsDto where
    parseJSON (Object b) =
        UserWrittenNewsDto <$> b .: "Content"
                           <*> b .: "Date"
                           <*> b .:? "UserName" .!= SimpleName "" Nothing
                           <*> b .: "Icon"
    parseJSON _ = mzero


instance ToJSON ConstructionFinishedNewsDto where
    toJSON ConstructionFinishedNewsDto { _constructionFinishedNewsDtoPlanetName = mpName
                                       , _constructionFinishedNewsDtoPlanetId = mpId
                                       , _constructionFinishedNewsDtoSystemName = sName
                                       , _constructionFinishedNewsDtoSystemId = sId
                                       , _constructionFinishedNewsDtoConstructionName = cName
                                       , _constructionFinishedNewsDtoBuildingId = mbId
                                       , _constructionFinishedNewsDtoShipId = msId
                                       } =
        case mbId of
            Just _ ->
                object [ "PlanetName" .= mpName
                       , "PlanetId" .= mpId
                       , "SystemName" .= sName
                       , "SystemId" .= sId
                       , "ConstructionName" .= cName
                       , "BuildingId" .= mbId
                       ]

            Nothing ->
                object [ "PlanetName" .= mpName
                       , "planetId" .= mpId
                       , "SystemName" .= sName
                       , "SystemId" .= sId
                       , "ConstructionName" .= cName
                       , "ShipId" .= msId
                       ]


instance FromJSON ConstructionFinishedNewsDto where
    parseJSON (Object b) =
        ConstructionFinishedNewsDto <$> b .:? "PlanetName"
                                    <*> b .:? "PlanetId"
                                    <*> b .: "SystemName"
                                    <*> b .: "SystemId"
                                    <*> b .: "ConstructionName"
                                    <*> b .:? "BuildingId"
                                    <*> b .:? "ShipId"
                                    <*> b .: "StarDate"
    parseJSON _ = mzero


data ProductionChangedNewsDto = ProductionChangedNewsDto
    { _productionChangedNewsDtoPlanetId :: !PlanetId
    , _productionChangedNewsDtoPlanetName :: !PlanetName
    , _productionChangedNewsDtoSystemId :: !StarSystemId
    , _productionChangedNewsDtoSystemName :: !StarSystemName
    , _productionChangedNewsDtoType :: !ResourceType
    , _productionChangedNewsDtoDate :: !StarDate
    }
    deriving (Show, Read, Eq)


data ResearchCompletedNewsDto = ResearchCompletedNewsDto
    { _researchCompletedNewsDtoTechnology :: !Technology
    , _researchCompletedNewsDtoName :: !Text
    , _researchCompletedNewsDtoDate :: !StarDate
    }
    deriving (Show, Read, Eq)


{-| Star date of the event that is being reported in this news article
-}
newsStarDate :: NewsArticleDto -> StarDate
newsStarDate article =
    case article of
        StarFoundDto details ->
            _starFoundNewsDtoDate details

        PlanetFoundDto details ->
            _planetFoundNewsDtoDate details

        UserWrittenDto details ->
            _userWrittenNewsDtoDate details

        DesignCreatedDto details ->
            _designCreatedNewsDtoDate details

        ConstructionFinishedDto details ->
            _constructionFinishedNewsDtoDate details

        ProductionBoostStartedDto details ->
            _productionChangedNewsDtoDate details

        ProductionSlowdownStartedDto details ->
            _productionChangedNewsDtoDate details

        ProductionBoostEndedDto details ->
            _productionChangedNewsDtoDate details

        ProductionSlowdownEndedDto details ->
            _productionChangedNewsDtoDate details

        ResearchCompletedDto details ->
            _researchCompletedNewsDtoDate details

        KragiiDto details ->
            _kragiiNewsDtoDate details

        ScurryingSoundsDto details ->
            _scurryingSoundsNewsDtoDate details

        NamingPetDto details ->
            _namingPetNewsDtoDate details

        SpecialDto (KragiiEventDto details) ->
            _kragiiWormsDtoDate details

        SpecialDto (MkScurryingSoundsEventDto details) ->
            _scurryingSoundsEventDtoDate details

        SpecialDto (MkNamingPetEventDto details) ->
            _namingPetEventDtoDate details


$(deriveJSON defaultOptions { constructorTagModifier = \x -> take (length x - 3) x } ''UserNewsIconDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 15 } ''KragiiWormsEventDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 14 } ''UserOptionDto)
$(deriveJSON defaultOptions { constructorTagModifier = \x -> take (length x - 3) x } ''KragiiWormsChoiceDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 14 } ''KragiiNewsDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 25 } ''ProductionChangedNewsDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 25 } ''ResearchCompletedNewsDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 23 } ''ScurryingSoundsNewsDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 24 } ''ScurryingSoundsEventDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 17 } ''NamingPetNewsDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 18 } ''NamingPetEventDto)
$(deriveJSON defaultOptions { constructorTagModifier = \x -> take (length x - 3) x } ''ScurryingSoundsChoiceDto)
$(deriveJSON defaultOptions { constructorTagModifier = \x -> take (length x - 3) x } ''NamingPetChoiceDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 21 } ''DesignCreatedNewsDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 19 } ''PlanetFoundNewsDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 17 } ''StarFoundNewsDto)

makeLenses ''NewsDto
makePrisms ''NewsArticleDto
makePrisms ''SpecialNewsDto
makeLenses ''KragiiWormsEventDto
makeLenses ''ScurryingSoundsEventDto
makeLenses ''NamingPetEventDto
makeLenses ''UserOptionDto
makeLenses ''KragiiNewsDto
makeLenses ''ScurryingSoundsNewsDto
makeLenses ''NamingPetNewsDto
makeLenses ''StarFoundNewsDto
makeLenses ''PlanetFoundNewsDto
makeLenses ''UserWrittenNewsDto
makeLenses ''DesignCreatedNewsDto
makeLenses ''ConstructionFinishedNewsDto
makeLenses ''ProductionChangedNewsDto
makeLenses ''ResearchCompletedNewsDto