{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TemplateHaskell       #-}

module Dto.Ship
    ( ChassisDto(..), RequiredComponentDto(..), PlannedComponentDto(..), DesignDto(..)
    , designToDesignDto, plannedComponentToComponentDto, toRequirement, componentDtoToPlannedComponent
    , toChassisDto, chassisDtoId, chassisDtoName, chassisDtoType, chassisDtoMaxTonnage
    , chassisDtoRequiredTypes, chassisDtoArmourSlots, chassisDtoInnerSlots, chassisDtoOuterSlots
    , chassisDtoSensorSlots, chassisDtoWeaponSlots, chassisDtoEngineSlots, chassisDtoMotiveSlots
    , chassisDtoSailSlots, requiredComponentDtoPower, requiredComponentDtoAmount
    , plannedComponentDtoId, plannedComponentDtoLevel, plannedComponentDtoAmount
    , designDtoId, designDtoChassisId, designDtoChassisLevel, designDtoName, designDtoComponents
    )
    where

import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import Import
import Control.Lens ( (^.), folded, (^..), to, makeLenses )
import Units.Data ( DesignName(..) )
import Units.Components ( ComponentId, ComponentPower, ChassisType, SlotAmount
                        , ChassisName, Weight, ComponentLevel, ComponentAmount
                        , ComponentPower(..) )


data ChassisDto = ChassisDto
    { _chassisDtoId :: !ChassisId
    , _chassisDtoName :: !ChassisName
    , _chassisDtoType :: !ChassisType
    , _chassisDtoMaxTonnage :: !Weight
    , _chassisDtoRequiredTypes :: ![ RequiredComponentDto ]
    , _chassisDtoArmourSlots :: !SlotAmount
    , _chassisDtoInnerSlots :: !SlotAmount
    , _chassisDtoOuterSlots :: !SlotAmount
    , _chassisDtoSensorSlots :: !SlotAmount
    , _chassisDtoWeaponSlots :: !SlotAmount
    , _chassisDtoEngineSlots :: !SlotAmount
    , _chassisDtoMotiveSlots :: !SlotAmount
    , _chassisDtoSailSlots :: !SlotAmount
    } deriving (Show, Read, Eq)


data RequiredComponentDto = RequiredComponentDto
    { _requiredComponentDtoPower :: !ComponentPower
    , _requiredComponentDtoAmount :: !ComponentAmount
    } deriving (Show, Read, Eq)


data PlannedComponentDto = PlannedComponentDto
    { _plannedComponentDtoId :: !ComponentId
    , _plannedComponentDtoLevel :: !ComponentLevel
    , _plannedComponentDtoAmount :: !ComponentAmount
    } deriving (Show, Read, Eq)


data DesignDto = DesignDto
    { _designDtoId :: !(Maybe DesignId)
    , _designDtoChassisId :: !ChassisId
    , _designDtoChassisLevel :: !Int
    , _designDtoName :: !DesignName
    , _designDtoComponents :: ![ PlannedComponentDto ]
    } deriving (Show, Read, Eq)


designToDesignDto :: (DesignId, Design) -> [ Entity PlannedComponent ] -> DesignDto
designToDesignDto (newId, design) comps =
    -- TODO: chassis level
    DesignDto (Just newId) (design ^. designChassisId) 1 (design ^. designName) $
        comps ^.. folded . entityValL . to plannedComponentToComponentDto


plannedComponentToComponentDto :: PlannedComponent -> PlannedComponentDto
plannedComponentToComponentDto comp =
    PlannedComponentDto
        { _plannedComponentDtoId = comp ^. plannedComponentComponentId
        , _plannedComponentDtoLevel = comp ^. plannedComponentLevel
        , _plannedComponentDtoAmount = comp ^. plannedComponentAmount
        }


componentDtoToPlannedComponent :: DesignId -> PlannedComponentDto -> PlannedComponent
componentDtoToPlannedComponent dId dto =
    PlannedComponent
        { _plannedComponentDesignId = dId
        , _plannedComponentComponentId = _plannedComponentDtoId dto
        , _plannedComponentLevel = _plannedComponentDtoLevel dto
        , _plannedComponentAmount = _plannedComponentDtoAmount dto
        }


-- | Map chassis and required component information into chassis dto
toChassisDto :: (Entity Chassis, [Entity RequiredComponent]) -> ChassisDto
toChassisDto (chassis, reqs) =
    ChassisDto
        { _chassisDtoId = entityKey chassis
        , _chassisDtoName = chassis ^. entityValL . chassisName
        , _chassisDtoType = chassis ^. entityValL . chassisType
        , _chassisDtoMaxTonnage = chassis ^. entityValL . chassisTonnage
        , _chassisDtoRequiredTypes = reqs ^.. folded . entityValL . to toRequirement
        , _chassisDtoArmourSlots = chassis ^. entityValL . chassisArmourSlots
        , _chassisDtoInnerSlots = chassis ^. entityValL . chassisInnerSlots
        , _chassisDtoOuterSlots = chassis ^. entityValL . chassisOuterSlots
        , _chassisDtoSensorSlots = chassis ^. entityValL . chassisSensorSlots
        , _chassisDtoWeaponSlots = chassis ^. entityValL . chassisWeaponSlots
        , _chassisDtoEngineSlots = chassis ^. entityValL . chassisEngineSlots
        , _chassisDtoMotiveSlots = chassis ^. entityValL . chassisMotiveSlots
        , _chassisDtoSailSlots =  chassis ^. entityValL . chassisSailSlots
        }


-- | Map required component to requirement
toRequirement :: RequiredComponent -> RequiredComponentDto
toRequirement comp =
    RequiredComponentDto
        { _requiredComponentDtoPower = ComponentPower
                                        { _componentPowerLevel = comp ^. requiredComponentLevel
                                        , _componentPowerType = comp ^. requiredComponentComponentType
                                        }
        , _requiredComponentDtoAmount = comp ^. requiredComponentAmount
        }


$(deriveJSON defaultOptions { fieldLabelModifier = drop 11 } ''ChassisDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 20 } ''PlannedComponentDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 10 } ''DesignDto)
$(deriveJSON defaultOptions { fieldLabelModifier = drop 21 } ''RequiredComponentDto)

makeLenses ''ChassisDto
makeLenses ''RequiredComponentDto
makeLenses ''PlannedComponentDto
makeLenses ''DesignDto
