{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE TemplateHaskell       #-}

module Dto.Common
    ( StarDateResponse(..), unStarDateResponseCurrentTime )
    where

import Import
import Control.Lens ( makeLenses, makeWrapped )
import Data.Aeson.TH ( deriveJSON, defaultOptions, fieldLabelModifier )
import CustomTypes ( StarDate )


data StarDateResponse = MkStarDateResponse
    { _unStarDateResponseCurrentTime :: StarDate }
    deriving (Show, Read, Eq)


$(deriveJSON defaultOptions { fieldLabelModifier = drop 19 } ''StarDateResponse)

makeLenses ''StarDateResponse
makeWrapped ''StarDateResponse