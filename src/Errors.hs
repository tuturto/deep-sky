{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE StandaloneDeriving         #-}

module Errors
    ( AccountError(..), QueryError(..), ErrorCodeClass(..), ECode(..), CommonError(..)    
    , resourceNotFound, insufficientRights, failedToParseDataInDatabase, noAvatarSelected
    , noAvatarFound, notAMemberOfAFaction, notLoggedIn, changingAvatarIsNotAllowed
    , incorrectQueryParameter, argumentOutOfRange
    )
    where

import Data.Aeson.TH
import Import.NoFoundation   as Import


class ErrorCodeClass a where
    httpStatusCode :: a -> Int
    description :: a -> Text


resourceNotFound :: ECode
resourceNotFound = ECode ResourceNotFound

insufficientRights :: ECode
insufficientRights = ECode InsufficientRights

failedToParseDataInDatabase :: ECode
failedToParseDataInDatabase = ECode FailedToParseDataInDatabase

{-| Common error codes that can happen pretty much anywhere on the server side
-}
data CommonError =
    ResourceNotFound
    | InsufficientRights
    | FailedToParseDataInDatabase
    deriving (Show, Read, Eq)

instance ErrorCodeClass CommonError where
    httpStatusCode = \case
        ResourceNotFound -> 404
        InsufficientRights -> 403
        FailedToParseDataInDatabase -> 500

    description = \case
        ResourceNotFound ->
            "Resource was not found"

        InsufficientRights ->
            "Insufficient rights to perform operation"

        FailedToParseDataInDatabase ->
            "Failed to parse contents of database"


noAvatarSelected :: ECode
noAvatarSelected = ECode NoAvatarSelected

noAvatarFound :: ECode
noAvatarFound = ECode NoAvatarFound

notAMemberOfAFaction :: ECode
notAMemberOfAFaction = ECode NotAMemberOfAFaction

notLoggedIn :: ECode
notLoggedIn = ECode NotLoggedIn

changingAvatarIsNotAllowed :: ECode
changingAvatarIsNotAllowed = ECode ChangingAvatarIsNotAllowed

{-| Error codes relating to user account and avatar
-}
data AccountError =
    NoAvatarSelected
    | NoAvatarFound
    | NotAMemberOfAFaction
    | NotLoggedIn
    | ChangingAvatarIsNotAllowed
    deriving (Show, Read, Eq)

instance ErrorCodeClass AccountError where
    httpStatusCode = \case
        NoAvatarSelected -> 400
        NoAvatarFound -> 500
        NotAMemberOfAFaction -> 400
        NotLoggedIn -> 401
        ChangingAvatarIsNotAllowed -> 400

    description = \case
        NoAvatarSelected ->
            "User has not selected an avatar"

        NoAvatarFound ->
            "Selected avatar missing from system"

        NotAMemberOfAFaction ->
            "Avatar is not a member of a faction"

        NotLoggedIn ->
            "User has not logged in"

        ChangingAvatarIsNotAllowed ->
            "Changing of avatar is not allowed"


incorrectQueryParameter :: Text -> ECode
incorrectQueryParameter s = ECode (IncorrectQueryParameter s)

argumentOutOfRange :: Text -> ECode
argumentOutOfRange s = ECode (ArgumentOutOfRange s)

{-| Error codes relating to queries and searches
-}
data QueryError =
    IncorrectQueryParameter Text
    | ArgumentOutOfRange Text
    deriving (Show, Read, Eq)

instance ErrorCodeClass QueryError where
    httpStatusCode = \case
        IncorrectQueryParameter _ -> 400
        ArgumentOutOfRange _ -> 400

    description = \case
        IncorrectQueryParameter details ->
            "Incorrect query parameter: '" ++ details ++ "'"

        ArgumentOutOfRange details ->
            "Argument out of range: '" ++ details ++ "'"


data ECode where
    ECode :: (ErrorCodeClass a, ToJSON a, Eq a, Show a) => a -> ECode


instance Show ECode where
    show (ECode a) = "ECode " ++ show a


instance Eq ECode where
    ECode a == ECode b =
        httpStatusCode a == httpStatusCode b
        &&  description a == description b


instance ErrorCodeClass ECode where
    httpStatusCode (ECode a) =
        httpStatusCode a

    description (ECode a) =
        description a


instance ToJSON ECode where
    toJSON (ECode a) =
        object [ "HttpCode" .= httpStatusCode a
               , "FaultCode" .= toJSON a
               , "ErrorDescription" .= description a
               ]


$(deriveJSON defaultOptions ''QueryError)
$(deriveJSON defaultOptions ''CommonError)
$(deriveJSON defaultOptions ''AccountError)
