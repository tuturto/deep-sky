{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE TypeFamilies               #-}

module Construction ( Constructable(..), constructionLeft
                    , ConstructionSpeedCoeff(..), OverallConstructionSpeed(..)
                    , speedLimitedByOverallSpeed, resourceScaledBySpeed, constructionWillFinish
                    , speedLimitedByWorkLeft, MaybeBuildingConstruction(..)
                    , overallSpeedBiological, overallSpeedMechanical, overallSpeedChemical
                    , unMaybeBuildingConstruction, _LimitedConstructionSpeed )
    where

import Import
import Control.Lens ( (^.), (%~), (&), (-~), to, view, makeLenses, makePrisms
                    , makeWrapped )
import Common ( FromDto(..) )
import Resources ( RawResources(..), RawResource(..), ConstructionDone, ResourceCost
                 , ConstructionLeft, Biological, Mechanical, Chemical, ConstructionSpeed
                 , ccdChemicalCost, ccdBiologicalCost, ccdMechanicalCost, unRawResource
                 )
import Dto.Construction
    ( ConstructionDto(..), bcdtoLevel, bcdtoType, bcdtoIndex
    , bcdtoPlanet
    )


-- | Object that can be placed in construction queue
class Constructable a where
    cIndex :: a -> Int
    cProgress :: a -> RawResources ConstructionDone


instance Constructable BuildingConstruction where
    cIndex = view buildingConstructionIndex
    cProgress a =
        RawResources { _ccdMechanicalCost =  a ^. buildingConstructionProgressMechanicals . to MkRawResource
                     , _ccdBiologicalCost = a ^. buildingConstructionProgressBiologicals . to MkRawResource
                     , _ccdChemicalCost = a ^. buildingConstructionProgressChemicals . to MkRawResource
                     }


-- | How much construction is there left based on total cost and construction done
constructionLeft :: RawResources ResourceCost -> RawResources ConstructionDone -> RawResources ConstructionLeft
constructionLeft cost done =
    cost & ccdMechanicalCost -~ done ^. ccdMechanicalCost
         & ccdBiologicalCost -~ done ^. ccdBiologicalCost
         & ccdChemicalCost -~ done ^. ccdChemicalCost


-- | Speed coefficient used to scale construction speed
data ConstructionSpeedCoeff t = NormalConstructionSpeed
    | LimitedConstructionSpeed Double
    deriving (Show, Read, Eq)


instance Ord (ConstructionSpeedCoeff t) where
    (<=) (LimitedConstructionSpeed a) NormalConstructionSpeed =
        a <= 1.0
    (<=) (LimitedConstructionSpeed a) (LimitedConstructionSpeed b) =
        a <= b
    (<=) NormalConstructionSpeed NormalConstructionSpeed = True
    (<=) NormalConstructionSpeed (LimitedConstructionSpeed a) =
        a >= 1.0


-- | Speed coefficients for all major resources
data OverallConstructionSpeed = OverallConstructionSpeed
    { _overallSpeedBiological :: ConstructionSpeedCoeff Biological
    , _overallSpeedMechanical :: ConstructionSpeedCoeff Mechanical
    , _overallSpeedChemical :: ConstructionSpeedCoeff Chemical
    }
    deriving (Show, Read, Eq)


-- | Construction speed based on overall construction speed and maximum construction speed
speedLimitedByOverallSpeed :: OverallConstructionSpeed -> RawResources ConstructionSpeed -> RawResources ConstructionSpeed
speedLimitedByOverallSpeed coeffSpeed speed =
    RawResources { _ccdBiologicalCost = resourceScaledBySpeed (speed ^. ccdBiologicalCost) (_overallSpeedBiological coeffSpeed)
                 , _ccdMechanicalCost = resourceScaledBySpeed (speed ^. ccdMechanicalCost) (_overallSpeedMechanical coeffSpeed)
                 , _ccdChemicalCost = resourceScaledBySpeed (speed ^. ccdChemicalCost) (_overallSpeedChemical coeffSpeed)
                 }


-- | Raw resource scaled by construction speed coefficient
resourceScaledBySpeed :: RawResource t -> ConstructionSpeedCoeff t -> RawResource t
resourceScaledBySpeed res NormalConstructionSpeed =
    res
resourceScaledBySpeed res (LimitedConstructionSpeed speed) =
    res & unRawResource %~ (\n -> floor $ speed * fromIntegral n)


-- | Limit construction speed to amount that there's work left to do
speedLimitedByWorkLeft :: RawResources ConstructionSpeed -> BuildingConstruction -> RawResources ResourceCost -> RawResources ConstructionSpeed
speedLimitedByWorkLeft cSpeed bConst cost =
    let
        limitPerResource progress speed total =
            if MkRawResource progress + speed > total
                then total - MkRawResource progress
                else speed
        bioSpeed = limitPerResource (bConst ^. buildingConstructionProgressBiologicals)
                                    (cSpeed ^. ccdBiologicalCost)
                                    (cost ^. ccdBiologicalCost)
        mechSpeed = limitPerResource (bConst ^. buildingConstructionProgressMechanicals)
                                     (cSpeed ^. ccdMechanicalCost)
                                     (cost ^. ccdMechanicalCost)
        chemSpeed = limitPerResource (bConst ^. buildingConstructionProgressChemicals)
                                     (cSpeed ^. ccdChemicalCost)
                                     (cost ^. ccdChemicalCost)
    in
        RawResources
            { _ccdMechanicalCost = mechSpeed
            , _ccdBiologicalCost = bioSpeed
            , _ccdChemicalCost = chemSpeed
            }


-- | Will construction finish based on speed, progress so far and required construction
constructionWillFinish :: RawResources ConstructionSpeed -> RawResources ConstructionDone -> RawResources ResourceCost -> Bool
constructionWillFinish speed progress total =
    speed ^. ccdMechanicalCost >= total ^. ccdMechanicalCost - progress ^. ccdMechanicalCost
    && speed ^. ccdBiologicalCost >= total ^. ccdBiologicalCost - progress ^. ccdBiologicalCost
    && speed ^. ccdChemicalCost >= total ^. ccdChemicalCost - progress ^. ccdChemicalCost


newtype MaybeBuildingConstruction =
    MkMaybeBuildingConstruction { _unMaybeBuildingConstruction :: Maybe BuildingConstruction}


instance FromDto MaybeBuildingConstruction ConstructionDto where
    fromDto dto =
        case dto of
            BuildingConstructionDto details ->
                MkMaybeBuildingConstruction $
                    Just $ BuildingConstruction { _buildingConstructionPlanetId = details ^. bcdtoPlanet
                                                , _buildingConstructionIndex = details ^. bcdtoIndex
                                                , _buildingConstructionProgressBiologicals = 0
                                                , _buildingConstructionProgressMechanicals = 0
                                                , _buildingConstructionProgressChemicals = 0
                                                , _buildingConstructionType = details ^. bcdtoType
                                                , _buildingConstructionLevel = details ^. bcdtoLevel
                                                }
            ShipConstructionDto {} -> MkMaybeBuildingConstruction Nothing

makeLenses ''OverallConstructionSpeed
makePrisms ''ConstructionSpeedCoeff
makeLenses ''MaybeBuildingConstruction
makeWrapped ''MaybeBuildingConstruction
