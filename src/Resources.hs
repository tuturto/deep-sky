{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE TypeFamilies               #-}

module Resources ( RawResources(..), ResourceCost(..), ResourceProduction(..), ConstructionSpeed(..)
                 , ConstructionLeft(..), ConstructionDone(..), ResourcesAvailable(..)
                 , RawResource(..), Biological(..), Mechanical(..), Chemical(..), ResourceType(..)
                 , subTotalCost, ccdMechanicalCost, ccdBiologicalCost, ccdChemicalCost
                 , unRawResource
                 )
    where

import Control.Lens ( makeLenses, makeWrapped )
import Data.Aeson ( withObject, withScientific )
import Data.Aeson.TH
import Data.Scientific ( toBoundedInteger )
import Database.Persist.Sql
import ClassyPrelude.Yesod   as Import
import Data.Monoid ()
import CustomTypes ( Bonus(..), Boostable(..) )


data RawResources a = RawResources
    { _ccdMechanicalCost :: RawResource Mechanical
    , _ccdBiologicalCost :: RawResource Biological
    , _ccdChemicalCost :: RawResource Chemical
    } deriving (Show, Read, Eq, Ord)


data ResourceCost = ResourceCost
data ResourceProduction = ResourceProduction
data ConstructionSpeed = ConstructionSpeed
data ConstructionLeft = ConstructionLeft
data ConstructionDone = ConstructionDone
data ResourcesAvailable = ResourcesAvailable


-- | Subtract one totalcost from another
subTotalCost :: RawResources t -> RawResources t -> RawResources t
subTotalCost a b =
    RawResources { _ccdMechanicalCost = _ccdMechanicalCost a - _ccdMechanicalCost b
                 , _ccdBiologicalCost = _ccdBiologicalCost a - _ccdBiologicalCost b
                 , _ccdChemicalCost = _ccdChemicalCost a - _ccdChemicalCost b
                 }


instance Semigroup (RawResources t) where
    (<>) a b = RawResources
        { _ccdMechanicalCost = _ccdMechanicalCost a <> _ccdMechanicalCost b
        , _ccdBiologicalCost = _ccdBiologicalCost a <> _ccdBiologicalCost b
        , _ccdChemicalCost = _ccdChemicalCost a <> _ccdChemicalCost b
        }


instance Monoid (RawResources t) where
    mempty = RawResources
        { _ccdMechanicalCost = MkRawResource 0
        , _ccdBiologicalCost = MkRawResource 0
        , _ccdChemicalCost = MkRawResource 0
        }


newtype RawResource a = MkRawResource { _unRawResource :: Int }
    deriving (Show, Read, Eq, Ord, Num)


instance ToJSON (RawResource a) where
    toJSON = toJSON . _unRawResource


instance FromJSON (RawResource a) where
    parseJSON =
        withScientific "raw resource"
            (\x -> case toBoundedInteger x of
                Nothing ->
                    mempty

                Just n ->
                    return $ MkRawResource n)


instance PersistField (RawResource a) where
    toPersistValue (MkRawResource n) =
        PersistInt64 $ fromIntegral n

    fromPersistValue (PersistInt64 n) =
        Right $ MkRawResource $ fromIntegral n

    fromPersistValue _ =
        Left "Failed to deserialize"


instance PersistFieldSql (RawResource a) where
    sqlType _ = SqlInt64


data ResourceType =
    BiologicalResource
    | MechanicalResource
    | ChemicalResource
    deriving (Show, Read, Eq)


data Biological = Biological
data Mechanical = Mechanical
data Chemical = Chemical


instance Semigroup (RawResource t) where
    (<>) a b = a + b


instance Monoid (RawResource t) where
    mempty = MkRawResource 0


instance Boostable (RawResource t) where
    applyBonus (Bonus a p) (MkRawResource r) =
        MkRawResource $ a + floor (fromIntegral r * p)


instance ToJSON (RawResources t) where
    toJSON (RawResources mech bio chem) =
        object [ "mechanical" .= toJSON mech
               , "biological" .= toJSON bio
               , "chemical" .= toJSON chem
               ]


instance FromJSON (RawResources t) where
    parseJSON = withObject "resource" $ \o -> do
        mechanical <- o .: "mechanical"
        biological <- o .: "biological"
        chemical <- o .: "chemical"
        return $ RawResources mechanical biological chemical


$(deriveJSON defaultOptions ''ResourceType)
$(deriveJSON defaultOptions ''Biological)
$(deriveJSON defaultOptions ''Mechanical)
$(deriveJSON defaultOptions ''Chemical)


makeLenses ''RawResources
makeLenses ''RawResource
makeWrapped ''RawResource
