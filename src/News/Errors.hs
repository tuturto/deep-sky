{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE TemplateHaskell            #-}

module News.Errors
    ( NewsError(..), specialEventHasAlreadyBeenResolved, unsupportedArticleType
    , specialNewsExtractionFailed, triedToMakeChoiceForRegularArticle
    )
    where

import Import
import Data.Aeson.TH
import Errors ( ErrorCodeClass(..), ECode(..) )

specialEventHasAlreadyBeenResolved :: ECode
specialEventHasAlreadyBeenResolved = ECode SpecialEventHasAlreadyBeenResolved

unsupportedArticleType :: ECode
unsupportedArticleType = ECode UnsupportedArticleType

specialNewsExtractionFailed :: ECode
specialNewsExtractionFailed = ECode SpecialNewsExtractionFailed

triedToMakeChoiceForRegularArticle :: ECode
triedToMakeChoiceForRegularArticle = ECode TriedToMakeChoiceForRegularArticle

{-| Error codes relating to news articles
-}
data NewsError =
    SpecialEventHasAlreadyBeenResolved
    | UnsupportedArticleType
    | SpecialNewsExtractionFailed
    | TriedToMakeChoiceForRegularArticle
    deriving (Show, Read, Eq)

instance ErrorCodeClass NewsError where
    httpStatusCode = \case
        SpecialEventHasAlreadyBeenResolved -> 409
        UnsupportedArticleType -> 400
        SpecialNewsExtractionFailed -> 500
        TriedToMakeChoiceForRegularArticle -> 500

    description = \case
        SpecialEventHasAlreadyBeenResolved ->
            "Special event has already been resolved"

        UnsupportedArticleType ->
            "This type of article can't be handeled"

        SpecialNewsExtractionFailed ->
            "Failed to extract special news from news article"

        TriedToMakeChoiceForRegularArticle ->
            "Tried to make a choice for regular article"


$(deriveJSON defaultOptions ''NewsError)
