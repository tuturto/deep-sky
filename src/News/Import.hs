{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE LambdaCase            #-}

module News.Import ( parseNewsEntities, userWrittenNews, parseNews
                   , planetFoundNews, starFoundNews, designCreatedNews
                   , buildingConstructionFinishedNews, iconMapper, userNewsIconMapper
                   , productionChangeEndedIconMapper, iconInfo, productionBoostStartedNews
                   , productionSlowdownStartedNews, researchCompleted )
    where

import Import
import Control.Lens ( (^.), to, view )
import Control.Lens.Wrapped ( _Unwrapping' )
import Data.Aeson ( decode )
import Data.ByteString.Builder( toLazyByteString )
import Data.Maybe ( fromJust )
import Data.Text.Encoding ( encodeUtf8Builder )
import Buildings ( building, BuildingLevel(..), buildingInfoName )
import CustomTypes ( StarDate )
import Dto.Icons ( IconMapper(..) )
import Dto.News ( NewsArticleDto(..), UserNewsIconDto(..)
                , SpecialNewsDto(..), ProductionChangedNewsDto(..)
                , productionChangedNewsDtoType, namingPetEventDtoPetType
                , namingPetNewsDtoPetType, scurryingSoundsNewsDtoPetType, userWrittenNewsDtoIcon
                )
import Events.Import ( eventOptions )
import News.Data ( NewsArticle(..), ConstructionFinishedNews(..), DesignCreatedNews(..)
                 , PlanetFoundNews(..), StarFoundNews(..), UserWrittenNews(..)
                 , UserNewsIcon(..), SpecialNews(..), ProductionChangedNews(..)
                 , ResearchCompletedNews(..), mkFactionNews
                 )
import People.Data ( PetType(..) )
import Research.Data ( Technology )
import Resources ( ResourceType(..) )


-- | Parse database entry of news and construct a possible news article
parseNews :: News -> Maybe NewsArticle
parseNews news =
    news ^. newsContent . to (decode . toLazyByteString . encodeUtf8Builder)


-- | Given a news entity, parse that into a tuple of key and possible news article
parseNewsEntity :: Entity News -> (NewsId, Maybe NewsArticle)
parseNewsEntity entity =
    let
        nId = entityKey entity
        news = entityVal entity
    in
        (nId, parseNews news)


-- | Given a list of news entities, parse them into a list of tuples of key and possible news article
-- Entries that failed to parse are removed from the end result
parseNewsEntities :: [Entity News] -> [(NewsId, NewsArticle)]
parseNewsEntities entities =
    let
        parsed = map parseNewsEntity entities
        removeFailed (_ , article) = isJust article
        simplify (key, article) = (key, fromJust article)
        addOptions (key, article) = case article of
                                        Special news ->
                                            (key, Special $ availableOptions news)
                                        _ ->
                                            (key, article)
    in
        addOptions . simplify <$> filter removeFailed parsed


-- | Evaluation current situation in respect to an special event and add available options
availableOptions :: SpecialNews -> SpecialNews
availableOptions x =
    case x of
        KragiiWorms event _ choice ->
            KragiiWorms event (eventOptions event) choice

        ScurryingSounds event _ choice ->
            ScurryingSounds event (eventOptions event) choice

        NamingPet event _ choice ->
            NamingPet event (eventOptions event) choice


-- | Use passed url render function to return link to news article's icon
-- This function is useful for example when returning JSON data to client
-- and supplying link to icon that should be displayed for it.
iconMapper :: (Route App -> Text) -> IconMapper UserNewsIconDto
    -> IconMapper ProductionChangedNewsDto
    -> IconMapper NewsArticleDto
iconMapper render userIconMapper changeIconMapper =
    IconMapper $ \case
            StarFoundDto _ ->
                render $ StaticR images_news_sun_png

            PlanetFoundDto _->
                render $ StaticR images_news_planet_png

            UserWrittenDto details ->
                runIconMapper userIconMapper $ details ^. userWrittenNewsDtoIcon

            DesignCreatedDto _ ->
                render $ StaticR images_news_blueprint_png

            ConstructionFinishedDto _ ->
                render $ StaticR images_news_crane_png

            ProductionBoostStartedDto details ->
                case details ^. productionChangedNewsDtoType of
                    BiologicalResource ->
                        render $ StaticR images_statuses_wheat_up_png

                    MechanicalResource ->
                        render $ StaticR images_statuses_cog_up_png

                    ChemicalResource ->
                        render $ StaticR images_statuses_droplets_up_png

            ProductionSlowdownStartedDto details ->
                case details ^. productionChangedNewsDtoType of
                    BiologicalResource ->
                        render $ StaticR images_statuses_wheat_down_png

                    MechanicalResource ->
                        render $ StaticR images_statuses_cog_down_png

                    ChemicalResource ->
                        render $ StaticR images_statuses_droplets_down_png

            ProductionBoostEndedDto details ->
                runIconMapper changeIconMapper details

            ProductionSlowdownEndedDto details ->
                runIconMapper changeIconMapper details

            ResearchCompletedDto _ ->
                render $ StaticR images_news_microscope_png

            SpecialDto (KragiiEventDto _) ->
                render $ StaticR images_news_hydra_png

            KragiiDto _ ->
                render $ StaticR images_news_hydra_png

            ScurryingSoundsDto event ->
                case event ^. scurryingSoundsNewsDtoPetType of
                    Just pType ->
                        (render . petTypeToIconRoute) pType

                    Nothing ->
                        render $ StaticR images_news_box_trap_png

            NamingPetDto event ->
                -- render $ petTypeToIconRoute namingPetNewsDtoPetType
                (render . petTypeToIconRoute . view namingPetNewsDtoPetType) event

            SpecialDto (MkScurryingSoundsEventDto _) ->
                render $ StaticR images_news_human_ear_png

            SpecialDto (MkNamingPetEventDto event) ->
                (render . petTypeToIconRoute . view namingPetEventDtoPetType) event


-- | Map pet type to route to respective news icon
petTypeToIconRoute :: PetType -> Route App
petTypeToIconRoute =
    \case
        Cat ->
            StaticR images_news_cat_png

        Rat ->
            StaticR images_news_rat_png


-- | Get url to image corresponding to icon selection in user news
userNewsIconMapper :: (Route App -> Text) -> IconMapper UserNewsIconDto
userNewsIconMapper render =
    IconMapper $ \case
            GenericUserNewsDto ->
                render $ StaticR images_news_question_png

            JubilationUserNewsDto ->
                render $ StaticR images_news_jubileum_png

            CatUserNewsDto ->
                render $ StaticR images_news_cat_png


productionChangeEndedIconMapper :: (Route App -> Text) -> IconMapper ProductionChangedNewsDto
productionChangeEndedIconMapper render =
    IconMapper $ \dto ->
        case dto ^. productionChangedNewsDtoType of
            BiologicalResource ->
                render $ StaticR images_statuses_wheat_right_png

            MechanicalResource ->
                render $ StaticR images_statuses_cog_right_png

            ChemicalResource ->
                render $ StaticR images_statuses_droplets_right_png


-- | List of tuples for all user news icon dtos, containing dto and link to
-- resource that can be used to retrieve image corresponding to dto
iconInfo :: IconMapper UserNewsIconDto -> [(UserNewsIconDto, Text)]
iconInfo mapper =
    map (\x -> (x, runIconMapper mapper x)) $ enumFrom minBound


-- | Construct news entry for user submitted news
userWrittenNews :: Text -> UserNewsIcon -> StarDate -> Person -> News
userWrittenNews msg icon date person =
    let
        content = UserWritten $ UserWrittenNews msg icon date (person ^. personName)
    in
        -- TODO: what is going on here?
        mkFactionNews (fromJust Nothing) date content


-- | Construct news entry for discovery of new planet
planetFoundNews :: Entity Planet -> StarSystem -> StarDate -> FactionId -> News
planetFoundNews planetEnt system date fId =
    let
        content = PlanetFound $
            PlanetFoundNews (planetEnt ^. entityValL . planetName)
                            (system ^. starSystemName)
                            (planetEnt ^. entityValL . planetStarSystemId)
                            (planetEnt ^. entityKeyL)
                            date
    in
        mkFactionNews fId date content


-- | Construct news entry for discovery of new star
starFoundNews :: Star -> Entity StarSystem -> StarDate -> FactionId -> News
starFoundNews star systemEnt date fId =
    let
        content = StarFound $
            StarFoundNews (star ^. starName)
                          (systemEnt ^. entityValL . starSystemName)
                          (systemEnt ^. entityKeyL)
                          date
    in
        mkFactionNews fId date content


-- | Construct news entry for creation of new space ship desgin
designCreatedNews :: Entity Design -> StarDate -> FactionId -> News
designCreatedNews design date fId =
    let
        dId = design ^. entityKeyL
        name = design ^. entityValL . designName
        content = DesignCreated $ DesignCreatedNews dId name date
    in
        mkFactionNews fId date content


-- | Construct news entry for a finished building construction
buildingConstructionFinishedNews :: Entity Planet -> Entity StarSystem -> Entity Building -> StarDate -> FactionId -> News
buildingConstructionFinishedNews planetE starSystemE buildingE date fId =
    let
        modelBuilding = building (buildingE ^. entityValL . buildingType) (buildingE ^. entityValL . buildingLevel . _Unwrapping' MkBuildingLevel)
        content = ConstructionFinished $ ConstructionFinishedNews
                    { _constructionFinishedNewsPlanetName = Just $ planetE ^. entityValL . planetName
                    , _constructionFinishedNewsPlanetId = Just $ planetE ^. entityKeyL
                    , _constructionFinishedNewsSystemName = starSystemE ^. entityValL . starSystemName
                    , _constructionFinishedNewsSystemId = starSystemE ^. entityKeyL
                    , _constructionFinishedConstructionName = modelBuilding ^. buildingInfoName
                    , _constructionFinishedBuildingId = Just $ buildingE ^. entityKeyL
                    , _constructionFinishedShipId = Nothing
                    , _constructionFinishedDate = date
                    }
    in
        mkFactionNews fId date content


productionBoostStartedNews :: Entity Planet -> Entity StarSystem -> ResourceType -> StarDate -> FactionId -> News
productionBoostStartedNews planet system rType date fId =
    let
        content = ProductionBoostStarted $ productionChanged planet system rType date
    in
        mkFactionNews fId date content


productionSlowdownStartedNews :: Entity Planet -> Entity StarSystem -> ResourceType -> StarDate -> FactionId -> News
productionSlowdownStartedNews planet system rType date fId =
    let
        content = ProductionSlowdownStarted $ productionChanged planet system rType date
    in
        mkFactionNews fId date content


productionChanged :: Entity Planet -> Entity StarSystem -> ResourceType -> StarDate -> ProductionChangedNews
productionChanged planet system rType date =
    ProductionChangedNews
        { _productionChangedNewsPlanetId = planet ^. entityKeyL
        , _productionChangedNewsPlanetName = planet ^. entityValL . planetName
        , _productionChangedNewsSystemId = system ^. entityKeyL
        , _productionChangedNewsSystemName = system ^. entityValL . starSystemName
        , _productionChangedNewsType = rType
        , _productionChangedNewsDate = date
        }


researchCompleted :: StarDate -> FactionId -> Technology -> News
researchCompleted date fId tech =
    let
        content = ResearchCompleted $ ResearchCompletedNews
                    { _researchCompletedNewsTechnology = tech
                    , _researchCompletedNewsData = date
                    }
    in
        mkFactionNews fId date content
