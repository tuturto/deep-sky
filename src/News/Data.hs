{-# LANGUAGE NoImplicitPrelude      #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE LambdaCase             #-}

module News.Data ( NewsArticle(..), UserNewsIcon(..), StarFoundNews(..)
                 , PlanetFoundNews(..), UserWrittenNews(..), DesignCreatedNews(..)
                 , ConstructionFinishedNews(..), SpecialNews(..), ProductionChangedNews(..)
                 , ResearchCompletedNews(..), mkFactionNews, mkFactionSpecialNews
                 , mkPersonalNews, mkPersonalSpecialNews, resolveType, researchCompletedNewsTechnology
                 , researchCompletedNewsData, _StarFound, _PlanetFound, _UserWritten
                 , _DesignCreated, _ConstructionFinished, _ProductionBoostStarted
                 , _ProductionSlowdownStarted, _ProductionBoostEnded, _ProductionSlowdownEnded
                 , _ResearchCompleted, _KragiiResolution, _ScurryingSoundsResolution
                 , _NamingPetResolution, _Special, starFoundNewsStarName, starFoundNewsSystemName
                 , starFoundNewsSystemId, starFoundNewsDate, planetFoundNewsPlanetName
                 , planetFoundNewsSystemName, planetFoundNewsSystemId, planetFoundNewsPlanetId
                 , planetFoundNewsDate, userWrittenNewsContent, userWrittenNewsIcon
                 , userWrittenNewsDate, userWrittenNewsUser, designCreatedNewsDesignId
                 , designCreatedNewsName, designCreatedDate, constructionFinishedNewsPlanetName
                 , constructionFinishedNewsPlanetId, constructionFinishedNewsSystemName
                 , constructionFinishedNewsSystemId, constructionFinishedConstructionName
                 , constructionFinishedBuildingId, constructionFinishedShipId
                 , constructionFinishedDate, productionChangedNewsPlanetId
                 , productionChangedNewsPlanetName, productionChangedNewsSystemId
                 , productionChangedNewsSystemName, productionChangedNewsType
                 , productionChangedNewsDate, _KragiiWorms, _ScurryingSounds, _NamingPet
                 )
    where

import Import
import Control.Lens ( (^.), to, makeLenses, makePrisms )
import Data.Aeson.TH
import Data.Aeson.Text ( encodeToLazyText )
import Common ( ToDto(..), FromDto(..) )
import CustomTypes ( SpecialEventStatus(..), StarDate )
import Dto.Icons ( IconMapper(..) )
import Dto.News ( NewsDto(..), NewsArticleDto(..), StarFoundNewsDto(..), PlanetFoundNewsDto(..)
                , UserWrittenNewsDto(..), DesignCreatedNewsDto(..), ConstructionFinishedNewsDto(..)
                , UserNewsIconDto(..), SpecialNewsDto(..), KragiiWormsEventDto(..)
                , ProductionChangedNewsDto(..), ResearchCompletedNewsDto(..)
                , ScurryingSoundsEventDto(..), NamingPetEventDto(..), UserOptionDto(..)
                , NamingPetChoiceDto(..), userOptionDtoChoice, namingPetEventDtoChoice
                , namingPetEventDtoOptions, namingPetEventDtoDate, namingPetEventDtoPetType
                , namingPetEventDtoPetId, namingPetEventDtoPersonId, scurryingSoundsEventDtoChoice
                , scurryingSoundsEventDtoDate, scurryingSoundsEventDtoPersonId
                , kragiiWormsDtoChoice, kragiiWormsDtoDate, kragiiWormsDtoFactionId
                , kragiiWormsDtoSystemName, kragiiWormsDtoSystemId, kragiiWormsDtoPlanetName
                , kragiiWormsDtoPlanetId, researchCompletedNewsDtoDate, researchCompletedNewsDtoTechnology
                , productionChangedNewsDtoDate, productionChangedNewsDtoType
                , productionChangedNewsDtoSystemName, productionChangedNewsDtoSystemId
                , productionChangedNewsDtoPlanetName, productionChangedNewsDtoPlanetId
                , constructionFinishedNewsDtoDate, constructionFinishedNewsDtoShipId
                , constructionFinishedNewsDtoBuildingId, constructionFinishedNewsDtoConstructionName
                , constructionFinishedNewsDtoSystemId, constructionFinishedNewsDtoSystemName
                , constructionFinishedNewsDtoPlanetId, constructionFinishedNewsDtoPlanetName
                , designCreatedNewsDtoDate, designCreatedNewsDtoName, designCreatedNewsDtoDesignId
                , userWrittenNewsDtoUser, userWrittenNewsDtoDate, userWrittenNewsDtoIcon
                , userWrittenNewsDtoContent, planetFoundNewsDtoDate, planetFoundNewsDtoPlanetId
                , planetFoundNewsDtoSystemId, planetFoundNewsDtoSystemName, planetFoundNewsDtoPlanetName
                , starFoundNewsDtoDate, starFoundNewsDtoSystemId, starFoundNewsDtoSystemName
                , starFoundNewsDtoStarName
                )
import Events.Import ( UserOption(..), EventResolveType(..) )
import qualified Events.Import as EI ( resolveType )
import Events.Kragii ( KragiiWormsEvent(..), KragiiWormsChoice(..), KragiiNews(..)
                     , kragiiWormsDate, kragiiWormsFactionId, kragiiWormsSystemName
                     , kragiiWormsSystemId, kragiiWormsPlanetName, kragiiWormsPlanetId
                     )
import Events.Pets ( ScurryingSoundsEvent(..), ScurryingSoundsChoice(..), ScurryingSoundsNews(..)
                   , NamingPetEvent(..), NamingPetChoice(..), NamingPetNews(..)
                   , namingPetEventDate, namingPetEventPetType, namingPetEventPetId
                   , namingPetEventPersonId, scurryingSoundsEventDate, scurryingSoundsEventPersonId
                   )
import People.Data ( PersonName, PetName )
import Research.Data ( Technology, researchName )
import Research.Tree ( techMap )
import Resources ( ResourceType )
import Space.Data ( PlanetName(..), StarName(..), StarSystemName(..) )
import Units.Data ( DesignName(..) )


-- | All possible news articles
data NewsArticle =
    StarFound StarFoundNews
    | PlanetFound PlanetFoundNews
    | UserWritten UserWrittenNews
    | DesignCreated DesignCreatedNews
    | ConstructionFinished ConstructionFinishedNews
    | ProductionBoostStarted ProductionChangedNews
    | ProductionSlowdownStarted ProductionChangedNews
    | ProductionBoostEnded ProductionChangedNews
    | ProductionSlowdownEnded ProductionChangedNews
    | ResearchCompleted ResearchCompletedNews
    | KragiiResolution KragiiNews
    | ScurryingSoundsResolution ScurryingSoundsNews
    | NamingPetResolution NamingPetNews
    | Special SpecialNews


-- | News announcing discovery of a new star
data StarFoundNews = StarFoundNews
    { _starFoundNewsStarName :: !StarName
    , _starFoundNewsSystemName :: !StarSystemName
    , _starFoundNewsSystemId :: !StarSystemId
    , _starFoundNewsDate :: !StarDate
    }


instance ToDto StarFoundNews StarFoundNewsDto where
    toDto news =
        StarFoundNewsDto { _starFoundNewsDtoStarName = _starFoundNewsStarName news
                         , _starFoundNewsDtoSystemName = _starFoundNewsSystemName news
                         , _starFoundNewsDtoSystemId = _starFoundNewsSystemId news
                         , _starFoundNewsDtoDate = _starFoundNewsDate news
                         }


instance FromDto StarFoundNews StarFoundNewsDto where
    fromDto dto =
        StarFoundNews { _starFoundNewsStarName = dto ^. starFoundNewsDtoStarName
                      , _starFoundNewsSystemName = dto ^. starFoundNewsDtoSystemName
                      , _starFoundNewsSystemId = dto ^. starFoundNewsDtoSystemId
                      , _starFoundNewsDate = dto ^. starFoundNewsDtoDate
                      }


-- | News announcing discovery of a new planet
data PlanetFoundNews = PlanetFoundNews
    { _planetFoundNewsPlanetName :: !PlanetName
    , _planetFoundNewsSystemName :: !StarSystemName
    , _planetFoundNewsSystemId :: !StarSystemId
    , _planetFoundNewsPlanetId :: !PlanetId
    , _planetFoundNewsDate :: !StarDate
    }


instance ToDto PlanetFoundNews PlanetFoundNewsDto where
    toDto news =
        PlanetFoundNewsDto { _planetFoundNewsDtoPlanetName = _planetFoundNewsPlanetName news
                           , _planetFoundNewsDtoSystemName = _planetFoundNewsSystemName news
                           , _planetFoundNewsDtoSystemId = _planetFoundNewsSystemId news
                           , _planetFoundNewsDtoPlanetId = _planetFoundNewsPlanetId news
                           , _planetFoundNewsDtoDate = _planetFoundNewsDate news
                           }


instance FromDto PlanetFoundNews PlanetFoundNewsDto where
    fromDto dto =
        PlanetFoundNews { _planetFoundNewsPlanetName = dto ^. planetFoundNewsDtoPlanetName
                        , _planetFoundNewsSystemName = dto ^. planetFoundNewsDtoSystemName
                        , _planetFoundNewsSystemId = dto ^. planetFoundNewsDtoSystemId
                        , _planetFoundNewsPlanetId = dto ^. planetFoundNewsDtoPlanetId
                        , _planetFoundNewsDate = dto ^. planetFoundNewsDtoDate
                        }


-- | User supplied news
data UserWrittenNews = UserWrittenNews
    { _userWrittenNewsContent :: !Text
    , _userWrittenNewsIcon :: !UserNewsIcon
    , _userWrittenNewsDate :: !StarDate
    , _userWrittenNewsUser :: !PersonName
    }


instance ToDto UserWrittenNews UserWrittenNewsDto where
    toDto news =
        UserWrittenNewsDto { _userWrittenNewsDtoContent = _userWrittenNewsContent news
                           , _userWrittenNewsDtoDate = _userWrittenNewsDate  news
                           , _userWrittenNewsDtoUser = _userWrittenNewsUser news
                           , _userWrittenNewsDtoIcon = toDto $ _userWrittenNewsIcon news
                           }


instance FromDto UserWrittenNews UserWrittenNewsDto where
    fromDto dto =
        UserWrittenNews { _userWrittenNewsContent = dto ^. userWrittenNewsDtoContent
                        , _userWrittenNewsIcon = dto ^. userWrittenNewsDtoIcon . to fromDto
                        , _userWrittenNewsDate = dto ^. userWrittenNewsDtoDate
                        , _userWrittenNewsUser = dto ^. userWrittenNewsDtoUser
                        }


-- | News announcing creation of a new design
data DesignCreatedNews = DesignCreatedNews
    { _designCreatedNewsDesignId :: !DesignId
    , _designCreatedNewsName :: !DesignName
    , _designCreatedDate :: !StarDate
    }


instance ToDto DesignCreatedNews DesignCreatedNewsDto where
    toDto news =
        DesignCreatedNewsDto { _designCreatedNewsDtoDesignId = _designCreatedNewsDesignId news
                             , _designCreatedNewsDtoName = _designCreatedNewsName news
                             , _designCreatedNewsDtoDate = _designCreatedDate news
                             }


instance FromDto DesignCreatedNews DesignCreatedNewsDto where
    fromDto dto =
        DesignCreatedNews { _designCreatedNewsDesignId = dto ^. designCreatedNewsDtoDesignId
                          , _designCreatedNewsName = dto ^. designCreatedNewsDtoName
                          , _designCreatedDate = dto ^. designCreatedNewsDtoDate
                          }


data ConstructionFinishedNews = ConstructionFinishedNews
    { _constructionFinishedNewsPlanetName :: !(Maybe PlanetName)
    , _constructionFinishedNewsPlanetId :: !(Maybe PlanetId)
    , _constructionFinishedNewsSystemName :: !StarSystemName
    , _constructionFinishedNewsSystemId :: !StarSystemId
    , _constructionFinishedConstructionName :: !Text
    , _constructionFinishedBuildingId :: !(Maybe BuildingId)
    , _constructionFinishedShipId :: !(Maybe ShipId)
    , _constructionFinishedDate :: !StarDate
    }


instance ToDto ConstructionFinishedNews ConstructionFinishedNewsDto where
    toDto news =
        ConstructionFinishedNewsDto { _constructionFinishedNewsDtoPlanetName = _constructionFinishedNewsPlanetName news
                                    , _constructionFinishedNewsDtoPlanetId = _constructionFinishedNewsPlanetId news
                                    , _constructionFinishedNewsDtoSystemName = _constructionFinishedNewsSystemName news
                                    , _constructionFinishedNewsDtoSystemId = _constructionFinishedNewsSystemId news
                                    , _constructionFinishedNewsDtoConstructionName = _constructionFinishedConstructionName news
                                    , _constructionFinishedNewsDtoBuildingId = _constructionFinishedBuildingId news
                                    , _constructionFinishedNewsDtoShipId = _constructionFinishedShipId news
                                    , _constructionFinishedNewsDtoDate = _constructionFinishedDate news
                                    }


instance FromDto ConstructionFinishedNews ConstructionFinishedNewsDto where
    fromDto dto =
        ConstructionFinishedNews { _constructionFinishedNewsPlanetName = dto ^. constructionFinishedNewsDtoPlanetName
                                 , _constructionFinishedNewsPlanetId = dto ^. constructionFinishedNewsDtoPlanetId
                                 , _constructionFinishedNewsSystemName = dto ^. constructionFinishedNewsDtoSystemName
                                 , _constructionFinishedNewsSystemId = dto ^. constructionFinishedNewsDtoSystemId
                                 , _constructionFinishedConstructionName = dto ^. constructionFinishedNewsDtoConstructionName
                                 , _constructionFinishedBuildingId = dto ^. constructionFinishedNewsDtoBuildingId
                                 , _constructionFinishedShipId = dto ^. constructionFinishedNewsDtoShipId
                                 , _constructionFinishedDate = dto ^. constructionFinishedNewsDtoDate
                                 }


data ProductionChangedNews = ProductionChangedNews
    { _productionChangedNewsPlanetId :: !PlanetId
    , _productionChangedNewsPlanetName :: !PlanetName
    , _productionChangedNewsSystemId :: !StarSystemId
    , _productionChangedNewsSystemName :: !StarSystemName
    , _productionChangedNewsType :: !ResourceType
    , _productionChangedNewsDate :: !StarDate
    }


instance ToDto ProductionChangedNews ProductionChangedNewsDto where
    toDto news = ProductionChangedNewsDto
        { _productionChangedNewsDtoPlanetId = _productionChangedNewsPlanetId news
        , _productionChangedNewsDtoPlanetName = _productionChangedNewsPlanetName news
        , _productionChangedNewsDtoSystemId = _productionChangedNewsSystemId news
        , _productionChangedNewsDtoSystemName = _productionChangedNewsSystemName news
        , _productionChangedNewsDtoType = _productionChangedNewsType news
        , _productionChangedNewsDtoDate = _productionChangedNewsDate news
        }


instance FromDto ProductionChangedNews ProductionChangedNewsDto where
    fromDto dto = ProductionChangedNews
        { _productionChangedNewsPlanetId = dto ^. productionChangedNewsDtoPlanetId
        , _productionChangedNewsPlanetName = dto ^. productionChangedNewsDtoPlanetName
        , _productionChangedNewsSystemId = dto ^. productionChangedNewsDtoSystemId
        , _productionChangedNewsSystemName = dto ^. productionChangedNewsDtoSystemName
        , _productionChangedNewsType = dto ^. productionChangedNewsDtoType
        , _productionChangedNewsDate = dto ^. productionChangedNewsDtoDate
        }


data ResearchCompletedNews = ResearchCompletedNews
    { _researchCompletedNewsTechnology :: !Technology
    , _researchCompletedNewsData :: !StarDate
    }


instance ToDto ResearchCompletedNews ResearchCompletedNewsDto where
    toDto news = ResearchCompletedNewsDto
        { _researchCompletedNewsDtoTechnology = tech
        , _researchCompletedNewsDtoName = name
        , _researchCompletedNewsDtoDate = _researchCompletedNewsData news
        }
        where
            tech = _researchCompletedNewsTechnology news
            name =  (techMap tech) ^. researchName

instance FromDto ResearchCompletedNews ResearchCompletedNewsDto where
    fromDto dto = ResearchCompletedNews
        { _researchCompletedNewsTechnology = dto ^. researchCompletedNewsDtoTechnology
        , _researchCompletedNewsData = dto ^. researchCompletedNewsDtoDate
        }


instance ToDto ((NewsId, NewsArticle), IconMapper NewsArticleDto) NewsDto where
    toDto ((nId, article), icons) =
        let
            content = toDto article
        in
        NewsDto { _newsDtoId = nId
                , _newsContents = content
                , _newsIcon = runIconMapper icons content
                }


instance FromDto NewsArticle NewsArticleDto where
    fromDto dto =
        case dto of
            StarFoundDto content ->
                StarFound $ fromDto content

            PlanetFoundDto content ->
                PlanetFound $ fromDto content

            UserWrittenDto content ->
                UserWritten $ fromDto content

            DesignCreatedDto content ->
                DesignCreated $ fromDto content

            ConstructionFinishedDto content ->
                ConstructionFinished $ fromDto content

            ProductionBoostStartedDto content ->
                ProductionBoostStarted $ fromDto content

            ProductionSlowdownStartedDto content ->
                ProductionSlowdownStarted $ fromDto content

            ProductionBoostEndedDto content ->
                ProductionBoostEnded $ fromDto content

            ProductionSlowdownEndedDto content ->
                ProductionSlowdownEnded $ fromDto content

            ResearchCompletedDto content ->
                ResearchCompleted $ fromDto content

            KragiiDto content ->
                KragiiResolution $ fromDto content

            ScurryingSoundsDto content ->
                ScurryingSoundsResolution $ fromDto content

            NamingPetDto content ->
                NamingPetResolution $ fromDto content

            SpecialDto content ->
                Special $ fromDto content


instance ToDto NewsArticle NewsArticleDto where
    toDto news =
        case news of
            (StarFound x) -> StarFoundDto $ toDto x
            (PlanetFound x) -> PlanetFoundDto $ toDto x
            (UserWritten x) -> UserWrittenDto $ toDto x
            (DesignCreated x) -> DesignCreatedDto $ toDto x
            (ConstructionFinished x) -> ConstructionFinishedDto $ toDto x
            (ProductionBoostStarted x) -> ProductionBoostStartedDto $ toDto x
            (ProductionSlowdownStarted x) -> ProductionSlowdownStartedDto $ toDto x
            (ProductionBoostEnded x) -> ProductionBoostEndedDto $ toDto x
            (ProductionSlowdownEnded x) -> ProductionSlowdownEndedDto $ toDto x
            (ResearchCompleted x) -> ResearchCompletedDto $ toDto x
            (KragiiResolution x) -> KragiiDto $ toDto x
            (ScurryingSoundsResolution x) -> ScurryingSoundsDto $ toDto x
            (NamingPetResolution x) -> NamingPetDto $ toDto x
            (Special x) -> SpecialDto $ toDto x


-- | Special news that require player interaction
data SpecialNews
    = KragiiWorms KragiiWormsEvent [UserOption KragiiWormsChoice] (Maybe KragiiWormsChoice)
    | ScurryingSounds ScurryingSoundsEvent [(UserOption ScurryingSoundsChoice)] (Maybe ScurryingSoundsChoice)
    | NamingPet NamingPetEvent [(UserOption NamingPetChoice)] (Maybe NamingPetChoice)
    deriving (Show, Read, Eq)


-- | How event in special news is resolved
resolveType :: NewsArticle -> Maybe EventResolveType
resolveType (Special article) =
    case article of
        (KragiiWorms event _ _) ->
            Just $ EI.resolveType event

        (ScurryingSounds event _ _) ->
            Just $ EI.resolveType event

        (NamingPet event _ _) ->
            Just $ EI.resolveType event

resolveType _ =
    Nothing


instance ToDto SpecialNews SpecialNewsDto where
    toDto (KragiiWorms event options choice) =
        KragiiEventDto $ KragiiWormsEventDto
            { _kragiiWormsDtoPlanetId = event ^. kragiiWormsPlanetId
            , _kragiiWormsDtoPlanetName = event ^. kragiiWormsPlanetName
            , _kragiiWormsDtoSystemId = event ^. kragiiWormsSystemId
            , _kragiiWormsDtoSystemName = event ^. kragiiWormsSystemName
            , _kragiiWormsDtoOptions = toDto <$> options
            , _kragiiWormsDtoChoice = toDto <$> choice
            , _kragiiWormsDtoFactionId = event ^. kragiiWormsFactionId
            , _kragiiWormsDtoDate = event ^. kragiiWormsDate
            , _kragiiWormsDtoResolveType = Just $ EI.resolveType event
            }

    toDto (ScurryingSounds event options choice) =
        MkScurryingSoundsEventDto $ ScurryingSoundsEventDto
            { _scurryingSoundsEventDtoPersonId = event ^. scurryingSoundsEventPersonId
            , _scurryingSoundsEventDtoDate = event ^. scurryingSoundsEventDate
            , _scurryingSoundsEventDtoOptions = toDto <$> options
            , _scurryingSoundsEventDtoChoice = toDto <$> choice
            , _scurryingSoundsEventDtoResolveType = Just $ EI.resolveType event
            }

    toDto (NamingPet event options choice) =
        MkNamingPetEventDto $ NamingPetEventDto
            { _namingPetEventDtoPersonId = event ^. namingPetEventPersonId
            , _namingPetEventDtoPetId = event ^. namingPetEventPetId
            , _namingPetEventDtoPetType = event ^. namingPetEventPetType
            , _namingPetEventDtoDate = event ^. namingPetEventDate
            , _namingPetEventDtoOptions = toDto <$> options
            , _namingPetEventDtoChoice = toDto <$> choice
            , _namingPetEventDtoResolveType = Just $ EI.resolveType event
            }


instance FromDto SpecialNews SpecialNewsDto where
    fromDto (KragiiEventDto dto) =
        KragiiWorms ( KragiiWormsEvent
                        { _kragiiWormsPlanetId = dto ^. kragiiWormsDtoPlanetId
                        , _kragiiWormsPlanetName = dto ^. kragiiWormsDtoPlanetName
                        , _kragiiWormsSystemId = dto ^. kragiiWormsDtoSystemId
                        , _kragiiWormsSystemName = dto ^. kragiiWormsDtoSystemName
                        , _kragiiWormsFactionId = dto ^. kragiiWormsDtoFactionId
                        , _kragiiWormsDate = dto ^. kragiiWormsDtoDate
                        } )
                    []
                    (fromDto <$> dto ^. kragiiWormsDtoChoice)

    fromDto (MkScurryingSoundsEventDto dto) =
        ScurryingSounds ( ScurryingSoundsEvent
                            { _scurryingSoundsEventPersonId = dto ^. scurryingSoundsEventDtoPersonId
                            , _scurryingSoundsEventDate = dto ^. scurryingSoundsEventDtoDate } )
                        []
                        (fromDto <$> dto ^. scurryingSoundsEventDtoChoice)

    fromDto (MkNamingPetEventDto dto) =
        NamingPet ( NamingPetEvent
                    { _namingPetEventPersonId = dto ^. namingPetEventDtoPersonId
                    , _namingPetEventPetId = dto ^. namingPetEventDtoPetId
                    , _namingPetEventPetType = dto ^. namingPetEventDtoPetType
                    , _namingPetEventDate = dto ^. namingPetEventDtoDate
                    , _namingPetNameOptions =  mapMaybe getPetName (dto ^. namingPetEventDtoOptions)
                    } )
                  []
                  (fromDto <$> dto ^. namingPetEventDtoChoice)


getPetName :: UserOptionDto NamingPetChoiceDto -> Maybe PetName
getPetName dto =
    case dto ^. userOptionDtoChoice of
        GiveNameDto name ->
            Just name

        LetSomeoneElseDecideDto ->
            Nothing


-- | Icon for user created news
data UserNewsIcon =
    GenericUserNews
    | JubilationUserNews
    | CatUserNews
    deriving (Show, Read, Eq)


instance ToDto UserNewsIcon UserNewsIconDto where
    toDto icon =
        case icon of
            GenericUserNews ->
                GenericUserNewsDto

            JubilationUserNews ->
                JubilationUserNewsDto

            CatUserNews ->
                CatUserNewsDto


instance FromDto UserNewsIcon UserNewsIconDto where
    fromDto icon =
        case icon of
            GenericUserNewsDto ->
                GenericUserNews

            JubilationUserNewsDto ->
                JubilationUserNews

            CatUserNewsDto ->
                CatUserNews


-- | Helper function for creating News that aren't special events and haven't been dismissed
mkFactionNews :: FactionId -> StarDate -> NewsArticle -> News
mkFactionNews fId date content =
    News { _newsContent = toStrict $ encodeToLazyText content
         , _newsFactionId = Just fId
         , _newsPersonId = Nothing
         , _newsDate = date
         , _newsDismissed = False
         , _newsSpecialEvent = NoSpecialEvent
         }


-- | Helper function for creating News that aren't special events and haven't been dismissed
mkPersonalNews :: PersonId -> StarDate -> NewsArticle -> News
mkPersonalNews pId date content =
    News { _newsContent = toStrict $ encodeToLazyText content
         , _newsFactionId = Nothing
         , _newsPersonId = Just pId
         , _newsDate = date
         , _newsDismissed = False
         , _newsSpecialEvent = NoSpecialEvent
         }


-- | Helper function for creating News that are special events and haven't been handled
mkFactionSpecialNews :: StarDate -> FactionId -> SpecialNews -> News
mkFactionSpecialNews date fId content =
    News { _newsContent = toStrict $ encodeToLazyText $ Special content
         , _newsFactionId = Just fId
         , _newsPersonId = Nothing
         , _newsDate = date
         , _newsDismissed = False
         , _newsSpecialEvent = UnhandledSpecialEvent
         }


-- | Helper function for creating News that are special events and haven't been handled
mkPersonalSpecialNews :: StarDate -> PersonId -> SpecialNews -> News
mkPersonalSpecialNews date pId content =
    News { _newsContent = toStrict $ encodeToLazyText $ Special content
         , _newsFactionId = Nothing
         , _newsPersonId = Just pId
         , _newsDate = date
         , _newsDismissed = False
         , _newsSpecialEvent = UnhandledSpecialEvent
         }


$(deriveJSON defaultOptions ''StarFoundNews)
$(deriveJSON defaultOptions ''PlanetFoundNews)
$(deriveJSON defaultOptions ''UserWrittenNews)
$(deriveJSON defaultOptions ''DesignCreatedNews)
$(deriveJSON defaultOptions ''ConstructionFinishedNews)
$(deriveJSON defaultOptions ''UserNewsIcon)
$(deriveJSON defaultOptions ''SpecialNews)
$(deriveJSON defaultOptions ''NewsArticle)
$(deriveJSON defaultOptions ''ProductionChangedNews)
$(deriveJSON defaultOptions ''ResearchCompletedNews)

makeLenses ''ResearchCompletedNews
makePrisms ''NewsArticle
makeLenses ''StarFoundNews
makeLenses ''PlanetFoundNews
makeLenses ''UserWrittenNews
makeLenses ''DesignCreatedNews
makeLenses ''ConstructionFinishedNews
makeLenses ''ProductionChangedNews
makePrisms ''SpecialNews
