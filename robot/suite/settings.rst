.. code:: robotframework

    *** Settings ***
    Documentation     A resource file with reusable keywords and variables.
    ...
    ...               The system specific keywords created here form our own
    ...               domain specific language. They utilize keywords provided
    ...               by the imported SeleniumLibrary.
    Library           SeleniumLibrary

    *** Variables ***
    ${SERVER}           localhost:3000
    ${BROWSER}          Firefox
    ${DELAY}            0

    ${ADMIN USER}       tuula
    ${ADMIN PWD}        salasana
    ${VALID USER}       aurora
    ${VALID PWD}        salasana

    ${MAIN URL}         http://${SERVER}/
    ${MESSAGES URL}     ${MAIN URL}message/
    ${LOGOUT URL}       ${MAIN URL}auth/logout


    *** Keywords ***
    Start Testing
        Open Browser   ${MAIN URL}   ${BROWSER}
        Maximize Browser Window
        Set Selenium Speed   ${DELAY}

    Stop Testing
        Close Browser

    Login As
        [Arguments]   ${user_name}   ${user_password}
        Wait Until Page Contains   Log In
        Wait Until Data Has Finished Loading
        Input Text   id:UserName   ${user_name}
        Input Text   id:Password   ${user_password}
        Click Button   id:btn-submit
        Wait Until Data Has Finished Loading

    Logout
        Click Link   id:logout
        Wait Until Data Has Finished Loading
        Wait Until Page Contains   Log In

    Error Bar Should Not Be Visible
        Element Should Not Be Visible   class:error-bar

All actions that communicate with server indicate their readiness. Common
loading indicator is enabled as long as at least one such an action is
being processed. ``Wait Until Data Has Finished Loading`` will wait until
indicator disappears or time-out expires.

.. code:: robotframework

    Wait Until Data Has Finished Loading
        Wait Until Element Is Not Visible   id:loading-indicator
