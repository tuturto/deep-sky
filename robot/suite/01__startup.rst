.. code:: robotframework

    *** Settings ***
    Resource            ./settings.rst
    Library             SeleniumLibrary

    *** Test Cases ***
    Opening Main Page
        [Tags]   smoke
        Go To       ${MAIN URL}
        Wait Until Data Has Finished Loading
        Error Bar Should Not Be Visible

    Creating Admin Account
        Go To       ${MAIN URL}
        Wait Until Page Contains   Log In
        Click Button   id:btn-signup
        Input Text   id:UserName   ${ADMIN USER}
        Input Text   id:Password   ${ADMIN PWD}
        Input Text   id:Password2   ${ADMIN PWD}
        Click Button   id:btn-submit
        Wait Until Page Contains   Log In
        Wait Until Data Has Finished Loading
        Error Bar Should Not Be Visible
