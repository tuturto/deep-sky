.. code:: robotframework

    *** Settings ***
    Resource            ./settings.rst
    Resource            ./common.rst
    Library             SeleniumLibrary
    Suite Setup         Admin Setup
    Suite Teardown      Admin Teardown
    Test Teardown       Error Bar Should Not Be Visible


Setting up the simulation
=========================
Administrator can setup the simulation in two different ways. One is to
start from scratch and create everything manually. The other option is to let
the system to create a initial setup and then fine tune it by hand.

Keywords
--------
.. code:: robotframework

    *** Keywords ***
    Admin Setup
        Login As    ${ADMIN USER}   ${ADMIN PWD}
        Open Admin Page

    Admin Teardown
        Logout

Test Cases
----------

Simulation Status
+++++++++++++++++
Simulation has four different states: offline, maintenance, online and
processing turn. Wiping and reseeding the database can only be performed
when the simulation is in offline state.

Wiping and reseeding
++++++++++++++++++++
Wiping the database will erase most of the data. Only users and roles will be
left behind. In addition, any avatars linked to administrator users are
preserved.

Reseeding first performs wipe and then fills in the default dataset into the
database. This contains information like shape of the universe and people
living in it. After reseeding the database, simulation can be turned online
and players can start playing in it.

.. code:: robotframework

    *** Test Cases ***
    Reseeding the Database
        [Tags]   admin
        Click Link   link:Status
        Wait Until Data Has Finished Loading
        Click Element   id:reseed-database
        Click Button   OK
        Wait Until Data Has Finished Loading

    Setting Simulation Online
        [Tags]   admin
        Click Link   link:Status
        Wait Until Data Has Finished Loading
        Click Element   id:btn-online
        Wait Until Data Has Finished Loading