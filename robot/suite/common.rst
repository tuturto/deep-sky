.. code:: robotframework

    *** Settings ***
    Resource            ./settings.rst
    Library             SeleniumLibrary

Admin page
==========
Admin page is only visible for users with sufficient rights. They can use it
to monitor game status and administer it. Admin page can be opened by clicking
the respective link on main menu bar.

Keywords
--------
.. code:: robotframework

    *** Keywords ***
    Open Admin Page
        Click Link   Admin
        Wait Until Data Has Finished Loading
