.. code:: robotframework

    *** Settings ***
    Resource            ./settings.rst
    Library             SeleniumLibrary
    Suite Setup         People Setup
    Suite Teardown      People Teardown
    Test Teardown       Error Bar Should Not Be Visible

.. code:: robotframework

    *** Keywords ***
    People Setup
        Login As    ${VALID USER}   ${VALID PWD}

    People Teardown
        Logout

People
======
Currently there is no easy way to search and view people.

Keywords
--------


Test Cases
----------

As a workaround, we'll go through star systems.

.. code:: robotframework

    *** Test Cases ***
    Opening People Page
        [Tags]   people   smoke
        Click Link   Star Systems
        Wait Until Data Has Finished Loading
        Click Element   id:system-name-0
        Wait Until Data Has Finished Loading
        Click Link   Procurator Aurora XII "the Just"
        Wait Until Data Has Finished Loading


    Changing Life Focus
        [Tags]   people
        Click Button   id:lifefocus-button
        Click Element   id:hunting-button
        Click Button   id:confirm-lifefocus
        Wait Until Data Has Finished Loading
        ${focus}=   Get Text   id:lifefocus-button
        Should Be Equal   ${focus}   hunting
