.. code:: robotframework

    *** Settings ***
    Resource            ./settings.rst
    Library             SeleniumLibrary

    *** Test Cases ***
    Opening Main Page
        [Tags]   smoke
        Go To       ${MAIN URL}
        Wait Until Data Has Finished Loading
        Error Bar Should Not Be Visible

    Creating User Account
        Go To       ${MAIN URL}
        Wait Until Data Has Finished Loading
        Click Button   id:btn-signup
        Input Text   id:UserName   ${VALID USER}
        Input Text   id:Password   ${VALID PWD}
        Input Text   id:Password2   ${ADMIN PWD}
        Click Button   id:btn-submit
        Wait Until Page Contains   Log In
        Wait Until Data Has Finished Loading
        Error Bar Should Not Be Visible

    Selecting Avatar
        Login As    ${VALID USER}   ${VALID PWD}
        Wait Until Data Has Finished Loading
        Click Element   id:person-0-name
        Click Button   OK
        Wait Until Data Has Finished Loading
        Page Should Contain   Hello from Home
        Error Bar Should Not Be Visible
        Logout
