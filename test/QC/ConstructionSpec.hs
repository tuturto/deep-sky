{-# LANGUAGE TemplateHaskell            #-}
module QC.ConstructionSpec (spec)
    where

import Test.QuickCheck
import Test.Hspec

import Control.Lens ( (^.), to )

import QC.Generators.Import
import Model
import Resources ( RawResource(..), ccdChemicalCost, ccdBiologicalCost
                 , ccdMechanicalCost
                 )
import Simulation.Construction ( overallConstructionSpeed )
import Construction ( ConstructionSpeedCoeff(..), speedLimitedByWorkLeft
                    , overallSpeedChemical, overallSpeedMechanical, overallSpeedBiological
                    )

spec :: Spec
spec = do
    describe "construction" $ do
        describe "speed" $ do
            it "construction speed is never greater than total cost in unstarted construction" $ do
                forAll unstartedConstructionsWithSomeSpeed $ \(cSpeed, bConst, cTotal) ->
                    let
                        speed = speedLimitedByWorkLeft cSpeed bConst cTotal
                    in
                        speed ^. ccdMechanicalCost <= cTotal ^. ccdMechanicalCost
                        && speed ^. ccdBiologicalCost <= cTotal ^. ccdBiologicalCost
                        && speed ^. ccdChemicalCost <= cTotal ^. ccdChemicalCost

            it "construction speed + construction done is never greater than total construction" $ do
                forAll unfinishedConstructionsWithSomeSpeed $ \(cSpeed, bConst, cTotal) ->
                    let
                        speed = speedLimitedByWorkLeft cSpeed bConst cTotal
                    in
                        speed ^. ccdMechanicalCost + bConst ^. buildingConstructionProgressMechanicals . to MkRawResource  <= cTotal ^. ccdMechanicalCost
                        && speed ^. ccdBiologicalCost + bConst ^. buildingConstructionProgressBiologicals . to MkRawResource  <= cTotal ^. ccdBiologicalCost
                        && speed ^. ccdChemicalCost + bConst ^. buildingConstructionProgressChemicals . to MkRawResource <= cTotal ^. ccdChemicalCost

            it "overall speed is full when there is enough resources" $ do
                forAll resourceCostAndEnoughAvailableResources $ \(cost, available) ->
                    let
                        speed = overallConstructionSpeed cost available
                    in
                        speed ^. overallSpeedBiological == NormalConstructionSpeed
                        && speed ^. overallSpeedMechanical == NormalConstructionSpeed
                        && speed ^. overallSpeedChemical == NormalConstructionSpeed

            it "overall speed is less than full when there is not enough resources" $ do
                forAll resourceCostAndLimitedResources $ \(cost, available) ->
                    let
                        speed = overallConstructionSpeed cost available
                    in
                        speed ^. overallSpeedBiological < NormalConstructionSpeed
                        && speed ^. overallSpeedMechanical < NormalConstructionSpeed
                        && speed ^. overallSpeedChemical < NormalConstructionSpeed
