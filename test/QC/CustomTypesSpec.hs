{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TemplateHaskell            #-}
module QC.CustomTypesSpec (spec)
    where

import Test.QuickCheck
import Test.Hspec

import QC.Generators.Import

import TestImport
import CustomTypes ( Age(..), age )

spec :: Spec
spec = do
    describe "Age" $ do
        it "difference between two star dates can always be calculated" $ do
            forAll arbitrary $ \(ArbStarDate date1) ->
                forAll arbitrary $ \(ArbStarDate date2) ->
                    age date1 date2 >= MkAge 0
