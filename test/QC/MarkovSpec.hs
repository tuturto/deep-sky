{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE OverloadedStrings          #-}

module QC.MarkovSpec (spec)
    where

import Prelude
import Test.Hspec
import Test.QuickCheck

import Control.Lens ( (^?), (^.), ix, _Just, _head, to, view )
import Control.Monad.Random ( evalRand )
import Data.Char ( isLower, isUpper )
import qualified Data.Text as DT

import QC.Generators.Common ( anyRandomGen )

import Markov ( addStart, addLink, itemFreq, itemItem, configStarts
              , configContinuations, emptyConfig )
import Names ( greekNameM )
import People.Data ( unFirstName )


spec :: Spec
spec = do
    describe "Markov chain configuration" $ do
        it "Adding new starting element to empty configuration creates item with frequency of 1" $ do
            let config = addStart ("AA" :: DT.Text) emptyConfig
            config ^? (configStarts . _head . itemFreq) `shouldBe` Just 1
            config ^? (configStarts . _head . itemItem . _Just) `shouldBe` Just "AA"

        it "Adding same element twice to empty configuration creates item with frequency of 2" $ do
            let config = addStart "AA" $
                         addStart ("AA" :: DT.Text) emptyConfig
            config ^? (configStarts . _head . itemFreq) `shouldBe` Just 2
            config ^? (configStarts . _head . itemItem . _Just) `shouldBe` Just "AA"

        it "Adding new continuation creates item with frequency of 1" $ do
            let config = addLink "AA" "BB" $
                         addStart ("AA" :: DT.Text) emptyConfig
            config ^? (configContinuations . ix "AA" . _head . itemFreq) `shouldBe` Just 1
            config ^? (configContinuations . ix "AA" . _head . itemItem . _Just) `shouldBe` Just "BB"

        it "Adding twice to same element creates item with frequency of 2" $ do
            let config = addLink "AA" "BB" $
                         addLink "AA" "BB" $
                         addStart ("AA" :: DT.Text) emptyConfig
            config ^? (configContinuations . ix "AA" . _head . itemFreq) `shouldBe` Just 2
            config ^? (configContinuations . ix "AA" . _head . itemItem . _Just) `shouldBe` Just "BB"

    describe "Markov chain generation" $ do
        it "Generated names are longer than zero" $ do
            forAll anyRandomGen
                   (\g -> DT.length (view unFirstName $ evalRand greekNameM g) > 0)

        it "Generated names start with upper case, followed by lower case" $ do
            forAll anyRandomGen
                    (\g ->
                        let
                            name = evalRand greekNameM g
                        in
                            isUpper (name ^. unFirstName . to DT.head)
                                && DT.all isLower (name ^. unFirstName . to DT.tail))
