{-# LANGUAGE NoImplicitPrelude          #-}

module QC.PeopleSpec (spec)
    where

import Test.QuickCheck
    ( suchThat, forAll, Arbitrary(arbitrary), Gen )

import QC.Generators.Database ( randomUserKey, randomPersonKey )
import QC.Generators.People
    ( PersonParameter(WithPersonId, WithLifeFocus),
      anyPersonIntel,
      intelWithoutFamilyOrSecretMatters,
      intelWithoutSecretMatters,
      intelWithFamilyOrSecretMatters,
      intelWithSecretMatters,
      anyRelationVisibility,
      secretRelation,
      familyRelation,
      publicRelation,
      relationWithVisibility,
      anyPersonDataLink,
      anyLifeFocus,
      anyPersonEntity,
      anyReportResult )
import QC.Generators.User ( anyUserWithAvatar )

import Database.Persist.Sql ( fromSqlKey )
import TestImport
    ( ($),
      Eq((/=), (==)),
      Monad(return),
      Num((+)),
      Ord((<=), (>), (<), (>=)),
      Show(show),
      Bool(..),
      Maybe(Just),
      (&&),
      not,
      any,
      isInfixOf,
      pack,
      (.),
      Entity(Entity),
      describe,
      it,
      expectationFailure,
      shouldContain,
      shouldSatisfy,
      Spec,
      entityValL,
      User,
      Person,
      entityKeyL,
      personLifeFocusChosen )
import Data.Either.Validation as V ( Validation(Failure, Success) )
import Control.Lens ( (^.), (^?), to, _Just )
import CustomTypes ( StarDate(..), unStarDate )
import Errors ( insufficientRights )
import Handler.People
    ( validatePatchApiPersonR, PatchPersonRequest(..)
    , patchPersonRequestLifeFocus
    )
import People.Errors ( canNotReselectSameLifeFocus )
import People.Import ( knownLink, flipRelation )
import People.Opinion ( OpinionReport(..), reportResultToOpinionResult )

spec :: Spec
spec =
    describe "people" $ do
    describe "relations" $ do
        it "relation flipped twice is the original relation" $
            forAll anyRelationVisibility $ \visibility ->
            forAll (relationWithVisibility visibility) $ \relation ->
                relation == (flipRelation . flipRelation) relation

        it "public relations are always known" $
            forAll (anyPersonDataLink publicRelation
                                  anyPersonIntel
                                  anyPersonIntel) $
            \item ->
                knownLink item

        it "family relations are not known if intel doesn't include family or secret relations" $
            forAll (anyPersonDataLink familyRelation
                                  intelWithoutFamilyOrSecretMatters
                                  intelWithoutFamilyOrSecretMatters) $
            \item ->
                not $ knownLink item

        it "family relations are known if intel of target person includes family or secret relations" $
            forAll (anyPersonDataLink familyRelation
                                  intelWithFamilyOrSecretMatters
                                  intelWithoutFamilyOrSecretMatters) $
            \item ->
                knownLink item

        it "family relations are known if intel of originator person includes family or secret relations" $
            forAll (anyPersonDataLink familyRelation
                                  intelWithoutFamilyOrSecretMatters
                                  intelWithFamilyOrSecretMatters) $
            \item ->
                knownLink item


        it "secret relations are not known if intel doesn't include secret relations" $
            forAll (anyPersonDataLink secretRelation
                                  intelWithoutSecretMatters
                                  intelWithoutSecretMatters) $
            \item ->
                not $ knownLink item

        it "secret relations are known if intel of target person includes secret relations" $
            forAll (anyPersonDataLink secretRelation
                                  intelWithSecretMatters
                                  intelWithoutSecretMatters) $
            \item ->
                knownLink item

        it "secret relations are known if intel of originator person includes secret relations" $
            forAll (anyPersonDataLink secretRelation
                                  intelWithoutSecretMatters
                                  intelWithSecretMatters) $
            \item ->
                knownLink item

    describe "opinions" $
        it "reported opinion score is always within -100 and 100 " $
        forAll anyReportResult $
        \report ->
            reportedScoreWithinRange $ reportResultToOpinionResult report

    describe "life focus" $
        describe "Validating requests" $ do
        it "Player can not update other player's avatar" $
            forAll anyUserUpdatingOtherPlayersAvatar $
            \(userE, personE, msg, date) ->
                let res = validatePatchApiPersonR (userE ^. entityKeyL, userE ^. entityValL, personE, msg, date)
                in
                    case res of
                        V.Success _ ->
                            expectationFailure "Invalid request was not detected"

                        V.Failure errs ->
                            errs `shouldContain` [insufficientRights]

        it "Player can not change their life focus before five years have passed since it was last changed" $
            forAll anyUserUpdatingLifeFocusTooSoon $
            \(userE, personE, msg, date) ->
                let res = validatePatchApiPersonR (userE ^. entityKeyL, userE ^. entityValL, personE, msg, date)
                in
                    case res of
                        V.Success _ ->
                            expectationFailure "Invalid request was not detected"

                        V.Failure errs ->
                            errs `shouldSatisfy` any (\x -> "CanNotChangeLifeFocusSoSoon" `isInfixOf` pack (show x))

        it "Player can not change their life focus to what it already is" $
            forAll anyUserSelectingOldLifeFocus $
            \(userE, personE, msg, date) ->
                let res = validatePatchApiPersonR (userE ^. entityKeyL, userE ^. entityValL, personE, msg, date)
                    newFocus = msg ^? patchPersonRequestLifeFocus . _Just . _Just
                in
                    case res of
                        V.Success _ ->
                            expectationFailure "Invalid request was not detected"

                        V.Failure errs ->
                            errs `shouldContain` [ canNotReselectSameLifeFocus newFocus ]

        it "All errors are reported" $
            forAll anyCompletelyFaultyRequest $
            \(userE, personE, msg, date) ->
                let res = validatePatchApiPersonR (userE ^. entityKeyL, userE ^. entityValL, personE, msg, date)
                    newFocus = msg ^? patchPersonRequestLifeFocus . _Just . _Just
                in
                    case res of
                        V.Success _ ->
                            expectationFailure "Invalid request was not detected"

                        V.Failure errs -> do
                            errs `shouldSatisfy` any (\x -> "CanNotChangeLifeFocusSoSoon" `isInfixOf` pack (show x))
                            errs `shouldContain` [ canNotReselectSameLifeFocus newFocus ]
                            errs `shouldContain` [ insufficientRights ]


{- | This is the case where player tries to update another player's avatar's life focus,
     and enough time has not passed and they're trying to reselect the same life focus as
     the avatar already has
-}
anyCompletelyFaultyRequest :: Gen (Entity User, Entity Person, PatchPersonRequest, StarDate)
anyCompletelyFaultyRequest = do
    f <- anyLifeFocus
    personId <- randomPersonKey
    personE <- anyPersonEntity [ WithPersonId personId
                               , WithLifeFocus $ Just f
                               ]

    avatarId <- randomPersonKey `suchThat` (\n -> fromSqlKey n /= fromSqlKey personId)
    userId <- randomUserKey
    user <- anyUserWithAvatar avatarId
    let userE = Entity userId user

    let msg = PatchPersonRequest
            { _patchPersonRequestLifeFocus = Just $ Just f
            }
    sinceLastChange <- arbitrary `suchThat` (< 50)

    date <- arbitrary `suchThat` (\n -> Just False /= personE ^? entityValL . personLifeFocusChosen . _Just . unStarDate . to (\sd -> n < sd + sinceLastChange))

    return ( userE, personE, msg, MkStarDate date )


{- | In this case player is trying to reselect already selected life focus -}
anyUserSelectingOldLifeFocus :: Gen (Entity User, Entity Person, PatchPersonRequest, StarDate)
anyUserSelectingOldLifeFocus = do
    f <- anyLifeFocus
    personId <- randomPersonKey
    personE <- anyPersonEntity [ WithPersonId personId
                               , WithLifeFocus $ Just f
                               ]

    userId <- randomUserKey
    user <- anyUserWithAvatar personId
    let userE = Entity userId user

    let msg = PatchPersonRequest
            { _patchPersonRequestLifeFocus = Just $ Just f
            }
    sinceLastChange <- arbitrary `suchThat` (>= 50)

    date <- arbitrary `suchThat` (\n -> Just False /= personE ^? entityValL . personLifeFocusChosen . _Just . unStarDate . to (\sd -> n > sd + sinceLastChange))

    return ( userE, personE, msg, MkStarDate date )


{- | In this case player is trying to update other player's avatar -}
anyUserUpdatingOtherPlayersAvatar :: Gen (Entity User, Entity Person, PatchPersonRequest, StarDate)
anyUserUpdatingOtherPlayersAvatar = do
    userId <- randomUserKey
    avatarId <- randomPersonKey
    user <- anyUserWithAvatar avatarId
    let userE = Entity userId user

    personId <- randomPersonKey `suchThat` (\n -> fromSqlKey n /= fromSqlKey avatarId)
    personE <- anyPersonEntity [ WithPersonId personId ]

    f <- anyLifeFocus

    let msg = PatchPersonRequest
            { _patchPersonRequestLifeFocus = Just $ Just f
            }
    sinceLastChange <- arbitrary `suchThat` (>= 50)

    date <- arbitrary `suchThat` (\n -> Just False /= personE ^? entityValL . personLifeFocusChosen . _Just . unStarDate . to (\sd -> n > sd + sinceLastChange))

    return ( userE, personE, msg, MkStarDate date )


{- | In this case player is trying to select new life focus before five years has passed -}
anyUserUpdatingLifeFocusTooSoon :: Gen (Entity User, Entity Person, PatchPersonRequest, StarDate)
anyUserUpdatingLifeFocusTooSoon = do
    personId <- randomPersonKey
    personE <- anyPersonEntity [ WithPersonId personId ]

    userId <- randomUserKey
    user <- anyUserWithAvatar personId
    let userE = Entity userId user

    f <- anyLifeFocus

    let msg = PatchPersonRequest
            { _patchPersonRequestLifeFocus = Just $ Just f
            }
    sinceLastChange <- arbitrary `suchThat` (< 50)

    date <- arbitrary `suchThat` (\n -> Just False /= personE ^? entityValL . personLifeFocusChosen . _Just . unStarDate . to (\sd -> n < sd + sinceLastChange))

    return ( userE, personE, msg, MkStarDate date )


-- | Check that reported score in opinion report is within range of -100 and 100
reportedScoreWithinRange :: OpinionReport -> Bool
reportedScoreWithinRange (BaseOpinionReport _) =
    True

reportedScoreWithinRange (OpinionReasonReport _ _) =
    True

reportedScoreWithinRange (DetailedOpinionReport score _) =
    score >= (-100) && score <= 100
