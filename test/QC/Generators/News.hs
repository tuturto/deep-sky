{-# LANGUAGE TemplateHaskell            #-}

module QC.Generators.News where

import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen
import Test.QuickCheck.Instances()

import QC.Generators.Common ( ArbStarDate(..) )
import QC.Generators.Database
import QC.Generators.People
import QC.Generators.Vehicles ( anyDesignName )
import Dto.News ( DesignCreatedNewsDto(..), UserWrittenNewsDto(..), UserNewsIconDto(..)
                , NewsArticleDto(..) )


newtype ArbUserNewsIconDto = ArbUserNewsIconDto
    { unArbUserNewsIconDto :: UserNewsIconDto }


instance Arbitrary ArbUserNewsIconDto where
    arbitrary = oneof [ return $ ArbUserNewsIconDto GenericUserNewsDto
                      , return $ ArbUserNewsIconDto JubilationUserNewsDto
                      , return $ ArbUserNewsIconDto CatUserNewsDto
                      ]


singleDesignCreatedNewsDto :: Gen DesignCreatedNewsDto
singleDesignCreatedNewsDto = do
    aDesignId <- randomDesignKey
    aName <- anyDesignName
    aDate <- arbitrary
    return $ DesignCreatedNewsDto { _designCreatedNewsDtoDesignId = aDesignId
                                  , _designCreatedNewsDtoName = aName
                                  , _designCreatedNewsDtoDate = unArbStarDate aDate
                                  }


singleUserWrittenNewsDto :: Gen UserWrittenNewsDto
singleUserWrittenNewsDto = do
    aMessage <- arbitrary
    aDate <- arbitrary
    aUserName <- anyPersonName
    aIcon <- arbitrary
    return $ UserWrittenNewsDto { _userWrittenNewsDtoContent = aMessage
                                , _userWrittenNewsDtoDate = unArbStarDate aDate
                                , _userWrittenNewsDtoUser = aUserName
                                , _userWrittenNewsDtoIcon = unArbUserNewsIconDto aIcon
                                }


singleNewsArticleDto :: Gen NewsArticleDto
singleNewsArticleDto = do
    oneof [ do
                content <- singleUserWrittenNewsDto
                return $ UserWrittenDto content
          , do
                content <- singleDesignCreatedNewsDto
                return $ DesignCreatedDto content
          ]

-- data NewsArticleDto =
-- StarFoundDto StarFoundNewsDto
-- | PlanetFoundDto PlanetFoundNewsDto
-- | ConstructionFinishedDto ConstructionFinishedNewsDto
-- deriving (Show, Read, Eq)
