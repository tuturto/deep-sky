{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}

module QC.Generators.Research
    where

import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen
import Test.QuickCheck.Instances()

import TestImport
import qualified Prelude as P

import Control.Lens ( (^..), folded, filtered, has, hasn't )

import Research.Data ( Research(..), researchAntecedents, unTechTree )
import Research.Tree ( techTree )


researchWithoutAntecedents :: Gen Research
researchWithoutAntecedents = do
    let targets = techTree ^.. unTechTree . folded . filtered (hasn't (researchAntecedents . folded))
    n <- arbitrary `suchThat` (\x -> x >= 0 && x < length targets)
    return $ targets P.!! n


researchWithAntecedents :: Gen Research
researchWithAntecedents = do
    let targets = techTree ^.. unTechTree . folded . filtered (has (researchAntecedents . folded))
    n <- arbitrary `suchThat` (\x -> x >= 0 && x < length targets)
    return $ targets P.!! n
