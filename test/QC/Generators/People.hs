{-# LANGUAGE NoImplicitPrelude          #-}

module QC.Generators.People
    where

import Test.QuickCheck.Arbitrary ( Arbitrary(arbitrary) )
import Test.QuickCheck.Gen
    ( elements, oneof, suchThat, vectorOf, Gen )
import Test.QuickCheck.Instances()
import QC.Generators.Common ( perhaps, ArbStarDate(..) )
import QC.Generators.Database ( randomFactionKey, randomPersonKey )

import TestImport
    ( ($),
      (<$>),
      Bounded(minBound),
      Eq((/=)),
      Monad(return),
      Ord((>=), (>)),
      Read,
      Show,
      Maybe(..),
      (&&),
      mapMaybe,
      IsSequence(filter),
      Entity(Entity, entityKey),
      Person(..),
      FactionId,
      PlanetId,
      StarSystemId,
      PersonId,
      DynastyId,
      HumanIntelligence(..),
      Relation(..), maybe )
import Common ( safeHead )
import People.Data
    ( PersonName(..), FirstName(..), FamilyName(..), Cognomen(..), RegnalNumber(..)
    , PersonIntel(..), RelationType(..), RelationVisibility(..), StatScore(..)
    , Diplomacy(..), Martial(..), Stewardship(..), Intrigue(..), Learning(..)
    , Gender(..), Sex(..), LifeFocus(..)
    )
import People.Opinion ( OpinionReason(..), ReportResult(..), OpinionScore(..) )
import Queries ( PersonDataLink(..) )


anyPersonName :: Gen PersonName
anyPersonName =
    oneof [ anySimpleName
          , anyRegularName
          , anyRegalName
          ]


-- | Generator for simple names consisting of first name and maybe cognomen
anySimpleName :: Gen PersonName
anySimpleName = do
    firstName <- anyFirstName
    cognomen <- perhaps anyCognomen
    return $ SimpleName firstName cognomen


-- | Generator for regular names consisting of first and family names and maybe a cognomen
anyRegularName :: Gen PersonName
anyRegularName = do
    firstName <- anyFirstName
    familyName <- anyFamilyName
    cognomen <- perhaps anyCognomen
    return $ RegularName firstName familyName cognomen


-- | Generator for regal names consisting of first and family names, regnal number and maybe a cognomen
anyRegalName :: Gen PersonName
anyRegalName = do
    firstName <- anyFirstName
    familyName <- anyFamilyName
    regnalNumber <- anyRegnalNumber
    cognomen <- perhaps anyCognomen
    return $ RegalName firstName familyName regnalNumber cognomen


anyFirstName :: Gen FirstName
anyFirstName = do
    MkFirstName <$> arbitrary


anyFamilyName :: Gen FamilyName
anyFamilyName = do
    MkFamilyName <$> arbitrary


anyCognomen :: Gen Cognomen
anyCognomen = do
    MkCognomen <$> arbitrary


anyRegnalNumber :: Gen RegnalNumber
anyRegnalNumber = do
    n <- arbitrary `suchThat` \x -> x > 0
    return $ MkRegnalNumber n


anyPersonIntel :: Gen PersonIntel
anyPersonIntel = elements [minBound..]


intelWithoutFamilyOrSecretMatters :: Gen PersonIntel
intelWithoutFamilyOrSecretMatters = do
    let relevant = filter (\x -> x /= FamilyRelations && x /= SecretRelations) [minBound..]
    elements relevant


intelWithoutSecretMatters :: Gen PersonIntel
intelWithoutSecretMatters = do
    let relevant = filter (/= SecretRelations) [minBound..]
    elements relevant


intelWithFamilyOrSecretMatters :: Gen PersonIntel
intelWithFamilyOrSecretMatters =
    elements [ FamilyRelations
         , SecretRelations
         ]


intelWithFamilyMatters :: Gen PersonIntel
intelWithFamilyMatters =
    elements [ FamilyRelations]


intelWithSecretMatters :: Gen PersonIntel
intelWithSecretMatters =
    elements [ SecretRelations ]


anyRelationType :: Gen RelationType
anyRelationType = elements [minBound..]


anyRelationVisibility :: Gen RelationVisibility
anyRelationVisibility = elements [minBound..]


constRelationVisibility :: RelationVisibility -> Gen RelationVisibility
constRelationVisibility = return


secretRelation :: Gen RelationVisibility
secretRelation = constRelationVisibility SecretRelation


familyRelation :: Gen RelationVisibility
familyRelation = constRelationVisibility FamilyRelation


publicRelation :: Gen RelationVisibility
publicRelation = constRelationVisibility PublicRelation


-- | Arbitrary relation with given visibility
relationWithVisibility :: RelationVisibility -> Gen Relation
relationWithVisibility visibility = do
    originator <- randomPersonKey
    target <- randomPersonKey `suchThat` \x -> x /= originator
    aType <- anyRelationType
    return $ Relation
                { _relationOriginatorId = originator
                , _relationTargetId = target
                , _relationType = aType
                , _relationVisibility = visibility
                }


anyPersonDataLink :: Gen RelationVisibility -> Gen PersonIntel -> Gen PersonIntel -> Gen PersonDataLink
anyPersonDataLink linkVisibility targetIntel originatorIntel = do
    targetPerson <- anyPersonEntity []
    ownerId <- randomPersonKey `suchThat` (\x -> x /= entityKey targetPerson)
    originatorPersonKey <- randomPersonKey `suchThat` (\x -> x /= entityKey targetPerson
                                                             && x /= ownerId)
    tIntel <- anyHumanIntelligence ownerId (entityKey targetPerson) targetIntel
    oIntel <- anyHumanIntelligence ownerId originatorPersonKey originatorIntel
    vis <- linkVisibility
    relation <- anyRelation (entityKey targetPerson) originatorPersonKey vis
    return $ PersonDataLink
        { _personDataLinkTargetIntelligence = Just tIntel
        , _personDataLinkRelation = relation
        , _personDataLinkPerson = targetPerson
        , _personDataLinkOriginatorIntelligence = Just oIntel
        }


anyHumanIntelligence :: PersonId -> PersonId -> Gen PersonIntel -> Gen HumanIntelligence
anyHumanIntelligence ownerId targetId levels = do
    aLevel <- levels
    return $ HumanIntelligence
                { _humanIntelligencePersonId = targetId
                , _humanIntelligenceOwnerId = ownerId
                , _humanIntelligenceLevel = aLevel
                }


anyRelation :: PersonId -> PersonId -> RelationVisibility -> Gen Relation
anyRelation targetId originatorId visibility = do
    aType <- anyRelationType
    return $ Relation
                { _relationOriginatorId = originatorId
                , _relationTargetId = targetId
                , _relationType = aType
                , _relationVisibility = visibility
                }


anySex :: Gen Sex
anySex =
    elements [minBound..]


anyGender :: Gen Gender
anyGender =
    elements [minBound..]


anyLifeFocus :: Gen LifeFocus
anyLifeFocus =
    elements [minBound..]


anyDiplomacyScore :: Gen (StatScore Diplomacy)
anyDiplomacyScore = do
    n <- arbitrary `suchThat` \x -> x > 0
    return $ MkStatScore n


anyMartialScore :: Gen (StatScore Martial)
anyMartialScore = do
    n <- arbitrary `suchThat` \x -> x > 0
    return $ MkStatScore n


anyStewardship :: Gen (StatScore Stewardship)
anyStewardship = do
    n <- arbitrary `suchThat` \x -> x > 0
    return $ MkStatScore n


anyLearning :: Gen (StatScore Learning)
anyLearning = do
    n <- arbitrary `suchThat` \x -> x > 0
    return $ MkStatScore n


anyIntrigue :: Gen (StatScore Intrigue)
anyIntrigue = do
    n <- arbitrary `suchThat` \x -> x > 0
    return $ MkStatScore n


anyPersonEntity  :: [PersonParameter] -> Gen (Entity Person)
anyPersonEntity params = do
    personId <- maybe randomPersonKey return (getPersonId params)
    name <- anyPersonName
    sex <- anySex
    gender <- anyGender
    aStarDate <- arbitrary
    diplomacy <- anyDiplomacyScore
    martial <- anyMartialScore
    stewardship <- anyStewardship
    intrigue <- anyIntrigue
    learning <- anyLearning
    factionId <- perhaps randomFactionKey -- todo implement
    let planetTitle = Nothing -- todo implement
    let starSystemTitle = Nothing -- todo implement
    let dynastyId = Nothing -- todo implement
    lifeFocus <- case getLifeFocus params of
                    Nothing ->
                        perhaps anyLifeFocus

                    Just Nothing ->
                        return Nothing

                    Just (Just focus) ->
                        return $ Just focus

    let lifeFocusChosen = (\_ -> Just $ unArbStarDate aStarDate) lifeFocus
    return $
        Entity personId $ Person
                { _personName = name
                , _personSex = sex
                , _personGender = gender
                , _personDateOfBirth = unArbStarDate aStarDate
                , _personDiplomacy = diplomacy
                , _personMartial = martial
                , _personStewardship = stewardship
                , _personIntrigue = intrigue
                , _personLearning = learning
                , _personFactionId = factionId
                , _personPlanetTitle = planetTitle
                , _personStarSystemTitle = starSystemTitle
                , _personDynastyId = dynastyId
                , _personLifeFocus = lifeFocus
                , _personLifeFocusChosen = lifeFocusChosen
                }


data PersonParameter =
    PlanetaryPrimaryTitle PlanetId
    | RandomPlanetaryPrimaryTitle
    | StarSystemPrimaryTitle StarSystemId
    | RandomStarSystemPrimaryTitle
    | WithPersonId PersonId
    | WithFactionId FactionId
    | RandomFactionId
    | WithDynastyId DynastyId
    | RandomDynastyId
    | WithLifeFocus (Maybe LifeFocus)
    deriving (Show, Read, Eq)


getLifeFocus :: [PersonParameter] -> Maybe (Maybe LifeFocus)
getLifeFocus params =
    safeHead $ mapMaybe (\x -> case x of
                            WithLifeFocus n ->
                                Just n
                            _ ->
                                Nothing)
                        params


getPersonId :: [PersonParameter] -> Maybe PersonId
getPersonId params =
    safeHead $ mapMaybe (\x -> case x of
                            WithPersonId n ->
                                Just n
                            _ ->
                                Nothing)
                        params


anyReportResult :: Gen ReportResult
anyReportResult = do
    n <- arbitrary
    let score = MkOpinionScore n
    k <- arbitrary `suchThat` \x -> x >= 0
    reasons <- vectorOf k anyOpinionReason
    oneof
        [
            return $ FeelingLevel score
        ,
            return $ ReasonsLevel score reasons
        ,
            return $ DetailedLevel score reasons
        ]


anyOpinionReason :: Gen OpinionReason
anyOpinionReason = do
    MkOpinionReason <$> arbitrary
