{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}

module QC.Generators.User
    ( anyUser, anyUserIdentity, anyPassword, anyUserWithAvatar, anyUserWithoutAvatar )
    where

import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen
import Test.QuickCheck.Instances()
import QC.Generators.Common ( perhaps )
import QC.Generators.Database

import TestImport
import Control.Lens ( (&), (.~) )
import CustomTypes ( UserIdentity(..) )


anyUserWithoutAvatar :: Gen (User)
anyUserWithoutAvatar = do
    user <- anyUser
    return $ user & userAvatar .~ Nothing


anyUserWithAvatar :: PersonId -> Gen (User)
anyUserWithAvatar pId = do
    user <- anyUser
    return $ user & userAvatar .~ Just pId


anyUser :: Gen (User)
anyUser = do
    ident <- anyUserIdentity
    pwd <- anyPassword
    pId <- perhaps randomPersonKey

    return $ User
        { _userIdent = ident
        , _userPassword = pwd
        , _userAvatar = pId
        }


anyUserIdentity :: Gen UserIdentity
anyUserIdentity = do
    uIdent <- arbitrary
    return $ MkUserIdentity uIdent


anyPassword :: Gen (Maybe Text)
anyPassword = do
    pwd <- arbitrary
    return pwd
