{-# LANGUAGE TemplateHaskell            #-}

module QC.Generators.Construction where

import Control.Lens ( (^.), to )

import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen
import Test.QuickCheck.Instances()
import QC.Generators.Database ( randomPlanetKey )

import CustomTypes ( BuildingType(..) )
import Resources ( RawResources(..), RawResource(..), ResourceCost, ConstructionSpeed
                 , ResourcesAvailable, ccdChemicalCost, ccdBiologicalCost
                 , ccdMechanicalCost, unRawResource )
import Buildings ( building, BuildingLevel(..), buildingInfoCost )
import Model


newtype ArbRawResource t =
    ArbRawResource { unArbRawResource :: RawResource t }


instance Arbitrary (ArbRawResource t) where
    arbitrary = do
        aValue <- arbitrary `suchThat` \x -> x >= 0
        return $ ArbRawResource $ MkRawResource aValue


newtype ArbRawResources t =
    ArbRawResources { unArbRawResources :: RawResources t }


instance Arbitrary (ArbRawResources t) where
    arbitrary = do
        aBio <- arbitrary
        aMech <- arbitrary
        aChem <- arbitrary
        return $ ArbRawResources $ RawResources { _ccdMechanicalCost = unArbRawResource aMech
                                                , _ccdBiologicalCost = unArbRawResource aBio
                                                , _ccdChemicalCost = unArbRawResource aChem
                                                }


rawResourcesThatAre :: (Int -> Int -> Bool) -> RawResources a -> Gen (RawResources b)
rawResourcesThatAre check base = do
    aBio <- arbitrary `suchThat` \x -> base ^. ccdBiologicalCost . unRawResource . to (check x)
    aMech <- arbitrary `suchThat` \x -> base ^. ccdMechanicalCost . unRawResource . to (check x)
    aChem <- arbitrary `suchThat` \x -> base ^. ccdChemicalCost . unRawResource . to (check x)
    return $ RawResources { _ccdMechanicalCost = MkRawResource aMech
                          , _ccdBiologicalCost = MkRawResource aBio
                          , _ccdChemicalCost = MkRawResource aChem
                          }


newtype ArbBuildingType =
    ArbBuildingType { unArbBuildingType :: BuildingType }


instance Arbitrary ArbBuildingType where
    arbitrary = oneof [ return $ ArbBuildingType SensorStation
                      , return $ ArbBuildingType ResearchComplex
                      , return $ ArbBuildingType Farm
                      , return $ ArbBuildingType ParticleAccelerator
                      , return $ ArbBuildingType NeutronDetector
                      , return $ ArbBuildingType BlackMatterScanner
                      , return $ ArbBuildingType GravityWaveSensor
                      ]


unstartedConstruction :: Gen BuildingConstruction
unstartedConstruction = do
    aPlanetId <- randomPlanetKey
    arbBuildingType <- arbitrary
    let aBuildingType = unArbBuildingType arbBuildingType
    return $ BuildingConstruction aPlanetId 0 0 0 0 aBuildingType 1


randomConstruction :: Gen BuildingConstruction
randomConstruction = do
    aPlanetId <- randomPlanetKey
    arbBuildingType <- arbitrary
    let aBuildingType = unArbBuildingType arbBuildingType
    let modelBuilding = building aBuildingType $ MkBuildingLevel 1
    bioProgress <- arbitrary `suchThat` \x -> x >= 0 && x <= modelBuilding ^. buildingInfoCost . ccdBiologicalCost . unRawResource
    mechProgress <- arbitrary `suchThat` \x -> x >= 0 && x <= modelBuilding ^. buildingInfoCost . ccdMechanicalCost . unRawResource
    chemProgress <- arbitrary `suchThat` \x -> x >= 0 && x <= modelBuilding ^. buildingInfoCost . ccdChemicalCost . unRawResource
    return $ BuildingConstruction aPlanetId 0 bioProgress mechProgress chemProgress aBuildingType 1


unstartedConstructionsWithSomeSpeed :: Gen (RawResources ConstructionSpeed, BuildingConstruction, RawResources ResourceCost)
unstartedConstructionsWithSomeSpeed = do
    aConstructionSpeed <- arbitrary
    aBuildingConstruction <- unstartedConstruction
    aTotalCost <- arbitrary
    return (unArbRawResources aConstructionSpeed, aBuildingConstruction, unArbRawResources aTotalCost)


unfinishedConstructionsWithSomeSpeed :: Gen (RawResources ConstructionSpeed, BuildingConstruction, RawResources ResourceCost)
unfinishedConstructionsWithSomeSpeed = do
    arbConstructionSpeed <- arbitrary
    let aConstructionSpeed = unArbRawResources arbConstructionSpeed
    aBuildingConstruction <- randomConstruction
    arbTotalCost <- arbitrary
    let aTotalCost = unArbRawResources arbTotalCost
    return (aConstructionSpeed, aBuildingConstruction, aTotalCost)


resourceCostAndEnoughAvailableResources :: Gen (RawResources ResourceCost, RawResources ResourcesAvailable)
resourceCostAndEnoughAvailableResources = do
    arbCost <- arbitrary
    let aCost = unArbRawResources arbCost
    aAvailable <- rawResourcesThatAre (>) aCost
    return (aCost, aAvailable)


resourceCostAndLimitedResources :: Gen (RawResources ResourceCost, RawResources ResourcesAvailable)
resourceCostAndLimitedResources = do
    arbCost <- arbitrary
    let aCost = unArbRawResources arbCost
    aAvailable <- rawResourcesThatAre (<) aCost
    return (aCost, aAvailable)
