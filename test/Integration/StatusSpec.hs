{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE OverloadedStrings  #-}

module Integration.StatusSpec ( spec )
    where

import Control.Lens ( (^.) )

import TestImport
import CustomTypes ( SystemStatus(..) )
import Simulation.Status ( removeExpiredStatuses )
import Space.Data ( PlanetaryStatus(..) )


spec :: Spec
spec = withApp $ do

    describe "Status handling"  $ do
        describe "Planet statuses"  $ do
            it "Expired planet statuses are removed and news created" $ do


                sId <- runDB $ insert $ StarSystem
                        { _starSystemName = "Aldebaraan"
                        , _starSystemCoordX = 10
                        , _starSystemCoordY = 20
                        , _starSystemRulerId = Nothing
                        }

                fId <- runDB $ insert $ Faction
                        { _factionName = "Star lords"
                        , _factionHomeSystem = sId
                        , _factionBiologicals = 10
                        , _factionMechanicals = 10
                        , _factionChemicals = 10
                        }

                pId1 <- runDB $ insert $ Planet
                        { _planetName = "New Earth"
                        , _planetPosition = 3
                        , _planetStarSystemId = sId
                        , _planetOwnerId = Just fId
                        , _planetGravity = 1.0
                        , _planetRulerId = Nothing
                        }

                _ <- runDB $ insert $ PlanetStatus
                        { _planetStatusPlanetId = pId1
                        , _planetStatusStatus = GoodHarvest
                        , _planetStatusExpiration = Just 20201
                        }

                let status = Simulation 20201 Online
                _ <- runDB $ insert status

                news <- runDB $ removeExpiredStatuses (status ^. simulationCurrentTime)

                statuses <- runDB $ selectList [ PlanetStatusPlanetId ==. pId1 ] []
                loadedNews <- runDB $ selectList [] [ Asc NewsDate ]

                liftIO $ statuses `shouldSatisfy` (\x -> length x == 0)
                liftIO $ news `shouldSatisfy` (\x -> length x == 1)
                liftIO $ loadedNews `shouldSatisfy` (\x -> length x == 1)
