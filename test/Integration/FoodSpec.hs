{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE OverloadedStrings  #-}

module Integration.FoodSpec ( spec )
    where

import TestImport
import CustomTypes ( BuildingType(..) )
import Simulation.Food ( getFoodProduction )
import Space.Data ( PlanetaryStatus(..) )


spec :: Spec
spec = withApp $ do

    describe "Food production"  $ do
        it "Good harvest boosts production" $ do
            sId <- runDB $ insert $ StarSystem
                    { _starSystemName = "Aldebaraan"
                    , _starSystemCoordX = 10
                    , _starSystemCoordY = 20
                    , _starSystemRulerId = Nothing
                    }

            pId1 <- runDB $ insert $ Planet
                    { _planetName = "New Earth"
                    , _planetPosition = 3
                    , _planetStarSystemId = sId
                    , _planetOwnerId = Nothing
                    , _planetGravity = 1.0
                    , _planetRulerId = Nothing
                     }

            pId2 <- runDB $ insert $ Planet
                    { _planetName = "New Mars"
                    , _planetPosition = 4
                    , _planetStarSystemId = sId
                    , _planetOwnerId = Nothing
                    , _planetGravity = 1.0
                    , _planetRulerId = Nothing
                    }

            _ <- runDB $ insert $ Building
                    { _buildingPlanetId = pId1
                    , _buildingType = Farm
                    , _buildingLevel = 1
                    , _buildingDamage = 0.0
                    }

            _ <- runDB $ insert $ Building
                    { _buildingPlanetId = pId2
                    , _buildingType = Farm
                    , _buildingLevel = 1
                    , _buildingDamage = 0.0
                    }

            _ <- runDB $ insert $ PlanetStatus
                    { _planetStatusPlanetId = pId1
                    , _planetStatusStatus = GoodHarvest
                    , _planetStatusExpiration = Nothing
                    }

            production1 <- runDB $ getFoodProduction pId1
            production2 <- runDB $ getFoodProduction pId2

            liftIO $ production1 `shouldSatisfy` (> production2)
