{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Handler.Helpers ( delete_, setupPerson )
    where

import TestImport

import People.Data ( Sex(..), Gender(..), PersonName(..), FirstName(..) )

-- | Perform delete request
delete_ :: Route App -> YesodExample App ()
delete_ = performMethod "DELETE"


-- | Insert faction and person into database and return their primary keys as tuple
setupPerson :: SIO (YesodExampleData App) (PersonId, FactionId)
setupPerson = do
    sId <- runDB $ insert $ StarSystem { _starSystemName = "Star system"
                                       , _starSystemCoordX = 0.0
                                       , _starSystemCoordY = 0.0
                                       , _starSystemRulerId = Nothing
                                       }
    fId <- runDB $ insert $ Faction { _factionName = "faction"
                                    , _factionHomeSystem = sId
                                    , _factionBiologicals = 100
                                    , _factionMechanicals = 100
                                    , _factionChemicals = 100
                                    }
    pId <- runDB $ insert $ Person { _personName = SimpleName (MkFirstName "Avatar") Nothing
                                   , _personSex = Female
                                   , _personGender = Woman
                                   , _personDateOfBirth = 20120
                                   , _personDiplomacy = 10
                                   , _personMartial = 10
                                   , _personStewardship = 10
                                   , _personIntrigue = 10
                                   , _personLearning = 10
                                   , _personFactionId = Just fId
                                   , _personPlanetTitle = Nothing
                                   , _personStarSystemTitle = Nothing
                                   , _personDynastyId = Nothing
                                   , _personLifeFocus = Nothing
                                   , _personLifeFocusChosen = Nothing
                                   }
    return (pId, fId)
