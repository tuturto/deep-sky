{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Handler.PersonSpec (spec)
    where

import TestImport
import Control.Lens ( (^?), _Just )
import Data.Aeson ( decode, encode )
import Network.Wai.Test ( SResponse(..) )

import CustomTypes ( SystemStatus(..) )
import Handler.Helpers ( setupPerson )
import Handler.People ( PatchPersonRequest(..) )
import People.Data ( LifeFocus(..) )
import People.Import ( personReportLifeFocus, personReportId )


spec :: Spec
spec = withApp $ do
    describe "Basic functionality" $ do
        it "Person data can be fetched" $ do
            (pId, _) <- setupPerson
            _ <- runDB $ insert $ Simulation 25250 Online
            user <- createUser "Pete" "salasana" (Just pId)
            authenticateAs user "salasana"

            _ <- get (ApiPersonR pId)
            resp <- getResponse

            let personReportM = join (decode <$> simpleBody <$> resp)

            assertEq "Person Id"
                (personReportM ^? _Just . personReportId)
                (Just pId)

            assertEq "Life focus"
                (personReportM ^? _Just . personReportLifeFocus)
                (Just Nothing)

            statusIs 200

    describe "Life focus" $ do
        it "Response to life focus change contains the new life focus" $ do
            (pId, _) <- setupPerson
            _ <- runDB $ insert $ Simulation 25250 Online
            user <- createUser "Pete" "salasana" (Just pId)
            authenticateAs user "salasana"

            request $ do
                setRequestBody (encode $ PatchPersonRequest
                                            { _patchPersonRequestLifeFocus = Just $ Just HuntingFocus
                                            })
                addRequestHeader ("Accept", "application/json")
                addRequestHeader ("Content-Type", "application/json")
                setMethod "PATCH"
                setUrl (ApiPersonR pId)

            resp <- getResponse

            let personReportM = join (decode <$> simpleBody <$> resp)

            assertEq "Person Id"
                (personReportM ^? _Just . personReportId)
                (Just pId)

            assertEq "Life focus"
                (personReportM ^? _Just . personReportLifeFocus)
                (Just $ Just HuntingFocus)

            statusIs 200
